--- 
customlog: 
  - 
    format: combined
    target: /usr/local/apache/domlogs/chefslice.com
  - 
    format: "\"%{%s}t %I .\\n%{%s}t %O .\""
    target: /usr/local/apache/domlogs/chefslice.com-bytes_log
documentroot: /home/chefslice/public_html
group: chefslice
hascgi: 1
homedir: /home/chefslice
ifmodulealiasmodule: 
  scriptalias: 
    - 
      path: /home/chefslice/public_html/cgi-bin/
      url: /cgi-bin/
ifmoduleincludemodule: 
  directoryhomechefslicepublichtml: 
    ssilegacyexprparser: 
      - 
        value: " On"
ifmodulemodsuphpc: 
  group: chefslice
ifmoduleuserdirmodule: 
  ifmodulempmitkc: 
    ifmoduleruidmodule: {}

include: 
  - 
    include: "\"/usr/local/apache/conf/userdata/std/2/chefslice/*.conf\""
ip: 198.54.114.223
owner: barbkdfu
phpopenbasedirprotect: 1
port: 81
scriptalias: 
  - 
    path: /home/chefslice/public_html/cgi-bin
    url: /cgi-bin/
  - 
    path: /home/chefslice/public_html/cgi-bin/
    url: /cgi-bin/
serveradmin: webmaster@chefslice.com
serveralias: mail.chefslice.com www.chefslice.com
servername: chefslice.com
usecanonicalname: 'Off'
user: chefslice
userdirprotect: ''
