@extends('layouts.app')

@section('content')

    <?php $allCategories = $categories->count(); ?>
    <div id="container" >


        @if($categories->count()>0)
            @foreach($categories as $category)

                <div class="ex-item">
                    <img data-content="{{url("$category->slug")}}" class="category-link" src="{{ asset('media/categories/images/'.$category->icon) }}" />
                </div>
            @endforeach
        @endif


    </div>
    <?php $mean = $allCategories / 2; ?>
@stop
@section('styles')
    <style type="text/css">
        img {
            vertical-align: middle;
        }
        #container {
            top: 150px;
            bottom: 0px;
            position: absolute;
            right: 0px;
            left: 0px;"
        }
        .ex-item {
            height: 356px;
            width: 270px;
            display: none;
            border: 5px solid white;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .ex-item img {
            width: 100%;
            height: 100%;

        }
    </style>

@stop
@section('scripts')
    <script>
        var categories = <?php echo json_encode($categories) ?>;
        var mean = <?php echo (int)$mean ?>;

    </script>
    <script src="{{ asset('theme/theta/bezier1/scripts/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('theme/theta/bezier1/scripts/theta-carousel.min.js') }}"></script>
    <script src="{{ asset('theme/theta/bezier1/scripts/example.js') }}"></script>

@stop

