@extends('layouts.app')

@section('content')


    <div id="container" >



        <?php $allCategories = $categories->count(); ?>
        @if($categories->count()>0)
            @foreach($categories as $category)


                <div class="ex-item" style="background-color: #F7DE41">
                    <div data-content="{{url("$category->slug")}}" class="category-link">
                    <h1>{{$category->text}}</h1>
                    <img   src="{{ asset('media/categories/images/'.$category->icon) }}" />
                    <p>{{$category->description}}.</p>
                    </div>
                </div>
            @endforeach
        @endif

   </div>
    <div id="popup" style="display: none; overflow: hidden;"></div>
    <?php $mean = $allCategories / 2; ?>
@stop
@section('styles')
    <style type="text/css">
        #container {
            top: 150px; bottom: 0px; position: absolute; right: 0px; left: 0px;
        }
        img {
            vertical-align: middle;
        }

        .ex-item {
            display: none;
            width: 310px;
            border-radius: 19px 0px 0px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .ex-item h1 {
            color: white;
            margin-left: 25px;
            font-size: 30px;
            font-weight: 300;
        }

        .ex-item p {
            margin: 27px;
            margin-top: 10px;
            font-size: 17px;
            color: black;
        }
        .ex-item img {
            width: 100%;
            height: 100%;

        }

    </style>
@stop
@section('scripts')
    <script>
        var categories = <?php echo json_encode($categories) ?>;
        var mean = <?php echo (int)$mean ?>;

    </script>
    <script src="{{ asset('theme/theta/html-content/scripts/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('theme/theta/html-content/scripts/theta-carousel.min.js') }}"></script>
    <script src="{{ asset('theme/theta/html-content/scripts/example.js') }}"></script>

@stop