@extends('layouts.app')

@section('content')

    <?php $allCategories = $categories->count(); ?>
    <div id="container" >


        <div style="height: 120px; position: absolute; right: 0px; width: 600px;">
            <div style="bottom: 0px; position: absolute; text-align: center; width: 100%; font-weight: 300; font-size: 40px; display: block; color: rgb(205, 238, 245);" class="title">title text</div>
        </div>

        @if($categories->count()>0)
            @foreach($categories as $category)

                <div class="ex-item">
                    <img data-content="{{url("$category->slug")}}" class="category-link"  src="{{ asset('media/categories/images/'.$category->icon) }}" />
                </div>
            @endforeach
        @endif
        <div style=" height: 200px; width: 553px; position: absolute; font-size: 23px; display: block; right: 22px; text-indent: 30px; top: 143px;" class="text">Description comment comment</div>

    </div>
    <?php $mean = $allCategories / 2; ?>
@stop
@section('styles')
    <link rel="stylesheet" href="{{ asset('theme/theta/line1/animate.min.css') }}">
    <style type="text/css">
        #container {
            top: 150px; bottom: 0px; position: absolute; right: 0px; left: 0px;
        }
        img {
            vertical-align: middle;
        }

        .ex-item {
            height: 433px;
            width: 260px;
            display: none;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .ex-item img {
            width: 100%;
            height: 100%;

        }
    </style>
@stop
@section('scripts')
    @yield('menuscripts')
    <script>
        var categories = <?php echo json_encode($categories) ?>;
        var mean = <?php echo (int)$mean ?>;

    </script>
    {{--    <script src="{{ asset('theme/scripts/theta-carousel.min.js') }}"></script>--}}
    <script src="{{ asset('theme/theta/line1/scripts/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('theme/theta/line1/scripts/theta-carousel.min.js') }}"></script>
    <script src="{{ asset('theme/theta/line1/scripts/example.js') }}"></script>

@stop

