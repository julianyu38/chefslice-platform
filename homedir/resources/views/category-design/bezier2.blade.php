@extends('layouts.app')

@section('content')
    <?php $allCategories = $categories->count(); ?>
    <div id="container" >



        {{--<div style="height: 200px; position: absolute; width: 100%;">--}}
            {{--<div style="bottom: 0px; position: absolute; text-align: center; width: 100%; font-weight:300; font-size: 60px; display: block; color: #080808;"--}}
                 {{--class="title">title text--}}
            {{--</div>--}}
        {{--</div>--}}

        @if($categories->count()>0)
            @foreach($categories as $category)

                <div class="ex-item">
                    <div data-content="{{url("$category->slug")}}" class="category-link" style="height: 100%; position: absolute; width: 100%;">
                        {{--<div style="top: -75px; position: absolute; text-align: center; width: 100%; font-weight:300; font-size: 60px; display: block; color: #080808;"--}}
                        <div style="    top: 42%;
    position: relative;
    text-align: center;
    width: 100%;
    font-size: 6rem;
    color: #000;
    border: 1px solid #ddd;
    background: rgba(255,255,255,.7);
    font-weight: bold;
    padding: 20px 0;"
                             class="title">{{$category->text}}

                        </div>
                    </div>

                    <div data-content="{{url("$category->slug")}}" class="categoryimage category-link"
                         style=" width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('media/categories/images/'.$category->icon) }})">
                    </div>

                </div>
            @endforeach
        @endif
        {{--<div style="height: 200px;bottom: 0px; width: 100%; position: absolute; text-align: center; font-size: 30px; display: block;"
             class="description">Description comment
        </div>--}}


    </div>

    <?php $mean = $allCategories / 2; ?>



@stop

@section('scripts')

    <script>
        var categories = <?php echo json_encode($categories) ?>;
        var mean = <?php echo (int)$mean ?>;

    </script>
{{--    <script src="{{ asset('theme/scripts/theta-carousel.min.js') }}"></script>--}}
    <script src="{{ asset('theme/scripts/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('theme/scripts/mustache.min.js') }}"></script>
    <script src="{{ asset('theme/scripts/theta-carousel.min.js') }}"></script>
    @desktop
{{--    <script src="{{ asset('theme/scripts/example.js') }}"></script>--}}
    <script src="{{ asset('theme/theta/bezier2/scripts/example.js') }}"></script>
    @enddesktop
    @tablet
    <script src="{{ asset('theme/theta/bezier2/scripts/tab/example.js') }}"></script>
    @endtablet
    @mobile
    <script src="{{ asset('theme/theta/bezier2/scripts/mobile/example.js') }}"></script>
    @endmobile

    <script src="{{ asset('theme/scripts/jquery.popupoverlay.js') }}"></script>
    <script>
        function configure() {

        }
    </script>
@stop


@section('styles')
    <style type="text/css">
        #container {
            top: 150px; bottom: 0px; position: absolute; right: 0px; left: 0px;
        }
        img {
            vertical-align: middle;
        }

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }



        .ex-item .round-button {
            font-size: 45px;
            height: 200px;
            width: 200px;
            background: rgba(71, 160, 71, 0.65);
            position: relative;
            top: -100px;
            border-radius: 100px;
            padding-left: 30px;
            padding-top: 27px;
            right: -600px;
            cursor: pointer;
        }

        .ex-item:hover .round-button {
            font-size: 64px;
            height: 300px;
            width: 300px;
            background: rgba(71, 160, 71, 0.85);
            position: relative;
            right: -550px;
            top: -150px;
            border-radius: 150px;
            padding-left: 45px;
            padding-top: 41px;
        }

        .button-hover-transition {
            -webkit-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
            -moz-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
            -ms-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
            -o-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
        }
    </style>
    @desktop
    <style>
        #container {
            top: 150px;
        }
        .ex-item {
            height: 815px;
            width: 715px;
            display: none;
            /*overflow: hidden;*/
            border: 7px solid white;
            background-color: #5EBDC4;
        }
        .categoryimage {
            height: 800px;

        }
    </style>

    @enddesktop

    @tablet
    <style>
        #container {
            top: 85px;
        }
        .ex-item {
            height: 650px;
            width: 715px;
            display: none;
            /*overflow: hidden;*/
            border: 7px solid white;
            background-color: #5EBDC4;
        }
        .categoryimage {
            height: 640px;

        }
    </style>
    @endtablet
    @mobile
    <style>
        #container {
            top: 85px;
        }
        .ex-item {
            height: 515px;
            width: 715px;
            display: none;
            /*overflow: hidden;*/
            border: 7px solid white;
            background-color: #5EBDC4;
        }
        .categoryimage {
            height: 500px;

        }
    </style>
    @endmobile
@stop