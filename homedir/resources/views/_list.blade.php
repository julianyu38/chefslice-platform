@extends('layouts.app')

@section('content')
        <!--Call your modal-->

<div class="pull-right backbtn "><a href="{{url('')}}"><img src="{{asset('img/back.png')}}"></a>
</div>



<div id="fh5co-main">
    <div class="container">

        <div class="row">

            <div id="fh5co-board" data-columns>
                <?php
                $path = asset('media/items/images/');
                $mediapath = asset('media/items/images/');
                ?>
                @if($items->count()>0)
                    @foreach($items as $item)
                        <?php if ($item->media_type == 2) {
                            $path = asset('media/items/videos/');
                        } ?>

                        <div class="item">

                            <div class="animate-box">

                               {{-- <a href="#" onclick="buildModel(this)" data-id="{{$item->id}}" class="image-popup cd-trigger  fh5co-board-img" title="{{$item->title}}">
                                    <img src="{{$path.'/'.$item->thumbnail }}"   alt="{{$item->title}}">
                                </a>--}}
                                <a  href="#modal-product-quick-view" data-id="{{$item->id}}" class="item-quick-view fh5co-board-img" title="{{$item->title}}">
                                    <img src="{{$path.'/'.$item->thumbnail }}"   alt="{{$item->title}}">
                                </a>

                                {{--<a href="#" onclick="buildModel(this)" data-id="{{$item->id}}"
                                   class="image-popup cd-trigger  fh5co-board-img"
                                   title="{{$item->title}}"><img
                                            src="{{$path.'/'.$item->thumbnail }}"
                                            alt="Free HTML5 Bootstrap template"></a>--}}
                            </div>
                            <div class="fh5co-desc">{{$item->title}}
                            </div>
                        </div>

                    @endforeach
                @endif

            </div>
        </div>
    </div>
</div>


{{--
<!-- Modal -->
<div class="modal animated bounceInRight" id="myModal" role="dialog">
    --}}
{{--<div class="modal-dialog">--}}{{--

    --}}
{{--<div class="container-fluid">--}}{{--

    <div class="container">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Item Details</h4>
            </div>
            <div class="row">


                <div class="col-md-8 pull-left">
                    <div class="col-md-7" style="width: 100%">
                        <div id="rightImgHolder">
                            <div id="myCarousel" class="Carousal slide" data-ride="carousel">
                                <!-- Indicators -->


                                <!-- Wrapper for slides -->
                                <div id="carousel-inner" class="carousel-inner">


                                </div>

                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4  pull-right">
                    <div id="title" class="title"
                         style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); color: rgb(68, 68, 68); visibility: visible; opacity: 1;">
                        Portfolio Gallery
                    </div>

                    <hr id="tophr" style="width: 100%;">
                    <div class="text trans3d" id="contentText"
                         style="height: 297px; top: 63px; overflow: hidden; opacity: 1; visibility: visible;">
                        <div class="scrollbar" id="style-1">
                            <div class="contentScroll" id="contentDescription"
                                 style="width: 270px; transition-property: -webkit-transform; transform-origin: 0px 0px 0px; transform: translate(0px, 0px) translateZ(0px); padding-right: 5px;">

                            </div>
                        </div>

                    </div>
                    <hr id="bottomhr" style="width: 100%; top: 380px;">
                    <div class="awards-list trans3d"
                         style="top: 400px; opacity: 1; visibility: visible; transform: matrix(1, 0, 0, 1, 0, 0);">
                        --}}
{{--<a class="launch" style="color: rgb(68, 68, 68);">LAUNCH &gt;</a></div>--}}{{--

                    </div>
                    <div id="numImages" style="width: 18px; margin-left: -9px;">
                        <div class="caroImgItem selected" id="caroImgItem0"
                             style="opacity: 1; visibility: visible; transform: matrix(1, 0, 0, 1, 0, 0);"></div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
--}}



        <!--DEMO02-->
<div id="modal-product-quick-view">
    <!--"THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID-->
    <div  id="btn-close-modal" class="close-modal-product-quick-view modal-close">
        <i class="fa fa-times" aria-hidden="true"></i>
    </div>

    <div class="modal-content">

        <div class="row" style="    height: 100%;">
            <div class="col-sm-6 col-xs-12" style="height: 100%;">
                <div style="height: 100%;background: #000;">
                    <div style="top: 50%;transform:translateY(-50%);" id="owl-demo" class="owl-carousel owl-theme">
                      {{--  <div class="item-video">
                            <video id="video">
                                <source src="http://imyourman.dk/demo/fashion.mp4" type="video/mp4" autoplay>
                                <source src="http://imyourman.dk/demo/fashion.ogg" type="video/ogg">
                            </video>
                        </div>--}}

                                                <div class="item"><img src="images/1.jpg" alt="The Last of us"></div>
                                                <div class="item"><img src="images/2.jpg" alt="GTA V"></div>
                        <div class="item-video">
                            <iframe width="420" height="315" src="https://www.youtube.com/embed/Oy9GFAQ4x4w">
                            </iframe>
                        </div>

                                                <div class="item"><img src="images/3.jpg" alt="Mirror Edge"></div>
                    </div>
                </div>


            </div>
            <div class="col-sm-6 col-xs-12" style="height: 96%;">
                <div style="padding-right: 30px;padding-top: 40px; overflow:hidden; height: 100%;">
                    <h1 style="color: #666;font-family: 'Courgette', cursive;">Product Name</h1>
                    <div style="    height: 100%;
    overflow-y: scroll;">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci at culpa dolore doloribus ea facere ipsam iste minima modi, neque nihil odio officia, provident quas quos reiciendis sint suscipit ut!
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci at culpa dolore doloribus ea facere ipsam iste minima modi, neque nihil odio officia, provident quas quos reiciendis sint suscipit ut!
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci at culpa dolore doloribus ea facere ipsam iste minima modi, neque nihil odio officia, provident quas quos reiciendis sint suscipit ut!
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci at culpa dolore doloribus ea facere ipsam iste minima modi, neque nihil odio officia, provident quas quos reiciendis sint suscipit ut!
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci at culpa dolore doloribus ea facere ipsam iste minima modi, neque nihil odio officia, provident quas quos reiciendis sint suscipit ut!
                        </p>
                        p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci at culpa dolore doloribus ea facere ipsam iste minima modi, neque nihil odio officia, provident quas quos reiciendis sint suscipit ut!
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci at culpa dolore doloribus ea facere ipsam iste minima modi, neque nihil odio officia, provident quas quos reiciendis sint suscipit ut!
                        </p>
                    </div>
                </div>

            </div>
        </div>
        <!--Your modal content goes here-->
    </div>
</div>

@stop


@section('scripts')
        <!-- Waypoints -->
        <script src="{{ asset('theme/hydrogen/js/jquery.waypoints.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>
        <!-- Salvattore -->
        <script src="{{ asset('theme/hydrogen/js/salvattore.min.js') }}"></script>
        <!-- Main JS -->
        <script src="{{ asset('theme/hydrogen/js/main.js') }}"></script>
        <!-- Modernizr JS -->
        <script src="{{ asset('theme/hydrogen/js/modernizr-2.6.2.min.js') }}"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="{{ asset('theme/hydrogen/js/respond.min.js') }}"></script>
        <![endif]-->

    <script src="{{ asset('assets/plugins/animatedModal/animatedModal.min.js') }}"></script>

    <script>
        var obj = <?php echo json_encode($items) ?>;
        var assetUrl = '<?php echo $mediapath ?>';
        //    console.log(obj);
        values = obj;

       /* function buildModel(el) {
            $('#carousel-inner').empty();
            $('.animate-box').removeClass('bounceInLeft');
            $('.animate-box').addClass('animated bounceOutLeft');


            item = el.getAttribute('data-id');
            selectedItem = values[values.findIndex(x = > x.id == item
        )]
            ;
            divclass = 'active';
            for (var i in selectedItem.images) {
                imgurl = assetUrl + '/' + item + '/' + selectedItem.images[i].image;

                $('#carousel-inner').prepend('<div class="item ' + divclass + '"><img src="' + imgurl + '" alt="Los Angeles"> </div>');
                divclass = ''
//                        console.log(selectedItem.images[i].image);
            }

            $('#caroImg0').css('width', '100%');
            $('#title').html(selectedItem.title);
            $('#contentDescription').html(selectedItem.description);
            setTimeout(function () {
                $('#myModal').modal();
            }, 400);


        }*/

        /*$(document).ready(function () {
            $('.modal-content').addClass('animated bounceInLeft');
            var hideDelay = true;
            $('#myModal').on('hide.bs.modal', function (e) {
                $('.animate-box').removeClass('bounceOutLeft');
                $('.animate-box').addClass('animated bounceInLeft');
                if (hideDelay) {
                    $('.modal-content').removeClass('animated bounceInLeft').addClass('animated bounceOutRight');
                    hideDelay = false;
                    setTimeout(function () {
                        $('#myModal').modal('hide');
                        $('.modal-content').removeClass('animated bounceOutRight').addClass('animated bounceInLeft');
                    }, 700);
                    return false;
                }
                hideDelay = true;
                $('.animate-box').removeClass('bounceOutLeft');
                $('.animate-box').addClass('animated bounceInLeft');
                return true;
            });
        });*/
    </script>
    <script>
        $(document).ready(function () {
            var __quick_view_product_id = null;
            $(".item-quick-view").click(function(){
                var self = $(this);

                __quick_view_product_id = self.attr('data-id');
            });
            //demo 02
            $(".item-quick-view").animatedModal({
                modalTarget: 'modal-product-quick-view',
                animatedIn: 'lightSpeedIn',
                animatedOut: 'bounceOutDown',
                color: '#eee',
                // Callbacks
                beforeOpen: function () {
                    console.log(__quick_view_product_id);
                    console.log("The animation was called");
                },
                afterOpen: function () {
                    console.log("The animation is completed");
                },
                beforeClose: function () {
                    console.log("The animation was called");
                },
                afterClose: function () {
                    console.log("The animation is completed");
                }
            });
        });
    </script>


    <script>
        $(document).ready(function() {

            $("#owl-demo").owlCarousel({
               autoPlay : 3000,
                navigation : true, // Show next and prev buttons
                slideSpeed : 300,
                paginationSpeed : 400,
                stopOnHover:true,
                singleItem:true,
                loop  : true,
                margin : 30,
                rewindNav : true,
                nav: true,
                video:true,
                lazyLoad:true,
                center:true,
                //videoHeight: 400,
                //videoWidth: 400,
               /* singleItem: true,afterAction: function(current) {
                    current.find('video').get(0).play();
                }*/


                // "singleItem:true" is a shortcut for:
                // items : 1,
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false

            });

           // $( ".owl-prev").html('<i class="fa fa-angle-left" aria-hidden="true"></i>');
            $( ".owl-prev").html('<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>');
            $( ".owl-next").html('<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>');
            //$( ".owl-next").html('<i class="fa fa-angle-right" aria-hidden="true"></i>');
        });
    </script>
    @stop


    @section('styles')
        <link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
            <!-- Animate.css -->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/animate.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">

    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/icomoon.css') }}">

    <!-- Salvattore -->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/salvattore.css') }}">
    <!-- Theme Style -->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/style.css') }}">

    <style>
        #modal-product-quick-view{
           /* overflow-y: inherit !important;
            width: 80% !important;
    height: 90% !important;
    top: 5% !important;
    left: 10% !important;*/

           /* overflow-y: inherit !important;
            width: 90% !important;
            height: 60% !important;
            top: 20% !important;
            left: 5% !important;*/

            overflow-y: inherit !important;
            width: 90% !important;
            height: 80% !important;
            top: 10% !important;
            left: 5% !important;
        }

       #modal-product-quick-view .modal-close{
           position: absolute;
           top: 0px;
           right: 5px;
           z-index: 9;
           font-size: 2.5em;
           color: #888888;
       }
        #modal-product-quick-view .modal-close:hover {
            color: #000;
        }
        #modal-product-quick-view .modal-content{
            border: none;
            border-radius: 0px;
            box-shadow: none;
           height: 100%;
           overflow: hidden;
        }
        #owl-demo .item img{
            display: block;
            width: 100%;
            height: auto;
        }
        #owl-demo .owl-nav{
            position: absolute;
            top: 45%;
            width: 100%;
        }
        .owl-carousel {
            position: relative;
        }
        .owl-prev,
        .owl-next {
            position: absolute;
            top: 45%;
            margin-top: -10px;
            font-size: 2.5em;
            color: rgba(255,255,255,.5);
        }
        .owl-prev:hover,
        .owl-next:hover {

            color: #fff;
        }
        .owl-prev {
            left: 8px;
        }
        .owl-next {
            right: 8px;
        }

    </style>
{{--
    <style>

        .scrollbar {
            /*margin-left: 30px;*/
            float: left;
            height: 280px;
            /*width: 165px;*/
            /*background: #F5F5F5;*/
            overflow-y: scroll;
            margin-bottom: 25px;
        }

        #style-1::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            border-radius: 10px;
            background-color: #F5F5F5;
        }

        #style-1::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5;
        }

        #style-1::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
            background-color: #555;
        }

        #myModal {
            margin-top: 150px;
        }

    </style>--}}
@stop