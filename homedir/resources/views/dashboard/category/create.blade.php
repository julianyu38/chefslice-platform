@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-th"></i>
        </div>
        <div class="header-title">
            <h1>Categories</h1>
            <small>Manage categories.</small>
            @include('dashboard.partials.quick-links')
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">Category</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-8">
                        <h4>New Category </h4>
                    </div>

                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="View Categories" data-placement="bottom" href="{{ url('dashboard/category')  }}" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> View Categories</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url'=>'dashboard/category','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            @if($users->count()>0)
                            <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
                                {!! Form::label('user_id', 'User', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    <select required class="form-control" name="user_id" id="user_id">
                                        <option value="">Category User</option>
                                        @foreach($users as $id=>$name)
                                            <option {!!  old('user_id')==trim($id)?'selected="selected"':""  !!} value="{{ trim($id) }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('user_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            @endif
                            <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                                {!! Form::label('text', 'Name *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('text',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category Name'])  !!}
                                    @if ($errors->has('text'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            {{--<div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                                {!! Form::label('slug', 'Slug *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('slug',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category Slug'])  !!}
                                    @if ($errors->has('slug'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>--}}
                                <div class="form-group {{ $errors->has('locale') ? ' has-error' : '' }}">
                                    {!! Form::label('locale', 'Select Language', ['class' => 'col-sm-3 control-label'])  !!}
                                    <div class="col-sm-9">
                                        <select required class="form-control" name="locale" id="locale">
                                            <option value="en">English</option>
                                            <option value="he">Hebrew</option>

                                        </select>
                                        @if ($errors->has('locale'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('locale') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                {!! Form::label('description', 'Description *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('description',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category description'])  !!}
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">
                                <label for="image" class="col-sm-3 control-label"> Image</label>
                                <div class="col-sm-9">
                                    <input type="file" name="icon" class="input-image" accept="image/*">
                                    @if ($errors->has('icon'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('icon') }}</strong>
                                                    </span>
                                    @endif
                                    <div class="form-group">
                                        <div class="logo-preview">
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <button type="reset" class="btn btn-default">Cancel</button>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Create</button>
                                </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        $(".input-image").change(function () {
            $('.logo-preview').html('');
            readURL(this,function (e) {
                $('.logo-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
            });
        });

    </script>

@stop