@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-university"></i>
        </div>
        <div class="header-title">
            <h1>Home Page</h1>
            <small>Manage site home page.</small>
            @include('dashboard.partials.quick-links')
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">Home Page</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Slider Settings</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    {!! Form::model($category,['url'=>['dashboard/category/'.$category->id],'method'=>'patch','class'=>'form-horizontal','files'=>true,'enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                                {!! Form::label('text', 'Name *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('text',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category Name'])  !!}
                                    @if ($errors->has('text'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('slug') ? ' has-error' : '' }}">
                                {!! Form::label('slug', 'Slug *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('slug',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category Slug'])  !!}
                                    @if ($errors->has('slug'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                {!! Form::label('description', 'Description *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('description',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category description'])  !!}
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">
                                <label for="image" class="col-sm-3 control-label"> Image</label>
                                <div class="col-sm-9">
                                    <input type="file" name="icon" class="input-image" accept="image/*">
                                    @if ($errors->has('icon'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('icon') }}</strong>
                                                    </span>
                                    @endif
                                    <div class="form-group">
                                        <div class="logo-preview">
                                        </div>
                                        @if(!empty($category->icon))
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <img class="img-thumbnail" src="{{ asset('media/categories/images/'.$category->icon) }}" alt="{{ $category->text }}">
                                                    <button  data-url="#" type="button" class="btn btn-block btn-xs btn-danger btn--delete--image"><i class="fa fa-trash-o"></i> Delete</button>
                                                    {{-- action('Dashboard\CategoryController@deleteImage',['id'=>$category->id]) --}}
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                            </div>



                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <a href="{{ url('dashboard/category')  }}" class="btn btn-default">Cancel</a>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Update</button>
                                </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script>
        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        $(".input-image").change(function () {
            $('.logo-preview').html('');
            readURL(this,function (e) {
                $('.logo-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
            });
        });

    </script>

@stop