<footer class="main-footer">
    <div class="pull-right hidden-xs"> <b>Version</b> 1.0</div>
    {{--<strong>Copyright &copy; 2016-2017 <a href="#">Barbados Sotheby's</a>.</strong> All rights reserved.</i>--}}
    <div class="col-sm-2">
        <div class="dropup show">
            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Select Language
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="{{url('locale/en')}}">English</a><br>
                <a class="dropdown-item" href="{{url('locale/he')}}">עִברִית</a>
            </div>
        </div>
    </div>
</footer>
<script>
    function setLocale(lang) {

    }
</script>