@extends('dashboard.layouts.app')

@section('header')

    <section class="content-header hidden-xs">

        <div class="header-icon">

            <i class="fa fa-users"></i>

        </div>

        <div class="header-title">

            <h1>Admins</h1>

            <small>Manage properties.</small>

            @include('dashboard.partials.quick-links')



            <ol class="breadcrumb">

                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>

                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>

                <li class="active">Admins</li>

            </ol>

        </div>

    </section>

@endsection

@section('content')

    <div class="row">

        <div class="col-sm-12 " >

            <div class="panel panel-bd ">

                <div class="panel-heading ">

                    <div  class="panel-title col-sm-10">

                        <h4>Admins </h4>

                    </div>

                    <div  class="col-sm-2 text-right">

                        <a data-toggle="tooltip" data-title="New Users" data-placement="bottom" href="{{ url('dashboard/user/create') }}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add New</a>

                    </div>

                    <div class="clearfix"></div>

                </div>

                <div class="panel-body">

                    <div class="table-responsive">

                        <table class="table table-bordered table-hover">

                            <thead>

                            <tr>

                                <th>#</th>

                                <th>Image </th>

                                <th>Name </th>

                                <th>Email</th>

                                <th>Mobile</th>

                                <th>Join Date</th>

                                <th>Actions</th>

                            </tr>

                            </thead>

                            <tbody>



                            @if($users->count()>0)

                                @foreach($users as $user)

                                <tr>

                                    <td>{{ $user->id }}</td>

                                    <td>

                                        @if(!empty(($user->image)) && file_exists(public_path('images/users/'.($user->image))))

                                            <img style="width:48px; height:48px;" class="img-circle" src="{{ asset('images/users/'.$user->image) }}" alt="{{$user->name }}">

                                        @else

                                            <img style="width:48px; height:48px;" class="img-circle" src="{{ asset('images/not-found.jpg') }}" alt="{{$user->name }}" >

                                        @endif

                                    </td>

                                    <td>{{ $user->name }}</td>

                                    <td>{{ $user->email }}</td>

                                    <td>{{ $user->mobile }}</td>

                                    <td>{{ $user->created_at->format('d M, Y') }}</td>

                                    <td>

                                        <a href="{{ url('dashboard/user/'.$user->user_name.'/edit') }}" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                        {!! Form::open(['url'=>['/dashboard/user/'.$user->id],'method'=>'delete','style'=>'display:inline;']) !!}

                                            <button type="button" class="btn btn-danger btn-sm btn--delete--item" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></button>

                                        {!! Form::close() !!}





                                    </td>

                                </tr>

                                @endforeach

                             @else

                                <tr>

                                    <td colspan="14">

                                        <p class="alert alert-warning text-center">

                                            No data found.

                                        </p>

                                    </td>

                                </tr>

                             @endif

                            </tbody>

                        </table>

                    </div>

                </div>

                <div class="box-footer clearfix text-center">

                    {{ $users->links() }}

                </div>

            </div>

        </div>

    </div>

@stop

@section('scripts')

    <script>

        $(document).on('click','.btn--delete--item', function () {

            var form = $(this).parents('form:first');



            $.confirm({

                icon: 'fa fa-trash-o',

                title: 'Delete Record!',

                closeIcon: true,

                animation: 'rotate',

                closeAnimation: 'rotate',

                content: 'Are you want to delete this record?',

                confirmButton: 'Yes',

                cancelButton: 'No',

                confirmButtonClass: 'btn-danger',

                cancelButtonClass: 'btn-info',

                confirm: function(){

                    form.submit();

                }

            });

        });

    </script>

@endsection