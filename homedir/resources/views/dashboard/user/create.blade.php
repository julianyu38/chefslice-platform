@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-users"></i>
        </div>
        <div class="header-title">
            <h1>Users</h1>
            <small>Manage Admins.</small>
            @include('dashboard.partials.quick-links')

            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">Users</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>New Admins User </h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="View Admins" data-placement="bottom" href="{{ url('dashboard/user') }}" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> View Admins</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">

                    {!! Form::open(['url'=>['dashboard/user'],'method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                {!! Form::label('name', 'Name *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('name',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'User Name ...'])  !!}
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('user_name') ? ' has-error' : '' }}">
                                {!! Form::label('user_name', 'Subdomain *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('user_name',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'User Subdomain ...'])  !!}
                                    @if ($errors->has('user_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('email', 'Email *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::email('email',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'User Email ...'])  !!}
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                {!! Form::label('password', 'Password', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('password',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Password ...'])  !!}
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('mobile') ? ' has-error' : '' }}">
                                {!! Form::label('mobile', 'Mobile', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::number('mobile',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Mobile no ...'])  !!}
                                    @if ($errors->has('mobile'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                <label for="image" class="col-sm-3 control-label"> Image</label>
                                <div class="col-sm-9">
                                    <input type="file" name="image" class="input-image" accept="image/*">
                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="image-preview col-sm-offset-3">
                                </div>
                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <button type="reset" class="btn btn-default">Cancel</button>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Create</button>
                                </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')

    <script type="text/javascript">
        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }
        $(function() {
            $(".input-image").change(function () {
                $('.image-preview').html('');
                readURL(this,function (e) {
                    $('.image-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
                });
            });

        });
    </script>
@stop