@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-lock"></i>
        </div>
        <div class="header-title">
            <h1>Roles</h1>
            <small>Manage User Roles.</small>
            @include('dashboard.partials.quick-links')

            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">User Roles</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>New User Role </h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="View Roles" data-placement="bottom" href="{{ action('Dashboard\RoleController@index') }}" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> View Roles</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">

                    {!! Form::open(['action'=>['Dashboard\RoleController@store'],'method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <div class="form-group {{ $errors->has('display_name') ? ' has-error' : '' }}">
                                {!! Form::label('display_name', 'Name *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('display_name',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Role Name ...','required'=>true,'autofocus'=>true])  !!}
                                    @if ($errors->has('display_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('display_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('permissions') ? ' has-error' : '' }}">
                                {!! Form::label('permissions', 'Permissions', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    <select class="form-control select2" multiple data-placeholder="Select permissions" name="permissions[]" id="permissions">
                                        @if($permissions->count() > 0 )
                                            @foreach($permissions as $permission)
                                                <option  value="{{$permission->id}}">{{ $permission->display_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('permissions'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('permissions') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('description',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Role Description ...'])  !!}
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <button type="reset" class="btn btn-default">Cancel</button>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Create</button>
                                </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('styles')
    <link href="{{ asset('assets/plugins/jquery.multiselect/jquery.multiselect.css') }}" rel="stylesheet" type="text/css">
@stop
@section('scripts')
    <script src="{{ asset('assets/plugins/jquery.multiselect/jquery.multiselect.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('select[multiple]').multiselect({
                columns:1,
                search: true,
                selectAll:'Select All'
            });
        });
    </script>
@stop