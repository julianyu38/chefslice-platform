@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-lock"></i>
        </div>
        <div class="header-title">
            <h1>Roles</h1>
            <small>Manage User Roles.</small>
            @include('dashboard.partials.quick-links')

            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">User Roles</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Roles </h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="New Role" data-placement="bottom" href="{{ action('Dashboard\RoleController@create') }}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add New</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name </th>
                                <th>Permissions </th>
                                <th>Description</th>
                                <th>Created At</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if($roles->count()>0)
                                @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->display_name }}</td>
                                    <td>
                                        <?php  $permissions = $role->permissions()->count(); ?>
                                        @if($permissions)
                                        {{ $permissions .' '.(( $permissions >1)?str_plural('Permission'):'Permission') }}
                                        @else
                                            No Permissions
                                        @endif
                                    </td>
                                    <td>{{ str_limit($role->description) }}</td>
                                    <td>{{ $role->created_at->format('d M, Y') }}</td>
                                    <td>
                                        <a href="{{ action('Dashboard\RoleController@edit',['id'=>$role->id]) }}" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        {!! Form::open(['url'=>['/dashboard/role/'.$role->id],'method'=>'delete','style'=>'display:inline;']) !!}
                                            <button type="button" class="btn btn-danger btn-sm btn--delete--item" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        {!! Form::close() !!}


                                    </td>
                                </tr>
                                @endforeach
                             @else
                                <tr>
                                    <td colspan="14">
                                        <p class="alert alert-warning text-center">
                                            No data found.
                                        </p>
                                    </td>
                                </tr>
                             @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix text-center">
                    {{ $roles->links() }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        $(document).on('click','.btn--delete--item', function () {
            var form = $(this).parents('form:first');

            $.confirm({
                icon: 'fa fa-trash-o',
                title: 'Delete Record!',
                closeIcon: true,
                animation: 'rotate',
                closeAnimation: 'rotate',
                content: 'Are you want to delete this record?',
                confirmButton: 'Yes',
                cancelButton: 'No',
                confirmButtonClass: 'btn-danger',
                cancelButtonClass: 'btn-info',
                confirm: function(){
                    form.submit();
                }
            });
        });
    </script>
@endsection