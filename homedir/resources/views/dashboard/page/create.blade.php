@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-th"></i>
        </div>
        <div class="header-title">
            <h1>Pages</h1>
            <small>Manage Pages.</small>
            @include('dashboard.partials.quick-links')
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">Page</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>New Page </h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="View Pages" data-placement="bottom" href="{{ url('dashboard/page')  }}" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> View Pages</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url'=>'dashboard/page','method'=>'post','class'=>'form-horizontal']) !!}
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            @if($users->count()>0)
                            <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
                                {!! Form::label('user_id', 'User', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    <select required class="form-control" name="user_id" id="user_id">
                                        <option value="">Page User</option>
                                        @foreach($users as $id=>$name)
                                            <option {!!  old('user_id')==trim($id)?'selected="selected"':""  !!} value="{{ trim($id) }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('user_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            @endif
                            <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                                {!! Form::label('title', 'Title *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('title',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Page Title'])  !!}
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('slug') ? ' has-error' : '' }}">
                                {!! Form::label('slug', 'Slug *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('slug',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category Slug'])  !!}
                                    @if ($errors->has('slug'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            {{--<div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">--}}
                                {{--<label for="image" class="col-sm-3 control-label"> Image</label>--}}
                                {{--<div class="col-sm-9">--}}
                                    {{--<input type="file" name="icon" class="input-image" accept="image/*">--}}
                                    {{--@if ($errors->has('icon'))--}}
                                        {{--<span class="help-block">--}}
                                                        {{--<strong>{{ $errors->first('icon') }}</strong>--}}
                                                    {{--</span>--}}
                                    {{--@endif--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="logo-preview">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            {{--</div>--}}


                        </div>


                        <div class="col-sm-9 col-sm-offset-2">
                            <div class="form-group {{ $errors->has('body') ? ' has-error' : '' }}">
                                {!! Form::label('body', 'Page Body *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9 ">
                                    {!! Form::textarea('body',$value= null, $attributes = ['id'=>'bodytext','class'=>'form-control summernote' ,'placeholder'=>'Page Body'])  !!}
                                    @if ($errors->has('body'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('body') }}</strong>
                                             </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <button type="reset" class="btn btn-default">Cancel</button>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Create</button>
                                </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('styles')
    <!-- summernote css -->
    <link href="{{ asset('assets/plugins/summernote/summernote.css') }}" rel="stylesheet" type="text/css"/>
@stop
@section('scripts')
    <!-- summernote js -->
    <script src="{{ asset('assets/plugins/summernote/summernote.min.js') }}" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            "use strict"; // Start of use strict
            //summernote
            $('textarea.summernote').summernote({
                height: 300, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: true                  // set focus to editable area after initializing summernote
            });
        });
    </script>
@stop