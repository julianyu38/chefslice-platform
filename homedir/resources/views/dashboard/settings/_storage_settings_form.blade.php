{!! Form::open(['route' => ['admin.settings.update.storage'], 'method'=>'POST', 'files' => true, 'class' => 'form-horizontal']) !!}
    
  
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">S3 Storage Settings</legend>         
            
            <div class="form-group">
                {!! Form::label("aws_key", "AWS Key:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="aws_key" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('AWS_KEY') }}" id="aws_key" class="form-control">
                    @if ($errors->has('aws_key'))
                        <div class="text-danger"><small>{{ $errors->first('aws_key') }}</small></div>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("aws_secret", "AWS Secret:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="aws_secret" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('AWS_SECRET') }}" id="aws_secret" class="form-control">
                    @if ($errors->has('aws_secret'))
                        <div class="text-danger"><small>{{ $errors->first('aws_secret') }}</small></div>
                    @endif
                </div>
            </div>
            <div class="form-group">
                {!! Form::label("aws_region", "AWS Region:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="aws_region" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('AWS_REGION') }}" id="aws_region" class="form-control">
                    @if ($errors->has('aws_region'))
                        <div class="text-danger"><small>{{ $errors->first('aws_region') }}</small></div>
                    @endif
                </div>
            </div>
            <div class="form-group">
                {!! Form::label("aws_bucket", "AWS Bucket:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="aws_bucket" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('AWS_BUCKET') }}" id="aws_bucket" class="form-control">
                    @if ($errors->has('aws_bucket'))
                        <div class="text-danger"><small>{{ $errors->first('aws_bucket') }}</small></div>
                    @endif
                </div>
            </div>
    </fieldset>   
    
    <div class="form-group">
        <div class="col-sm-10">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>
  
  {{ csrf_field() }}
  {{ method_field('PUT') }}
  
{!! Form::close() !!}