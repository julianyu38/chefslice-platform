{{--{!! Form::open(['route' => ['admin.settings.update.system'], 'method'=>'POST', 'files' => true, 'class' => 'form-horizontal']) !!}--}}
<div class="row">
    <div class="col-md-9">
        <legend>Static Links</legend>
        <div class="list-group">
            <a target="_blank"
               href="{{ url(env('APP_URL_PROTOCOL').'://'.auth()->user()->user_name.env('SESSION_DOMAIN').'/?heatmap=1') }}"
               class="list-group-item">Home Page</a>
            <a target="_blank"
               href="{{ url(env('APP_URL_PROTOCOL').'://'.auth()->user()->user_name.env('SESSION_DOMAIN').'/special?heatmap=1') }}"
               class="list-group-item">Special Items</a>
        </div>
        <legend>Categories</legend>
        <div class="list-group">
            @if($categories->count()>0)
                @foreach($categories as $category)
                    <a target="_blank" href="{{url("$category->slug?heatmap=1")}}"
                       class="list-group-item">{{$category->text}}</a>
                @endforeach
            @endif
        </div>
    </div>
</div>