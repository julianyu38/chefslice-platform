{!! Form::open(['route' => ['admin.settings.update.system'], 'method'=>'POST', 'files' => true, 'class' => 'form-horizontal']) !!}
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">System Settings</legend>         
            
            <div class="form-group">
                {!! Form::label("app_debug", "Enable Debug Mode:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <div class="form-group">
                        <input type="checkbox" {{ env('APP_DEBUG') == '1' ? 'checked':''}} data-toggle="toggle" value="1" data-style="ios" name="app_debug" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("app_env", "APP Environment:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <select name="app_env" class="form-control">
                        <option value="local" {{ env('APP_ENV') == 'local' ? 'selected' : '' }}>Local</option>
                        <option value="production" {{ env('APP_ENV') == 'production' ? 'selected' : '' }}>Production</option>
                    </select>
                    @if ($errors->has('app_env'))
                        <div class="text-danger"><small>{{ $errors->first('app_env') }}</small></div>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("app_url", "APP URL:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="app_url" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('APP_URL') }}" id="app_url" class="form-control">
                    @if ($errors->has('app_url'))
                        <div class="text-danger"><small>{{ $errors->first('app_url') }}</small></div>
                    @endif
                </div>
            </div>
            <div class="form-group">
                {!! Form::label("site_name", "Site Name:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="site_name" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('SITE_NAME') }}" id="site_name" class="form-control">
                    @if ($errors->has('site_name'))
                        <div class="text-danger"><small>{{ $errors->first('site_name') }}</small></div>
                    @endif
                </div>
            </div>
            <div class="form-group">
                {!! Form::label("db_connection", "DB Connection:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="db_connection" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('DB_CONNECTION') }}" id="db_connection" class="form-control">
                    @if ($errors->has('db_connection'))
                        <div class="text-danger"><small>{{ $errors->first('db_connection') }}</small></div>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("db_host", "DB Host:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="db_host" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('DB_HOST') }}" id="db_host" class="form-control">
                    @if ($errors->has('db_host'))
                        <div class="text-danger"><small>{{ $errors->first('db_host') }}</small></div>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("db_port", "DB Port:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="db_port" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('DB_PORT') }}" id="db_port" class="form-control">
                    @if ($errors->has('db_port'))
                        <div class="text-danger"><small>{{ $errors->first('db_port') }}</small></div>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("db_database", "DB Database:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="db_database" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('DB_DATABASE') }}" id="db_database" class="form-control">
                    @if ($errors->has('db_database'))
                        <div class="text-danger"><small>{{ $errors->first('db_database') }}</small></div>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("db_username", "DB Username:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="db_username" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('DB_USERNAME') }}" id="db_username" class="form-control">
                    @if ($errors->has('db_username'))
                        <div class="text-danger"><small>{{ $errors->first('db_username') }}</small></div>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("db_password", "DB Password:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="db_password" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('DB_PASSWORD') }}" id="db_password" class="form-control">
                    @if ($errors->has('db_password'))
                        <div class="text-danger"><small>{{ $errors->first('db_password') }}</small></div>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("queue_driver", "Queue Driver:", ['class' => 'col-sm-2 control-label']) !!}
                
                <div class="col-sm-10">
                    <p>If you set this to Database, then make sure you setup the cron job to process your queues. Otherwise, just leave it to Sync</p>
                    <select name="queue_driver" class="form-control">
                        <option value="database" {{ env('QUEUE_DRIVER') == 'database' ? 'selected' : '' }}>Database</option>
                        <option value="sync" {{ env('QUEUE_DRIVER') == 'sync' ? 'selected' : '' }}>Sync</option>
                    </select>
                </div>
            </div>
            
    </fieldset>   
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">NOCAPTCHA Settings</legend>    
        <span><i class="icon-info"></i> You can get your captcha keys <a href="https://www.google.com/recaptcha/admin" target="_blank">here.</a></span>
        <hr />
        
        <div class="form-group">
            {!! Form::label("nocaptcha_secret", "NoCaptcha Secret Key:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <input name="nocaptcha_secret" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('NOCAPTCHA_SECRET') }}" id="nocaptcha_secret" class="form-control">
                @if ($errors->has('nocaptcha_secret'))
                    <div class="text-danger"><small>{{ $errors->first('nocaptcha_secret') }}</small></div>
                @endif
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::label("nocaptcha_sitekey", "NoCaptcha Site Key:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <input name="nocaptcha_sitekey" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('NOCAPTCHA_SITEKEY') }}" id="nocaptcha_sitekey" class="form-control">
                @if ($errors->has('nocaptcha_sitekey'))
                    <div class="text-danger"><small>{{ $errors->first('nocaptcha_sitekey') }}</small></div>
                @endif
            </div>
        </div>
    </fieldset>
    <div class="form-group">
        <div class="col-sm-10">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>

            
  {{ csrf_field() }}
  {{ method_field('PUT') }}
  
{!! Form::close() !!}