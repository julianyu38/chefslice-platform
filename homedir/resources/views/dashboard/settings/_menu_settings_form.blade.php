{{--{!! Form::model($settings,['action'=>['Dashboard\SettingsController@update',$account],'method'=>'patch','class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}--}}
{!! Form::open(['action'=>['Dashboard\SettingsController@menu','account'=>$account],'method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data','id'=>'menuform']) !!}
<fieldset class="scheduler-border">
    <legend class="scheduler-border">Circular Menu</legend>
    {{--<p>Obtain your keys from <a href="https://stripe.com" target="_blank">Stripe.</a> Make sure you change these to your Production keys when deploying live.</p>--}}
    @if($menus->count()>0)
        @foreach($menus as $menu )
    <div class="form-group">

        <div class="col-sm-3">
            <input name="title[{{$menu->id}}]" type="text" value="{{$menu->title}}" id="title{{$menu->id}}" class="form-control">
            @if ($errors->has('title'))
                <div class="text-danger"><small>{{ $errors->first('title') }}</small></div>
            @endif
        </div>
        <div class="col-sm-3">
            <input name="slug[{{$menu->id}}]" type="text" value="{{$menu->slug}}" id="slug{{$menu->id}}" class="form-control">
            @if ($errors->has('slug'))
                <div class="text-danger"><small>{{ $errors->first('slug') }}</small></div>
            @endif
        </div>
        <div class="col-sm-3">
            <input name="icon[{{$menu->id}}]" type="text" value="{{$menu->icon}}" id="icon{{$menu->id}}" class="form-control">
            @if ($errors->has('icon'))
                <div class="text-danger"><small>{{ $errors->first('icon') }}</small></div>
            @endif
        </div>
        {{--<div class="col-sm-3">--}}
            {{--<button value="Update" id="{{$menu->id}}" onclick="update(this.id)">Update</button>--}}
        {{--</div>--}}
    </div>
        @endforeach
    @endif

</fieldset>
{{ csrf_field() }}
{{--  {{ method_field('PATCH') }}--}}
<div class="form-group">
    <div class="col-sm-10">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
    </div>
</div>
{!! Form::close() !!}