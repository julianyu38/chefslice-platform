@extends('dashboard.layouts.app')
<?php
$GROUP = 'site_pricing';
$INFO = $SITE_SETTING($GROUP);
?>
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-file-text" aria-hidden="true"></i>
        </div>
        <div class="header-title">
            <h1>Home Page</h1>
            <small>Manage site home page.</small>
            @include('dashboard.partials.quick-links')
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">Home Page</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Home Page "Pricing" Section</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url'=>'dashboard/home-page/update_settings','method'=>'post','class'=>'form-horizontal','files'=>true,'enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <input type="hidden" name="_group" value="{{ $GROUP }}">
                            <div class="form-group {{ $errors->has('site_pricing') ? ' has-error' : '' }}">
                                {!! Form::label('site_pricing', "Enable Pricing", ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    <select class="form-control select2" data-placeholder="site_pricing" name="site_pricing" id="site_pricing">

                                        <option {{$sections->site_pricing==1?'Selected':''}} value="1">Enabled</option>
                                        <option {{$sections->site_pricing==0?'Selected':''}} value="0">Disabled</option>
                                    </select>
                                    @if ($errors->has('site_pricing'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('site_pricing') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            @if(count($INFO) >0)
                                @foreach($INFO as $key=>$site_info)
                                    <div class="form-group {{ $errors->has($key) ? ' has-error' : '' }}">
                                        {!! Form::label($key, $site_info->title, ['class' => 'col-sm-3 control-label'])  !!}
                                        <div class="col-sm-9">
                                            @if($key=='description')
                                            {!! Form::textarea($key,$value= str_replace('<br />', "\n", $site_info->value), $attributes = ['class'=>'form-control','placeholder'=>$site_info->title])  !!}
                                            @else
                                                {!! Form::text($key,$value= $site_info->value, $attributes = ['class'=>'form-control','placeholder'=>$site_info->title])  !!}
                                            @endif
                                            @if ($errors->has($key))
                                                <span class="help-block">
                                        <strong>{{ $errors->first($key) }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <a href="{{ url('dashboard/home-page/clients') }}" class="btn btn-default">Cancel</a>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Update</button>
                                </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script>


        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        $(document).on('change','.input-image',function(){
            var preview_section = $(this).parents('.slider-image-item').find('.image-preview');
            preview_section.html('');
            readURL(this,function (e) {
                preview_section.append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
            });
        });


    </script>

@stop