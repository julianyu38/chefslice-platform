@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-wrench"></i>
        </div>
        <div class="header-title">
            <h1>Currencies</h1>
            <small>Manage Admins.</small>
            @include('dashboard.partials.quick-links')
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">Currencies</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>New Currency </h4>
                    </div>
                   {{-- <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="View Admins" data-placement="bottom" href="{{ action('Dashboard\UserController@index') }}" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> View Admins</a>
                    </div>--}}
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">

                    {!! Form::open(['action'=>['Dashboard\CurrencyController@store'],'method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        @foreach($currencies as $key=>$currency)
                            <div class="col-sm-3 form-group">
                                <div class="col-sm-12  text-left">
                                    <div class="i-check">
                                        <input {!! in_array($key,$selected_currencies->toArray())?'checked':'' !!}  value="{{ $key }}" name="currencies[]" class="i-check" type="checkbox" id="currency-{{ $key }}">
                                        <label for="currency-{{ $key }}">{{ str_limit($currency['name'],20) }}</label>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-info "> &nbsp; &nbsp; &nbsp;Update &nbsp; &nbsp; &nbsp;</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
@section('styles')
<!-- iCheck -->
    <link href="{{ asset('assets/plugins/icheck/skins/all.css') }}" rel="stylesheet" type="text/css"/>
@stop
@section('scripts')
    <!-- iCheck js -->
    <script src="{{ asset('assets/plugins/icheck/icheck.min.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {

            "use strict"; // Start of use strict
            $('.i-check input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue'
            });


        });
    </script>
@stop