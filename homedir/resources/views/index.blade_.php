@extends('layouts.app')
<?php
$_home = $Settings('home_page');
?>

@if(!empty($_home['meta_title']['value']))
    @section('title')
        {{$_home['meta_title']['value']}}
    @stop
@endif
@section('meta')
    <meta name="description" itemprop="description" content="{{ $_home['meta_description']['value'] }}" />
    <meta name="keywords" content="{{ $_home['meta_keywords']['value'] }}" />
    <meta property="og:title" content="{{ $_home['meta_title']['value'] }}" />
    <meta property="og:description" content="{{ $_home['meta_description']['value'] }}" />
@stop


@section('content')

<section class="bg-parallax parallax-window" style="background: url({{ asset('images/'.$_home['slider_background']['value']) }});background-size: cover;background-attachment: fixed;background-repeat: no-repeat;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="parallax-text">
                    <h2 class="parallax_t __white">{{ $_home['slider_title']['value'] }}</h2>
                    {{--<h2 class="parallax_t __green">available from 10.12.2016</h2>--}}
                    <p>
                        {{ $_home['slider_message']['value'] }}
                    </p>
                </div>
            </div>
            <!-- planner-->
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 planner">
                <div class="planner-block">
                    {!! Form::open(['action'=>['PropertyController@search'],'method'=>'get','class'=>'form-planner form-horizontal']) !!}
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Check in</label>
                                    <input name="from_date" data-theme="blue-theme" data-large-mode="true"  data-provide="datepicker" class="form-control __plannerInput" id="datetimepicker1" type="text" placeholder="10-05-2015"/>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Check out</label>
                                    <input name="to_date" data-large-mode="true" data-theme="blue-theme" data-provide="datepicker" class="form-control __plannerInput" id="datetimepicker2" type="text" placeholder="17-05-2015"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Property type</label>
                                    <div class="theme-select">
                                        <select name="type" class="form-control __plannerSelect">
                                            <option value="">Select Property Category</option>
                                            @foreach($menu as $item)
                                            <option value="{{ $item->slug }}">{{ $item->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <label>Adults</label>
                                    <div class="theme-select">
                                        <select name="adults" class="form-control __plannerSelect">
                                            @for($i=1; $i<21; $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <label>Kids</label>
                                    <div class="theme-select">
                                        <select name="kids" class="form-control __plannerSelect">
                                            @for($i=1; $i<21; $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Enter your email</label>
                                    <input required name="email" type="email" class="form-control" placeholder="E-mail"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label>Price range</label>
                                    <input id="min_price" value="{{ round(($MaxPrice->price>0?$MaxPrice->price:10000)/100) }}" type="hidden" name="min_price">
                                    <input id="max_price" value="{{ round(($MaxPrice->price>0?$MaxPrice->price:10000)/2)  }}" type="hidden" name="max_price">
                                    <div id="slider-range"></div>
                                    <div id="amount"><span id="amount1"></span><span id="amount2"></span></div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Search Type</label>
                                    <div class="checkbox-container">
                                        <input type="checkbox" id="on-off-switch" name="search_type" value="1" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <a  href="{{ action('PropertyController@advanceSearch') }}" class="btn btn-advance-search btn-default">Advance Search</a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="planner-check-availability">
                                    <button type="submit" class="btn btn-default">Check availability </button>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}}
                </div>
            </div>
            <!-- /planner-->
        </div>
    </div>
</section>
<!-- /parallax -->


@if($best_offers->count()>0)
<!-- chose best rooms -->
<section class="best-room wizzard">
    <div class="container ">
        <div class="title-main">
            <h2 class="h2">Best Offer For Weekend <span class="title-secondary">Look Our Featured Properties</span></h2>
        </div>
        <div class="best-room-carousel wizzard-list">
            <ul class="row best-room_ul">
                @foreach($best_offers as $property)
                <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">
                    <div class="best-room_img">
                        <a href="{{ action('PropertyController@show',['id'=>$property->id]) }}">
                            <img class="lazy" src="{{ asset('img/img-loading.gif') }}" data-original="{{ asset('images/'.($property->images()->count()>0?'property/'.$property->images()->first()->name:'blank.png')) }}" alt="{{ $property->name }}"/>
                        </a>
                        <div class="best-room_overlay">
                            <div class="overlay_icn"><a data-toggle="tooltip" title="Show property details" href="{{ action('PropertyController@show',['id'=>$property->id]) }}"></a></div>
                        </div>
                    </div>
                    <div class="best-room-info">
                        <div class="best-room_t"><a href="{{ action('PropertyController@show',['id'=>$property->id]) }}">{{ str_limit($property->name,25) }}</a></div>
                        <div class="best-room_desc">
                            {{ str_limit($property->short_description,200) }}
                        </div>
                        <div class="best-room_price">
                            {{ currency($property->price,'USD',$CURRENCY()) }} <span> / {{ $property->price_unit }}</span>
                        </div>
                        <div class="wizzard_footer">
                            <?php $_count = 0 ?>
                            @if(!empty($property->beds) && $property->beds>0)
                                <?php $_count++ ?>
                                <div data-toggle="tooltip" title="{{ $property->beds }} Beds" class="footer_el __bed"><i class="fa fa-bed"></i> {{ $property->beds }}</div>
                            @endif
                            @if(!empty($property->max_people) && $property->max_people > 0)
                                    <?php $_count++ ?>
                                <div data-toggle="tooltip" title="Maximum People" class="footer_el __ppl"><i class="fa fa-user"></i> {{ (int)$property->max_people }}</div>
                            @endif
                            @if(!empty($property->year_build))
                                <?php $_count++ ?>
                                <div data-toggle="tooltip" title="Year Build" class="footer_el"><i class="fa fa-calendar-check-o"></i> {{ $property->year_build }}</div>
                            @endif
                            @if(!empty($property->bathrooms) && $property->bathrooms>0)
                                    <?php $_count++ ?>
                                <div data-toggle="tooltip" title="{{ $property->bathrooms }} Bathrooms" class="footer_el"> <i class="fa fa-shower"></i>{{ $property->bathrooms }}</div>
                            @endif
                            @if(!empty($property->bedrooms) && $property->bedrooms>0 &&   $_count < 4)
                                <?php $_count++ ?>
                                <div data-toggle="tooltip" title="Bedrooms" class="footer_el"><i class="fa fa-home"></i> {{ (int)$property->bedrooms }}</div>
                            @endif
                            @if(!empty($property->kids) && $property->kids>0 && $_count < 4 )
                                <?php $_count++ ?>
                                    <div data-toggle="tooltip" title="Kids" class="footer_el"><i class="icon-child"></i> {{(int)$property->kids }}</div>
                            @endif
                            @if(!empty($property->adult) && $property->adult>0 && $_count < 4 )
                                <?php $_count++ ?>
                                <div data-toggle="tooltip" title="Adults" class="footer_el"><i class="icon-adult"></i> {{ (int)$property->adult }}</div>
                            @endif
                            @if($_count < 4)
                                <?php $_count++ ?>
                                <div data-toggle="tooltip" title="Parking" class="footer_el"><i class="fa fa-car"></i> {{ (int)$property->parking }}</div>
                            @endif
                            @if( $_count < 4 )
                                <?php $_count++ ?>
                                <div data-toggle="tooltip" title="Pools" class="footer_el"><i class="icon-swimming"></i> {{ (int)$property->pool }}</div>
                            @endif
                            @if($_count < 4)
                                <?php $_count++ ?>
                                <div data-toggle="tooltip" title="{{ $property->bathrooms }} Bathrooms" class="footer_el"> <i class="fa fa-shower"></i>{{ $property->bathrooms }}</div>
                            @endif
                            @if($_count < 4)
                                <?php $_count++ ?>
                                <div data-toggle="tooltip" title="{{ $property->beds }} Beds" class="footer_el __bed"><i class="fa fa-bed"></i> {{ $property->beds }}</div>
                            @endif
                            <div  class="clearfix"></div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="view-more"><a href="{{ action('PropertyController@offers') }}"><strong>View More</strong></a></div>
    </div>
</section>
@endif

@foreach($menu as $item)
    @if($item->properties()->available()->count()>0)
        <section class="best-room wizzard">
        <div class="container">
            <div class="title-main">
                <h2 class="h2">{{ title_case($item->title) }}<span class="title-secondary">{{ $item->description }}</span></h2>
            </div>
            <div class="best-room-carousel wizzard-list">
                <ul class="row best-room_ul">
                    @foreach($properties = $item->properties()->inRandomOrder()->limit(6)->get() as $property)
                        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">
                            <div class="best-room_img">
                                <a href="{{ action('PropertyController@show',['id'=>$property->id]) }}">
                                    <img class="lazy" src="{{ asset('img/img-loading.gif') }}" data-original="{{ asset('images/'.($property->images()->count()>0?'property/'.$property->images()->first()->name:'blank.png')) }}" alt="{{ $property->name }}"/>
                                </a>
                                <div class="best-room_overlay">
                                    <div class="overlay_icn"><a data-toggle="tooltip" title="Show property details" href="{{ action('PropertyController@show',['id'=>$property->id]) }}"></a></div>
                                </div>
                            </div>
                            <div class="best-room-info">
                                <div class="best-room_t"><a href="{{ action('PropertyController@show',['id'=>$property->id]) }}">{{ str_limit($property->name,25) }}</a></div>
                                <div class="best-room_desc">
                                    {{ str_limit($property->short_description,200) }}
                                </div>
                                <div class="best-room_price">
                                    {{ currency($property->price,'USD',$CURRENCY()) }}<span>/ {{ $property->price_unit }}</span>
                                </div>
                                <div class="wizzard_footer">
                                    <?php $_count = 0 ?>

                                    @if(!empty($property->beds) && $property->beds>0)
                                        <?php $_count++ ?>
                                        <div data-toggle="tooltip" title="{{ $property->beds }} Beds" class="footer_el __bed"><i class="fa fa-bed"></i> {{ $property->beds }}</div>
                                    @endif
                                    @if(!empty($property->max_people) && $property->max_people > 0)
                                        <?php $_count++ ?>
                                        <div data-toggle="tooltip" title="Maximum People" class="footer_el __ppl"><i class="fa fa-user"></i> {{ (int)$property->max_people }}</div>
                                    @endif
                                    @if(!empty($property->year_build))
                                        <?php $_count++ ?>
                                        <div data-toggle="tooltip" title="Year Build" class="footer_el"><i class="fa fa-calendar-check-o"></i> {{ $property->year_build }}</div>
                                    @endif
                                    @if(!empty($property->bathrooms) && $property->bathrooms>0)
                                        <?php $_count++ ?>
                                        <div data-toggle="tooltip" title="{{ $property->bathrooms }} Bathrooms" class="footer_el"> <i class="fa fa-shower"></i>{{ $property->bathrooms }}</div>
                                    @endif
                                    @if(!empty($property->bedrooms) && $property->bedrooms>0 &&   $_count < 4)
                                        <?php $_count++ ?>
                                        <div data-toggle="tooltip" title="Bedrooms" class="footer_el"><i class="fa fa-home"></i> {{ (int)$property->bedrooms }}</div>
                                    @endif
                                    @if(!empty($property->kids) && $property->kids>0 && $_count < 4 )
                                        <?php $_count++ ?>
                                        <div data-toggle="tooltip" title="Kids" class="footer_el"><i class="icon-child"></i> {{(int)$property->kids }}</div>
                                    @endif
                                    @if(!empty($property->adult) && $property->adult>0 && $_count < 4 )
                                        <?php $_count++ ?>
                                        <div data-toggle="tooltip" title="Adults" class="footer_el"><i class="icon-adult"></i> {{ (int)$property->adult }}</div>
                                    @endif
                                    @if($_count < 4)
                                        <?php $_count++ ?>
                                        <div data-toggle="tooltip" title="Parking" class="footer_el"><i class="fa fa-car"></i> {{ (int)$property->parking }}</div>
                                    @endif
                                    @if( $_count < 4 )
                                        <?php $_count++ ?>
                                        <div data-toggle="tooltip" title="Pools" class="footer_el"><i class="icon-swimming"></i> {{ (int)$property->pool }}</div>
                                    @endif
                                    @if($_count < 4)
                                        <?php $_count++ ?>
                                        <div data-toggle="tooltip" title="{{ $property->bathrooms }} Bathrooms" class="footer_el"> <i class="fa fa-shower"></i>{{ $property->bathrooms }}</div>
                                    @endif
                                    @if($_count < 4)
                                        <?php $_count++ ?>
                                        <div data-toggle="tooltip" title="{{ $property->beds }} Beds" class="footer_el __bed"><i class="fa fa-bed"></i> {{ $property->beds }}</div>
                                    @endif


                                    <div  class="clearfix"></div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="view-more"><a href="{{ url('properties/'.$item->slug) }}"><strong>View More</strong></a></div>
        </div>
    </section>
    @endif
@endforeach



<!-- lux banner parallax -->
<section class="banner bg-parallax2" style="background: url({{ $luxury->images()->count() > 0 ?asset('images/property/'.$luxury->images()->first()->name):'' }});background-size: cover;background-attachment: fixed;background-repeat: no-repeat;">
    <div class="overlay"></div>
    <div class="banner-parallax">
        <div class="container">
            <div class="text-center">
              {{--  <div class="banner-parallax_raiting">
                    <a href="#"><span class="star __selected"></span></a>
                    <a href="#"><span class="star __selected"></span></a>
                    <a href="#"><span class="star __selected"></span></a>
                    <a href="#"><span class="star"></span></a>
                    <a href="#"><span class="star"></span></a>
                </div>--}}
                <h2 class="banner-parallax_t">{{ title_case($luxury->name) }}</h2>
                <div class="banner-parallax_price"><span>{{ currency($luxury->price,'USD',$CURRENCY()) }}</span> {{ !empty($luxury->price_unit)?' / '.$luxury->price_unit:'' }}</div>
                <a href="{{ action('PropertyController@show',$luxury->id) }}" class="btn btn-default">View Details</a>
            </div>
        </div>
    </div>
</section>
<!-- /lux banner parallax -->
<!-- enjoy our services -->

<section class="section">
    <div class="container">
        <div class="title-main"><h2 class="h2">{{ $_home['services_heading']['value'] }}<span class="title-secondary">{{ $_home['services_description']['value'] }}</span></h2></div>
        <div class="row">
            @foreach($services as $service)
            <div class="col-lg-4 col-md-4 col-sm-6">
                <h3 class="service_title"><i class="fa {{ $service->icon }}"></i> {{ $service->title }}</h3>
                <p>{{ $service->short_description }}</p>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- /enjoy our services -->

@if($testimonials->count()>0)
<!-- testiomonials -->
<section class="testimonials">
    <div class="container">
        <div class="title-main"><h2 class="h2">Testimonials<span class="title-secondary">What People Says About Us?</span></h2></div>
        <div class="owl-carousel">
            @foreach($testimonials as $testimonial)
            <div class="item">
                <div class="testimonials-block_i">
                    <div class="testimonials-block_t">{{ $testimonial->title }}</div>
                    <p>
                        {{ $testimonial->quote }}
                    </p>
                </div>
                <div class="testimonials-block_user">

                        @if(!empty($testimonial->image))
                        <div class="user_img">
                            <img src="{{ asset('images/testimonials/'.$testimonial->image) }}" alt="{{ $testimonial->name }}"/>
                         </div>
                        @else
                        <div class="user_img testimonial-icon">
                            <i class="fa fa-user fa-3x"></i>
                        </div>
                        @endif
                    <div class="user_n">{{ $testimonial->name }}</div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
 @endif

@include('partials.map',['Map'=>$_home])
@stop
@section('styles')
 <style>
        section.map iframe{
            width: 100%;
            display: block;
            pointer-events: none;
            position: relative; /* IE needs a position other than static */
        }
        section.map iframe.clicked{
            pointer-events: auto;
        }
    </style>
@stop
@section('scripts')
<script>
    $(document).ready(function () {
        if($.ui) {
            $( "#slider-range" ).slider({
                range: true,
                min: 0,
                max: {{  $MaxPrice->price>0?$MaxPrice->price:10000 }},
                values: [ {{ round(($MaxPrice->price>0?$MaxPrice->price:10000)/100) }}, {{ round(($MaxPrice->price>0?$MaxPrice->price:10000)/2)  }} ],
                slide: function( event, ui ) {
                    $( "#amount1" ).text("$" + ui.values[ 0 ]);
                    $( "#amount2" ).text("$" + ui.values[ 1 ]);

                    $('input#min_price').attr('value',ui.values[ 0 ]);
                    $('input#max_price').attr('value',ui.values[ 1 ]);
                }
            });
            $( "#amount1" ).text( "$" + $( "#slider-range" ).slider( "values", 0));
            $( "#amount2" ).text( "$" + $( "#slider-range" ).slider( "values", 1));
        }


        $('section.map')
                .click(function(){
                    $(this).find('iframe').addClass('clicked')})
                .mouseleave(function(){
                    $(this).find('iframe').removeClass('clicked')});
    });
</script>
@stop