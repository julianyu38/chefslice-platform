<?php $SITE_INFO = $SITE_SETTING('site_info'); ?>
<nav class="header one-page-header navbar navbar-default navbar-fixed-top restaurant-header one-page-nav-scrolling " data-role="navigation">
    <div class="container"> <!-- Maybe should be changed to container-fluid-->
        <div class="menu-container page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand main-font" href="#body">
                 <img src="{{ asset('assets/home/img/').'/'.$SITE_INFO['logo']->value }}" alt="Logo">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <div class="menu-container">
                <ul class="nav navbar-nav">
                    <li class="page-scroll home">
                        <a href="{{ request()->segment(1)? url ('/') :'#body' }}">Home</a>
                    </li>
                    @yield('nav')

                        @if(auth()->check())
                        <li class="">
                            <a href="{{ url(env('APP_URL_PROTOCOL').'://'.auth()->user()->user_name.env('SESSION_DOMAIN').'/dashboard') }}">Dashboard</a>
                        </li>
                        <li class="">
                            <a href="{{ url('/logout') }}">Logout</a>
                        </li>
                        @else
                        <li class="">
                            <a href="{{ url('/login') }}">Login</a>
                        </li>
                        @endif
                </ul>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<!-- End Header -->
