<!-- Scripts -->
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
<!-- jQuery Easing -->
<script src="{{ asset('theme/scripts/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('theme/hydrogen/js/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
{{--<script src="https://hammerjs.github.io/dist/hammer.js"></script>--}}
{{--<script src="{{ asset('assets/plugins/jquery.mousewheel.min.js') }}"></script>--}}
{{--HeatMap--}}
<script src="{{ asset('assets/plugins/heatmap.min.js') }}"></script>
@if($settings->heatmap)
    @include('partials.restaurant.heatmap')

    @endif


{{--<script>

    function rotateMenu(el ,rotation) {
        if(rotation == null)
            rotation = 0;
        //   $('.menu__listings ul.circle').css('transform','rotate('+rotation+' deg)');
        var el =  document.getElementsByClassName('circle')[0];
        el.style.transform = 'rotate('+rotation+'deg)';
    }
    $(document).ready(function(){
        var __koc_menu_rotation = 0;
        var myElement = document.getElementsByClassName('koc-menu__listings')[0];
        $('html').on('mousewheel', function(event) {
            if(event.deltaY === -1 ){
                __koc_menu_rotation -= 10;
                rotateMenu(myElement,__koc_menu_rotation);
            }else {
                __koc_menu_rotation += 10;
                rotateMenu(myElement,__koc_menu_rotation);
            }
        });

        var mc = new Hammer(myElement);
        mc.on("panup pandown panleft", function(ev) {
            if(ev.type === 'panup'){
                __koc_menu_rotation = (Math.round(ev.distance) + 20) * -1;
                rotateMenu(myElement,__koc_menu_rotation);
            }else {
                __koc_menu_rotation = Math.round(ev.distance);
                rotateMenu(myElement,__koc_menu_rotation);
            }
        });

    });
    </script>--}}

@yield('scripts')
<script>
    var heatmapurl= '{{url('heatmap')}}';

    $(document).ready(function() {
        heatmapdot = <?php echo json_encode($heatmap)?>;
        $('body').click(function(e){
//            e.stopPropagation();
            var x = e.clientX;
            var y = e.clientY;
            $.post(heatmapurl, {
                x:x,
                y:y,
                "_token": "{{ csrf_token() }}",
                location:escape(document.location.pathname)
            });
        });
    });





</script>

<script>
    $(document).ready(function(){
        var el = $('.special-offer img');

        var animation = setInterval(function(){
            if(el.hasClass('animated') && el.hasClass('tada')){
                el.removeClass('animated');
                el.removeClass('tada');
                //el.removeClass('infinite');

            }else {
                el.addClass('animated');
                el.addClass('tada');
                // el.addClass('infinite');
            }
        },3000);

    });
</script>