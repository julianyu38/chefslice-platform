
<footer>
    <div  class="vertical-text">
        <i class="fa fa-hand-o-left bounce shift" aria-hidden="true"></i> {{ $settings->specialoffer  }}
    </div>
    <?php $request = request()->route()->getAction(); ?>
        <div class="offer">
     @if(strpos($request['controller'], 'special') && strpos($request['controller'],'HomeController') )
            <a href="{{url('/')}}" class="special-offer animated tada">
                <img id="specialoffer"  width="100px" src="{{asset('img/chaferopen.png')}}" alt="" style=" width: 12rem; left: 20px;" >
            </a>{{--<span class="specialtext">{{ $settings->specialoffer  }}</span>--}}
     @else

            <a href="{{url('/special')}}" class="special-offer animated tada">
                <img id="specialoffer"  src="{{asset('img/chafer.png')}}" alt="" >
            </a>
                {{--<span class="specialtext">{{ $settings->specialoffer  }}</span>--}}
        @endif

         </div>
    <div class="language">

        <ul class="lang list-unstyled">

            <li><a href="{{url('locale/en')}}"><img src="{{asset('img/flags/united_kingdom_640.png')}}"></a> </li>
            <li><a href="{{url('locale/he')}}"><img src="{{asset('img/flags/israel_640.png')}}"></a> </li>

        </ul>

    </div>
</footer>

<!-- Trigger the modal with a button -->
{{--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>--}}

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@section('scripts')
    <script>
//        $("#specialoffer").click(function(){
//            alert();
//        });
//        $( "#specialoffer" ).click(function() {
//            alert( "Handler for .click() called." );
//        });
    </script>
    @stop