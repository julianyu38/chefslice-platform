<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
@include('partials.home.head')
<body id="body" data-spy="scroll" data-target=".one-page-header" class="demo-lightbox-gallery font-main promo-padding-top">
    <main class="wrapper">
        <!-- Header -->
    @include('partials.home.nav')
    @yield('content')
    @include('partials.home.footer')

   </main>
@include('partials.home.scripts')
</body>
</html>
