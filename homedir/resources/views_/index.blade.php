@extends('layouts.app')

@section('content')

    <?php $allCategories = $categories->count(); ?>
    @include('category-design.'.$settings->category_design);
    <?php $mean = $allCategories / 2; ?>
@stop
@section('scripts')
    @yield('menuscripts')
    <script>
        var categories = <?php echo json_encode($categories) ?>;
        var mean = <?php echo (int)$mean ?>;

    </script>
    <script src="{{ asset('theme/scripts/theta-carousel.min.js') }}"></script>
    {{--<script src="{{ asset('theme/scripts/example.js') }}"></script>--}}
    {{--<script src="{{ asset('theme/scripts/jquery.popupoverlay.js') }}"></script>--}}

@stop