<footer>
    <?php $request = request()->route()->getAction(); ?>
        <div class="offer">
     @if(strpos($request['controller'], 'special') && strpos($request['controller'],'HomeController') )
            <a href="{{url('/')}}" class="specialofferopen" style="position: absolute; bottom: 15px;">
                <img id="specialoffer"  width="100px" src="{{asset('img/chaferopen.png')}}" alt="" >
            </a><span class="specialtext">{{ $settings->specialoffer  }}</span>
     @else

    <a href="{{url('/special')}}" class="specialoffer" style=" position: absolute; bottom: -10px;">
        <img id="specialoffer"  width="100px" src="{{asset('img/chafer.png')}}" alt="" >
    </a><span class="specialtext">{{ $settings->specialoffer  }}</span>
        @endif

         </div>
    <div class="language">
        {{--<select name="lang">--}}
            {{--<option value="en">English</option>--}}
            {{--<option value="he">Hebrew</option>--}}
        {{--</select>--}}
        <ul class="lang">
            <li><a href=""><img src="{{asset('img/flags/United-Kingdom.png')}}"></a> </li>
            <li><a href=""><img src="{{asset('img/flags/Israel.png')}}"></a> </li>

        </ul>

    </div>
</footer>

<!-- Trigger the modal with a button -->
{{--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>--}}

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@section('scripts')
    <script>
//        $("#specialoffer").click(function(){
//            alert();
//        });
//        $( "#specialoffer" ).click(function() {
//            alert( "Handler for .click() called." );
//        });
    </script>
    @stop