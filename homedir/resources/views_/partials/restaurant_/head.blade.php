<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
     <title>
        @hasSection('title')
        @yield('title')
        @else
            {{ env('APP_NAME',"Barbados Sotheby's") }} - Danial
        @endif
    </title>
    @yield('meta')
<!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content=""/>
    <meta name="twitter:image" content=""/>
    <meta name="twitter:url" content=""/>
    <meta name="twitter:card" content=""/>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- Google Webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <meta name="google-site-verification" content="GwbDuLf7Z5maleR_VuKrhOzAA_JPX5PXqn7UCRpcaP0" />
    <link href="{{ asset('theme/animate.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('theme/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"/>
    <link defer rel="stylesheet" href="{{ asset('theme/circularmenu/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/style.css') }}">
    @yield('styles')
    <style>
        .heatmap-canvas {
            /*height: 100vh !important;*/
            width:100% !important;
        }
    </style>
            <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
   
   
</head>