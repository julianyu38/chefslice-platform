@extends('layouts.app')

@section('content')

    <?php $allCategories = $categories->count(); ?>
    <div id="container" >
        @if($categories->count()>0)
            @foreach($categories as $category)

                <div class="ex-item">
                    <img data-content="{{url("$category->slug")}}" class="category-link" src="{{ asset('media/categories/images/'.$category->icon) }}" />
                </div>
            @endforeach
        @endif


    </div>
    <?php $mean = $allCategories / 2; ?>
@stop
@section('styles')
    <style type="text/css">
        #container {
            top: 150px; bottom: 0px; position: absolute; right: 0px; left: 0px;
        }
        img {
            vertical-align: middle;
        }

        .ex-item {
            height: 360px;
            width: 480px;
            display: none;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            background-color: black;
        }

        .player-container {
            display: none;
        }

        .current .player-container {
            display: inherit;
        }

        .current img {
            display: none;
        }

        .in-motion .player-container {
            display: none !important;
        }

        .in-motion img {
            display: inherit !important;
        }

        .loading .player-container {
            display: none !important;
        }

        .loading img {
            display: inherit !important;
        }
    </style>
@stop
@section('scripts')
    <script>
        var categories = <?php echo json_encode($categories) ?>;
        var mean = <?php echo (int)$mean ?>;

    </script>
    {{--    <script src="{{ asset('theme/scripts/theta-carousel.min.js') }}"></script>--}}
    <script src="{{ asset('theme/theta/video/scripts/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('theme/theta/video/scripts/theta-carousel.min.js') }}"></script>
    <script src="{{ asset('theme/theta/video/scripts/example.js') }}"></script>

@stop

