@extends('layouts.app')

@section('content')

    <?php $allCategories = $categories->count(); ?>
    <div id="container" >

        <div class="shelf" style="height: 200px; position: absolute; width: 100%; top: 345px; margin-left: 100px; width: 1400px;">

        @if($categories->count()>0)
            @foreach($categories as $category)

                <div class="ex-item">
                    <img data-content="{{url("$category->slug")}}" class="category-link"  src="{{ asset('media/categories/images/'.$category->icon) }}" />
                </div>
            @endforeach
        @endif


    </div>
    <?php $mean = $allCategories / 2; ?>
@stop

@section('styles')
            <style type="text/css">
                #container {
                    top: 150px; bottom: 0px; position: absolute; right: 0px; left: 0px;
                }
                img {
                    vertical-align: middle;
                }

                .ex-item {
                    height: 218px;
                    width: 200px;
                    display: none;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .shelf {
                    background-image: linear-gradient(to bottom, rgba(67, 95, 186, 0.21) 0%, #36C2DB 100%);
                    background: -webkit-linear-gradient(top, rgba(67, 95, 186, 0.21) 0%, #36C2DB 100%);
                    transform: skew(-45deg);
                    -webkit-transform: skew(-45deg);
                    border-bottom: 4px solid #448A97;
                    border-right: 4px solid #52A0AE;
                }
                .ex-item img {
                    width: 100%;
                    height: 100%;

                }
            </style>
@stop
@section('scripts')
    <script>
        var categories = <?php echo json_encode($categories) ?>;
        var mean = <?php echo (int)$mean ?>;

    </script>
    {{--    <script src="{{ asset('theme/scripts/theta-carousel.min.js') }}"></script>--}}
    <script src="{{ asset('theme/theta/store-shelf/scripts/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('theme/theta/store-shelf/scripts/theta-carousel.min.js') }}"></script>
    <script src="{{ asset('theme/theta/store-shelf/scripts/example.js') }}"></script>

@stop

