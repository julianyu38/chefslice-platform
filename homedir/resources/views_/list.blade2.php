<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hydrogen &mdash; A free HTML5 Template by FREEHTML5.CO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Template by FREEHTML5.CO"/>
    <meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive"/>
    <meta name="author" content="FREEHTML5.CO"/>

    <!--
      //////////////////////////////////////////////////////

      FREE HTML5 TEMPLATE
      DESIGNED & DEVELOPED by FREEHTML5.CO

      Website:        http://freehtml5.co/
      Email:          info@freehtml5.co
      Twitter:        http://twitter.com/fh5co
      Facebook:       https://www.facebook.com/fh5co

      //////////////////////////////////////////////////////
       -->

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content=""/>
    <meta name="twitter:image" content=""/>
    <meta name="twitter:url" content=""/>
    <meta name="twitter:card" content=""/>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Google Webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Animate.css -->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/animate.css') }}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/icomoon.css') }}">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/magnific-popup.css') }}">
    <!-- Salvattore -->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/salvattore.css') }}">
    <!-- Theme Style -->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/style.css') }}">
    <!-- Modernizr JS -->
    <script src="{{ asset('theme/hydrogen/js/modernizr-2.6.2.min.js') }}"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="{{ asset('theme/hydrogen/js/respond.min.js') }}"></script>
    <![endif]-->

</head>
<body style="background: #392338;">


<div id="fh5co-main">
    <div class="container">

        <div class="row">

            <div id="fh5co-board" data-columns>

                <div class="item">
                    <div class="animate-box">
                        <a href="#" data-toggle="modal" data-target="#myModal" data-id="1"
                           class="image-popup fh5co-board-img"
                           title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, eos?"><img
                                    src="{{ asset('theme/hydrogen/images/img_1.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                    </div>
                    <div class="fh5co-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, eos?
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_2.jpg') }}"
                           class="image-popup fh5co-board-img"><img src="{{ asset('theme/hydrogen/images/img_2.jpg') }}"
                                                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Veniam voluptatum voluptas tempora debitis harum totam vitae hic quos.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_3.jpg') }}"
                           class="image-popup fh5co-board-img"><img src="{{ asset('theme/hydrogen/images/img_3.jpg') }}"
                                                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Optio commodi quod vitae, vel, officiis similique quaerat odit dicta.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_4.jpg') }}"
                           class="image-popup fh5co-board-img"><img src="{{ asset('theme/hydrogen/images/img_4.jpg') }}"
                                                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Dolore itaque deserunt sit, at exercitationem delectus, consequuntur
                            quaerat sapiente.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_5.jpg') }}"
                           class="image-popup fh5co-board-img"><img src="{{ asset('theme/hydrogen/images/img_5.jpg') }}"
                                                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Tempora distinctio inventore, nisi excepturi pariatur tempore sit quasi
                            animi.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_6.jpg') }}"
                           class="image-popup fh5co-board-img"><img src="{{ asset('theme/hydrogen/images/img_6.jpg') }}"
                                                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Sequi, eaque suscipit accusamus. Necessitatibus libero, unde a nesciunt
                            repellendus!
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_7.jpg') }}"
                           class="image-popup fh5co-board-img"><img src="{{ asset('theme/hydrogen/images/img_7.jpg') }}"
                                                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Necessitatibus distinctio eos ipsam cum hic temporibus assumenda
                            deleniti, soluta.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_8.jpg') }}"
                           class="image-popup fh5co-board-img"><img src="{{ asset('theme/hydrogen/images/img_8.jpg') }}"
                                                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Debitis voluptatum est error nulla voluptate eum maiores animi quasi?
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_9.jpg') }}"
                           class="image-popup fh5co-board-img"><img src="{{ asset('theme/hydrogen/images/img_9.jpg') }}"
                                                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Maxime qui eius quisquam quidem quos unde consectetur accusamus
                            adipisci!
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_21.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_21.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Deleniti aliquid, accusantium, consectetur harum eligendi vitae quaerat
                            reiciendis sit?
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_10.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_10.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Incidunt, eaque et. Et odio excepturi, eveniet facilis explicabo
                            assumenda.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_11.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_11.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Laborum dolores nihil voluptates quas alias distinctio fugiat tempora
                            sit.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_12.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_12.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Sit, quis nulla amet numquam fugit, in reiciendis laboriosam dolor.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_13.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_13.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Possimus explicabo voluptatem natus nisi similique ipsa repudiandae?
                            Quibusdam, fuga.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_14.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_14.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Magni repellendus iusto mollitia, quibusdam facilis incidunt. Sunt,
                            repellat, voluptatem.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_15.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_15.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Unde iure rerum cupiditate explicabo quam aut vel earum numquam.</div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_16.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_16.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Qui nisi error dolorum dolor delectus, alias doloremque perspiciatis
                            nemo.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_18.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_18.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Neque porro vero cumque natus nam voluptatibus, ratione, commodi
                            labore.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_17.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_17.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Quisquam quia totam, sit ea maxime sint sed excepturi quod.</div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_19.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_19.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Nesciunt non iste ex nemo sapiente eum, provident nam corporis.</div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_20.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_20.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Harum repellat labore est cum ipsa, nesciunt neque mollitia adipisci?
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_22.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_22.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Quos repellendus repudiandae debitis reprehenderit cupiditate cumque
                            accusamus exercitationem, harum.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_23.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_23.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Sunt numquam itaque delectus, dignissimos dolorem obcaecati vel, atque
                            eos.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_24.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_24.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Adipisci consequuntur ipsa fugit perspiciatis eligendi. Omnis
                            blanditiis, totam placeat.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_25.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_25.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Quos repellendus repudiandae debitis reprehenderit cupiditate cumque
                            accusamus exercitationem, harum.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_26.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_26.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Sunt numquam itaque delectus, dignissimos dolorem obcaecati vel, atque
                            eos.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_27.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_27.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Adipisci consequuntur ipsa fugit perspiciatis eligendi. Omnis
                            blanditiis, totam placeat.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_28.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_28.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Adipisci consequuntur ipsa fugit perspiciatis eligendi. Omnis
                            blanditiis, totam placeat.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_29.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_29.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Adipisci consequuntur ipsa fugit perspiciatis eligendi. Omnis
                            blanditiis, totam placeat.
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="animate-box">
                        <a href="{{ asset('theme/hydrogen/images/img_30.jpg') }}"
                           class="image-popup fh5co-board-img"><img
                                    src="{{ asset('theme/hydrogen/images/img_30.jpg') }}"
                                    alt="Free HTML5 Bootstrap template"></a>
                        <div class="fh5co-desc">Adipisci consequuntur ipsa fugit perspiciatis eligendi. Omnis
                            blanditiis, totam placeat.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    {{--<div class="modal-dialog">--}}
    {{--<div class="container-fluid">--}}
    <div class="container">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="row">



                    <div class="col-md-8 pull-left">

                        <div id="rightImgHolder">
                            <div id="imageContainer" style="width: 600px;"><img
                                        src="http://design18.flash-gallery.net/projects/photos/project09.jpg"
                                        class="caroImg" id="caroImg0"></div>
                        </div>
                    </div>
                    <div class="col-md-4  pull-right">
                        <div id="title" class="title"
                             style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); color: rgb(68, 68, 68); visibility: visible; opacity: 1;">
                            Portfolio Gallery
                        </div>
                        <div id="created-at" class="created-at"
                             style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); color: rgb(68, 68, 68); visibility: visible; opacity: 1;">
                            Created at Flash-gallery for FGN
                        </div>
                        <hr id="tophr" style="width: 100%;">
                        <div class="text trans3d" id="contentText"
                             style="height: 297px; top: 63px; overflow: hidden; opacity: 1; visibility: visible;">
                            <div class="contentScroll"
                                 style="width: 270px; transition-property: -webkit-transform; transform-origin: 0px 0px 0px; transform: translate(0px, 0px) translateZ(0px);">
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                                    consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro
                                    quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
                                    sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam
                                    quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam
                                    corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</p>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                                    architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                                    voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos
                                    qui ratione voluptatem sequi nesciunt. .</p>
                                <br><i>ROLES:</i> Flash development
                            </div>
                            <div style="position: absolute; z-index: 100; width: 7px; bottom: 2px; top: 2px; right: 1px; transition-property: opacity; overflow: hidden; opacity: 1;">
                                <div data-scrollbarindicator="true"
                                     style="position: absolute; z-index: 100; background: rgba(0, 0, 0, 0.5); border: 1px solid rgba(255, 255, 255, 0.9); -webkit-background-clip: padding-box; box-sizing: border-box; width: 100%; border-radius: 3px; transition-property: -webkit-transform; transition-timing-function: cubic-bezier(0.33, 0.66, 0.66, 1); transform: translate(0px, 0px) translateZ(0px); height: 238px;"></div>
                            </div>
                        </div>
                        <hr id="bottomhr" style="width: 100%; top: 380px;">
                        <div class="awards-list trans3d"
                             style="top: 400px; opacity: 1; visibility: visible; transform: matrix(1, 0, 0, 1, 0, 0);">
                            <a class="launch" style="color: rgb(68, 68, 68);">LAUNCH &gt;</a></div>
                    </div>
                    <div id="numImages" style="width: 18px; margin-left: -9px;">
                        <div class="caroImgItem selected" id="caroImgItem0"
                             style="opacity: 1; visibility: visible; transform: matrix(1, 0, 0, 1, 0, 0);"></div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<footer id="fh5co-footer">

    <div class="container">
        <div class="row row-padded">
            <div class="col-md-12 text-center">
                <p class="fh5co-social-icons">
                    <a href="#"><i class="icon-twitter"></i></a>
                    <a href="#"><i class="icon-facebook"></i></a>
                    <a href="#"><i class="icon-instagram"></i></a>
                    <a href="#"><i class="icon-dribbble"></i></a>
                    <a href="#"><i class="icon-youtube"></i></a>
                </p>
                <p>
                    <small>&copy; Hydrogen Free HTML5 Template. All Rights Reserved. <br>Designed by: <a
                                href="http://freehtml5.co/" target="_blank">FREEHTML5.co</a> | Images by: <a
                                href="http://pexels.com" target="_blank">Pexels</a></small>
                </p>
            </div>
        </div>
    </div>
</footer>


<!-- jQuery -->
<script src="{{ asset('theme/hydrogen/js/jquery.min.js') }}"></script>
<!-- jQuery Easing -->
<script src="{{ asset('theme/hydrogen/js/jquery.easing.1.3.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('theme/hydrogen/js/bootstrap.min.js') }}"></script>
<!-- Waypoints -->
<script src="{{ asset('theme/hydrogen/js/jquery.waypoints.min.js') }}"></script>
<!-- Magnific Popup -->
<script src="{{ asset('theme/hydrogen/js/jquery.magnific-popup.min.js') }}"></script>
<!-- Salvattore -->
<script src="{{ asset('theme/hydrogen/js/salvattore.min.js') }}"></script>
<!-- Main JS -->
<script src="{{ asset('theme/hydrogen/js/main.js') }}"></script>
<script>
    setTimeout(function() {
        $('#myModal').modal();
    }, 20000);
</script>


</body>
</html>
