@extends('layouts.app')

@section('content')
    <div class="pull-right backbtn "><a href="{{url('')}}"><img src="{{asset('img/back.png')}}"></a>
    </div>


    <div id="fh5co-main">
        <div class="container">

            <div class="row">

                <div id="fh5co-board" data-columns>
                    <?php
                    $path = asset('media/items/images/');
                    $mediapath = asset('media/items/images/');
                    ?>
                    @if($items->count()>0)
                        @foreach($items as $item)
                            <?php if ($item->media_type == 2) {
                                $path = asset('media/items/videos/');
                            } ?>
                            <div class="item">
                                <div class="animate-box">
                                    <a href="#" onclick="buildModel(this)" data-id="{{$item->id}}"
                                       class="image-popup fh5co-board-img"
                                       title="{{$item->title}}"><img
                                                src="{{$path.'/'.$item->thumbnail }}"
                                                alt="Free HTML5 Bootstrap template"></a>
                                </div>
                                <div class="fh5co-desc">{{$item->title}}
                                </div>
                            </div>

                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal animated bounceInRight" id="myModal" role="dialog">
        {{--<div class="modal-dialog">--}}
        {{--<div class="container-fluid">--}}
        <div class="container">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Item Details</h4>
                </div>
                <div class="row">


                    <div class="col-md-8 pull-left">
                        <div class="col-md-7" style="width: 100%">
                            <div id="rightImgHolder">
                                <div id="myCarousel" class="Carousal slide" data-ride="carousel" >
                                    <!-- Indicators -->


                                <!-- Wrapper for slides -->
                                    <div id="carousel-inner" class="carousel-inner">


                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4  pull-right">
                        <div id="title" class="title"
                             style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); color: rgb(68, 68, 68); visibility: visible; opacity: 1;">
                            Portfolio Gallery
                        </div>

                        <hr id="tophr" style="width: 100%;">
                        <div class="text trans3d" id="contentText"
                             style="height: 297px; top: 63px; overflow: hidden; opacity: 1; visibility: visible;">
                            <div class="scrollbar" id="style-1">
                                <div class="contentScroll" id="contentDescription"
                                     style="width: 270px; transition-property: -webkit-transform; transform-origin: 0px 0px 0px; transform: translate(0px, 0px) translateZ(0px); padding-right: 5px;">

                                </div>
                            </div>

                        </div>
                        <hr id="bottomhr" style="width: 100%; top: 380px;">
                        <div class="awards-list trans3d"
                             style="top: 400px; opacity: 1; visibility: visible; transform: matrix(1, 0, 0, 1, 0, 0);">
                            {{--<a class="launch" style="color: rgb(68, 68, 68);">LAUNCH &gt;</a></div>--}}
                        </div>
                        <div id="numImages" style="width: 18px; margin-left: -9px;">
                            <div class="caroImgItem selected" id="caroImgItem0"
                                 style="opacity: 1; visibility: visible; transform: matrix(1, 0, 0, 1, 0, 0);"></div>
                        </div>

                    </div>
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        @stop

@section('styles')
    <!-- Animate.css -->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/animate.css') }}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/icomoon.css') }}">

    <!-- Salvattore -->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/salvattore.css') }}">
    <!-- Theme Style -->
    <link rel="stylesheet" href="{{ asset('theme/hydrogen/css/style.css') }}">

    <style>

        .scrollbar {
            /*margin-left: 30px;*/
            float: left;
            height: 280px;
            /*width: 165px;*/
            /*background: #F5F5F5;*/
            overflow-y: scroll;
            margin-bottom: 25px;
        }

        #style-1::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            border-radius: 10px;
            background-color: #F5F5F5;
        }

        #style-1::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5;
        }

        #style-1::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
            background-color: #555;
        }
        #myModal {
            margin-top: 150px;
        }

    </style>
@stop
        @section('scripts')

            <script>
                var obj = <?php echo json_encode($items) ?>;
                var assetUrl = '<?php echo $mediapath ?>';
                //    console.log(obj);
                values = obj;

                function buildModel(el) {
                    $('#carousel-inner').empty();
                    $('.animate-box').removeClass('bounceInLeft');
                    $('.animate-box').addClass('animated bounceOutLeft');


                    item = el.getAttribute('data-id');
                    selectedItem = values[values.findIndex(x => x.id == item
                )];
                    divclass = 'active';
                    for (var i in selectedItem.images) {
                        imgurl = assetUrl + '/' + item + '/' + selectedItem.images[i].image;

                        $('#carousel-inner').prepend('<div class="item ' + divclass + '"><img src="' + imgurl + '" alt="Los Angeles"> </div>');
                        divclass = ''
//                        console.log(selectedItem.images[i].image);
                    }

                    $('#caroImg0').css('width', '100%');
                    $('#title').html(selectedItem.title);
                    $('#contentDescription').html(selectedItem.description);
                    setTimeout(function () {
                        $('#myModal').modal();
                    }, 400);


                }

                $(document).ready(function () {
                    $('.modal-content').addClass('animated bounceInLeft');
                    var hideDelay = true;
                    $('#myModal').on('hide.bs.modal', function (e) {
                        $('.animate-box').removeClass('bounceOutLeft');
                        $('.animate-box').addClass('animated bounceInLeft');
                        if (hideDelay) {
                            $('.modal-content').removeClass('animated bounceInLeft').addClass('animated bounceOutRight');
                            hideDelay = false;
                            setTimeout(function () {
                                $('#myModal').modal('hide');
                                $('.modal-content').removeClass('animated bounceOutRight').addClass('animated bounceInLeft');
                            }, 700);
                            return false;
                        }
                        hideDelay = true;
                        $('.animate-box').removeClass('bounceOutLeft');
                        $('.animate-box').addClass('animated bounceInLeft');
                        return true;
                    });
                });
            </script>




            <!-- Waypoints -->
            <script src="{{ asset('theme/hydrogen/js/jquery.waypoints.min.js') }}"></script>

            <!-- Salvattore -->
            <script src="{{ asset('theme/hydrogen/js/salvattore.min.js') }}"></script>
            <!-- Main JS -->
            <script src="{{ asset('theme/hydrogen/js/main.js') }}"></script>
            <!-- Modernizr JS -->
            <script src="{{ asset('theme/hydrogen/js/modernizr-2.6.2.min.js') }}"></script>
            <!-- FOR IE9 below -->
            <!--[if lt IE 9]>
            <script src="{{ asset('theme/hydrogen/js/respond.min.js') }}"></script>
            <![endif]-->


@stop