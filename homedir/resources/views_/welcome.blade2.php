<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Theta Carousel Example bezier2</title>
    <script src="{{ asset('theme/scripts/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('theme/scripts/theta-carousel.min.js') }}"></script>
    <script src="{{ asset('theme/scripts/example.js') }}"></script>
    <script src="{{ asset('theme/scripts/jquery.popupoverlay.js') }}"></script>
    <script src="{{ asset('theme/scripts/mustache.min.js') }}"></script>
    <link href="{{ asset('theme/animate.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('theme/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"/>
    <link defer rel="stylesheet" href="{{ asset('theme/circularmenu/styles.css') }}">
</head>

<body style="background: #392338;">
<!-- Menu Start -->

<section class="koc-menu koc-menu--circle koc-menu--top-right; koc-menu--bottom-left; koc-menu--bottom-right;">
    <input type="checkbox" id="koc-menu__active"/>
    <label for="koc-menu__active" class="koc-menu__active">

        <!-- Toggle Start -->

        <div class="koc-menu__toggle">
            <div class="icon">
                <div class="hamburger"></div>
            </div>
        </div>

        <!-- Toggle End -->

        <input type="radio" name="arrow--up" id="degree--up-0"/>
        <input type="radio" name="arrow--up" id="degree--up-1"/>
        <input type="radio" name="arrow--up" id="degree--up-2"/>

        <!-- Listings Start -->

        <div class="koc-menu__listings">
            <ul class="circle">

                <!-- Empty Start -->

                <li>
                    <div class="placeholder">
                        <div class="upside"><a href="#" class="button">&nbsp</a></div>
                    </div>
                </li>

                <!-- Empty End -->


                <!-- Ternary Start -->

                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-commenting"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-trash"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-battery-4"></i></a>
                        </div>
                    </div>
                </li>

                <!-- Ternary Stop -->


                <!-- Ternary Start -->

                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-calendar"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-cloud"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-wifi"></i></a>
                        </div>
                    </div>
                </li>

                <!-- Ternary Stop -->


                <!-- Ternary Start -->

                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-envelope-o"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-user"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                </li>

                <!-- Ternary Stop -->

            </ul>
        </div>

        <!-- Listings End -->


        <!-- Arrow Container Start -->

        <div class="koc-menu__arrow koc-menu__arrow--top">
            <ul>
                <li>
                    <label for="degree--up-0">
                        <div class="arrow"></div>
                    </label>
                    <label for="degree--up-1">
                        <div class="arrow"></div>
                    </label>
                    <label for="degree--up-2">
                        <div class="arrow"></div>
                    </label>
                </li>
            </ul>
        </div>

        <!-- Arrow Container End -->

    </label>
</section>

<!-- Menu End -->
<div id="container" style="top: 0px; bottom: 0px; position: absolute; right: 0px; left: 0px;">
    <style type="text/css">
        img {
            vertical-align: middle;
        }

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .ex-item {
            height: 500px;
            width: 700px;
            display: none;
            overflow: hidden;
            border: 7px solid white;
            background-color: #5EBDC4;
        }

        .ex-item .round-button {
            font-size: 45px;
            height: 200px;
            width: 200px;
            background: rgba(71, 160, 71, 0.65);
            position: relative;
            top: -100px;
            border-radius: 100px;
            padding-left: 30px;
            padding-top: 27px;
            right: -600px;
            cursor: pointer;
        }

        .ex-item:hover .round-button {
            font-size: 64px;
            height: 300px;
            width: 300px;
            background: rgba(71, 160, 71, 0.85);
            position: relative;
            right: -550px;
            top: -150px;
            border-radius: 150px;
            padding-left: 45px;
            padding-top: 41px;
        }

        .button-hover-transition {
            -webkit-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
            -moz-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
            -ms-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
            -o-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
        }
    </style>
    <script id="popupLayout" type="text/mustache">
            <div class="close-btn" style="background: white;
          background: white;
          position: absolute;
          right: -40px;
          width: 80px;
          height: 80px;
          top: -40px;
          border-radius: 40px;
          padding-top: 45px;
          padding-left: 14px;
          font-size: 18px;">
                <i class="fa fa-close fa-lg" style="cursor: pointer;"></i>
            </div>
            <img style="border: 7px solid white;" width="725" height="562" src="" />

    </script>
    
    <div style="height: 200px; position: absolute; width: 100%;">
        <div style="bottom: 0px; position: absolute; text-align: center; width: 100%; font-weight:300; font-size: 60px; display: block; color: rgb(205, 238, 245);"
             class="title">title text
        </div>
    </div>
    <div class="ex-item">


        <div data-content="{{url('list')}}" class="category-link" style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/1.jpg') }})">
            
        </div>

        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/1.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/2.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/2.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/3.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/3.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/4.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/4.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/5.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/5.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/6.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/6.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/7.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/7.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/8.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/8.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/9.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/9.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/10.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/10.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/11.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/11.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/12.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/12.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/13.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/13.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/14.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/14.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/15.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/15.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/16.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/16.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/17.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/17.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/18.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/18.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/19.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/19.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/20.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/20.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/21.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/21.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/22.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/22.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/23.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/23.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/24.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/24.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/25.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/25.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/26.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/26.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/27.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/27.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/28.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/28.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/29.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/29.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/30.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/30.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/31.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/31.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/32.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/32.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/33.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/33.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/34.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/34.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/35.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/35.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/36.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/36.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/37.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/37.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/38.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/38.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div class="ex-item">


        <div style="height: 500px; width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url({{ asset('theme/Images/39.jpg') }})">

        </div>
        <div class="round-button button-hover-transition">
            <i data-content="{{ asset('theme/Images/39.jpg') }}" class="popup-link fa fa-folder-open fa-lg"></i>
        </div>
    </div>
    <div style="height: 200px;bottom: 0px; width: 100%; position: absolute; text-align: center; font-size: 30px; display: block;"
         class="description">Description comment
    </div>

</div>
<div id="popup" style="display: none; overflow: hidden;"></div>
</body>

</html>