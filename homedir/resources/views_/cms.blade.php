@extends('layouts.app')

@section('content')
	<section class="contact-block" id="contact-form">
		<div class="container">
			{!! $page->body !!}
		</div>
	</section>
@stop
