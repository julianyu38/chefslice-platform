@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header">
        <div class="header-icon">
            <i class="ti-home"></i>
        </div>
        <div class="header-title">
            <h1>
                Dashboard
            </h1>
            <small>Dashboard of  your account.</small>
            @include('dashboard.partials.quick-links')
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')

    <div class="row dashboard-links">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-bd">
                        <div class="panel-body">
                            <a href="{{ url(env('APP_URL_PROTOCOL').'://'.env('APP_URL')) }}">
                                <img src="{{ asset('img/home.png') }}" alt="Home Page">
                                <h2 class="text-center">Home Page</h2>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">

                    <div class="panel panel-bd">
                        <div class="panel-body">
                            <a href="{{ url('dashboard/profile') }}">
                                <img src="{{ asset('img/profile.png') }}" alt="Profile">
                                <h2 class="text-center">Profile</h2>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-bd">
                        <div class="panel-body">
                            <a  href="{{ url(env('APP_URL_PROTOCOL').'://'.auth()->user()->user_name.env('SESSION_DOMAIN')) }}">
                                <img src="{{ asset('img/website.png') }}" alt="My Website">
                                <h2 class="text-center">My Website</h2>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">

                    <div class="panel panel-bd">
                        <div class="panel-body">
                            <a href="{{ url('dashboard/theme') }}">
                                <img src="{{ asset('img/themes.png') }}" alt="Theme">
                                <h2 class="text-center">Theme</h2>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-bd">
                    <div class="panel-body">
                        <a href="{{ url('dashboard/category') }}">
                            <img src="{{ asset('img/categories.png') }}" alt="Categories">
                            <h2 class="text-center">Categories</h2>
                        </a>
                    </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel panel-bd">
                        <div class="panel-body">
                            <a href="{{ url('dashboard/page') }}">
                                <img src="{{ asset('img/web-pages.png') }}" alt="Web Pages">
                                <h2 class="text-center">Web Pages</h2>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-bd">
                        <div class="panel-body">
                            <a href="{{ url('dashboard/item') }}">
                                <img src="{{ asset('img/products.png') }}" alt="Products">
                                <h2 class="text-center">Products</h2>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="col-sm-12">
                    <div class="panel panel-bd">
                        <div class="panel-body">
                            <a href="{{ url(env('APP_URL_PROTOCOL').'://'.env('APP_URL').'/logout') }}">
                                <img src="{{ asset('img/logout.ico') }}" alt="Logout">
                                <h2 class="text-center">Logout</h2>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('styles')
    <style>

        .dashboard-links img{
            width: 100%;
        }
    </style>
@stop