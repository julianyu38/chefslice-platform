<header class="main-header">
    <a href="{{ url('dashboard') }}" class="logo"> <!-- Logo -->
        <span class="logo-lg">
              <h4>{{ env('APP_NAME',"Chef Slice") }}</h4>
              <h6>Your slogan here</h6>
       </span>
        <span class="logo-mini">
            {{--<img src="{{ asset('img/mini-logo.png') }}" alt="{{ env('APP_NAME',"Barbados Sotheby's") }}">--}}
            CS
        </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
            <span class="sr-only">Toggle navigation</span>
            <span class="pe-7s-keypad"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages -->
                {{--<li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="pe-7s-mail"></i>

                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">

                        </li>
                        <li>
                            <ul class="menu">
                                <li><!-- start message -->
                                    <p class="alert alert-warning text-center"> No Message Found.</p>
                                </li>

                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li>--}}
                <!-- Notifications -->

                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 0 new notifications
                        </li>
                        <li>
                            <ul class="menu">

                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>
                <!-- settings -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="pe-7s-settings"></i></a>
                    <ul class="dropdown-menu">
                       {{-- <li><a href="{{  action('Dashboard\UserController@edit',['id'=>auth()->user()->id]) }}"><i class="pe-7s-users"></i> Update Profile</a></li>--}}
                        <li><a href="#"><i class="pe-7s-settings"></i> Settings</a></li>
                        <li><a href="{{ url(env('APP_URL_PROTOCOL').'://'.env('APP_URL').'/logout') }}"><i class="pe-7s-key"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>