<aside class="main-sidebar">
    <!-- sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel text-center">
            <div class="image">
                @if(!empty((auth()->user()->image)) && file_exists(public_path('images/users/'.(auth()->user()->image))))
                    <img class="img-circle" src="{{ asset('images/users/'.auth()->user()->image) }}" alt="{{ auth()->user()->name }}">
                @else
                    <img class="img-circle" src="{{ asset('images/not-found.jpg') }}"  alt="{{ auth()->user()->name }}">
                @endif
            </div>
            <div class="info">
                <p>{{ auth()->user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                            </span>
            </div>
        </form> <!-- /.search form -->
        <!-- sidebar menu -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="{{ url('/dashboard') }}"><i class="ti-home"></i> <span>Dashboard</span>
                              {{--  <span class="pull-right-container">
                                    <span class="label label-primary pull-right">v.1</span>
                                </span>--}}
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-university"></i><span>Property</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ action('Dashboard\PropertyController@create') }}">Add Property</a></li>
                    <li>
                        <a href="#">Category
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ action('Dashboard\CategoryController@create') }}"> Add New</a></li>
                            <li><a href="{{ action('Dashboard\CategoryController@index') }}">View All</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Development
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ action('Dashboard\DevelopmentController@create') }}"> Add New</a></li>
                            <li><a href="{{ action('Dashboard\DevelopmentController@index') }}">View All</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Locations
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ action('Dashboard\LocationController@create') }}"> Add New</a></li>
                            <li><a href="{{ action('Dashboard\LocationController@index') }}">View All</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Lifestyle
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ action('Dashboard\LifeStyleController@create') }}"> Add New</a></li>
                            <li><a href="{{ action('Dashboard\LifeStyleController@index') }}">View All</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Area
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ action('Dashboard\AreaController@create') }}"> Add New</a></li>
                            <li><a href="{{ action('Dashboard\AreaController@index') }}">View All</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> Amenities
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ action('Dashboard\AmenitiesController@create') }}"> Add New</a></li>
                            <li><a href="{{ action('Dashboard\AmenitiesController@index') }}">View All</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ action('Dashboard\PropertyController@offers') }}">Best Offers</a></li>
                    <li><a href="{{ action('Dashboard\PropertyController@sale') }}">For Sale</a></li>
                    <li><a href="{{ action('Dashboard\PropertyController@holiday') }}">Holiday Rental</a></li>
                    <li><a href="{{ action('Dashboard\PropertyController@rent') }}">Long Term Rental</a></li>
                    <li><a href="{{ action('Dashboard\PropertyController@index') }}">All Properties</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-newspaper-o"></i><span>News</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ action('Dashboard\NewsController@create') }}">Post News</a></li>
                    <li><a href="{{ action('Dashboard\NewsCommentsController@index') }}">Comments</a></li>
                    <li><a href="{{ action('Dashboard\NewsController@index') }}">All News</a></li>
                    <li><a href="{{ action('Dashboard\TagsController@index') }}">News Tags</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Users</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ action('Dashboard\SubscriberController@index') }}">Subscribers</a></li>
                    <li>
                        <a href="#"> Admin
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ action('Dashboard\UserController@create') }}"> New Account</a></li>
                            <li><a href="{{ action('Dashboard\UserController@index') }}"> View Admins</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ action('Dashboard\MessagesController@index') }}">
                    <i class="fa fa-envelope-o"></i> <span>Contacts</span>
                    @if($Counts['messages']>0)
                        <small class="label pull-right bg-yellow">{{ $Counts['messages'] }}</small>
                    @endif
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-comments-o"></i> <span>Testimonials</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ action('Dashboard\TestimonialController@create') }}">Add New</a></li>
                    <li><a href="{{ action('Dashboard\TestimonialController@index') }}">All Records</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-tasks"></i> <span>Activities</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-database"></i> <span>SEO</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#">Interested in property</a></li>
                    <li><a href="{{ action('Dashboard\SubscriberController@index') }}">Subscribers</a></li>
                    <li>
                        <a href="#"> Admin
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#"> New Account</a></li>
                            <li><a href="#"> View Admins</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ action('Dashboard\EnquiryController@index') }}">
                        <i class="fa fa-question"></i> <span>Enquiries</span>
                        @if($Counts['enquiries']>0)
                         <small class="label pull-right bg-yellow">{{ $Counts['enquiries'] }}</small>
                         @endif
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-o"></i> <span>Pages</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ action('Dashboard\PagesController@create') }}">New Page</a></li>
                    <li><a href="{{ action('Dashboard\PagesController@index') }}">All Page</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i> <span>Analytics</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ action('Dashboard\AnalyticController@countries') }}">Sessions by Country</a></li>
                    <li><a href="{{ action('Dashboard\AnalyticController@browsers') }}">Browser</a></li>
                    <li><a href="{{ action('Dashboard\AnalyticController@os') }}">Operating System</a></li>
                    <li><a href="{{ action('Dashboard\AnalyticController@sources') }}">Traffic Sources</a></li>
                    <li><a href="{{ action('Dashboard\AnalyticController@mobile') }}">Mobile Traffic</a></li>
                    <li><a href="{{ action('Dashboard\AnalyticController@sites') }}">Referring Sites</a></li>
                    <li><a href="{{ action('Dashboard\AnalyticController@keywords') }}">Keywords</a></li>
                    <li><a href="{{ action('Dashboard\AnalyticController@landing_pages') }}">Top Landing Pages</a></li>
                    <li><a href="{{ action('Dashboard\AnalyticController@exit_pages') }}">Top Exit Pages</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cloud-upload"></i> <span>Campaigns</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#">Interested in property</a></li>
                    <li><a href="{{ action('Dashboard\SubscriberController@index') }}">Subscribers</a></li>
                    <li>
                        <a href="#"> Admin
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#"> New Account</a></li>
                            <li><a href="#"> View Admins</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-paint-brush"></i> <span>Theme</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#">Interested in property</a></li>
                    <li><a href="{{ action('Dashboard\SubscriberController@index') }}">Subscribers</a></li>
                    <li>
                        <a href="#"> Admin
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#"> New Account</a></li>
                            <li><a href="#"> View Admins</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span>Settings</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#">Interested in property</a></li>
                    <li><a href="{{ action('Dashboard\SubscriberController@index') }}">Subscribers</a></li>
                    <li>
                        <a href="#"> Admin
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#"> New Account</a></li>
                            <li><a href="#"> View Admins</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

{{--

            <li class="treeview">
                <a href="#">
                    <i class="ti-paint-bucket"></i><span>UI Elements</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="buttons.html">Buttons</a></li>
                    <li><a href="tabs.html">Tabs</a></li>
                    <li><a href="notification.html">Notification</a></li>
                    <li><a href="tree-view.html">Tree View</a></li>
                    <li><a href="progressbars.html">Progressber</a></li>
                    <li><a href="list.html">List View</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="panels.html">Panels</a></li>
                    <li><a href="modals.html">Modals</a></li>
                    <li><a href="icheck_toggle_pagination.html">iCheck, Toggle, Pagination</a></li>
                    <li><a href="labels-badges-alerts.html">Labels, Badges, Alerts</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ti-pencil-alt"></i> <span>Forms</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="forms_basic.html">Basic Forms</a></li>
                    <li><a href="forms_validation.html">Validation Forms</a></li>
                    <li><a href="forms_cropper.html">Cropper</a></li>
                    <li><a href="form_file_upload.html">Forms File Upload</a></li>
                    <li><a href="forms_editor_ck.html">CK Editor</a></li>
                    <li><a href="forms_editor_summernote.html">Summernote</a></li>
                    <li><a href="form_wizard.html">Form Wizaed</a></li>
                    <li><a href="forms_editor_markdown.html">Markdown</a></li>
                    <li><a href="forms_editor_trumbowyg.html">Trumbowyg</a></li>
                    <li><a href="form_editor_wysihtml5.html">Wysihtml5</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ti-layout"></i> <span>Tables</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="table.html">Simple tables</a></li>
                    <li><a href="dataTables.html">Data tables</a></li>
                    <li><a href="footable.html">FooTable</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ti-location-pin"></i> <span>Maps</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="maps_data.html">Data Maps</a></li>
                    <li><a href="maps_jvector.html">Jvector Maps</a></li>
                    <li><a href="maps_google.html">Google map</a></li>
                    <li><a href="maps_snazzy.html">Snazzy Map</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ti-bar-chart-alt"></i><span>Charts</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="charts_flot.html">Flot Chart</a></li>
                    <li><a href="charts_Js.html">Chart js</a></li>
                    <li><a href="charts_morris.html">Morris Charts</a></li>
                    <li><a href="charts_sparkline.html">Sparkline Charts</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ti-email"></i> <span>Mailbox</span>
                                <span class="pull-right-container">
                                    <small class="label pull-right bg-yellow">19</small>
                                    <small class="label pull-right bg-green">13</small>
                                    <small class="label pull-right bg-red">3</small>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="mailbox.html">Mailbox</a></li>
                    <li class="disabled">
                        <a href="#">Mailbox 2
                                        <span class="pull-right-container">
                                            <small class="label pull-right bg-green">coming</small>
                                        </span>
                        </a>
                    </li>
                    <li><a href="mailDetails.html">Mailbox Details</a></li>
                    <li><a href="compose.html">Compose</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ti-spray"></i>
                    <span>Icons</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="icons_bootstrap.html">Bootstrap Icons</a></li>
                    <li><a href="icons_fontawesome.html">Fontawesome Icon</a></li>
                    <li><a href="icons_flag.html">Flag Icons</a></li>
                    <li><a href="icons_material.html">Material Icons</a></li>
                    <li><a href="icons_weather.html">Weather Icons </a></li>
                    <li><a href="icons_line.html">Line Icons</a></li>
                    <li><a href="icons_pe.html">Pe Icons</a></li>
                    <li><a href="icon_socicon.html">Socicon Icons</a></li>
                </ul>
            </li>
            <li>
                <a href="widgets.html">
                    <i class="ti-microsoft"></i> <span>Widgets</span>
                                <span class="pull-right-container">
                                    <small class="label pull-right bg-green">new</small>
                                </span>
                </a>
            </li>
            <li>
                <a href="calender.html">
                    <i class="ti-calendar"></i><span>Calendar</span>
                                <span class="pull-right-container">
                                    <small class="label pull-right bg-red">5</small>
                                    <small class="label pull-right bg-yellow">21</small>
                                </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ti-ruler-pencil"></i> <span>App Views</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="invoice.html">Invoice</a></li>
                    <li><a href="timeline.html">Vertical timeline</a></li>
                    <li><a href="horizontal_timeline.html">Horzontal timeline</a></li>
                    <li><a href="pricing.html">Pricing Table</a></li>
                    <li><a href="slider.html">Slider</a></li>
                    <li><a href="carousel.html">Carousel</a></li>
                    <li><a href="code_editor.html">Code editor</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ti-layers"></i> <span>Other page</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="login.html">Login</a></li>
                    <li><a href="register.html">Register</a></li>
                    <li><a href="profile.html">Profile</a></li>
                    <li><a href="forget_password.html">Forget password</a></li>
                    <li><a href="lockscreen.html">Lockscreen</a></li>
                    <li><a href="404.html">404 Error</a></li>
                    <li><a href="505.html">505 Error</a></li>
                    <li><a href="blank.html">Blank Page</a></li>
                </ul>
            </li>
            <li>
                <a href="gridSystem.html">
                    <i class="ti-layout-grid2"></i> <span>Grid Style</span>
                                <span class="pull-right-container">
                                    <small class="label pull-right bg-green">new</small>
                                </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ti-layout"></i> <span>Layout</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="layout_fixed.html">Layout Fixed</a></li>
                    <li><a href="layout_boxed.html">Layout Boxed</a></li>
                    <li><a href="layout_collapsed_sidebar.html">Layout Collapsed</a></li>
                </ul>
            </li>--}}
            {{--<li class="treeview">
                <a href="#">
                    <i class="ti-direction-alt"></i> <span>Multilevel</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#">Level One</a></li>
                    <li>
                        <a href="#">Level One
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#"> Level Two</a></li>
                            <li>
                                <a href="#">Level Two
                                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href="#">Level Three</a></li>
                                    <li><a href="#">Level Three</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">Level One</a></li>
                </ul>
            </li>--}}
        </ul>
    </div> <!-- /.sidebar -->
</aside>