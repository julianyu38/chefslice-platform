@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-wrench"></i>
        </div>
        <div class="header-title">
            <h1>Settings</h1>
            <small>Site settings.</small>
            @include('dashboard.partials.quick-links')
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">Social Information</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Update Social Information</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">

                    {!! Form::open(['action'=>['Dashboard\SettingsController@update','slug'=>'social'],'method'=>'patch','class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <h3 class="text-center">Social Information</h3>
                            <hr>
                            @foreach($Settings('social') as $key=>$setting)
                                <div class="form-group {{ $errors->has($key) ? ' has-error' : '' }}">
                                    {!! Form::label($key, $setting->title, ['class' => 'col-sm-2 control-label'])  !!}
                                    <div class="col-sm-9">
                                        {!! Form::text($key,$value= $setting->value, $attributes = ['class'=>'form-control','placeholder'=>$setting->title])  !!}
                                        @if ($errors->has($key))
                                            <span class="help-block">
                                            <strong>{{ $errors->first($key) }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                                <span class="pull-right">
                                     <button type="reset" class="btn btn-default">Cancel</button>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Update</button>
                                </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
