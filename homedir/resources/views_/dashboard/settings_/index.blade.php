@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-wrench"></i>
        </div>
        <div class="header-title">
            <h1>Settings</h1>
            <small>Site settings.</small>
            @include('dashboard.partials.quick-links')
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">General Information</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 m-b-20">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab">my First Tab</a></li>
                <li><a href="#tab2" data-toggle="tab">My Second Tab</a></li>
            </ul>
            <!-- Tab panels -->
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tab1">
                    <div class="panel-body">
                        <p><strong>Lorem Ipsum is simply dummy text of the printing and. </strong></p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's
                            standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
                            scrambled it to make
                            a type specimen book. It has survived not only five centuries, but also the leap into
                            electronic typesetting,
                            remaining essentially unchanged. It was popularised in the 1960s with the release of
                            Letraset sheets containing
                            Lorem Ipsum passages, and more recently with desktop publishing software like Aldus .</p>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab2">
                    <div class="panel-body">
                        <p><strong>Lorem Ipsum is simply dummy text of the printing and. </strong></p>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a
                            page when looking at its layout.
                            The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,
                            as opposed to using 'Content
                            here, content here', making it look like readable English. Many desktop publishing packages
                            and web page editors now use
                            Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many
                            web sites still in their infancy.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
