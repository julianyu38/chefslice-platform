{!! Form::open(['route' => ['admin.settings.update.site'], 'method'=>'POST', 'files' => true, 'class' => 'form-horizontal']) !!}
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Site Info</legend>
        
        <div class="form-group">
            {!! Form::label("enable_https", "Enable HTTPS sitewide:", ['class' => 'col-sm-2 control-label']) !!}
            <input type="checkbox" {{ config('siteConfiguration.enable_https') ? 'checked':null}} data-toggle="toggle" value="1" data-style="ios" name="enable_https" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">
        </div>
        <div class="form-group">
            {!! Form::label("demo_mode", "Enable Demo Mode:", ['class' => 'col-sm-2 control-label']) !!}
            <input type="checkbox" {{ config('siteConfiguration.demo_mode') ? 'checked':null}} data-toggle="toggle" value="1" data-style="ios" name="demo_mode" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">
        </div>
        <div class="form-group">
            {!! Form::label("enable_email_activation", "Require email activation when user registers?", ['class' => 'col-sm-2 control-label']) !!}
            <input type="checkbox" {{ config('siteConfiguration.enable_email_activation') ? 'checked':null}} data-toggle="toggle" value="1" data-style="ios" name="enable_email_activation" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">
        </div>
        <div class="form-group">
            {!! Form::label("send_emails", "Send Email notifications to users?", ['class' => 'col-sm-2 control-label']) !!}
            <input type="checkbox" {{ config('siteConfiguration.send_emails') ? 'checked':null}} data-toggle="toggle" value="1" data-style="ios" name="send_emails" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">
            <span>Make sure you configure mail settings if you set this to YES</span>
        </div>
        <div class="form-group">
            
            {!! Form::label("uploadLocation", "Video Upload location:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <p>Choose where you want to save your uploaded videos. TutorPro supports local storage and Amazon s3.</p>
                <select name="uploadLocation" class="form-control">
                    <option value="local" {{ config('siteConfiguration.uploadLocation') == 'local' ? 'selected' : '' }}>Save on local server</option>
                    <option value="s3" {{ config('siteConfiguration.uploadLocation') == 's3' ? 'selected' : '' }}>Save on Amazon s3 Bucket</option>
                </select>
                <small class="text-danger">If you select Amazon s3, please enter your s3 credentials in the Storage tab:</small>
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::label("site_description", "Site description:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::textarea("site_description", config('siteConfiguration.site_description'), ['class'=>'form-control', 'rows' => '3']) !!}
                @if ($errors->has('site_description'))
                    <div class="text-danger"><small>{{ $errors->first('site_description') }}</small></div>
                @endif
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::label("google_analytics_code", "Google analytics code:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::text("google_analytics_code", config('siteConfiguration.google_analytics'), ['class'=>'form-control', 'placeholder' => 'UA-XXXXX-Y' ]) !!}
                 @if ($errors->has('google_analytics_code'))
                    <div class="text-danger"><small>{{ $errors->first('google_analytics_code') }}</small></div>
                @endif
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::label("author_percent", "Author percentage of sale:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::text("author_percent", config('siteConfiguration.author_percent'), ['class'=>'form-control', 'placeholder' => '100' ]) !!}
                 @if ($errors->has('author_percent'))
                    <div class="text-danger"><small>{{ $errors->first('author_percent') }}</small></div>
                @endif
            </div>
        </div>
    </fieldset>
    
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Social Network Accounts</legend>
        
        <div class="form-group">
            {!! Form::label("facebook_page_id", "Facebook page id:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-facebook text-primary"></i> https://www.facebook.com/</span>
                    {!! Form::text("facebook_page_id", config('siteConfiguration.facebook'), ['class'=>'form-control']) !!}
                    @if ($errors->has('facebook_page_id'))
                        <div class="text-danger"><small>{{ $errors->first('facebook_page_id') }}</small></div>
                    @endif
                </div>
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::label("twitter_handle", "Twitter username:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-twitter text-info"></i> https://www.twitter.com/</span>
                    {!! Form::text("twitter_handle", config('siteConfiguration.twitter'), ['class'=>'form-control']) !!}
                     @if ($errors->has('twitter_handle'))
                        <div class="text-danger"><small>{{ $errors->first('twitter_handle') }}</small></div>
                    @endif
                </div>
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::label("google_plus_id", "Googleplus id:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-google text-danger"></i> https://plus.google.com/</span>
                    {!! Form::text("google_plus_id", config('siteConfiguration.google_plus'), ['class'=>'form-control']) !!}
                     @if ($errors->has('google_plus_id'))
                        <div class="text-danger"><small>{{ $errors->first('google_plus_id') }}</small></div>
                    @endif
                </div>
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::label("youtube_channel", "Youtube Channel:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-youtube text-danger"></i> https://youtube.com/</span>
                    {!! Form::text("youtube_channel", config('siteConfiguration.youtube'), ['class'=>'form-control']) !!}
                     @if ($errors->has('youtube_channel'))
                        <div class="text-danger"><small>{{ $errors->first('youtube_channel') }}</small></div>
                    @endif
                </div>
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::label("github_repo", "Github Account:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-github text-default"></i> https://github.com/</span>
                    {!! Form::text("github_repo", config('siteConfiguration.github'), ['class'=>'form-control']) !!}
                     @if ($errors->has('github_repo'))
                        <div class="text-danger"><small>{{ $errors->first('github_repo') }}</small></div>
                    @endif
                </div>
            </div>
        </div>
        
    </fieldset>   

    <div class="form-group">
        <div class="col-sm-10">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>
  
  {{ csrf_field() }}
  {{ method_field('PUT') }}
  
{!! Form::close() !!}