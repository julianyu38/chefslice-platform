@extends('dashboard.layouts.app')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 m-b-20">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab">Site Settings</a></li>
                <li><a href="#tab2" data-toggle="tab">Circular Menu</a></li>
            </ul>
            <!-- Tab panels -->
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tab1">
                    <div class="panel-body">
                        @include('dashboard.settings._design_settings_form')
                    </div>
                </div>
                <div class="tab-pane fade" id="tab2">
                    <div class="panel-body">
                         @include('dashboard.settings._menu_settings_form')
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>   
@endsection

@section('script')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
    <style type="text/css">
        fieldset.scheduler-border {
            border: 1px solid #d2d6de !important;
            padding: 0 1em 1em 1em !important;
            margin: 0 0 1.5em 0 !important;
            -webkit-box-shadow:  0px 0px 0px 0px #d2d6de;
                    box-shadow:  0px 0px 0px 0px #d2d6de;
        }
        
        legend.scheduler-border {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width:auto;
            padding:0 10px;
            border-bottom:none;
            color: #9a9c9e;
        }
        
        .nav-tabs-custom > .nav-tabs > li a {
            font-weight: 700;
            text-transform: uppercase;
            color: #3c8dbc;
        }

    </style>
    
    
    <style type="text/css">
        .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
        .toggle.ios .toggle-handle { border-radius: 20px; }
    </style>
    
@endsection
@section('scripts')
    <script>
        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        $(".input-logo").change(function () {
            $('.logo-preview').html('');
            readURL(this,function (e) {
                $('.logo-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
            });
        });
$(".input-background").change(function () {
    $('.background-preview').html('');
    readURL(this,function (e) {
        $('.background-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
    });
});

    </script>

@stop