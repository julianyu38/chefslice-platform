{!! Form::open(['route' => ['admin.settings.update.payment'], 'method'=>'POST', 'files' => true, 'class' => 'form-horizontal']) !!}

    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Stripe Credentials</legend>         
        <p>Obtain your keys from <a href="https://stripe.com" target="_blank">Stripe.</a> Make sure you change these to your Production keys when deploying live.</p>    
        <div class="form-group">
            {!! Form::label("stripe_publishable_key", "Stripe Publishable Key:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <input name="stripe_publishable_key" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('STRIPE_KEY') }}" id="stripe_publishable_key" class="form-control">
                @if ($errors->has('stripe_publishable_key'))
                    <div class="text-danger"><small>{{ $errors->first('stripe_publishable_key') }}</small></div>
                @endif
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::label("stripe_secret", "Stripe Secret Key:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <input name="stripe_secret" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('STRIPE_SECRET') }}" id="stripe_secret" class="form-control">
                @if ($errors->has('stripe_secret'))
                    <div class="text-danger"><small>{{ $errors->first('stripe_secret') }}</small></div>
                @endif
            </div>
        </div>
    </fieldset>   
    
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">PayPal Credentials</legend>         
        <p>Obtain your keys from <a href="https://developer.paypal.com" target="_blank">PayPal.</a> Make sure you change these to your Production keys when deploying live.</p>    
        <div class="form-group">
            {!! Form::label("paypal_client_id", "PayPal Client ID:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <input name="paypal_client_id" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('PAYPAL_CLIENT_ID') }}" id="paypal_client_id" class="form-control">
                @if ($errors->has('paypal_client_id'))
                    <div class="text-danger"><small>{{ $errors->first('paypal_client_id') }}</small></div>
                @endif
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::label("paypal_secret", "PayPal Secret Key:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <input name="paypal_secret" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('PAYPAL_SECRET') }}" id="paypal_secret" class="form-control">
                @if ($errors->has('paypal_secret'))
                    <div class="text-danger"><small>{{ $errors->first('paypal_secret') }}</small></div>
                @endif
            </div>
        </div>
    </fieldset>   

    
    <div class="form-group">
        <div class="col-sm-10">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>
  
  {{ csrf_field() }}
  {{ method_field('PUT') }}
  
{!! Form::close() !!}