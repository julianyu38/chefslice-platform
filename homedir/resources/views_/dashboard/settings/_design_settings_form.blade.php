{{--{!! Form::model($settings,['action'=>['Dashboard\SettingsController@update',$account],'method'=>'patch','class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}--}}
{!! Form::open(['action'=>['Dashboard\SettingsController@store','account'=>$account],'method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
<fieldset class="form-group">

    <legend class="scheduler-border">User Site Logo</legend>
    <div class="form-group {{ $errors->has('logo') ? ' has-error' : '' }}">
        <label for="logo" class="col-sm-3 control-label">Logo</label>
        <div class="col-sm-9">
            <input type="file" name="logo" class="input-logo" accept="image/*">
            @if ($errors->has('logo'))
                <span class="help-block">
                                            <strong>{{ $errors->first('logo') }}</strong>
                                        </span>
            @endif
            <div class="form-group">
                <div class="logo-preview">
                </div>
                @if(!empty($settings->logo))
                    <div class="row">
                        <div class="col-sm-4">
                            <img width="150px" class="img-logo"
                                 src="{{ asset('media/design/logo/'.$settings->id.'/'.$settings->logo) }}">
                            <button data-url="#" type="button"
                                    class="btn btn-block btn-xs btn-danger btn--delete--image"><i
                                        class="fa fa-trash-o"></i> Delete
                            </button>
                            {{-- action('Dashboard\CategoryController@deleteImage',['id'=>$category->id]) --}}
                        </div>
                    </div>
                @endif
            </div>
        </div>

    </div>
</fieldset>


<fieldset class="form-group">
    <legend class="scheduler-border">User Site Background</legend>

    <div class="form-group {{ $errors->has('background') ? ' has-error' : '' }}">
        <label for="background" class="col-sm-3 control-label">Background</label>
        <div class="col-sm-9">
            <input type="file" name="background" class="input-background" accept="image/*">
            @if ($errors->has('background'))
                <span class="help-block">
                                            <strong>{{ $errors->first('background') }}</strong>
                                        </span>
            @endif
            <div class="form-group">
                <div class="background-preview">
                </div>
                @if(!empty($settings->background))
                    <div class="row">
                        <div class="col-sm-4">
                            <img width="150px" class="img-background"
                                 src="{{ asset('media/design/background/'.$settings->id.'/'.$settings->background) }}">
                            <button data-url="#" type="button"
                                    class="btn btn-block btn-xs btn-danger btn--delete--image"><i
                                        class="fa fa-trash-o"></i> Delete
                            </button>
                            {{-- action('Dashboard\CategoryController@deleteImage',['id'=>$category->id]) --}}
                        </div>
                    </div>
                @endif
            </div>
        </div>

    </div>


    <div class="form-group">
        {!! Form::label("heatmap", "Enable Heatmap:", ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-9">
            <select name="heatmap" class="form-control">
                <option value="1" {{ $settings->heatmap == 1 ? 'selected' : '' }}>Yes</option>
                <option value="0" {{ $settings->heatmap == 0 ? 'selected' : '' }}>No</option>
            </select>
            @if ($errors->has('heatmap'))
                <div class="text-danger">
                    <small>{{ $errors->first('heatmap') }}</small>
                </div>
            @endif
        </div>
    </div>
    <div class="form-group {{ $errors->has('count') ? ' has-error' : '' }}">
        {!! Form::label('count', 'Heatmap count *', ['class' => 'col-sm-3 control-label'])  !!}
        <div class="col-sm-9">
            {!! Form::text('count',$value= $settings->count, $attributes = ['class'=>'form-control','placeholder'=>'Heatmap click count'])  !!}
            @if ($errors->has('count'))
                <span class="help-block">
                                        <strong>{{ $errors->first('count') }}</strong>
                                    </span>
            @endif
        </div>
    </div>
    <div class="form-group {{ $errors->has('specialoffer') ? ' has-error' : '' }}">
        {!! Form::label('specialoffer', 'Special Offer Text', ['class' => 'col-sm-3 control-label'])  !!}
        <div class="col-sm-9">
            {!! Form::text('specialoffer',$value= $settings->specialoffer, $attributes = ['class'=>'form-control','placeholder'=>'Enter text for Special Offer label'])  !!}
            @if ($errors->has('specialoffer'))
                <span class="help-block">
                                        <strong>{{ $errors->first('specialoffer') }}</strong>
                                    </span>
            @endif
        </div>
    </div>


</fieldset>


<fieldset class="form-group">
    <legend>User Menu Design</legend>
    <div>
        <label class="form-check-label">
            <input type="radio" name="category_design" id="category_design1"
                   value="bezier1" {!! $settings->category_design=='bezier1'?'checked':'' !!} >
            <img src="{{ asset('media/design/bezier1.PNG') }}">
        </label>
    </div>
    <div>
        <label class="form-check-label">
            <input type="radio" name="category_design" id="category_design2"
                   value="bezier2" {!! $settings->category_design=='bezier2'?'checked':'' !!} >
            <img src="{{ asset('media/design/bezier2.PNG') }}">
        </label>
    </div>
    <div>
        <label class="form-check-label">
            <input type="radio" name="category_design" id="category_design3"
                   value="cubical-parabola" {!! $settings->category_design=='cubical-parabola'?'checked':'' !!} >
            <img src="{{ asset('media/design/cubical-parabola.PNG') }}">
        </label>
    </div>
    <div>
        <label class="form-check-label">
            <input type="radio" name="category_design" id="category_design4"
                   value="ellipse" {!! $settings->category_design=='ellipse'?'checked':'' !!} >
            <img src="{{ asset('media/design/ellipse.PNG') }}">
        </label>
    </div>
    <div>
        <label class="form-check-label">
            <input type="radio" name="category_design" id="category_design5"
                   value="ellipse-vert" {!! $settings->category_design=='ellipse-vert'?'checked':'' !!} >
            <img src="{{ asset('media/design/ellipse-vert.PNG') }}">
        </label>
    </div>
    <div>
        <label class="form-check-label">
            <input type="radio" name="category_design" id="category_design6"
                   value="html-content" {!! $settings->category_design=='html-content'?'checked':'' !!} >
            <img src="{{ asset('media/design/html-content.PNG') }}">
        </label>
    </div>
    <div>
        <label class="form-check-label">
            <input type="radio" name="category_design" id="category_design7"
                   value="line1" {!! $settings->category_design=='line1'?'checked':'' !!} >
            <img src="{{ asset('media/design/line1.PNG') }}">
        </label>
    </div>
    <div>
        <label class="form-check-label">
            <input type="radio" name="category_design" id="category_design8"
                   value="line2" {!! $settings->category_design=='line2'?'checked':'' !!} >
            <img src="{{ asset('media/design/line2.PNG') }}">
        </label>
    </div>
    <div>
        <label class="form-check-label">
            <input type="radio" name="category_design" id="category_design9"
                   value="parabola1" {!! $settings->category_design=='parabola1'?'checked':'' !!} >
            <img src="{{ asset('media/design/parabola1.PNG') }}">
        </label>
    </div>
    <div>
        <label class="form-check-label">
            <input type="radio" name="category_design" id="category_design10"
                   value="slideshow" {!! $settings->category_design=='slideshow'?'checked':'' !!} >
            <img src="{{ asset('media/design/slideshow.PNG') }}">
        </label>
    </div>
    <div>
        <label class="form-check-label">
            <input type="radio" name="category_design" id="category_design11"
                   value="spiral" {!! $settings->category_design=='spiral'?'checked':'' !!} >
            <img src="{{ asset('media/design/spiral.PNG') }}">
        </label>
    </div>


</fieldset>

<div class="form-group">
    <div class="col-sm-10">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
    </div>
</div>

{{ csrf_field() }}
{{--  {{ method_field('PUT') }}--}}

{!! Form::close() !!}