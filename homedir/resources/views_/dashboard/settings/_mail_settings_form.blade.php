{!! Form::open(['route' => ['admin.settings.update.mail'], 'method'=>'POST', 'files' => true, 'class' => 'form-horizontal']) !!}
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">System Email Address</legend>
        <div class="form-group">
            {!! Form::label("mail_from_address", "Email FROM address:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <input name="mail_from_address" type="email" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('MAIL_FROM_ADDRESS') }}" id="mail_from_address" class="form-control">
                @if ($errors->has('mail_from_address'))
                    <div class="text-danger"><small>{{ $errors->first('mail_from_address') }}</small></div>
                @endif
            </div>
        </div>
        <div class="form-group">
            {!! Form::label("mail_from_name", "Email FROM name:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <input name="mail_from_name" type="text" value="{{ env('MAIL_FROM_NAME') }}" id="mail_from_name" class="form-control">
                @if ($errors->has('mail_from_name'))
                    <div class="text-danger"><small>{{ $errors->first('mail_from_name') }}</small></div>
                @endif
            </div>
        </div>
    </fieldset>                                
    <div class="form-group">
        {!! Form::label("mail_driver", "Mail Driver:", ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            <select name="mail_driver" class="form-control">
                <option value="smtp" {{ env('MAIL_DRIVER') == 'smtp' ? 'selected' : '' }}>SMTP</option>
                <option value="mailgun" {{ env('MAIL_DRIVER') == 'mailgun' ? 'selected' : '' }}>Mailgun</option>
            </select>
        </div>
    </div>
    
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">SMTP Settings</legend>
            
            <div class="form-group">
                {!! Form::label("smtp_host", "Host:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="smtp_host" type="text" value="{{config('siteConfiguration.demo_mode') ? '************' :  env('MAIL_HOST') }}" id="smtp_host" class="form-control">
                    @if ($errors->has('smtp_host'))
                        <div class="text-danger"><small>{{ $errors->first('smtp_host') }}</small></div>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("smtp_username", "Username:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="smtp_username" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('MAIL_USERNAME') }}" id="smtp_username" class="form-control">
                    @if ($errors->has('smtp_username'))
                        <div class="text-danger"><small>{{ $errors->first('smtp_username') }}</small></div>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("smtp_password", "Password:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="smtp_password" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('MAIL_PASSWORD') }}" id="smtp_password" class="form-control">
                    @if ($errors->has('smtp_password'))
                        <div class="text-danger"><small>{{ $errors->first('smtp_password') }}</small></div>
                    @endif
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label("smtp_port", "Port:", ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <input name="smtp_port" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('MAIL_PORT') }}" id="smtp_port" class="form-control">
                    @if ($errors->has('smtp_port'))
                        <div class="text-danger"><small>{{ $errors->first('smtp_port') }}</small></div>
                    @endif
                </div>
            </div>
            
    </fieldset>
    
    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Mailgun Settings</legend>
        <div class="form-group">
            {!! Form::label("mailgun_domain", "Domain:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <input name="mailgun_domain" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' :  env('MAILGUN_DOMAIN') }}" id="mailgun_domain" class="form-control">
                @if ($errors->has('mailgun_domain'))
                    <div class="text-danger"><small>{{ $errors->first('mailgun_domain') }}</small></div>
                @endif
            </div>
        </div>
        <div class="form-group">
            {!! Form::label("mailgun_secret", "Secret:", ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                <input name="mailgun_secret" type="text" value="{{ config('siteConfiguration.demo_mode') ? '************' : env('MAILGUN_SECRET') }}" id="mailgun_secret" class="form-control">
                @if ($errors->has('mailgun_secret'))
                    <div class="text-danger"><small>{{ $errors->first('mailgun_secret') }}</small></div>
                @endif
            </div>
        </div>
    </fieldset>
    
    <div class="form-group">
        <div class="col-sm-10">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>
  
  {{ csrf_field() }}
  {{ method_field('PUT') }}
  
{!! Form::close() !!}