@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-university"></i>
        </div>
        <div class="header-title">
            <h1>Products</h1>
            <small>Manage Products.</small>
            @include('dashboard.partials.quick-links')
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">Products</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Update Product </h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="View Products" data-placement="bottom" href="{{ url('dashboard/item') }}" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> View Products</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    {!! Form::model($item,['url'=>['dashboard/item/'.$item->id],'method'=>'patch','class'=>'form-horizontal','files'=>true,'enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                {!! Form::label('title', 'Name *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('title',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category Name'])  !!}
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                {!! Form::label('category_id', "Category", ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    <select class="form-control select2" data-placeholder="Select Item's Category" name="category_id" id="category_id">
                                        <option selected="selected" value="">Select Product Category</option>
                                        @foreach($categories as $id=>$category)
                                            <option {{ $item->category_id==$category->id?'selected="selected"':"" }} value="{{ $category->id }}">{{ $category->text }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category_id'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('category_id') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('short_description') ? ' has-error' : '' }}">
                                {!! Form::label('short_description', 'Short Description *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('short_description',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Short description','rows'=>3])  !!}
                                    @if ($errors->has('short_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('short_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                {!! Form::label('description', 'Description *', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('description',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category description','rows'=>5])  !!}
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('thumbnail') ? ' has-error' : '' }}">
                                <label for="image" class="col-sm-3 control-label">Thumbnail</label>
                                <div class="col-sm-9">
                                    <input type="file" name="thumbnail" class="input-image" accept="image/*">
                                    @if ($errors->has('thumbnail'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('thumbnail') }}</strong>
                                        </span>
                                    @endif
                                    <div class="form-group">
                                        <div class="thumbnail-preview">
                                        </div>
                                        @if(!empty($item->thumbnail))
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <img class="img-thumbnail" src="{{ asset('media/items/images/'.$item->thumbnail) }}" alt="{{ $item->title }}">
                                                    <button  data-url="#" type="button" class="btn btn-block btn-xs btn-danger btn--delete--image"><i class="fa fa-trash-o"></i> Delete</button>
                                                    {{-- action('Dashboard\CategoryController@deleteImage',['id'=>$category->id]) --}}
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="form-group {{ $errors->has('media_type') ? ' has-error' : '' }}">
                                {!! Form::label('media_type', 'Media Type', ['class' => 'col-sm-3 control-label'])  !!}
                                <div class="col-sm-9">
                                    <select class="form-control select2" data-placeholder="Select Media Type" name="media_type" id="media_type">
                                        <option selected="selected" value="">Select Media Type</option>

                                        <option {{ $item->media_type=1?'selected="selected"':"" }} value="1">Image</option>
                                        <option {{ $item->media_type==2?'selected="selected"':"" }} value="2">Video</option>

                                    </select>
                                    @if ($errors->has('media_type'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('media_type') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('media') ? ' has-error' : '' }}">
                                <label for="image" class="col-sm-3 control-label">Image</label>
                                <div class="col-sm-9">
                                    <input type="file" name="media[]" class="input-media" accept="image/*" multiple>
                                    @if ($errors->has('media'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('media') }}</strong>
                                                    </span>
                                    @endif
                                    <div class="form-group">
                                        <div class="media-preview">
                                        </div>
                                        @if(!empty($item->images) && $item->type!=2)
                                            <div class="row">


                                                    @foreach($item->images as $image)
                                                        <div class="col-sm-3">
                                                    <img class="img-thumbnail" src="{{ asset('media/items/images/'.$item->id.'/'.$image->image) }}" alt="{{ $item->title }}">
                                                    {{--<button  data-url="#" type="button" class="btn btn-block btn-xs btn-danger btn--delete--image"><i class="fa fa-trash-o"></i> Delete</button>--}}
                                                    {{-- action('Dashboard\CategoryController@deleteImage',['id'=>$category->id]) --}}
                                                        </div>
                                                        @endforeach


                                            </div>
                                        @endif
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <a href="{{ url('dashboard/item') }}" class="btn btn-default">Cancel</a>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Update</button>
                                </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script>
        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        $(".input-image").change(function () {
            $('.thumbnail-preview').html('');
            readURL(this,function (e) {
                $('.thumbnail-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
            });
        });
        $(".input-media").change(function () {
            $('.media-preview').html('');
            readURL(this,function (e) {
                $('.media-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="media_upload_preview" class="img-responsive img-thumbnail">')
            });
        });

    </script>

@stop