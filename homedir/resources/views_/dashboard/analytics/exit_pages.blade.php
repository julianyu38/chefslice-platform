@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-bar-chart-o"></i>
        </div>
        <div class="header-title">
            <h1>Analytics</h1>
            <small>Things related to site analytics.</small>
            @include('dashboard.partials.quick-links')
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
                <li class="active">Analytics</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Top Exit Pages</h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <select id="num_days">
                            <option  value="30">Last 30 days</option>
                            <option {!!   isset($_GET['days']) && $_GET['days'] == 1?'selected=""':'' !!} value="1">Today</option>
                            <option {!!   isset($_GET['days']) && $_GET['days'] == 7?'selected=""':'' !!} value="7">Last week</option>
                            <option {!!   isset($_GET['days']) && $_GET['days'] == 90?'selected=""':'' !!} value="90">Last 3 months</option>
                            <option {!!   isset($_GET['days']) && $_GET['days'] == 180?'selected=""':'' !!} value="180">Last 6 months</option>
                            <option {!!   isset($_GET['days']) && $_GET['days'] == 365?'selected=""':'' !!} value="365">Last 1 Year</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">

                    <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Page</th>
                                            <th>Exits</th>
                                            <th>Pages Views</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($exit_pages)>0)
                                            @foreach($exit_pages as $key=>$exit_page)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $exit_page[0] }}</td>
                                                    <td>{{ $exit_page[1] }}</td>
                                                    <td>{{ $exit_page[2] }}</td>
                                                </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td colspan="4">
                                                    <p class="alert text-center alert-warning">
                                                        No data found.
                                                    </p>
                                                </td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
    $(document).ready(function(){
            $('#num_days').change(function () {
                    window.location = '{{ url('dashboard/analytic/landing_pages') }}?days='+($(this).val());
            });
    });
    </script>
@stop