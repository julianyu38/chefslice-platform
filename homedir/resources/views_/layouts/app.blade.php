<!DOCTYPE html>
<html lang="en">

{{--<html xmlns="http://www.w3.org/1999/xhtml">--}}
@include('partials.restaurant.head')
<body  id="heatmaptag"  style="background-repeat: no-repeat; background-size: cover;
        background-attachment: fixed; background-image: url({{ asset('media/design/background/'.$settings->id.'/'.$settings->background) }})">
<!-- main wrapper -->
<div id="wrapper">
    <!-- header -->
    <header>
        @include('partials.restaurant.circular');
        <div class="logo"><a href="{{url('')}}"><img width="150px"
                                                     src="{{asset('media/design/logo/'.$settings->id.'/'.$settings->logo)}}"></a>
        </div>
    </header>
@yield('content')
<!-- /main wrapper -->
    <!-- footer -->
    @include('partials.restaurant.footer');
{{-- @include('partials.footer',['Info'=>$Info]) --}}


<!-- /footer -->
</div>
<div style="clear: both"></div>
@include('partials.restaurant.scripts')
</body>
</html>
