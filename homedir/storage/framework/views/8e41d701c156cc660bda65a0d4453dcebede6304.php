<?php $__env->startSection('header'); ?>
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-university"></i>
        </div>
        <div class="header-title">
            <h1>Products</h1>
            <small>Manage Products.</small>
            <?php echo $__env->make('dashboard.partials.quick-links', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/')); ?>"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="<?php echo e(url('/dashboard')); ?>"> Dashboard</a></li>
                <li class="active">Products</li>
            </ol>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Update Product </h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="View Products" data-placement="bottom" href="<?php echo e(url('dashboard/item')); ?>" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> View Products</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <?php echo Form::model($item,['url'=>['dashboard/item/'.$item->id],'method'=>'patch','class'=>'form-horizontal','files'=>true,'enctype'=>'multipart/form-data']); ?>

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <div class="form-group <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('title', 'Name *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::text('title',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category Name']); ?>

                                    <?php if($errors->has('title')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('category_id') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('category_id', "Category", ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <select class="form-control select2" data-placeholder="Select Item's Category" name="category_id" id="category_id">
                                        <option selected="selected" value="">Select Product Category</option>
                                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option <?php echo e($item->category_id==$category->id?'selected="selected"':""); ?> value="<?php echo e($category->id); ?>"><?php echo e($category->text); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php if($errors->has('category_id')): ?>
                                        <span class="help-block">
                                                <strong><?php echo e($errors->first('category_id')); ?></strong>
                                            </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('locale') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('locale', 'Select Language', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <select required class="form-control" name="locale" id="locale">
                                        <option value="en">English</option>
                                        <option value="he">Hebrew</option>

                                    </select>
                                    <?php if($errors->has('locale')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('locale')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('short_description') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('short_description', 'Short Description *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::textarea('short_description',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Short description','rows'=>3]); ?>

                                    <?php if($errors->has('short_description')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('short_description')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('description', 'Description *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::textarea('description',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category description','rows'=>5]); ?>

                                    <?php if($errors->has('description')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('description')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo e($errors->has('thumbnail') ? ' has-error' : ''); ?>">
                                <label for="image" class="col-sm-3 control-label">Thumbnail</label>
                                <div class="col-sm-9">
                                    <input type="file" name="thumbnail" class="input-image" accept="image/*">
                                    <?php if($errors->has('thumbnail')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('thumbnail')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <div class="thumbnail-preview">
                                        </div>
                                        <?php if(!empty($item->thumbnail)): ?>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <img class="img-thumbnail" src="<?php echo e(asset('media/items/images/'.$item->thumbnail)); ?>" alt="<?php echo e($item->title); ?>">
                                                    <button  data-url="#" type="button" class="btn btn-block btn-xs btn-danger btn--delete--image"><i class="fa fa-trash-o"></i> Delete</button>
                                                    
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group <?php echo e($errors->has('media_type') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('media_type', 'Media Type', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <select class="form-control select2" data-placeholder="Select Media Type" name="media_type" id="media_type">
                                        <option selected="selected" value="">Select Media Type</option>

                                        <option <?php echo e($item->media_type=1?'selected="selected"':""); ?> value="1">Image</option>
                                        <option <?php echo e($item->media_type==2?'selected="selected"':""); ?> value="2">Video</option>

                                    </select>
                                    <?php if($errors->has('media_type')): ?>
                                        <span class="help-block">
                                                <strong><?php echo e($errors->first('media_type')); ?></strong>
                                            </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo e($errors->has('media') ? ' has-error' : ''); ?>">
                                <label for="image" class="col-sm-3 control-label">Image</label>
                                <div class="col-sm-9">
                                    <input type="file" name="media[]" class="input-media" accept="image/*" multiple>
                                    <?php if($errors->has('media')): ?>
                                        <span class="help-block">
                                                        <strong><?php echo e($errors->first('media')); ?></strong>
                                                    </span>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <div class="media-preview">
                                        </div>
                                        <?php if(!empty($item->images) && $item->type!=2): ?>
                                            <div class="row">


                                                    <?php $__currentLoopData = $item->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <div class="col-sm-3">
                                                    <img class="img-thumbnail" src="<?php echo e(asset('media/items/images/'.$item->id.'/'.$image->image)); ?>" alt="<?php echo e($item->title); ?>">
                                                    
                                                    
                                                        </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <a href="<?php echo e(url('dashboard/item')); ?>" class="btn btn-default">Cancel</a>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Update</button>
                                </span>
                        </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script>
        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        $(".input-image").change(function () {
            $('.thumbnail-preview').html('');
            readURL(this,function (e) {
                $('.thumbnail-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
            });
        });
        $(".input-media").change(function () {
            $('.media-preview').html('');
            readURL(this,function (e) {
                $('.media-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="media_upload_preview" class="img-responsive img-thumbnail">')
            });
        });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>