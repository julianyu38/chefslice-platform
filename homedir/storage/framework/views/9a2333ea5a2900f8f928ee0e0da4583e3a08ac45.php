<?php $__env->startSection('back-button'); ?>
    <div class="btn-go-back pull-right">
        <a href="<?php echo e(url('/')); ?>"><i class="fa fa-long-arrow-left bounce shift" aria-hidden="true"></i>  <?php echo app('translator')->getFromJson('site.back'); ?></a>
        <div class="mask"></div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<section class="container main-section">
    <div class="row active-with-click">
        <?php
        $path = asset('media/items/images/');
        $mediapath = asset('media/items/images/');
        ?>
        <?php if($items->count()>0): ?>
            <?php
            $colors = ['Red','Blue','Lime','Purple','Deep-Purple','Pink','Cyan','Teal','Green','Light-Green','Yellow','Light-Blue','Amber','Indigo','Orange','Deep-Orange','Brown','Grey','Blue-Grey'];
            $i = -1;
            $count = count($colors);
            ?>
            <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php
                  $i++;
                if ($item->media_type == 2) {
                    $path = asset('media/items/videos/');
                }
                 // $i = $loop->index;
                 if ($i ==  $count ){
                    $i = 0;
                 }
                ?>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <article class="material-card <?php echo e($colors[$i]); ?>">
                        <h2>
                            <span><?php echo e($item->title); ?></span>
                            <strong>
                                <i class="fa fa-cutlery" aria-hidden="true"></i>
                                <?php echo e(isset($category)? $category->text :'Special'); ?>

                            </strong>
                        </h2>
                        <div class="mc-content">
                            <div class="img-container">
                                <a  href="#modal-product-quick-view" data-id="<?php echo e($item->id); ?>" class="item-quick-view" title="<?php echo e($item->title); ?>">

                                <img style="    min-height: 350px;" class="img-responsive" src="<?php echo e($path.'/'.$item->thumbnail); ?>" alt="<?php echo e($item->title); ?>">
                                </a>
                            </div>
                            <div class="mc-description">
                                <?php echo e($item->short_description); ?>

                                
                            </div>
                        </div>
                        <a class="mc-btn-action">
                            <i class="fa fa-bars"></i>
                        </a>
                        <div class="mc-footer">
                            <h4>
                                Social
                            </h4>
                            <a class="fa fa-fw fa-facebook"></a>
                            <a class="fa fa-fw fa-twitter"></a>
                            <a class="fa fa-fw fa-linkedin"></a>
                            <a class="fa fa-fw fa-google-plus"></a>
                        </div>
                    </article>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </div>
</section>



        <!--DEMO02-->
<div id="modal-product-quick-view">
    <!--"THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID-->
    <div  id="btn-close-modal" class="close-modal-product-quick-view modal-close">
        <i class="fa fa-times" aria-hidden="true"></i>
    </div>

    <div class="modal-content">
        <div class="row" style="    height: 100%;">
            <div class="col-sm-6 col-xs-12 item-detail-modal-images">
                <div style="height: 100%;background: #000;">
                    <div style="top: 50%;transform:translateY(-50%);" id="owl-demo" class="owl-carousel owl-theme">
                      

                                                
                    </div>
                </div>


            </div>
            <div class="col-sm-6 col-xs-12 item-detail-modal-info">
                <div>
                    <h1 id="owl_product" style="color: #666;font-family: 'Courgette', cursive;"></h1>
                   <div id="owl_product_desc" style="    height: 100%;
    overflow-y: scroll;">

                    </div>
                </div>

            </div>
        </div>
        <!--Your modal content goes here-->
    </div>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
        <!-- Waypoints -->
        <script src="<?php echo e(asset('theme/hydrogen/js/jquery.waypoints.min.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/plugins/owl-carousel2/owl.carousel.min.js')); ?>"></script>
        <!-- Salvattore -->
        <script src="<?php echo e(asset('theme/hydrogen/js/salvattore.min.js')); ?>"></script>
        <!-- Main JS -->
        <script src="<?php echo e(asset('theme/hydrogen/js/main.js')); ?>"></script>
        <!-- Modernizr JS -->
        <script src="<?php echo e(asset('theme/hydrogen/js/modernizr-2.6.2.min.js')); ?>"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="<?php echo e(asset('theme/hydrogen/js/respond.min.js')); ?>"></script>
        <![endif]-->

    <script src="<?php echo e(asset('assets/plugins/animatedModal/animatedModal.min.js')); ?>"></script>

    <script>
        var obj = <?php  echo json_encode($items) ?>;
        var assetUrl = '<?php echo $mediapath ?>';
        values = obj;


    </script>
    <script>
        $(document).ready(function () {
            var __quick_view_product_id = null;
            $(".item-quick-view").click(function(){
                var self = $(this);

                __quick_view_product_id = self.attr('data-id');
            });
            //demo 02
            $(".item-quick-view").animatedModal({
                modalTarget: 'modal-product-quick-view',
                animatedIn: 'lightSpeedIn',
                animatedOut: 'bounceOutDown',
                color: '#eee',
                // Callbacks
                beforeOpen: function () {

                    var product = $.grep(values, function (e) {
                        return e.id == __quick_view_product_id;
                    })[0];


                    $('#owl-demo').trigger('destroy.owl.carousel');
                    $('#owl-demo').empty();
                    for (var i in product.images) {
                        imgurl = assetUrl + '/' + __quick_view_product_id + '/' + product.images[i].image;

                        $('#owl-demo').append('<div class="item "><img src="' + imgurl + '" alt="' + product.title + '"> </div>');


                    }
                     $("#owl-demo").owlCarousel({
                         autoPlay : 3000,
                         navigation : true, // Show next and prev buttons
                         slideSpeed : 300,
                         paginationSpeed : 400,
                         stopOnHover:true,
                         loop  : ($('#owl-demo').children().length)==1 ? false:true,
                         margin : 30,
                         items : 1,
                         rewindNav : true,
                         nav: true,
                         video:true,
                         lazyLoad:true,
                         center:true
                       
                    }).trigger('refresh.owl.carousel');
                    $('#owl_product').html(product.title);
                    $('#owl_product_desc').html(product.description);
                    $( ".owl-prev").html('<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>');
                    $( ".owl-next").html('<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>');
                },
                afterOpen: function () {
//                    console.log("The animation is completed");
                },
                beforeClose: function () {
//                    console.log("The animation was called");
                },
                afterClose: function () {

//                    console.log("The animation is completed");
                }
            });
        });
    </script>


    <script>
        $(document).ready(function() {

            $("#owl-demo").owlCarousel({
               autoPlay : 3000,
                navigation : true, // Show next and prev buttons
                slideSpeed : 300,
                paginationSpeed : 400,
                stopOnHover:true,
                loop  : true,
                margin : 30,
                items : 1,
                rewindNav : true,
                nav: true,
                video:true,
                lazyLoad:true
            });

            $( ".owl-prev").html('<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>');
            $( ".owl-next").html('<i class="fa fa-chevron-circle-right" aria-hidden="true"></i>');
        });
    </script>

<script>
    $(document).ready(function(){
        $(".boton").wrapInner('<div class=botontext></div>');

        $(".botontext").clone().appendTo( $(".boton") );

        $(".boton").append('<span class="twist"></span><span class="twist"></span><span class="twist"></span><span class="twist"></span>');

        $(".twist").css("width", "25%").css("width", "+=3px");
    });
    $(function() {
        $('.material-card > .mc-btn-action').click(function () {
            var card = $(this).parent('.material-card');
            var icon = $(this).children('i');
            icon.addClass('fa-spin-fast');

            if (card.hasClass('mc-active')) {
                card.removeClass('mc-active');

                window.setTimeout(function() {
                    icon
                            .removeClass('fa-arrow-left')
                            .removeClass('fa-spin-fast')
                            .addClass('fa-bars');

                }, 800);
            } else {
                card.addClass('mc-active');

                window.setTimeout(function() {
                    icon
                            .removeClass('fa-bars')
                            .removeClass('fa-spin-fast')
                            .addClass('fa-arrow-left');

                }, 800);
            }
        });
    });
</script>

    <?php $__env->stopSection(); ?>


    <?php $__env->startSection('styles'); ?>
        <link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
            <!-- Animate.css -->
    <link rel="stylesheet" href="<?php echo e(asset('theme/hydrogen/css/animate.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/owl-carousel2/assets/owl.carousel.css')); ?>">

    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?php echo e(asset('theme/hydrogen/css/icomoon.css')); ?>">


    <style>
        #modal-product-quick-view{

            overflow-y: inherit !important;
            width: 90% !important;
            height: 80% !important;
            top: 10% !important;
            left: 5% !important;
        }

       #modal-product-quick-view .modal-close{
           position: absolute;
           top: 0px;
           right: 5px;
           z-index: 9;
           font-size: 2.5em;
           color: #888888;
       }
        #modal-product-quick-view .modal-close:hover {
            color: #000;
        }
        #modal-product-quick-view .modal-content{
            border: none;
            border-radius: 0px;
            box-shadow: none;
           height: 100%;
           overflow: hidden;
        }
        #owl-demo .item img{
            display: block;
            width: 100%;
            height: auto;
        }
        #owl-demo .owl-nav{
            position: absolute;
            top: 45%;
            width: 100%;
        }
        .owl-carousel {
            position: relative;
        }
        .owl-prev,
        .owl-next {
            position: absolute;
            top: 45%;
            margin-top: -10px;
            font-size: 2.5em;
            color: rgba(255,255,255,.5);
        }
        .owl-prev:hover,
        .owl-next:hover {

            color: #fff;
        }
        .owl-prev {
            left: 8px;
        }
        .owl-next {
            right: 8px;
        }

    </style>


        <style>
            @import  "https://fonts.googleapis.com/css?family=Raleway:400,300,200,500,600,700";
            .fa-spin-fast {
                -webkit-animation: fa-spin-fast 0.2s infinite linear;
                animation: fa-spin-fast 0.2s infinite linear;
            }
            @-webkit-keyframes fa-spin-fast {
                0% {
                    -webkit-transform: rotate(0deg);
                    transform: rotate(0deg);
                }
                100% {
                    -webkit-transform: rotate(359deg);
                    transform: rotate(359deg);
                }
            }
            @keyframes  fa-spin-fast {
                0% {
                    -webkit-transform: rotate(0deg);
                    transform: rotate(0deg);
                }
                100% {
                    -webkit-transform: rotate(359deg);
                    transform: rotate(359deg);
                }
            }
            .material-card {
                position: relative;
                height: 0;
                padding-bottom: calc(100% - 16px);
                margin-bottom: 6.6em;
            }
            .material-card h2 {
                position: absolute;
                top: calc(100% - 16px);
                left: 0;
                width: 100%;
                padding: 10px 16px;
                color: #fff;
                font-size: 1.4em;
                line-height: 1.6em;
                margin: 0;
                z-index: 10;
                -webkit-transition: all 0.3s;
                -moz-transition: all 0.3s;
                -ms-transition: all 0.3s;
                -o-transition: all 0.3s;
                transition: all 0.3s;
            }
            .material-card h2 span {
                display: block;
            }
            .material-card h2 strong {
                font-weight: 400;
                display: block;
                font-size: .8em;
            }
            .material-card h2:before,
            .material-card h2:after {
                content: ' ';
                position: absolute;
                left: 0;
                top: -16px;
                width: 0;
                border: 8px solid;
                -webkit-transition: all 0.3s;
                -moz-transition: all 0.3s;
                -ms-transition: all 0.3s;
                -o-transition: all 0.3s;
                transition: all 0.3s;
            }
            .material-card h2:after {
                top: auto;
                bottom: 0;
            }
            @media  screen and (max-width: 767px) {
                .material-card.mc-active {
                    padding-bottom: 0;
                    height: auto;
                }
            }
            .material-card.mc-active h2 {
                top: 0;
                padding: 10px 16px 10px 90px;
            }
            .material-card.mc-active h2:before {
                top: 0;
            }
            .material-card.mc-active h2:after {
                bottom: -16px;
            }
            .material-card .mc-content {
                position: absolute;
                right: 0;
                top: 0;
                bottom: 16px;
                left: 16px;
                -webkit-transition: all 0.3s;
                -moz-transition: all 0.3s;
                -ms-transition: all 0.3s;
                -o-transition: all 0.3s;
                transition: all 0.3s;
            }
            .material-card .mc-btn-action {
                position: absolute;
                right: 16px;
                top: 15px;
                -webkit-border-radius: 50%;
                -moz-border-radius: 50%;
                border-radius: 50%;
                border: 5px solid;
                width: 54px;
                height: 54px;
                line-height: 44px;
                text-align: center;
                color: #fff;
                cursor: pointer;
                z-index: 20;
                -webkit-transition: all 0.3s;
                -moz-transition: all 0.3s;
                -ms-transition: all 0.3s;
                -o-transition: all 0.3s;
                transition: all 0.3s;
            }
            .material-card.mc-active .mc-btn-action {
                top: 62px;
            }
            .material-card .mc-description {
                position: absolute;
                top: 100%;
                right: 30px;
                left: 30px;
                bottom: 54px;
                overflow: hidden;
                opacity: 0;
                filter: alpha(opacity=0);
                -webkit-transition: all 1.2s;
                -moz-transition: all 1.2s;
                -ms-transition: all 1.2s;
                -o-transition: all 1.2s;
                transition: all 1.2s;
            }
            .material-card .mc-footer {
                height: 0;
                overflow: hidden;
                -webkit-transition: all 0.3s;
                -moz-transition: all 0.3s;
                -ms-transition: all 0.3s;
                -o-transition: all 0.3s;
                transition: all 0.3s;
            }
            .material-card .mc-footer h4 {
                position: absolute;
                top: 200px;
                left: 30px;
                padding: 0;
                margin: 0;
                font-size: 16px;
                font-weight: 700;
                -webkit-transition: all 1.4s;
                -moz-transition: all 1.4s;
                -ms-transition: all 1.4s;
                -o-transition: all 1.4s;
                transition: all 1.4s;
            }
            .material-card .mc-footer a {
                display: block;
                float: left;
                position: relative;
                width: 52px;
                height: 52px;
                margin-left: 5px;
                margin-bottom: 15px;
                font-size: 28px;
                color: #fff;
                line-height: 52px;
                text-decoration: none;
                top: 200px;
            }
            .material-card .mc-footer a:nth-child(1) {
                -webkit-transition: all 0.5s;
                -moz-transition: all 0.5s;
                -ms-transition: all 0.5s;
                -o-transition: all 0.5s;
                transition: all 0.5s;
            }
            .material-card .mc-footer a:nth-child(2) {
                -webkit-transition: all 0.6s;
                -moz-transition: all 0.6s;
                -ms-transition: all 0.6s;
                -o-transition: all 0.6s;
                transition: all 0.6s;
            }
            .material-card .mc-footer a:nth-child(3) {
                -webkit-transition: all 0.7s;
                -moz-transition: all 0.7s;
                -ms-transition: all 0.7s;
                -o-transition: all 0.7s;
                transition: all 0.7s;
            }
            .material-card .mc-footer a:nth-child(4) {
                -webkit-transition: all 0.8s;
                -moz-transition: all 0.8s;
                -ms-transition: all 0.8s;
                -o-transition: all 0.8s;
                transition: all 0.8s;
            }
            .material-card .mc-footer a:nth-child(5) {
                -webkit-transition: all 0.9s;
                -moz-transition: all 0.9s;
                -ms-transition: all 0.9s;
                -o-transition: all 0.9s;
                transition: all 0.9s;
            }
            .material-card .img-container {
                overflow: hidden;
                position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                z-index: 3;
                -webkit-transition: all 0.3s;
                -moz-transition: all 0.3s;
                -ms-transition: all 0.3s;
                -o-transition: all 0.3s;
                transition: all 0.3s;
            }
            .material-card.mc-active .img-container {
                -webkit-border-radius: 50%;
                -moz-border-radius: 50%;
                border-radius: 50%;
                left: 0;
                top: 12px;
                width: 60px;
                height: 60px;
                z-index: 20;
            }
            .material-card.mc-active .mc-content {
                padding-top: 5.6em;
            }
            @media  screen and (max-width: 767px) {
                .material-card.mc-active .mc-content {
                    position: relative;
                    margin-right: 16px;
                }
            }
            .material-card.mc-active .mc-description {
                top: 50px;
                padding-top: 5.6em;
                opacity: 1;
                filter: alpha(opacity=100);
            }
            @media  screen and (max-width: 767px) {
                .material-card.mc-active .mc-description {
                    position: relative;
                    top: auto;
                    right: auto;
                    left: auto;
                    padding: 50px 30px 70px 30px;
                    bottom: 0;
                }
            }
            .material-card.mc-active .mc-footer {
                overflow: visible;
                position: absolute;
                top: calc(100% - 16px);
                left: 16px;
                right: 0;
                height: 82px;
                padding-top: 15px;
                padding-left: 25px;
            }
            .material-card.mc-active .mc-footer a {
                top: 0;
            }
            .material-card.mc-active .mc-footer h4 {
                top: -32px;
            }
            .material-card.Red h2 {
                background-color: #F44336;
            }
            .material-card.Red h2:after {
                border-top-color: #F44336;
                border-right-color: #F44336;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Red h2:before {
                border-top-color: transparent;
                border-right-color: #B71C1C;
                border-bottom-color: #B71C1C;
                border-left-color: transparent;
            }
            .material-card.Red.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #F44336;
                border-bottom-color: #F44336;
                border-left-color: transparent;
            }
            .material-card.Red.mc-active h2:after {
                border-top-color: #B71C1C;
                border-right-color: #B71C1C;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Red .mc-btn-action {
                background-color: #F44336;
            }
            .material-card.Red .mc-btn-action:hover {
                background-color: #B71C1C;
            }
            .material-card.Red .mc-footer h4 {
                color: #B71C1C;
            }
            .material-card.Red .mc-footer a {
                background-color: #B71C1C;
            }
            .material-card.Red.mc-active .mc-content {
                background-color: #FFEBEE;
            }
            .material-card.Red.mc-active .mc-footer {
                background-color: #FFCDD2;
            }
            .material-card.Red.mc-active .mc-btn-action {
                border-color: #FFEBEE;
            }
            .material-card.Blue-Grey h2 {
                background-color: #607D8B;
            }
            .material-card.Blue-Grey h2:after {
                border-top-color: #607D8B;
                border-right-color: #607D8B;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Blue-Grey h2:before {
                border-top-color: transparent;
                border-right-color: #263238;
                border-bottom-color: #263238;
                border-left-color: transparent;
            }
            .material-card.Blue-Grey.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #607D8B;
                border-bottom-color: #607D8B;
                border-left-color: transparent;
            }
            .material-card.Blue-Grey.mc-active h2:after {
                border-top-color: #263238;
                border-right-color: #263238;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Blue-Grey .mc-btn-action {
                background-color: #607D8B;
            }
            .material-card.Blue-Grey .mc-btn-action:hover {
                background-color: #263238;
            }
            .material-card.Blue-Grey .mc-footer h4 {
                color: #263238;
            }
            .material-card.Blue-Grey .mc-footer a {
                background-color: #263238;
            }
            .material-card.Blue-Grey.mc-active .mc-content {
                background-color: #ECEFF1;
            }
            .material-card.Blue-Grey.mc-active .mc-footer {
                background-color: #CFD8DC;
            }
            .material-card.Blue-Grey.mc-active .mc-btn-action {
                border-color: #ECEFF1;
            }
            .material-card.Pink h2 {
                background-color: #E91E63;
            }
            .material-card.Pink h2:after {
                border-top-color: #E91E63;
                border-right-color: #E91E63;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Pink h2:before {
                border-top-color: transparent;
                border-right-color: #880E4F;
                border-bottom-color: #880E4F;
                border-left-color: transparent;
            }
            .material-card.Pink.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #E91E63;
                border-bottom-color: #E91E63;
                border-left-color: transparent;
            }
            .material-card.Pink.mc-active h2:after {
                border-top-color: #880E4F;
                border-right-color: #880E4F;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Pink .mc-btn-action {
                background-color: #E91E63;
            }
            .material-card.Pink .mc-btn-action:hover {
                background-color: #880E4F;
            }
            .material-card.Pink .mc-footer h4 {
                color: #880E4F;
            }
            .material-card.Pink .mc-footer a {
                background-color: #880E4F;
            }
            .material-card.Pink.mc-active .mc-content {
                background-color: #FCE4EC;
            }
            .material-card.Pink.mc-active .mc-footer {
                background-color: #F8BBD0;
            }
            .material-card.Pink.mc-active .mc-btn-action {
                border-color: #FCE4EC;
            }
            .material-card.Purple h2 {
                background-color: #9C27B0;
            }
            .material-card.Purple h2:after {
                border-top-color: #9C27B0;
                border-right-color: #9C27B0;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Purple h2:before {
                border-top-color: transparent;
                border-right-color: #4A148C;
                border-bottom-color: #4A148C;
                border-left-color: transparent;
            }
            .material-card.Purple.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #9C27B0;
                border-bottom-color: #9C27B0;
                border-left-color: transparent;
            }
            .material-card.Purple.mc-active h2:after {
                border-top-color: #4A148C;
                border-right-color: #4A148C;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Purple .mc-btn-action {
                background-color: #9C27B0;
            }
            .material-card.Purple .mc-btn-action:hover {
                background-color: #4A148C;
            }
            .material-card.Purple .mc-footer h4 {
                color: #4A148C;
            }
            .material-card.Purple .mc-footer a {
                background-color: #4A148C;
            }
            .material-card.Purple.mc-active .mc-content {
                background-color: #F3E5F5;
            }
            .material-card.Purple.mc-active .mc-footer {
                background-color: #E1BEE7;
            }
            .material-card.Purple.mc-active .mc-btn-action {
                border-color: #F3E5F5;
            }
            .material-card.Deep-Purple h2 {
                background-color: #673AB7;
            }
            .material-card.Deep-Purple h2:after {
                border-top-color: #673AB7;
                border-right-color: #673AB7;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Deep-Purple h2:before {
                border-top-color: transparent;
                border-right-color: #311B92;
                border-bottom-color: #311B92;
                border-left-color: transparent;
            }
            .material-card.Deep-Purple.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #673AB7;
                border-bottom-color: #673AB7;
                border-left-color: transparent;
            }
            .material-card.Deep-Purple.mc-active h2:after {
                border-top-color: #311B92;
                border-right-color: #311B92;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Deep-Purple .mc-btn-action {
                background-color: #673AB7;
            }
            .material-card.Deep-Purple .mc-btn-action:hover {
                background-color: #311B92;
            }
            .material-card.Deep-Purple .mc-footer h4 {
                color: #311B92;
            }
            .material-card.Deep-Purple .mc-footer a {
                background-color: #311B92;
            }
            .material-card.Deep-Purple.mc-active .mc-content {
                background-color: #EDE7F6;
            }
            .material-card.Deep-Purple.mc-active .mc-footer {
                background-color: #D1C4E9;
            }
            .material-card.Deep-Purple.mc-active .mc-btn-action {
                border-color: #EDE7F6;
            }
            .material-card.Indigo h2 {
                background-color: #3F51B5;
            }
            .material-card.Indigo h2:after {
                border-top-color: #3F51B5;
                border-right-color: #3F51B5;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Indigo h2:before {
                border-top-color: transparent;
                border-right-color: #1A237E;
                border-bottom-color: #1A237E;
                border-left-color: transparent;
            }
            .material-card.Indigo.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #3F51B5;
                border-bottom-color: #3F51B5;
                border-left-color: transparent;
            }
            .material-card.Indigo.mc-active h2:after {
                border-top-color: #1A237E;
                border-right-color: #1A237E;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Indigo .mc-btn-action {
                background-color: #3F51B5;
            }
            .material-card.Indigo .mc-btn-action:hover {
                background-color: #1A237E;
            }
            .material-card.Indigo .mc-footer h4 {
                color: #1A237E;
            }
            .material-card.Indigo .mc-footer a {
                background-color: #1A237E;
            }
            .material-card.Indigo.mc-active .mc-content {
                background-color: #E8EAF6;
            }
            .material-card.Indigo.mc-active .mc-footer {
                background-color: #C5CAE9;
            }
            .material-card.Indigo.mc-active .mc-btn-action {
                border-color: #E8EAF6;
            }
            .material-card.Blue h2 {
                background-color: #2196F3;
            }
            .material-card.Blue h2:after {
                border-top-color: #2196F3;
                border-right-color: #2196F3;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Blue h2:before {
                border-top-color: transparent;
                border-right-color: #0D47A1;
                border-bottom-color: #0D47A1;
                border-left-color: transparent;
            }
            .material-card.Blue.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #2196F3;
                border-bottom-color: #2196F3;
                border-left-color: transparent;
            }
            .material-card.Blue.mc-active h2:after {
                border-top-color: #0D47A1;
                border-right-color: #0D47A1;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Blue .mc-btn-action {
                background-color: #2196F3;
            }
            .material-card.Blue .mc-btn-action:hover {
                background-color: #0D47A1;
            }
            .material-card.Blue .mc-footer h4 {
                color: #0D47A1;
            }
            .material-card.Blue .mc-footer a {
                background-color: #0D47A1;
            }
            .material-card.Blue.mc-active .mc-content {
                background-color: #E3F2FD;
            }
            .material-card.Blue.mc-active .mc-footer {
                background-color: #BBDEFB;
            }
            .material-card.Blue.mc-active .mc-btn-action {
                border-color: #E3F2FD;
            }
            .material-card.Light-Blue h2 {
                background-color: #03A9F4;
            }
            .material-card.Light-Blue h2:after {
                border-top-color: #03A9F4;
                border-right-color: #03A9F4;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Light-Blue h2:before {
                border-top-color: transparent;
                border-right-color: #01579B;
                border-bottom-color: #01579B;
                border-left-color: transparent;
            }
            .material-card.Light-Blue.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #03A9F4;
                border-bottom-color: #03A9F4;
                border-left-color: transparent;
            }
            .material-card.Light-Blue.mc-active h2:after {
                border-top-color: #01579B;
                border-right-color: #01579B;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Light-Blue .mc-btn-action {
                background-color: #03A9F4;
            }
            .material-card.Light-Blue .mc-btn-action:hover {
                background-color: #01579B;
            }
            .material-card.Light-Blue .mc-footer h4 {
                color: #01579B;
            }
            .material-card.Light-Blue .mc-footer a {
                background-color: #01579B;
            }
            .material-card.Light-Blue.mc-active .mc-content {
                background-color: #E1F5FE;
            }
            .material-card.Light-Blue.mc-active .mc-footer {
                background-color: #B3E5FC;
            }
            .material-card.Light-Blue.mc-active .mc-btn-action {
                border-color: #E1F5FE;
            }
            .material-card.Cyan h2 {
                background-color: #00BCD4;
            }
            .material-card.Cyan h2:after {
                border-top-color: #00BCD4;
                border-right-color: #00BCD4;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Cyan h2:before {
                border-top-color: transparent;
                border-right-color: #006064;
                border-bottom-color: #006064;
                border-left-color: transparent;
            }
            .material-card.Cyan.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #00BCD4;
                border-bottom-color: #00BCD4;
                border-left-color: transparent;
            }
            .material-card.Cyan.mc-active h2:after {
                border-top-color: #006064;
                border-right-color: #006064;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Cyan .mc-btn-action {
                background-color: #00BCD4;
            }
            .material-card.Cyan .mc-btn-action:hover {
                background-color: #006064;
            }
            .material-card.Cyan .mc-footer h4 {
                color: #006064;
            }
            .material-card.Cyan .mc-footer a {
                background-color: #006064;
            }
            .material-card.Cyan.mc-active .mc-content {
                background-color: #E0F7FA;
            }
            .material-card.Cyan.mc-active .mc-footer {
                background-color: #B2EBF2;
            }
            .material-card.Cyan.mc-active .mc-btn-action {
                border-color: #E0F7FA;
            }
            .material-card.Teal h2 {
                background-color: #009688;
            }
            .material-card.Teal h2:after {
                border-top-color: #009688;
                border-right-color: #009688;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Teal h2:before {
                border-top-color: transparent;
                border-right-color: #004D40;
                border-bottom-color: #004D40;
                border-left-color: transparent;
            }
            .material-card.Teal.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #009688;
                border-bottom-color: #009688;
                border-left-color: transparent;
            }
            .material-card.Teal.mc-active h2:after {
                border-top-color: #004D40;
                border-right-color: #004D40;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Teal .mc-btn-action {
                background-color: #009688;
            }
            .material-card.Teal .mc-btn-action:hover {
                background-color: #004D40;
            }
            .material-card.Teal .mc-footer h4 {
                color: #004D40;
            }
            .material-card.Teal .mc-footer a {
                background-color: #004D40;
            }
            .material-card.Teal.mc-active .mc-content {
                background-color: #E0F2F1;
            }
            .material-card.Teal.mc-active .mc-footer {
                background-color: #B2DFDB;
            }
            .material-card.Teal.mc-active .mc-btn-action {
                border-color: #E0F2F1;
            }
            .material-card.Green h2 {
                background-color: #4CAF50;
            }
            .material-card.Green h2:after {
                border-top-color: #4CAF50;
                border-right-color: #4CAF50;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Green h2:before {
                border-top-color: transparent;
                border-right-color: #1B5E20;
                border-bottom-color: #1B5E20;
                border-left-color: transparent;
            }
            .material-card.Green.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #4CAF50;
                border-bottom-color: #4CAF50;
                border-left-color: transparent;
            }
            .material-card.Green.mc-active h2:after {
                border-top-color: #1B5E20;
                border-right-color: #1B5E20;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Green .mc-btn-action {
                background-color: #4CAF50;
            }
            .material-card.Green .mc-btn-action:hover {
                background-color: #1B5E20;
            }
            .material-card.Green .mc-footer h4 {
                color: #1B5E20;
            }
            .material-card.Green .mc-footer a {
                background-color: #1B5E20;
            }
            .material-card.Green.mc-active .mc-content {
                background-color: #E8F5E9;
            }
            .material-card.Green.mc-active .mc-footer {
                background-color: #C8E6C9;
            }
            .material-card.Green.mc-active .mc-btn-action {
                border-color: #E8F5E9;
            }
            .material-card.Light-Green h2 {
                background-color: #8BC34A;
            }
            .material-card.Light-Green h2:after {
                border-top-color: #8BC34A;
                border-right-color: #8BC34A;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Light-Green h2:before {
                border-top-color: transparent;
                border-right-color: #33691E;
                border-bottom-color: #33691E;
                border-left-color: transparent;
            }
            .material-card.Light-Green.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #8BC34A;
                border-bottom-color: #8BC34A;
                border-left-color: transparent;
            }
            .material-card.Light-Green.mc-active h2:after {
                border-top-color: #33691E;
                border-right-color: #33691E;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Light-Green .mc-btn-action {
                background-color: #8BC34A;
            }
            .material-card.Light-Green .mc-btn-action:hover {
                background-color: #33691E;
            }
            .material-card.Light-Green .mc-footer h4 {
                color: #33691E;
            }
            .material-card.Light-Green .mc-footer a {
                background-color: #33691E;
            }
            .material-card.Light-Green.mc-active .mc-content {
                background-color: #F1F8E9;
            }
            .material-card.Light-Green.mc-active .mc-footer {
                background-color: #DCEDC8;
            }
            .material-card.Light-Green.mc-active .mc-btn-action {
                border-color: #F1F8E9;
            }
            .material-card.Lime h2 {
                background-color: #CDDC39;
            }
            .material-card.Lime h2:after {
                border-top-color: #CDDC39;
                border-right-color: #CDDC39;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Lime h2:before {
                border-top-color: transparent;
                border-right-color: #827717;
                border-bottom-color: #827717;
                border-left-color: transparent;
            }
            .material-card.Lime.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #CDDC39;
                border-bottom-color: #CDDC39;
                border-left-color: transparent;
            }
            .material-card.Lime.mc-active h2:after {
                border-top-color: #827717;
                border-right-color: #827717;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Lime .mc-btn-action {
                background-color: #CDDC39;
            }
            .material-card.Lime .mc-btn-action:hover {
                background-color: #827717;
            }
            .material-card.Lime .mc-footer h4 {
                color: #827717;
            }
            .material-card.Lime .mc-footer a {
                background-color: #827717;
            }
            .material-card.Lime.mc-active .mc-content {
                background-color: #F9FBE7;
            }
            .material-card.Lime.mc-active .mc-footer {
                background-color: #F0F4C3;
            }
            .material-card.Lime.mc-active .mc-btn-action {
                border-color: #F9FBE7;
            }
            .material-card.Yellow h2 {
                background-color: #FFEB3B;
            }
            .material-card.Yellow h2:after {
                border-top-color: #FFEB3B;
                border-right-color: #FFEB3B;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Yellow h2:before {
                border-top-color: transparent;
                border-right-color: #F57F17;
                border-bottom-color: #F57F17;
                border-left-color: transparent;
            }
            .material-card.Yellow.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #FFEB3B;
                border-bottom-color: #FFEB3B;
                border-left-color: transparent;
            }
            .material-card.Yellow.mc-active h2:after {
                border-top-color: #F57F17;
                border-right-color: #F57F17;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Yellow .mc-btn-action {
                background-color: #FFEB3B;
            }
            .material-card.Yellow .mc-btn-action:hover {
                background-color: #F57F17;
            }
            .material-card.Yellow .mc-footer h4 {
                color: #F57F17;
            }
            .material-card.Yellow .mc-footer a {
                background-color: #F57F17;
            }
            .material-card.Yellow.mc-active .mc-content {
                background-color: #FFFDE7;
            }
            .material-card.Yellow.mc-active .mc-footer {
                background-color: #FFF9C4;
            }
            .material-card.Yellow.mc-active .mc-btn-action {
                border-color: #FFFDE7;
            }
            .material-card.Amber h2 {
                background-color: #FFC107;
            }
            .material-card.Amber h2:after {
                border-top-color: #FFC107;
                border-right-color: #FFC107;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Amber h2:before {
                border-top-color: transparent;
                border-right-color: #FF6F00;
                border-bottom-color: #FF6F00;
                border-left-color: transparent;
            }
            .material-card.Amber.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #FFC107;
                border-bottom-color: #FFC107;
                border-left-color: transparent;
            }
            .material-card.Amber.mc-active h2:after {
                border-top-color: #FF6F00;
                border-right-color: #FF6F00;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Amber .mc-btn-action {
                background-color: #FFC107;
            }
            .material-card.Amber .mc-btn-action:hover {
                background-color: #FF6F00;
            }
            .material-card.Amber .mc-footer h4 {
                color: #FF6F00;
            }
            .material-card.Amber .mc-footer a {
                background-color: #FF6F00;
            }
            .material-card.Amber.mc-active .mc-content {
                background-color: #FFF8E1;
            }
            .material-card.Amber.mc-active .mc-footer {
                background-color: #FFECB3;
            }
            .material-card.Amber.mc-active .mc-btn-action {
                border-color: #FFF8E1;
            }
            .material-card.Orange h2 {
                background-color: #FF9800;
            }
            .material-card.Orange h2:after {
                border-top-color: #FF9800;
                border-right-color: #FF9800;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Orange h2:before {
                border-top-color: transparent;
                border-right-color: #E65100;
                border-bottom-color: #E65100;
                border-left-color: transparent;
            }
            .material-card.Orange.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #FF9800;
                border-bottom-color: #FF9800;
                border-left-color: transparent;
            }
            .material-card.Orange.mc-active h2:after {
                border-top-color: #E65100;
                border-right-color: #E65100;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Orange .mc-btn-action {
                background-color: #FF9800;
            }
            .material-card.Orange .mc-btn-action:hover {
                background-color: #E65100;
            }
            .material-card.Orange .mc-footer h4 {
                color: #E65100;
            }
            .material-card.Orange .mc-footer a {
                background-color: #E65100;
            }
            .material-card.Orange.mc-active .mc-content {
                background-color: #FFF3E0;
            }
            .material-card.Orange.mc-active .mc-footer {
                background-color: #FFE0B2;
            }
            .material-card.Orange.mc-active .mc-btn-action {
                border-color: #FFF3E0;
            }
            .material-card.Deep-Orange h2 {
                background-color: #FF5722;
            }
            .material-card.Deep-Orange h2:after {
                border-top-color: #FF5722;
                border-right-color: #FF5722;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Deep-Orange h2:before {
                border-top-color: transparent;
                border-right-color: #BF360C;
                border-bottom-color: #BF360C;
                border-left-color: transparent;
            }
            .material-card.Deep-Orange.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #FF5722;
                border-bottom-color: #FF5722;
                border-left-color: transparent;
            }
            .material-card.Deep-Orange.mc-active h2:after {
                border-top-color: #BF360C;
                border-right-color: #BF360C;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Deep-Orange .mc-btn-action {
                background-color: #FF5722;
            }
            .material-card.Deep-Orange .mc-btn-action:hover {
                background-color: #BF360C;
            }
            .material-card.Deep-Orange .mc-footer h4 {
                color: #BF360C;
            }
            .material-card.Deep-Orange .mc-footer a {
                background-color: #BF360C;
            }
            .material-card.Deep-Orange.mc-active .mc-content {
                background-color: #FBE9E7;
            }
            .material-card.Deep-Orange.mc-active .mc-footer {
                background-color: #FFCCBC;
            }
            .material-card.Deep-Orange.mc-active .mc-btn-action {
                border-color: #FBE9E7;
            }
            .material-card.Brown h2 {
                background-color: #795548;
            }
            .material-card.Brown h2:after {
                border-top-color: #795548;
                border-right-color: #795548;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Brown h2:before {
                border-top-color: transparent;
                border-right-color: #3E2723;
                border-bottom-color: #3E2723;
                border-left-color: transparent;
            }
            .material-card.Brown.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #795548;
                border-bottom-color: #795548;
                border-left-color: transparent;
            }
            .material-card.Brown.mc-active h2:after {
                border-top-color: #3E2723;
                border-right-color: #3E2723;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Brown .mc-btn-action {
                background-color: #795548;
            }
            .material-card.Brown .mc-btn-action:hover {
                background-color: #3E2723;
            }
            .material-card.Brown .mc-footer h4 {
                color: #3E2723;
            }
            .material-card.Brown .mc-footer a {
                background-color: #3E2723;
            }
            .material-card.Brown.mc-active .mc-content {
                background-color: #EFEBE9;
            }
            .material-card.Brown.mc-active .mc-footer {
                background-color: #D7CCC8;
            }
            .material-card.Brown.mc-active .mc-btn-action {
                border-color: #EFEBE9;
            }
            .material-card.Grey h2 {
                background-color: #9E9E9E;
            }
            .material-card.Grey h2:after {
                border-top-color: #9E9E9E;
                border-right-color: #9E9E9E;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Grey h2:before {
                border-top-color: transparent;
                border-right-color: #212121;
                border-bottom-color: #212121;
                border-left-color: transparent;
            }
            .material-card.Grey.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #9E9E9E;
                border-bottom-color: #9E9E9E;
                border-left-color: transparent;
            }
            .material-card.Grey.mc-active h2:after {
                border-top-color: #212121;
                border-right-color: #212121;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Grey .mc-btn-action {
                background-color: #9E9E9E;
            }
            .material-card.Grey .mc-btn-action:hover {
                background-color: #212121;
            }
            .material-card.Grey .mc-footer h4 {
                color: #212121;
            }
            .material-card.Grey .mc-footer a {
                background-color: #212121;
            }
            .material-card.Grey.mc-active .mc-content {
                background-color: #FAFAFA;
            }
            .material-card.Grey.mc-active .mc-footer {
                background-color: #F5F5F5;
            }
            .material-card.Grey.mc-active .mc-btn-action {
                border-color: #FAFAFA;
            }
            .material-card.Blue-Grey h2 {
                background-color: #607D8B;
            }
            .material-card.Blue-Grey h2:after {
                border-top-color: #607D8B;
                border-right-color: #607D8B;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Blue-Grey h2:before {
                border-top-color: transparent;
                border-right-color: #263238;
                border-bottom-color: #263238;
                border-left-color: transparent;
            }
            .material-card.Blue-Grey.mc-active h2:before {
                border-top-color: transparent;
                border-right-color: #607D8B;
                border-bottom-color: #607D8B;
                border-left-color: transparent;
            }
            .material-card.Blue-Grey.mc-active h2:after {
                border-top-color: #263238;
                border-right-color: #263238;
                border-bottom-color: transparent;
                border-left-color: transparent;
            }
            .material-card.Blue-Grey .mc-btn-action {
                background-color: #607D8B;
            }
            .material-card.Blue-Grey .mc-btn-action:hover {
                background-color: #263238;
            }
            .material-card.Blue-Grey .mc-footer h4 {
                color: #263238;
            }
            .material-card.Blue-Grey .mc-footer a {
                background-color: #263238;
            }
            .material-card.Blue-Grey.mc-active .mc-content {
                background-color: #ECEFF1;
            }
            .material-card.Blue-Grey.mc-active .mc-footer {
                background-color: #CFD8DC;
            }
            .material-card.Blue-Grey.mc-active .mc-btn-action {
                border-color: #ECEFF1;
            }



            @-moz-keyframes bounce {
                0%, 20%, 50%, 80%, 100% {
                    -moz-transform: translateX(0);
                    transform: translateX(0);
                }
                40% {
                    -moz-transform: translateX(-30px);
                    transform: translateX(-30px);
                }
                60% {
                    -moz-transform: translateX(-15px);
                    transform: translateX(-15px);
                }
            }
            @-webkit-keyframes bounce {
                0%, 20%, 50%, 80%, 100% {
                    -webkit-transform: translateX(0);
                    transform: translateX(0);
                }
                40% {
                    -webkit-transform: translateX(-30px);
                    transform: translateX(-30px);
                }
                60% {
                    -webkit-transform: translateX(-15px);
                    transform: translateX(-15px);
                }
            }
            @keyframes  bounce {
                0%, 20%, 50%, 80%, 100% {
                    -moz-transform: translateX(0);
                    -ms-transform: translateX(0);
                    -webkit-transform: translateX(0);
                    transform: translateX(0);
                }
                40% {
                    -moz-transform: translateX(-30px);
                    -ms-transform: translateX(-30px);
                    -webkit-transform: translateX(-30px);
                    transform: translateX(-30px);
                }
                60% {
                    -moz-transform: translateX(-15px);
                    -ms-transform: translateX(-15px);
                    -webkit-transform: translateX(-15px);
                    transform: translateX(-15px);
                }
            }

            .bounce {
                -moz-animation: bounce 2s infinite;
                -webkit-animation: bounce 2s infinite;
                animation: bounce 2s infinite;
            }


            .btn-go-back {
                z-index: 30;
                right: 30px;
                top: 20px;
                background: rgba(0,0,0,.7);
                border-radius: 29px;
                position: fixed;
                text-align: center;
                max-width: 150px;
                min-width: 135px;
                overflow: hidden;
                border: 2px solid rgba(255,255,255,.3);

            }

            .btn-go-back a {
                color: #969696;
                text-decoration: none;
                padding: 1rem 0px;
                -webkit-transition: border 1s cubic-bezier(0.19,1,.22,1),color .6s cubic-bezier(0.19,1,.22,1);
                transition: border 1s cubic-bezier(0.19,1,.22,1), color .6s cubic-bezier(0.19,1,.22,1), background 5s cubic-bezier(0.19,1,.22,1);
                cursor: pointer;
                text-transform: uppercase;
                letter-spacing: .3rem;
                display: block;
                background-size: cover;

            }

            .btn-go-back .mask {
                background: #fff;
                background: rgba(255,255,255,0.5);
            }

            .btn-go-back .mask {
                position: absolute;
                display: block;
                width: 200px;
                height: 100px;
                -webkit-transform: translate3d(-120%,-50px,0) rotate3d(0,0,1,45deg);
                transform: translate3d(-120%,-50px,0) rotate3d(0,0,1,45deg);
                -webkit-transition: all 1.1s cubic-bezier(0.19,1,.22,1);
                transition: all 1.1s cubic-bezier(0.19,1,.22,1);
            }

            .btn-go-back .shift {
                -webkit-transition: all 1.1s cubic-bezier(0.19,1,.22,1);
                transition: all 1.1s cubic-bezier(0.19,1,.22,1);
            }

            .btn-go-back:hover {
                border-color: #fff;
            }

            .btn-go-back:hover a {
                color: #fff;
            }

            .btn-go-back:hover .mask {
                background: #fff;
                -webkit-transform: translate3d(120%,-100px,0) rotate3d(0,0,1,90deg);
                transform: translate3d(120%,-100px,0) rotate3d(0,0,1,90deg);
            }

            .btn-go-back:hover .shift {
                padding-left: 5px;
            }


            .item-detail-modal-images{
                height: 100%;
            }
            .item-detail-modal-info{
                height: 96%;
            }
            .item-detail-modal-info > div{
                padding-right: 30px;padding-top: 40px; overflow:hidden; height: 100%;
            }
            .main-section{
                margin-top: 8rem;
            }
            @media(max-width: 768px){
                .item-detail-modal-images{
                    height: 50%;
                }
                .item-detail-modal-info{
                    height: 50%;
                }
                .item-detail-modal-info > div{
                    padding-top:0;
                    padding-right:0;
                    padding:20px;
                }
                .mc-content img{
                    height :100%;
                }
                .btn-go-back{
                    top: 38px;
                }
                .main-section{
                    margin-top: 13rem;
                }
                .menu{
                    left: -15px;
                }
                .__app-logo a{
                    top: 100px;
                }
            }
        </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>