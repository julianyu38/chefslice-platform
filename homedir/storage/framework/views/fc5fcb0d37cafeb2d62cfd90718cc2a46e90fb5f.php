
<?php echo Form::open(['action'=>['Dashboard\SettingsController@menu','account'=>$account],'method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data','id'=>'menuform']); ?>

<fieldset class="scheduler-border">
    <legend class="scheduler-border">Circular Menu</legend>
    
    <?php if($menus->count()>0): ?>
        <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="form-group">

        <div class="col-sm-3">
            <input name="title[<?php echo e($menu->id); ?>]" type="text" value="<?php echo e($menu->title); ?>" id="title<?php echo e($menu->id); ?>" class="form-control">
            <?php if($errors->has('title')): ?>
                <div class="text-danger"><small><?php echo e($errors->first('title')); ?></small></div>
            <?php endif; ?>
        </div>
        <div class="col-sm-3">
            <input name="slug[<?php echo e($menu->id); ?>]" type="text" value="<?php echo e($menu->slug); ?>" id="slug<?php echo e($menu->id); ?>" class="form-control">
            <?php if($errors->has('slug')): ?>
                <div class="text-danger"><small><?php echo e($errors->first('slug')); ?></small></div>
            <?php endif; ?>
        </div>
        <div class="col-sm-3">
            <input name="icon[<?php echo e($menu->id); ?>]" type="text" value="<?php echo e($menu->icon); ?>" id="icon<?php echo e($menu->id); ?>" class="form-control">
            <?php if($errors->has('icon')): ?>
                <div class="text-danger"><small><?php echo e($errors->first('icon')); ?></small></div>
            <?php endif; ?>
        </div>
        
            
        
    </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>

</fieldset>
<?php echo e(csrf_field()); ?>


<div class="form-group">
    <div class="col-sm-10">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
    </div>
</div>
<?php echo Form::close(); ?>