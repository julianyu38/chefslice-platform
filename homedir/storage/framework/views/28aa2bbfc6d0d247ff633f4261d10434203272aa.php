<?php $__env->startSection('header'); ?>

    <section class="content-header hidden-xs">

        <div class="header-icon">

            <i class="fa fa-users"></i>

        </div>

        <div class="header-title">

            <h1>Admins</h1>

            <small>Manage properties.</small>

            <?php echo $__env->make('dashboard.partials.quick-links', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



            <ol class="breadcrumb">

                <li><a href="<?php echo e(url('/')); ?>"><i class="pe-7s-home"></i> Home</a></li>

                <li><a href="<?php echo e(url('/dashboard')); ?>"> Dashboard</a></li>

                <li class="active">Admins</li>

            </ol>

        </div>

    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">

        <div class="col-sm-12 " >

            <div class="panel panel-bd ">

                <div class="panel-heading ">

                    <div  class="panel-title col-sm-10">

                        <h4>Admins </h4>

                    </div>

                    <div  class="col-sm-2 text-right">

                        <a data-toggle="tooltip" data-title="New Users" data-placement="bottom" href="<?php echo e(url('dashboard/user/create')); ?>" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add New</a>

                    </div>

                    <div class="clearfix"></div>

                </div>

                <div class="panel-body">

                    <div class="table-responsive">

                        <table class="table table-bordered table-hover">

                            <thead>

                            <tr>

                                <th>#</th>

                                <th>Image </th>

                                <th>Name </th>

                                <th>Email</th>

                                <th>Mobile</th>

                                <th>Join Date</th>

                                <th>Actions</th>

                            </tr>

                            </thead>

                            <tbody>



                            <?php if($users->count()>0): ?>

                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <tr>

                                    <td><?php echo e($user->id); ?></td>

                                    <td>

                                        <?php if(!empty(($user->image)) && file_exists(public_path('images/users/'.($user->image)))): ?>

                                            <img style="width:48px; height:48px;" class="img-circle" src="<?php echo e(asset('images/users/'.$user->image)); ?>" alt="<?php echo e($user->name); ?>">

                                        <?php else: ?>

                                            <img style="width:48px; height:48px;" class="img-circle" src="<?php echo e(asset('images/not-found.jpg')); ?>" alt="<?php echo e($user->name); ?>" >

                                        <?php endif; ?>

                                    </td>

                                    <td><?php echo e($user->name); ?></td>

                                    <td><?php echo e($user->email); ?></td>

                                    <td><?php echo e($user->mobile); ?></td>

                                    <td><?php echo e($user->created_at->format('d M, Y')); ?></td>

                                    <td>

                                        <a href="<?php echo e(url('dashboard/user/'.$user->user_name.'/edit')); ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                        <?php echo Form::open(['url'=>['/dashboard/user/'.$user->id],'method'=>'delete','style'=>'display:inline;']); ?>


                                            <button type="button" class="btn btn-danger btn-sm btn--delete--item" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></button>

                                        <?php echo Form::close(); ?>






                                    </td>

                                </tr>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                             <?php else: ?>

                                <tr>

                                    <td colspan="14">

                                        <p class="alert alert-warning text-center">

                                            No data found.

                                        </p>

                                    </td>

                                </tr>

                             <?php endif; ?>

                            </tbody>

                        </table>

                    </div>

                </div>

                <div class="box-footer clearfix text-center">

                    <?php echo e($users->links()); ?>


                </div>

            </div>

        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

    <script>

        $(document).on('click','.btn--delete--item', function () {

            var form = $(this).parents('form:first');



            $.confirm({

                icon: 'fa fa-trash-o',

                title: 'Delete Record!',

                closeIcon: true,

                animation: 'rotate',

                closeAnimation: 'rotate',

                content: 'Are you want to delete this record?',

                confirmButton: 'Yes',

                cancelButton: 'No',

                confirmButtonClass: 'btn-danger',

                cancelButtonClass: 'btn-info',

                confirm: function(){

                    form.submit();

                }

            });

        });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>