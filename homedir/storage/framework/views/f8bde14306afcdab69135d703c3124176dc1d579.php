<?php
$ABOUT = $SITE_SETTING('site_about');
$slider_images = $SITE_SETTING('site_slider_images');
$SERVICES = $SITE_SETTING('site_services');
$CLIENT_INFO = $SITE_SETTING('site_clients');
$SITE_HOW = $SITE_SETTING('site_how');
$SITE_PRICING = $SITE_SETTING('site_pricing');
$SITE_INFO = $SITE_SETTING('site_info');
$SITE_MENU = $SITE_SETTING('site_menu');
$SITE_MAP = $SITE_SETTING('site_map');

?>
<?php $__env->startSection('nav'); ?>
    <?php $__currentLoopData = $SITE_MENU; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if( strlen(trim($SITE_MENU['pricing']->value)) >0 ): ?>
            <li class="page-scroll">
                <a href="#<?php echo e($menu->type); ?>"><?php echo e($menu->value); ?></a>
            </li>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- About -->
    <?php if( strlen(trim($SITE_MENU['about']->value)) >0 ): ?>
        <section id="about">

            <div class="container-fluid no-padding">
                <div class="row no-margin equal-height-columns">
                    <div class="col-md-6 bg-color-main equal-height-column about-us">
                        <div class="valign__middle">
                            <div class="heading-v20 heading-v20--diff text-center">
                                <span class="heading-v20__pre-title g-dp-block g-mb-20">&nbsp;</span>
                                <h2 class="heading-v20__title g-mb-30"><?php echo e($ABOUT['title']->value); ?></h2>
                                <i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
                                <div class="heading-v20__text">
                                    <?php echo $ABOUT['description']->value; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 no-padding">
                        <div id="myCarousel" class="carousel slide carousel-v5">
                            <div class="carousel-inner">
                                <?php $__currentLoopData = $slider_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="item <?php echo $loop->first?'active':''; ?> equal-height-column">
                                        <div class="shadow"></div>
                                        <img class="equal-height-column" src="<?php echo e(asset('images/'.$image->value)); ?>"
                                             alt="">
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                
                            </div>
                            <div class="carousel-arrow">
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
    <!-- End About -->
    <br/>
    <br/>
    <?php if( strlen(trim($SITE_MENU['services']->value)) >0 && $sections->site_services !='0' ): ?>
        <!-- Services -->
        <section id="services">
            <div class="container g-pb-80">
                <div class="heading-v20 text-center">
                    <span class="heading-v20__pre-title g-dp-block g-mb-20"><?php echo e($SITE_MENU['services']->value); ?></span>
                    <h2 class="heading-v20__title g-mb-30"><?php echo e($SERVICES['title']->value); ?></h2>
                    <i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
                    <p class="heading-v20__text g-mb-20"><?php echo $SERVICES['description']->value; ?></p>
                </div>

                <div class="row">
                    <?php if($services->count() > 0): ?>
                        <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-sm-3 service text-center">
                                <span aria-hidden="true" class="<?php echo e($service->icon); ?> service__icon"></span>
                                <h3 class="service__title g-mb-30"><a href=""><?php echo e($service->title); ?></a></h3>
                                <p class="service__text">
                                    <?php echo e($service->description); ?>

                                </p>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                    
                </div>
            </div>
        </section>
        <!-- End Services -->
    <?php endif; ?>

    <?php if( strlen(trim($SITE_MENU['our_clients']->value)) >0 &&  $sections->site_clients != '0' ): ?>
        <!-- Good Taste -->
        <section id="our_clients">
            <div class="container-fluid bg-color-secondary">
                <div class="news g-pt-85 g-pb-100 text-center">
                    <div class="container">
                        <div class="heading-v20 text-center g-mb-50">
                            <span class="heading-v20__pre-title g-dp-block g-mb-20"><?php echo e($SITE_MENU['our_clients']->value); ?></span>
                            <h2 class="heading-v20__title g-mb-30"><?php echo e($CLIENT_INFO['title']->value); ?></h2>
                            <i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
                            <p class="heading-v20__text"><?php echo $CLIENT_INFO['description']->value; ?></p>
                        </div>
                        <!-- Owl Carousel v4-->
                        <div class="owl2-carousel-v4 owl-theme">
                            <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="item">
                                    <div class="post">
                                        <img class="post__img img-responsive"
                                             src="<?php echo e(asset('media/design/logo/'.$client->id.'/'.($client->settings?$client->settings->logo:''))); ?>"
                                             alt="">
                                        <div class="post__child text-left">
                                            
                                            <h3 class="post__child--title g-mb-20"><?php echo e($client->site_name); ?></h3>
                                            <p class="post__child--text g-mb-20"><?php echo e($client->intro); ?></p>
                                            <p>
                                                <a href="<?php echo e($APP_URL($client->user_name)); ?>"
                                                   class="btn btn-block btn-u global__btn-u--diff">View</a>
                                            </p>
                                            
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div class="g-mb-40"></div>
                        
                    </div>
                </div>
            </div>
        </section>
        <!-- End Good Taste -->
    <?php endif; ?>

    <?php if( strlen(trim($SITE_MENU['how_it_works']->value)) >0 && $sections->site_how !='0' ): ?>
        <!-- Booking Form -->
        <section id="how_it_works">
            <div class="container-fluid no-padding bg-color-main">
                <div class="booking content-lg">
                    <div class="container">
                        <div class="heading-v20 heading-v20--diff text-center g-mb-50">
                            <span class="heading-v20__pre-title g-dp-block g-mb-20"><?php echo e($SITE_MENU['how_it_works']->value); ?></span>
                            <h2 class="heading-v20__title g-mb-30"><?php echo e($SITE_HOW['title']->value); ?></h2>
                            <i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
                            <p class="heading-v20__text"><?php echo $SITE_HOW['description']->value; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Booking Form -->
    <?php endif; ?>

    <?php if( strlen(trim($SITE_MENU['pricing']->value)) >0 && $sections->site_pricing !='0' ): ?>
        <div id="generic_price_table">
            <section id="pricing">
                <div class="container">
                    <div class="news g-pt-85 g-pb-50 text-center">
                        <div class="heading-v20 text-center g-mb-50">
                            <span class="heading-v20__pre-title g-dp-block g-mb-20"><?php echo e($SITE_MENU['pricing']->value); ?></span>
                            <h2 class="heading-v20__title g-mb-30"><?php echo e($SITE_PRICING['title']->value); ?></h2>
                            <i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
                            <p class=""><?php echo $SITE_PRICING['description']->value; ?></p>
                        </div>
                    </div>
                    <!--BLOCK ROW START-->
                    <div class="row">
                        <div class="col-md-4">

                            <!--PRICE CONTENT START-->
                            <div class="generic_content clearfix">

                                <!--HEAD PRICE DETAIL START-->
                                <div class="generic_head_price clearfix">

                                    <!--HEAD CONTENT START-->
                                    <div class="generic_head_content clearfix">

                                        <!--HEAD START-->
                                        <div class="head_bg"></div>
                                        <div class="head">
                                            <span>Basic</span>
                                        </div>
                                        <!--//HEAD END-->

                                    </div>
                                    <!--//HEAD CONTENT END-->

                                    <!--PRICE START-->
                                    <div class="generic_price_tag clearfix">
                                <span class="price">
                                    <span class="sign">$</span>
                                    <span class="currency">99</span>
                                    <span class="cent">.99</span>
                                    <span class="month">/MON</span>
                                </span>
                                    </div>
                                    <!--//PRICE END-->

                                </div>
                                <!--//HEAD PRICE DETAIL END-->

                                <!--FEATURE LIST START-->
                                <div class="generic_feature_list">
                                    <ul>
                                        <li><span>2GB</span> Bandwidth</li>
                                        <li><span>150GB</span> Storage</li>
                                        <li><span>12</span> Accounts</li>
                                        <li><span>7</span> Host Domain</li>
                                        <li><span>24/7</span> Support</li>
                                    </ul>
                                </div>
                                <!--//FEATURE LIST END-->

                                <!--BUTTON START-->
                                <div class="generic_price_btn clearfix">
                                    
                                </div>
                                <!--//BUTTON END-->

                            </div>
                            <!--//PRICE CONTENT END-->

                        </div>

                        <div class="col-md-4">

                            <!--PRICE CONTENT START-->
                            <div class="generic_content active clearfix">

                                <!--HEAD PRICE DETAIL START-->
                                <div class="generic_head_price clearfix">

                                    <!--HEAD CONTENT START-->
                                    <div class="generic_head_content clearfix">

                                        <!--HEAD START-->
                                        <div class="head_bg"></div>
                                        <div class="head">
                                            <span>Standard</span>
                                        </div>
                                        <!--//HEAD END-->

                                    </div>
                                    <!--//HEAD CONTENT END-->

                                    <!--PRICE START-->
                                    <div class="generic_price_tag clearfix">
                                <span class="price">
                                    <span class="sign">$</span>
                                    <span class="currency">199</span>
                                    <span class="cent">.99</span>
                                    <span class="month">/MON</span>
                                </span>
                                    </div>
                                    <!--//PRICE END-->

                                </div>
                                <!--//HEAD PRICE DETAIL END-->

                                <!--FEATURE LIST START-->
                                <div class="generic_feature_list">
                                    <ul>
                                        <li><span>2GB</span> Bandwidth</li>
                                        <li><span>150GB</span> Storage</li>
                                        <li><span>12</span> Accounts</li>
                                        <li><span>7</span> Host Domain</li>
                                        <li><span>24/7</span> Support</li>
                                    </ul>
                                </div>
                                <!--//FEATURE LIST END-->

                                <!--BUTTON START-->
                                <div class="generic_price_btn clearfix">
                                    
                                </div>
                                <!--//BUTTON END-->

                            </div>
                            <!--//PRICE CONTENT END-->

                        </div>
                        <div class="col-md-4">

                            <!--PRICE CONTENT START-->
                            <div class="generic_content clearfix">

                                <!--HEAD PRICE DETAIL START-->
                                <div class="generic_head_price clearfix">

                                    <!--HEAD CONTENT START-->
                                    <div class="generic_head_content clearfix">

                                        <!--HEAD START-->
                                        <div class="head_bg"></div>
                                        <div class="head">
                                            <span>Unlimited</span>
                                        </div>
                                        <!--//HEAD END-->

                                    </div>
                                    <!--//HEAD CONTENT END-->

                                    <!--PRICE START-->
                                    <div class="generic_price_tag clearfix">
                                <span class="price">
                                    <span class="sign">$</span>
                                    <span class="currency">299</span>
                                    <span class="cent">.99</span>
                                    <span class="month">/MON</span>
                                </span>
                                    </div>
                                    <!--//PRICE END-->

                                </div>
                                <!--//HEAD PRICE DETAIL END-->

                                <!--FEATURE LIST START-->
                                <div class="generic_feature_list">
                                    <ul>
                                        <li><span>2GB</span> Bandwidth</li>
                                        <li><span>150GB</span> Storage</li>
                                        <li><span>12</span> Accounts</li>
                                        <li><span>7</span> Host Domain</li>
                                        <li><span>24/7</span> Support</li>
                                    </ul>
                                </div>

                                <div class="generic_price_btn clearfix">
                                    
                                </div>
                            </div>
                            <!--//PRICE CONTENT END-->

                        </div>
                    </div>
                    <!--//BLOCK ROW END-->

                </div>
                <br>
                <br>
            </section>
        </div>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
 <?php if(  $sections->site_map !='0' ): ?>
        
   
<?php $__env->startSection('footer'); ?>
    <div class="row">
        <div class="col-sm-12 no-padding">
            <div class="map-wrapper">
                <div class="contact__floating-block">
                    <div class="row no-column-space equal-height-columns">
                        <div class="col-sm-6 no-padding equal-height-column">
                            <img style="height: 400px;" class="img-responsive hidden-xs"
                                 src="<?php echo e(asset('images/'.$SITE_MAP['image']->value)); ?>" alt="Chef Slice">
                        </div>
                        <div class="col-sm-6 col-xs-12 contact__right--list text-center no-side-padding equal-height-column">
                            <div class="valign__middle">
                                <ul class="list-unstyled g-mb-20">
                                    <li class="first-item">Address</li>
                                    <li class="second-item"><?php echo e($SITE_INFO['address']->value); ?></li>
                                </ul>

                                <ul class="list-unstyled g-mb-20">
                                    <li class="first-item">Phone number</li>
                                    <li class="second-item"><?php echo e($SITE_INFO['phone']->value); ?></li>
                                </ul>

                                <ul class="list-unstyled g-mb-20">
                                    <li class="first-item">Email</li>
                                    <li class="second-item"><?php echo e($SITE_INFO['email']->value); ?></li>
                                </ul>

                                <ul class="list-unstyled">
                                    <li class="first-item">Mobile</li>
                                    <li class="second-item"><?php echo e($SITE_INFO['mobile']->value); ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="map" class="contact__map"></div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
 <?php endif; ?>

<?php $__env->startSection('styles'); ?>
    <style>

        @import  url(https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,700,900italic,900);
        @import  url(https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900);
        @import  url(https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900);

        #generic_price_table {
            background-color: #f0eded;
        }

        /*PRICE COLOR CODE START*/
        #generic_price_table .generic_content {
            background-color: #fff;
        }

        #generic_price_table .generic_content .generic_head_price {
            background-color: #f6f6f6;
        }

        #generic_price_table .generic_content .generic_head_price .generic_head_content .head_bg {
            border-color: #e4e4e4 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #e4e4e4;
        }

        #generic_price_table .generic_content .generic_head_price .generic_head_content .head span {
            color: #525252;
        }

        #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .sign {
            color: #414141;
        }

        #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .currency {
            color: #414141;
        }

        #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .cent {
            color: #414141;
        }

        #generic_price_table .generic_content .generic_head_price .generic_price_tag .month {
            color: #414141;
        }

        #generic_price_table .generic_content .generic_feature_list ul li {
            color: #a7a7a7;
        }

        #generic_price_table .generic_content .generic_feature_list ul li span {
            color: #414141;
        }

        #generic_price_table .generic_content .generic_feature_list ul li:hover {
            background-color: #E4E4E4;
            border-left: 5px solid #FF6600;
            /*border-left: 5px solid #2ECC71;*/
        }

        #generic_price_table .generic_content .generic_price_btn a {
            border: 1px solid #FF6600;
            /*border: 1px solid #2ECC71;
            color: #2ECC71;*/
            color: #FF6600;
        }

        #generic_price_table .generic_content.active .generic_head_price .generic_head_content .head_bg,
        #generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head_bg {
            /*border-color: #2ECC71 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #2ECC71;*/
            border-color: #FF6600 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #FF6600;
            color: #fff;
        }

        #generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head span,
        #generic_price_table .generic_content.active .generic_head_price .generic_head_content .head span {
            color: #fff;
        }

        #generic_price_table .generic_content:hover .generic_price_btn a,
        #generic_price_table .generic_content.active .generic_price_btn a {
            background-color: #FF6600;
            /*background-color: #2ECC71;*/
            color: #fff;
        }

        #generic_price_table {
            font-family: 'Raleway', sans-serif;
        }

        .row .table {
            padding: 28px 0;
        }

        /*PRICE BODY CODE START*/

        #generic_price_table .generic_content {
            overflow: hidden;
            position: relative;
            text-align: center;
        }

        #generic_price_table .generic_content .generic_head_price {
            margin: 0 0 20px 0;
        }

        #generic_price_table .generic_content .generic_head_price .generic_head_content {
            margin: 0 0 50px 0;
        }

        #generic_price_table .generic_content .generic_head_price .generic_head_content .head_bg {
            border-style: solid;
            border-width: 90px 1411px 23px 399px;
            position: absolute;
        }

        #generic_price_table .generic_content .generic_head_price .generic_head_content .head {
            padding-top: 40px;
            position: relative;
            z-index: 1;
        }

        #generic_price_table .generic_content .generic_head_price .generic_head_content .head span {
            font-family: "Raleway", sans-serif;
            font-size: 28px;
            font-weight: 400;
            letter-spacing: 2px;
            margin: 0;
            padding: 0;
            text-transform: uppercase;
        }

        #generic_price_table .generic_content .generic_head_price .generic_price_tag {
            padding: 0 0 20px;
        }

        #generic_price_table .generic_content .generic_head_price .generic_price_tag .price {
            display: block;
        }

        #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .sign {
            display: inline-block;
            font-family: "Lato", sans-serif;
            font-size: 28px;
            font-weight: 400;
            vertical-align: middle;
        }

        #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .currency {
            font-family: "Lato", sans-serif;
            font-size: 60px;
            font-weight: 300;
            letter-spacing: -2px;
            line-height: 60px;
            padding: 0;
            vertical-align: middle;
        }

        #generic_price_table .generic_content .generic_head_price .generic_price_tag .price .cent {
            display: inline-block;
            font-family: "Lato", sans-serif;
            font-size: 24px;
            font-weight: 400;
            vertical-align: bottom;
        }

        #generic_price_table .generic_content .generic_head_price .generic_price_tag .month {
            font-family: "Lato", sans-serif;
            font-size: 18px;
            font-weight: 400;
            letter-spacing: 3px;
            vertical-align: bottom;
        }

        #generic_price_table .generic_content .generic_feature_list ul {
            list-style: none;
            padding: 0;
            margin: 0;
        }

        #generic_price_table .generic_content .generic_feature_list ul li {
            font-family: "Lato", sans-serif;
            font-size: 18px;
            padding: 15px 0;
            transition: all 0.3s ease-in-out 0s;
        }

        #generic_price_table .generic_content .generic_feature_list ul li:hover {
            transition: all 0.3s ease-in-out 0s;
            -moz-transition: all 0.3s ease-in-out 0s;
            -ms-transition: all 0.3s ease-in-out 0s;
            -o-transition: all 0.3s ease-in-out 0s;
            -webkit-transition: all 0.3s ease-in-out 0s;

        }

        #generic_price_table .generic_content .generic_feature_list ul li .fa {
            padding: 0 10px;
        }

        #generic_price_table .generic_content .generic_price_btn {
            margin: 20px 0 32px;
        }

        #generic_price_table .generic_content .generic_price_btn a {
            border-radius: 50px;
            -moz-border-radius: 50px;
            -ms-border-radius: 50px;
            -o-border-radius: 50px;
            -webkit-border-radius: 50px;
            display: inline-block;
            font-family: "Lato", sans-serif;
            font-size: 18px;
            outline: medium none;
            padding: 12px 30px;
            text-decoration: none;
            text-transform: uppercase;
        }

        #generic_price_table .generic_content,
        #generic_price_table .generic_content:hover,
        #generic_price_table .generic_content .generic_head_price .generic_head_content .head_bg,
        #generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head_bg,
        #generic_price_table .generic_content .generic_head_price .generic_head_content .head h2,
        #generic_price_table .generic_content:hover .generic_head_price .generic_head_content .head h2,
        #generic_price_table .generic_content .price,
        #generic_price_table .generic_content:hover .price,
        #generic_price_table .generic_content .generic_price_btn a,
        #generic_price_table .generic_content:hover .generic_price_btn a {
            transition: all 0.3s ease-in-out 0s;
            -moz-transition: all 0.3s ease-in-out 0s;
            -ms-transition: all 0.3s ease-in-out 0s;
            -o-transition: all 0.3s ease-in-out 0s;
            -webkit-transition: all 0.3s ease-in-out 0s;
        }

        @media (max-width: 320px) {
        }

        @media (max-width: 767px) {
            #generic_price_table .generic_content {
                margin-bottom: 75px;
            }
        }

        @media (min-width: 768px) and (max-width: 991px) {
            #generic_price_table .col-md-3 {
                float: left;
                width: 50%;
            }

            #generic_price_table .col-md-4 {
                float: left;
                width: 50%;
            }

            #generic_price_table .generic_content {
                margin-bottom: 75px;
            }
        }

        @media (min-width: 992px) and (max-width: 1199px) {
        }

        @media (min-width: 1200px) {
        }

        #generic_price_table_home {
            font-family: 'Raleway', sans-serif;
        }

        .text-center h1,
        .text-center h1 a {
            color: #7885CB;
            font-size: 30px;
            font-weight: 300;
            text-decoration: none;
        }

        .demo-pic {
            margin: 0 auto;
        }

        .demo-pic:hover {
            opacity: 0.7;
        }

        #generic_price_table_home ul {
            margin: 0 auto;
            padding: 0;
            list-style: none;
            display: table;
        }

        #generic_price_table_home li {
            float: left;
        }

        #generic_price_table_home li + li {
            margin-left: 10px;
            padding-bottom: 10px;
        }

        #generic_price_table_home li a {
            display: block;
            width: 50px;
            height: 50px;
            font-size: 0px;
        }

        #generic_price_table_home .blue {
            background: #3498DB;
            transition: all 0.3s ease-in-out 0s;
        }

        #generic_price_table_home .emerald {
            background: #2ECC71;
            transition: all 0.3s ease-in-out 0s;
        }

        #generic_price_table_home .grey {
            background: #7F8C8D;
            transition: all 0.3s ease-in-out 0s;
        }

        #generic_price_table_home .midnight {
            background: #34495E;
            transition: all 0.3s ease-in-out 0s;
        }

        #generic_price_table_home .orange {
            background: #E67E22;
            transition: all 0.3s ease-in-out 0s;
        }

        #generic_price_table_home .purple {
            background: #9B59B6;
            transition: all 0.3s ease-in-out 0s;
        }

        #generic_price_table_home .red {
            background: #E74C3C;
            transition: all 0.3s ease-in-out 0s;
        }

        #generic_price_table_home .turquoise {
            background: #1ABC9C;
            transition: all 0.3s ease-in-out 0s;
        }

        #generic_price_table_home .blue:hover,
        #generic_price_table_home .emerald:hover,
        #generic_price_table_home .grey:hover,
        #generic_price_table_home .midnight:hover,
        #generic_price_table_home .orange:hover,
        #generic_price_table_home .purple:hover,
        #generic_price_table_home .red:hover,
        #generic_price_table_home .turquoise:hover {
            border-bottom-left-radius: 50px;
            border-bottom-right-radius: 50px;
            border-top-left-radius: 50px;
            border-top-right-radius: 50px;
            transition: all 0.3s ease-in-out 0s;
        }

        #generic_price_table_home .divider {
            border-bottom: 1px solid #ddd;
            margin-bottom: 20px;
            padding: 20px;
        }

        #generic_price_table_home .divider span {
            width: 100%;
            display: table;
            height: 2px;
            background: #ddd;
            margin: 50px auto;
            line-height: 2px;
        }

        #generic_price_table_home .itemname {
            text-align: center;
            font-size: 50px;
            padding: 50px 0 20px;
            border-bottom: 1px solid #ddd;
            margin-bottom: 40px;
            text-decoration: none;
            font-weight: 300;
        }

        #generic_price_table_home .itemnametext {
            text-align: center;
            font-size: 20px;
            padding-top: 5px;
            text-transform: uppercase;
            display: inline-block;
        }

        #generic_price_table_home .footer {
            padding: 40px 0;
        }

        .price-heading {
            text-align: center;
        }

        .price-heading h1 {
            color: #666;
            margin: 0;
            padding: 0 0 50px 0;
        }

        .demo-button {
            background-color: #333333;
            color: #ffffff;
            display: table;
            font-size: 20px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 20px;
            margin-bottom: 50px;
            outline-color: -moz-use-text-color;
            outline-style: none;
            outline-width: medium;
            padding: 10px;
            text-align: center;
            text-transform: uppercase;
        }

        .bottom_btn {
            background-color: #333333;
            color: #ffffff;
            display: table;
            font-size: 28px;
            margin: 60px auto 20px;
            padding: 10px 25px;
            text-align: center;
            text-transform: uppercase;
        }

        .demo-button:hover {
            background-color: #666;
            color: #FFF;
            text-decoration: none;

        }

        .bottom_btn:hover {
            background-color: #666;
            color: #FFF;
            text-decoration: none;
        }

    </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>