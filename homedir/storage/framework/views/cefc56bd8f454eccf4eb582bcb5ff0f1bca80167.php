<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<?php echo $__env->make('partials.home.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body id="body" data-spy="scroll" data-target=".one-page-header" class="demo-lightbox-gallery font-main promo-padding-top">
    <main class="wrapper">
        <!-- Header -->
    <?php echo $__env->make('partials.home.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('content'); ?>
    <?php echo $__env->make('partials.home.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

   </main>
<?php echo $__env->make('partials.home.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>
