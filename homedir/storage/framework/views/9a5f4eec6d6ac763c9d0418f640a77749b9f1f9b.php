<!-- Start Core Plugins
=====================================================================-->
<!-- jQuery -->
<script src="<?php echo e(asset('assets/plugins/jquery/jquery-1.12.4.min.js')); ?>" type="text/javascript"></script>
<!-- jquery-ui -->
<script src="<?php echo e(asset('assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')); ?>" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="<?php echo e(asset('assets/backend/bootstrap/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
<!-- lobipanel -->
<script src="<?php echo e(asset('assets/plugins/lobipanel/lobipanel.min.js')); ?>" type="text/javascript"></script>
<!-- Pace js -->
<script src="<?php echo e(asset('assets/plugins/pace/pace.min.js')); ?>" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="<?php echo e(asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')); ?>" type="text/javascript"></script>
<!-- FastClick -->
<script src="<?php echo e(asset('assets/plugins/fastclick/fastclick.min.js')); ?>" type="text/javascript"></script>
<!-- AdminBD frame -->
<script src="<?php echo e(asset('assets/backend/dist/js/frame.js')); ?>" type="text/javascript"></script>

<!-- Toastr JS -->
<script src="<?php echo e(asset('assets/plugins/toastr/toastr.min.js')); ?>" type="text/javascript"></script>
<!-- Dashboard js -->
<script src="<?php echo e(asset('assets/backend/dist/js/dashboard.js')); ?>" type="text/javascript"></script>
<!-- End Theme label Script
=====================================================================-->
<!-- Notify -->
<script src="<?php echo e(asset('assets/plugins/jquery-confirm/jquery-confirm.min.js')); ?>"></script>


<?php if(Session::has('toastr') && isset(Session::get('toastr')['message'])): ?>
    <script>
        $(document).ready(function () {
            setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 5000
    //                        positionClass: "toast-top-left"
                };
                <?php if(Session::get('toastr')['type'] =='success'): ?>
                    toastr.success("<?php echo Session::get('toastr')['message']; ?>", "<?php echo Session::get('toastr')['title']; ?>");
                <?php elseif(Session::get('toastr')['type'] =='warning'): ?>
                    toastr.warning("<?php echo Session::get('toastr')['message']; ?>", "<?php echo Session::get('toastr')['title']; ?>");
                <?php elseif(Session::get('toastr')['type'] =='error'): ?>
                    toastr.error("<?php echo Session::get('toastr')['message']; ?>", "<?php echo Session::get('toastr')['title']; ?>");
                <?php endif; ?>
            }, 1300);
        });
    </script>
<?php endif; ?>

<?php echo $__env->yieldContent('scripts'); ?>