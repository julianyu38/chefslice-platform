<header class="main-header">
    <a href="<?php echo e(url('dashboard')); ?>" class="logo"> <!-- Logo -->
        <span class="logo-lg">
              <h4><?php echo e(env('APP_NAME',"Chef Slice")); ?></h4>
              <h6>Your slogan here</h6>
       </span>
        <span class="logo-mini">
            
            CS
        </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
            <span class="sr-only">Toggle navigation</span>
            <span class="pe-7s-keypad"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages -->
                
                <!-- Notifications -->

                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 0 new notifications
                        </li>
                        <li>
                            <ul class="menu">

                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>
                <!-- settings -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="pe-7s-settings"></i></a>
                    <ul class="dropdown-menu">
                       
                        <li><a href="#"><i class="pe-7s-settings"></i> Settings</a></li>
                        <li><a href="<?php echo e(url(env('APP_URL_PROTOCOL').'://'.env('APP_URL').'/logout')); ?>"><i class="pe-7s-key"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>