
<?php echo Form::open(['action'=>['Dashboard\SettingsController@store','account'=>$account],'method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data']); ?>

<fieldset class="form-group">

    <legend class="scheduler-border">User Site Logo</legend>
    <div class="form-group <?php echo e($errors->has('logo') ? ' has-error' : ''); ?>">
        <label for="logo" class="col-sm-3 control-label">Logo</label>
        <div class="col-sm-9">
            <input type="file" name="logo" class="input-logo" accept="image/*">
            <?php if($errors->has('logo')): ?>
                <span class="help-block">
                                            <strong><?php echo e($errors->first('logo')); ?></strong>
                                        </span>
            <?php endif; ?>
            <div class="form-group">
                <div class="logo-preview">
                </div>
                <?php if(!empty($settings->logo)): ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <img width="150px" class="img-logo"
                                 src="<?php echo e(asset('media/design/logo/'.$settings->id.'/'.$settings->logo)); ?>">
                            <button data-url="#" type="button"
                                    class="btn btn-block btn-xs btn-danger btn--delete--image"><i
                                        class="fa fa-trash-o"></i> Delete
                            </button>
                            
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>

    </div>
</fieldset>


<fieldset class="form-group">
    <legend class="scheduler-border">User Site Background</legend>

    <div class="form-group <?php echo e($errors->has('background') ? ' has-error' : ''); ?>">
        <label for="background" class="col-sm-3 control-label">Background</label>
        <div class="col-sm-9">
            <input type="file" name="background" class="input-background" accept="image/*">
            <?php if($errors->has('background')): ?>
                <span class="help-block">
                                            <strong><?php echo e($errors->first('background')); ?></strong>
                                        </span>
            <?php endif; ?>
            <div class="form-group">
                <div class="background-preview">
                </div>
                <?php if(!empty($settings->background)): ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <img width="150px" class="img-background"
                                 src="<?php echo e(asset('media/design/background/'.$settings->id.'/'.$settings->background)); ?>">
                            <button data-url="#" type="button"
                                    class="btn btn-block btn-xs btn-danger btn--delete--image"><i
                                        class="fa fa-trash-o"></i> Delete
                            </button>
                            
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>

    </div>


    <div class="form-group">
        <?php echo Form::label("heatmap", "Enable Heatmap:", ['class' => 'col-sm-3 control-label']); ?>

        <div class="col-sm-9">
            <select name="heatmap" class="form-control">
                <option value="1" <?php echo e($settings->heatmap == 1 ? 'selected' : ''); ?>>Yes</option>
                <option value="0" <?php echo e($settings->heatmap == 0 ? 'selected' : ''); ?>>No</option>
            </select>
            <?php if($errors->has('heatmap')): ?>
                <div class="text-danger">
                    <small><?php echo e($errors->first('heatmap')); ?></small>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="form-group <?php echo e($errors->has('count') ? ' has-error' : ''); ?>">
        <?php echo Form::label('count', 'Heatmap count *', ['class' => 'col-sm-3 control-label']); ?>

        <div class="col-sm-9">
            <?php echo Form::text('count',$value= $settings->count, $attributes = ['class'=>'form-control','placeholder'=>'Heatmap click count']); ?>

            <?php if($errors->has('count')): ?>
                <span class="help-block">
                                        <strong><?php echo e($errors->first('count')); ?></strong>
                                    </span>
            <?php endif; ?>
        </div>
    </div>
    <div class="form-group <?php echo e($errors->has('specialoffer') ? ' has-error' : ''); ?>">
        <?php echo Form::label('specialoffer', 'Special Offer Text', ['class' => 'col-sm-3 control-label']); ?>

        <div class="col-sm-9">
            <?php echo Form::text('specialoffer',$value= $settings->specialoffer, $attributes = ['class'=>'form-control','placeholder'=>'Enter text for Special Offer label']); ?>

            <?php if($errors->has('specialoffer')): ?>
                <span class="help-block">
                                        <strong><?php echo e($errors->first('specialoffer')); ?></strong>
                                    </span>
            <?php endif; ?>
        </div>
    </div>


</fieldset>


<fieldset class="form-group">
    <legend>User Menu Design</legend>
    <div class="row">
        <div class="col-sm-6 ">
            <div>
                <input type="radio" name="category_design" id="category_design2" class="input-hidden"
                       value="bezier2" <?php echo $settings->category_design=='bezier2'?'checked':''; ?> >
                <label for="category_design2" class="form-check-label">

                    <img src="<?php echo e(asset('media/design/bezier2.PNG')); ?>">
                </label>
            </div>
            <div>
                <input type="radio" name="category_design" id="category_design3" class="input-hidden"
                       value="cubical-parabola" <?php echo $settings->category_design=='cubical-parabola'?'checked':''; ?> >
                <label for="category_design3" class="form-check-label">

                    <img src="<?php echo e(asset('media/design/cubical-parabola.PNG')); ?>">
                </label>
            </div>
            <div>
                <input type="radio" name="category_design" id="category_design5" class="input-hidden"
                       value="ellipse-vert" <?php echo $settings->category_design=='ellipse-vert'?'checked':''; ?> >
                <label for="category_design5" class="form-check-label">

                    <img src="<?php echo e(asset('media/design/ellipse-vert.PNG')); ?>">
                </label>
            </div>
            <div>
                <input type="radio" name="category_design" id="category_design7" class="input-hidden"
                       value="line1" <?php echo $settings->category_design=='line1'?'checked':''; ?> >
                <label for="category_design7" class="form-check-label">

                    <img src="<?php echo e(asset('media/design/line1.PNG')); ?>">
                </label>
            </div>
            <div>
                <input type="radio" name="category_design" id="category_design9" class="input-hidden"
                       value="parabola1" <?php echo $settings->category_design=='parabola1'?'checked':''; ?> >
                <label for="category_design9" class="form-check-label">

                    <img src="<?php echo e(asset('media/design/parabola1.PNG')); ?>">
                </label>
            </div>
            <div>
                <input type="radio" name="category_design" id="category_design11" class="input-hidden"
                       value="spiral" <?php echo $settings->category_design=='spiral'?'checked':''; ?> >
                <label for="category_design11" class="form-check-label">

                    <img src="<?php echo e(asset('media/design/spiral.PNG')); ?>">
                </label>
            </div>
        </div>


        <div class="col-sm-6 ">
            <div>
                <input type="radio" name="category_design" id="category_design1" class="input-hidden"
                       value="bezier1" <?php echo $settings->category_design=='bezier1'?'checked':''; ?> >
                <label for="category_design1" class="form-check-label">

                    <img src="<?php echo e(asset('media/design/bezier1.PNG')); ?>">
                </label>
            </div>
            <div>
                <input type="radio" name="category_design" id="category_design4" class="input-hidden"
                       value="ellipse" <?php echo $settings->category_design=='ellipse'?'checked':''; ?> >
                <label for="category_design4" class="form-check-label">

                    <img src="<?php echo e(asset('media/design/ellipse.PNG')); ?>">
                </label>
            </div>
            <div>
                <input type="radio" name="category_design" id="category_design6" class="input-hidden"
                       value="html-content" <?php echo $settings->category_design=='html-content'?'checked':''; ?> >
                <label for="category_design6" class="form-check-label">

                    <img src="<?php echo e(asset('media/design/html-content.PNG')); ?>">
                </label>
            </div>
            <div>
                <input type="radio" name="category_design" id="category_design8" class="input-hidden"
                       value="line2" <?php echo $settings->category_design=='line2'?'checked':''; ?> >
                <label for="category_design8" class="form-check-label">

                    <img src="<?php echo e(asset('media/design/line2.PNG')); ?>">
                </label>
            </div>
            <div>
                <input type="radio" name="category_design" id="category_design10" class="input-hidden"
                       value="slideshow" <?php echo $settings->category_design=='slideshow'?'checked':''; ?> >
                <label for="category_design10" class="form-check-label">

                    <img src="<?php echo e(asset('media/design/slideshow.PNG')); ?>">
                </label>
            </div>
        </div>
    </div>















</fieldset>

<div class="form-group">
    <div class="col-sm-10">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
    </div>
</div>

<?php echo e(csrf_field()); ?>



<?php echo Form::close(); ?>