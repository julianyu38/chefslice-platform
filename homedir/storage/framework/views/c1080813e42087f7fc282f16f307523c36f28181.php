<?php $__env->startSection('header'); ?>
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-th"></i>
        </div>
        <div class="header-title">
            <h1>Products</h1>
            <small>Manage Products.</small>
            <?php echo $__env->make('dashboard.partials.quick-links', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/')); ?>"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="<?php echo e(url('/dashboard')); ?>"> Dashboard</a></li>
                <li class="active">Product</li>
            </ol>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>New Product </h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="View Product" data-placement="bottom" href="<?php echo e(url('dashboard/item')); ?>" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> View Products</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <?php echo Form::open(['url'=>['dashboard/item'],'method'=>'post','class'=>'form-horizontal','files'=>true,'enctype'=>'multipart/form-data']); ?>

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <div class="form-group <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('title', 'Name *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::text('title',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Item Name']); ?>

                                    <?php if($errors->has('title')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('category_id') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('category_id', "Category", ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <select class="form-control select2" data-placeholder="Product Category" name="category_id" id="category_id">
                                        <option selected="selected" value="">SelectProduct Category</option>
                                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option <?php echo e(old('category_id')==$id?'selected="selected"':""); ?> value="<?php echo e($category->id); ?>"><?php echo e($category->text); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php if($errors->has('category_id')): ?>
                                        <span class="help-block">
                                                <strong><?php echo e($errors->first('category_id')); ?></strong>
                                            </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('locale') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('locale', 'Select Language', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <select required class="form-control" name="locale" id="locale">
                                        <option value="en">English</option>
                                        <option value="he">Hebrew</option>
                                    </select>
                                    <?php if($errors->has('locale')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('locale')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                    <div class="checkbox">
                                        <?php echo Form::label('is_special', 'Is Special *', ['class' => 'col-sm-3 control-label']); ?>


                                            <input type="checkbox" id="is_special" name="is_special" data-on="Yes" data-off="No" data-onstyle="success"   data-toggle="toggle">
                                    </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('short_description') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('short_description', 'Short Description *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::textarea('short_description',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Short description','rows'=>3]); ?>

                                    <?php if($errors->has('short_description')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('short_description')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('description', 'Description *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::textarea('description',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Description','rows'=>5]); ?>

                                    <?php if($errors->has('description')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('description')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('thumbnail') ? ' has-error' : ''); ?>">
                                <label for="thumbnail" class="col-sm-3 control-label"> Thumbnail</label>
                                <div class="col-sm-9">
                                    <input type="file" name="thumbnail" class="input-thumbnail" accept="image/*">
                                    <?php if($errors->has('thumbnail')): ?>
                                        <span class="help-block">
                                                        <strong><?php echo e($errors->first('thumbnail')); ?></strong>
                                                    </span>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <div class="thumbnail-preview">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group <?php echo e($errors->has('media_type') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('media_type', 'Media Type', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <select class="form-control select2" data-placeholder="Select Media Type" name="media_type" id="media_type" onchange="selectMedia(this.value);">
                                        <option selected="selected" value="">Select Media Type</option>

                                        <option <?php echo e(old('media_type')==1?'selected="selected"':""); ?> value="1">Image</option>
                                        <option <?php echo e(old('media_type')==2?'selected="selected"':""); ?> value="2">Video</option>

                                    </select>
                                    <?php if($errors->has('media_type')): ?>
                                        <span class="help-block">
                                                <strong><?php echo e($errors->first('media_type')); ?></strong>
                                            </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo e($errors->has('media') ? ' has-error' : ''); ?>">
                                <label for="media" class="col-sm-3 control-label">Media File</label>
                                <div class="col-sm-9">
                                    <input type="file" name="media[]" class="input-image" accept="image/*" multiple>
                                    <?php if($errors->has('media')): ?>
                                        <span class="help-block">
                                                        <strong><?php echo e($errors->first('media')); ?></strong>
                                                    </span>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <div class="logo-preview">
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <button type="reset" class="btn btn-default">Cancel</button>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Create</button>
                                </span>
                        </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('styles'); ?>

    <link href="<?php echo e(asset('assets/plugins/bootstrap/css/bootstrap-toggle.min.css')); ?>" rel="stylesheet">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('assets/plugins/bootstrap/js/bootstrap-toggle.min.js')); ?>" type="text/javascript"></script>

    <script>
        function selectMedia(media_type) {
            if(media_type==1) {
                $('#carousel-inner').prepend('<div class="item ' + divclass + '"><img src="' + imgurl + '" alt="Los Angeles"> </div>');
            } elseif (media_type==2) {
                $('#carousel-inner').prepend('<div class="item ' + divclass + '"><img src="' + imgurl + '" alt="Los Angeles"> </div>');

            }
        }
        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }


        $(".input-thumbnail").change(function () {
            $('.thumbnail-preview').html('');
            readURL(this,function (e) {
                $('.thumbnail-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="thumbnail_upload_preview" class="img-responsive img-thumbnail">')
            });
        });
        $(".input-image").change(function () {
            $('.logo-preview').html('');
            readURL(this,function (e) {
                $('.logo-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
            });
        });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>