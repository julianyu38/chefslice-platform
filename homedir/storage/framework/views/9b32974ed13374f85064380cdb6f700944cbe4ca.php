<!-- JS Global Compulsory -->
<script src="<?php echo e(asset('assets/plugins/jquery/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/jquery/jquery-migrate.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/bootstrap/js/bootstrap.min.js')); ?>"></script>

<!-- JS Implementing Plugins -->
<script src="<?php echo e(asset('assets/plugins/smoothScroll.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/jquery.easing.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/pace/pace.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/owl-carousel2/owl.carousel.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/modernizr.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/slick/slick.min.js')); ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBixOX5q1rXiDnPSsMtg_WdP4DgmOiMFtM" ></script>


<script src="<?php echo e(asset('assets/plugins/master-slider/masterslider/masterslider.min.js')); ?>"></script>

<!-- JS Page Level-->
<script src="<?php echo e(asset('assets/home/js/one.app.js')); ?>"></script>
<script src="<?php echo e(asset('assets/home/js/plugins/owl2-carousel-v1.js')); ?>"></script>
<script src="<?php echo e(asset('assets/home/js/plugins/owl2-carousel-v3.js')); ?>"></script>
<script src="<?php echo e(asset('assets/home/js/plugins/owl2-carousel-v4.js')); ?>"></script>
<script src="<?php echo e(asset('assets/home/js/plugins/cube-portfolio.js')); ?>"></script>
<script src="<?php echo e(asset('assets/home/js/plugins/datepicker.js')); ?>"></script>
<script src="<?php echo e(asset('assets/home/js/plugins/gmaps-ini.js')); ?>"></script>
<script src="<?php echo e(asset('assets/home/js/plugins/promo.js')); ?>"></script>
<script src="<?php echo e(asset('assets/home/js/plugins/special-dishes.js')); ?>"></script>
<script>
    $(function() {
        App.init();
        Owl2Carouselv1.initOwl2Carouselv1();
        Owl2Carouselv3.initOwl2Carouselv3();
        Owl2Carouselv4.initOwl2Carouselv4();
        Datepicker.initDatepicker();
    });
</script>
<!--[if lt IE 10]>
<script src="<?php echo e(asset('assets/plugins/sky-forms-pro/skyforms/js/jquery.placeholder.min.js')); ?>"></script>
<![endif]-->
<?php echo $__env->yieldContent('scripts'); ?>
