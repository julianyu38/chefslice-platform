<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
     <title>
        <?php if (! empty(trim($__env->yieldContent('title')))): ?>
        <?php echo $__env->yieldContent('title'); ?>
        <?php else: ?>
            <?php echo e(env('APP_NAME',"Barbados Sotheby's")); ?> - Danial
        <?php endif; ?>
    </title>
    <?php echo $__env->yieldContent('meta'); ?>
<!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content=""/>
    <meta name="twitter:image" content=""/>
    <meta name="twitter:url" content=""/>
    <meta name="twitter:card" content=""/>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- Google Webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <meta name="google-site-verification" content="GwbDuLf7Z5maleR_VuKrhOzAA_JPX5PXqn7UCRpcaP0" />
    <link href="<?php echo e(asset('theme/animate.min.css')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('theme/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet"/>
    <link defer rel="stylesheet" href="<?php echo e(asset('theme/circularmenu/styles.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/assets/plugins/bootstrap/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/css/animate.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/css/hover-min.css')); ?>">

    <?php echo $__env->yieldContent('styles'); ?>
    <style>
        .heatmap-canvas {
            /*height: 100vh !important;*/
            width:100% !important;
        }
    </style>
            <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
    </script>

</head>