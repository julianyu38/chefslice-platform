<?php $__env->startSection('header'); ?>
    <section class="content-header">
        <div class="header-icon">
            <i class="ti-home"></i>
        </div>
        <div class="header-title">
            <h1>
                Dashboard

            </h1>
            <small>Admin panel of <?php echo e(env('APP_NAME',"Barbados Sotheby's")); ?>.</small>
            <?php echo $__env->make('dashboard.partials.quick-links', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/')); ?>"><i class="pe-7s-home"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <div class="panel panel-bd">
                <div class="panel-body">
                    <div class="statistic-box">
                        <?php
                        $new_sessions = array_column($analytics,1);
                        $total_visitors = array_column($analytics,2);
                        $total_users = array_column($analytics,3);
                        $bounce_rate = array_column($analytics,4);

                        ?>
                        <h2>
                            <span class="count-number"><?php echo e(array_sum($new_sessions)); ?></span>
                            <?php
                            $thisMonthSessions = array_sum(array_splice($new_sessions,15));

                            $previousMonthSessions = array_sum(array_splice($new_sessions,0,14))>0?array_sum(array_splice($new_sessions,0,14)):1;
                            //$previousMonthSessions = array_sum(array_splice($new_sessions,0,14));
                            if($previousMonthSessions==0){
                                $previousMonthSessions = 1;
                            }
                            $new_session_percentage = round((($thisMonthSessions - $previousMonthSessions) / $previousMonthSessions) * 100);
                            ?>
                            <span class="slight"><i class="fa fa-play fa-rotate-<?php echo e($new_session_percentage>=0?'270 text-success':'90 text-danger'); ?> "> </i>

                                <?php echo e($new_session_percentage); ?>%
                            </span>
                        </h2>
                        <div class="small">% New Sessions</div>
                        <div class="sparkline1 text-center"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <div class="panel panel-bd">
                <div class="panel-body">
                    <div class="statistic-box">
                        <h2>
                            <span class="count-number"><?php echo e(array_sum($total_visitors)); ?></span>
                            <?php
                            $thisMonthVisitors = array_sum(array_splice($total_visitors,15));
                            $previousMonthVisitors = array_sum(array_splice($total_visitors,0,14))>0?array_sum(array_splice($total_visitors,0,14)):1;
                            //$previousMonthVisitors = array_sum(array_splice($total_visitors,0,14));
                            if($previousMonthVisitors==0){
                                $previousMonthVisitors = 1;
                            }
                            $percentage_visitors = round((($thisMonthVisitors - $previousMonthVisitors) / $previousMonthVisitors) * 100);
                            ?>
                            <span class="slight">
                                <i class="fa fa-play fa-rotate-<?php echo e($percentage_visitors>=0?'270 text-success':'90 text-danger'); ?> c-white"> </i>
                                <?php echo e($percentage_visitors); ?>%
                            </span>
                        </h2>
                        <div class="small">Total visitors</div>
                        <div class="sparkline2 text-center"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <div class="panel panel-bd">
                <div class="panel-body">
                    <div class="statistic-box">
                        <h2>
                            <span class="count-number"><?php echo e(array_sum($total_users)); ?></span>
                            <?php
                            $thisMonthUsers = array_sum(array_splice($total_users,15));
                            $previousMonthUsers = array_sum(array_splice($total_users,0,14))>0?array_sum(array_splice($total_users,0,14)):1;
                            //$previousMonthUsers = array_sum(array_splice($total_users,0,14));
                            if($previousMonthUsers==0){
                                $previousMonthUsers = 1;
                            }
                            $percentage_users = round((($thisMonthUsers - $previousMonthUsers) / $previousMonthUsers) * 100);
                            ?>
                            <span class="slight">
                                <i class="fa fa-play fa-rotate-<?php echo e($percentage_users>=0?'270 text-success':'90 text-danger'); ?> text-warning"> </i> <?php echo e($percentage_users); ?>%
                            </span>
                        </h2>
                        <div class="small">Total users</div>
                        <div class="sparkline3 text-center"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <div class="panel panel-bd">
                <div class="panel-body">
                    <div class="statistic-box">
                        <h2>
                            <span class="count-number"><?php echo e(round(array_sum($bounce_rate))); ?></span>
                            <?php
                            $thisMonthBounceRate = array_sum(array_splice($bounce_rate,15));
                            $previousMonthBounceRate = array_sum(array_splice($bounce_rate,0,14))>0?array_sum(array_splice($bounce_rate,0,14)):1;
                            //$previousMonthBounceRate = array_sum(array_splice($bounce_rate,0,14));
                            if($previousMonthBounceRate==0){
                                $previousMonthBounceRate = 1;
                            }
                            $percentage_BounceRate = round((($thisMonthBounceRate - $previousMonthBounceRate) / $previousMonthBounceRate) * 100);
                            ?>
                            <span class="slight">
                                <i class="fa fa-play fa-rotate-<?php echo e($percentage_BounceRate>=0?'270 text-success':'90 text-danger'); ?> c-white"> </i> <?php echo e($percentage_BounceRate); ?>%
                            </span></h2>

                        <div class="small">Bounce Rate</div>
                        <div class="sparkline4 text-center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <div class="panel panel-bd" style="min-height: 215px;">
                        <div class="panel-body">
                            <div id="clock_aus"></div>
                            <div id="clock_bar"></div>
                            <div id="clock_can"></div>
                            <div id="clock_isr"></div>
                            <div id="clock_lon"></div>
                            <div id="clock_new"></div>
                            <div id="clock_france"></div>
                            <div id="clock_russia"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-7 ">
            <div class="panel panel-bd">
                <div class="panel-body">
                    
                    <div id="top-countries" style="min-width: 300px; height: 355px; margin: 0 auto"></div>
                    
                </div>
            </div>
        </div>



    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <div class="panel panel-bd" >
                    <div class="panel-body">
                        <div id="betterweather"></div>
                        <select  class="styled select-weather-locations">
                            <option  value="-33.8548,151.2165">Australia</option>
                            <option   value="13.0978,-59.6184" selected="selected">Barbados </option>
                            <option   value="43.6529,-79.3849">Canada </option>
                            <option   value="32.0805,34.7805">Israel </option>
                            <option   value="51.5073,-0.1276">London </option>
                            <option   value="40.7306,-73.9866">New York </option>
                        </select>
                    </div>
                </div>
            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
            <div class="panel panel-bd">
                <div class="panel-body">
                    <div id="top-browsers"  style="min-width: 300px; height: 300px; margin: 0 auto"></div>

                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="social-widget">
                <ul>
                    <li>
                        <div class="fb_inner">
                            <i class="fa fa-facebook"></i>
                            <span class="sc-num"><?php echo e((isset($SocialCounts)?(int)$SocialCounts->get_facebook():0)); ?></span>
                            <small>Fans</small>
                        </div>
                    </li>
                    <li>
                        <div style="background: #966842" class="twitter_inner">
                            <i class="fa fa-instagram"></i>
                            <span class="sc-num"><?php echo e((isset($SocialCounts)?(int)$SocialCounts->get_instagram():0)); ?></span>
                            <small>Followers</small>
                        </div>
                    </li>
                    <li>
                        <div class="g_plus_inner">
                            <i class="fa fa-google-plus"></i>
                            <span class="sc-num"><?php echo e((isset($SocialCounts)?(int)$SocialCounts->get_google():0)); ?></span>
                            <small>Followers</small>
                        </div>
                    </li>
                    <li>
                        <div class="dribble_inner">
                            <i class="fa fa-pinterest"></i>
                            <span class="sc-num"><?php echo e((isset($SocialCounts)?(int)$SocialCounts->get_pinterest():0)); ?></span>
                            <small>Followers</small>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
            <div class="panel panel-bd">
                <div class="panel-body">
                    <div id="jquery-todolist" align="center"></div>
                </div>
            </div>
        </div>

    </div> <!-- /.row -->
















<?php $__env->stopSection(); ?>
<?php $__env->startSection('styles'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/jClocksGMT/css/jClocksGMT.css')); ?>">
    <!-- Weather icon -->
    
    <link href="<?php echo e(asset('assets/backend/better-weather/css/bw-style.min.css')); ?>" rel="stylesheet">

    <style>
        .jcgmt-container .jcgmt-lbl {
            font-size: 12px;
            font-weight: 800;
            color: #7A7A7D;
            position: relative;
        }
        .highcharts-credits{
            display:    none !important;
        }
        .jcgmt-container {
            margin: 0 10px;
            min-height:180px;
        }
        .select-weather-locations{
            z-index: 99;
            width: 100%;
            border: 1px solid #ccc;
            background: rgba(0,0,0,.5);
            border-radius: 0px;
            color: #fff;
            padding: 0 10px;
        }
        @media(min-width: 1400px){
            .jcgmt-container {
                margin: 0px 12px;
            }
        }
        @media(max-width: 1868px){
            #clock_france,#clock_russia{
                display: none;
            }
        }
        @media(min-width: 1869px){
            #clock_france,#clock_russia{
                display: inline-block;
            }
        }
        @media(min-width: 1758px){
            #betterweather,.select-weather-locations{
                width: 400px;
                margin: auto;
                display: block;
            }

            .jcgmt-container{
                margin: 0 18px;
            }
        }
        @media(min-width: 2100px){
            .jcgmt-container {
                margin: 0px 8px;
            }
        }
        @media(min-width: 2300px){
            .jcgmt-container {
                margin: 0px 9%;
            }
        }
        @media(min-width: 2500px){
            .jcgmt-container {
                margin: 0px 10%;
            }
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('assets/plugins/counterup/waypoints.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/plugins/counterup/jquery.counterup.min.js')); ?>" type="text/javascript"></script>
    <!-- Sparkline js -->
    <script src="<?php echo e(asset('assets/plugins/sparkline/sparkline.min.js')); ?>" type="text/javascript"></script>

    <!-- Monthly js -->
    <script src="<?php echo e(asset('assets/plugins/monthly/monthly.js')); ?>" type="text/javascript"></script>


    
    <script src="<?php echo e(asset('assets/plugins/jClocksGMT/js/jClocksGMT.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/plugins/jClocksGMT/js/jquery.rotate.js')); ?>"></script>

    <script src="<?php echo e(asset('assets/plugins/jquery-todolist/jquery.todoList.js')); ?>"></script>
    <script type="text/javascript">
        function update_todo(tasks,deleteAll) {
            $.ajax({
                url: '<?php echo e(url('/dashboard/todo')); ?>?_='+new Date().getTime()+(deleteAll!=null?'&clear=true':''),
                type:'post',
                dataType:'JSON',
                data:{data:tasks},
                success:function(data){
                    if(data.type !=null){
                        if(data.type =='success'){
                            toastr.success(data.message, "Success!");
                        }else if(data.type =='warning'){
                            toastr.warning(data.message, "Warning!");
                        }else{
                            toastr.error(data.message, "Error!");
                        }
                    }else{
                        toastr.error(data.message!=null?data.message:"Oops something went wrong", "Oops!");
                    }
                },
                error:function(error){
                    toastr.error("Oops something went wrong.", "Oops!");
                }
            });
        }
        function generate_todo(tasks){
            $("#jquery-todolist").todoList({
                items: tasks
            });
        }
        function get_tasks(){
            $.ajax({
                url: '<?php echo e(asset('assets/todo.json')); ?>?_='+new Date().getTime(),
                type:'get',
                dataType:'JSON',
                success:function(data){
                    var tasks = [];
                    for(var i=0; i<data.length; i++){
                        tasks[i] = {
                            id: i,
                            title : data[i].title,
                            done  : (data[i].done !=null && data[i].done == 'true'?true:false)
                        }
                    }
                    generate_todo(tasks);
                },
                error:function(error){
                    generate_todo([]);
                }
            });
        }
        $(document).ready(function(){
            get_tasks();

            $("#jquery-todolist").on("todolist-add", function(ev, options, $ui){
                var list = $("#jquery-todolist").todoList("getSetup").items;
                update_todo(list);
            });
            $("#jquery-todolist").on("todolist-edit", function(ev, options, $ui){
                var list = $("#jquery-todolist").todoList("getSetup").items;
                update_todo(list);
            });
            $("#jquery-todolist").on("todolist-checked", function(ev, options, $ui){
                var list = $("#jquery-todolist").todoList("getSetup").items;
                update_todo(list);
            });
            $("#jquery-todolist").on("todolist-unchecked", function(ev, options, $ui){
                var list = $("#jquery-todolist").todoList("getSetup").items;
                update_todo(list);
            });
            $("#jquery-todolist").on("todolist-remove", function(ev, options, $ui){
                var list = $("#jquery-todolist").todoList("getSetup").items;
                var task = $(ev.target).parents('.jquery-todolist-item').find('.jquery-todolist-item-title-text').text().trim().toLowerCase();
                var tasks = [];
                for(var i=0; i<list.length; i++){
                    if(list[i].title.trim().toLowerCase() != task) {
                        tasks[i] = {
                            id: i,
                            title: list[i].title,
                            done: (list[i].done != null && list[i].done ? true : false)
                        }
                    }
                }
                if(tasks.length==0){
                    update_todo(tasks,true);
                }else {
                    update_todo(tasks);
                }

            });


        });

    </script>
    <script>
        $(document).ready(function(){
            $('#clock_aus').jClocksGMT({
                title: 'Australia',
                offset: '+10',
                /*dst: false,
                 timeformat: 'HH:mm',*/
                skin:2/*
                 skin: 1*/
            });

            $('#clock_bar').jClocksGMT({
                title: 'Barbados',
                offset: '-4',
                skin: 2
            });



            $('#clock_can').jClocksGMT(
                    {
                        title: 'Canada',
                        offset: '+8',
                        skin: 2
                        /*date: true*/
                    });
            $('#clock_isr').jClocksGMT(
                    {
                        title:'Israel',
                        offset:'+3',
                        /*date: true,
                         dateformat: 'YYYY/MM/DD',*/
                        skin: 2
                    });

            $('#clock_lon').jClocksGMT(
                    {

                        title:'London',
                        offset: '+1',
                        /*date: true,
                         dateformat: 'YYYY/MM/DD',*/
                        skin: 2
                    });
            $('#clock_new').jClocksGMT(
                    {
                        title: 'New York',
                        offset: '-5',
                        skin: 2
                    });
            $('#clock_france').jClocksGMT(
                    {
                        title: 'France',
                        offset: '+2',
                        skin: 2
                    });
            $('#clock_russia').jClocksGMT(
                    {
                        title: 'Russia',
                        offset: '+3',
                        skin: 2
                    });

        });
    </script>


    <script>
        $(document).ready(function () {
            "use strict"; // Start of use strict
            // notification
            //counter
            $('.count-number').counterUp({
                delay: 3,
                time: 1000
            });

            //Chat list
            $('.chat_list').slimScroll({
                size: '3px',
                height: '305px'
            });

            // Message
            $('.message_inner').slimScroll({
                size: '3px',
                height: '320px'
//                    position: 'left'
            });

            //monthly calender
           /* $('#m_calendar').monthly({
                mode: 'event',
                //jsonUrl: '',
                dataType: 'xml',
                xmlUrl: ''
            });*/

            <?php
            $new_sessions = array_column($analytics,1);
            $total_visitors = array_column($analytics,2);
            $total_users = array_column($analytics,3);
            $bounce_rate = array_column($analytics,4);
            ?>

            //Sparklines Charts
            $('.sparkline1').sparkline([ <?php echo e(implode(',',$new_sessions)); ?> ], {
                type: 'bar',
                barColor: '#002349',
                height: '35',
                barWidth: '3',
                barSpacing: 2
            });

            $(".sparkline2").sparkline([<?php echo e(implode(',',$total_visitors)); ?>], {
                type: 'line',
                height: '35',
                width: '100%',
                lineColor: '#002349',
                fillColor: '#fff'
            });

            $(".sparkline3").sparkline([<?php echo e(implode(',',$total_users)); ?>], {
                type: 'line',
                height: '35',
                width: '100%',
                lineColor: '#002349',
                fillColor: '#fff'
            });

            $(".sparkline4").sparkline([<?php echo e(implode(',',array_map('round',$bounce_rate))); ?>], {
                type: 'line',
                height: '35',
                width: '100%',
                lineColor: '#37a000',
                fillColor: 'rgba(0, 35, 73,.7)'
            });



            /*$(".sparkline5").sparkline([4, 2], {
             type: 'pie',
             sliceColors: ['#37a000', '#2c3136']
             });*/

            /*  $(".sparkline6").sparkline([3, 2], {
             type: 'pie',
             sliceColors: ['#37a000', '#2c3136']
             });

             $(".sparkline7").sparkline([4, 1], {
             type: 'pie',
             sliceColors: ['#37a000', '#2c3136']
             });

             $(".sparkline8").sparkline([1, 3], {
             type: 'pie',
             sliceColors: ['#37a000', '#2c3136']
             });

             $(".sparkline9").sparkline([3, 5], {
             type: 'pie',
             sliceColors: ['#37a000', '#2c3136']
             });
             */




        });
    </script>




    <!-- jQuery, plugins and BetterWeather script-->
    <script>
        var BW_Localized = {
            monthList: {
                January     : 'January',
                February    : 'February',
                March       : 'March',
                April       : 'April',
                May         : 'May',
                June        : 'June',
                July        : 'July',
                August      : 'August',
                September   : 'September',
                October     : 'October',
                November    : 'November',
                December    : 'December'
            },
            daysList: {
                Sat : 'Sat',
                Sun : 'Sun',
                Mon : 'Mon',
                Tue : 'Tue',
                Wed : 'Wed',
                Thu : 'Thu',
                Fri : 'Fri'
            },
            stateList: {
                clear           : 'Clear',
                rain            : 'Rain',
                snow            : 'Snow',
                sleet           : 'Sleet',
                wind            : 'Wind',
                foggy           : 'Foggy',
                cloudy          : 'Cloudy',
                mostly_cloudy   : 'Mostly Cloudy',
                partly_cloudy   : 'Partly Cloudy',
                thunderstorm    : 'Thunderstorm',
                drizzle         : 'Drizzle',
                light_Rain      : 'Light Rain',
                overcast        : 'Overcast',
                breezy_and_Partly_Cloudy: 'Breezy and Partly Cloudy'
            }
        } ;
    </script>
    <script src="<?php echo e(asset('assets/backend/better-weather/js/skycons.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/better-weather/js/elementQuery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/backend/better-weather/js/betterweather.min.js')); ?>"></script>
    <script type='text/javascript'>
        (function($){
            var betterWeather = function (location) {
                $('#betterweather').html('');
                if(location==null){
                    location = $('select.select-weather-locations').val();
                }
                $('#betterweather').betterWeather({
                    apiKey      :   "0b787e550cd9b83ca20fe5b30ff980a9",
                    location    :  location,
                    showLocation:   true,
                    showDate    :   true,
                    visitorLocation: false,
                    fontColor   :   "#fff",
                    bgColor     :   "#4f4f4f",
                    style       :   "normal", // Modern
                    nextDays    :   true,
                    animatedIcons:  true,
                    naturalBackground:  true,
                    url         :   "<?php echo e(asset('assets/backend/better-weather/ajax/ajax.php')); ?>",
                    unit        :   "C" ,// F
                    showUnit    :   true,
                    mode        :   "widget" , // inline
                    inlineSize  :   "medium"  // small, medium, large
                });
            };
            betterWeather();
            $('select.select-weather-locations').change(function () {
                var location = $(this).val();
                betterWeather(location);
            });
        })(jQuery);
    </script>


    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script>
        $(document).ready(function () {

            Highcharts.chart('top-countries', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Site Top 10 Most Visited Countries'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Number of visitors'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Visitors During Last Month: <b>{point.y} </b>'
                },
                series: [{
                    name: 'Top Countries',
                    data: [
                            <?php if(count($countries)>0): ?>
                            <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        ['<?php echo e($country[0]); ?>', <?php echo e($country[1]); ?>] <?php echo e($loop->last?'':','); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>

                    ],
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        //format: '{point.y:.0f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });



            // Build the chart
            Highcharts.chart('top-browsers', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Site Visited From Top 5 Browsers'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Top Browsers',
                    colorByPoint: true,
                    data: [
                            <?php $__currentLoopData = $browsers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $browser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        {
                            name: '<?php echo e($browser['browser']); ?>',
                            y: <?php echo e($browser['sessions']); ?>

                        }
                        <?php echo e($loop->last?'':','); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    ]
                }]
            });
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>