<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('dashboard.partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <?php echo $__env->make('dashboard.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- =============================================== -->
    <!-- Left side column. contains the sidebar -->
    <?php echo $__env->make('dashboard.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?php echo $__env->yieldContent('header'); ?>
        <!-- Main content -->
        <section class="content">
            <?php echo $__env->yieldContent('content'); ?>
        </section> <!-- /.content -->
    </div> <!-- /.content-wrapper -->
    <?php echo $__env->make('dashboard.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
<!-- ./wrapper -->

<!-- Start Core Plugins
=====================================================================-->
<?php echo $__env->make('dashboard.partials.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>