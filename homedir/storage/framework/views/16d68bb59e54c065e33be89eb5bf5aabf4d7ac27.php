
<footer>
    <div  class="vertical-text">
        <i class="fa fa-hand-o-left bounce shift" aria-hidden="true"></i> <?php echo e($settings->specialoffer); ?>

    </div>
    <?php $request = request()->route()->getAction(); ?>
        <div class="offer">
     <?php if(strpos($request['controller'], 'special') && strpos($request['controller'],'HomeController') ): ?>
            <a href="<?php echo e(url('/')); ?>" class="special-offer animated tada">
                <img id="specialoffer"  width="100px" src="<?php echo e(asset('img/chaferopen.png')); ?>" alt="" style=" width: 12rem; left: 20px;" >
            </a>
     <?php else: ?>

            <a href="<?php echo e(url('/special')); ?>" class="special-offer animated tada">
                <img id="specialoffer"  src="<?php echo e(asset('img/chafer.png')); ?>" alt="" >
            </a>
                
        <?php endif; ?>

         </div>
    <div class="language">

        <ul class="lang list-unstyled">

            <li><a href="<?php echo e(url('locale/en')); ?>"><img src="<?php echo e(asset('img/flags/united_kingdom_640.png')); ?>"></a> </li>
            <li><a href="<?php echo e(url('locale/he')); ?>"><img src="<?php echo e(asset('img/flags/israel_640.png')); ?>"></a> </li>

        </ul>

    </div>
</footer>

<!-- Trigger the modal with a button -->


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<?php $__env->startSection('scripts'); ?>
    <script>
//        $("#specialoffer").click(function(){
//            alert();
//        });
//        $( "#specialoffer" ).click(function() {
//            alert( "Handler for .click() called." );
//        });
    </script>
    <?php $__env->stopSection(); ?>