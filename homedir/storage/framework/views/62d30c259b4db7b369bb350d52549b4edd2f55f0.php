<?php $__env->startSection('header'); ?>
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-university"></i>
        </div>
        <div class="header-title">
            <h1>Products</h1>
            <small>Manage  Products .</small>
            <?php echo $__env->make('dashboard.partials.quick-links', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/')); ?>"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="<?php echo e(url('/dashboard')); ?>"> Dashboard</a></li>
               <li class="active">Products</li>
            </ol>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Products </h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="New Product" data-placement="bottom" href="<?php echo e(url('dashboard/item/create')); ?>" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add New</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name </th>
                                <th>Category </th>
                                <th>Description </th>
                                <th>Crated</th>
                                <th>Updated</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if($items->count()>0): ?>
                                <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $item_category = $item->category; ?>
                                <tr>
                                    <td><?php echo e($item->id); ?></td>
                                    <td><?php echo e($item->title); ?></td>
                                    <td><?php echo e($item_category?$item_category->text:''); ?></td>
                                    <td><?php echo e(str_limit($item->short_description,50)); ?></td>
                                    <td><?php echo e($item->created_at->diffForHumans()); ?></td>
                                    <td><?php echo e($item->updated_at->diffForHumans()); ?></td>
                                    <td>
                                       
                                        <a href="<?php echo e(url('dashboard/item/'.$item->id.'/edit')); ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <?php echo Form::open(['url'=>['/dashboard/item/'.$item->id],'method'=>'delete','style'=>'display:inline;']); ?>

                                            <button type="button" class="btn btn-danger btn-sm btn--delete--item" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        <?php echo Form::close(); ?>

                                        <button id="special<?php echo e($item->id); ?>"  onclick="makeSpecial(<?php echo e($item->id); ?>,<?php echo e($item->is_special); ?>);" type="button" class="btn btn-info btn-sm btn-special" data-toggle="tooltip" data-placement="right" title="Special "><i class="glyphicon <?php echo e($item->is_special?'glyphicon-star':"glyphicon-star-empty"); ?> " aria-hidden="true"></i></button>


                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php else: ?>
                                <tr>
                                    <td colspan="9">
                                        <p class="alert alert-warning text-center">
                                            No category found.
                                        </p>
                                    </td>
                                </tr>
                             <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix text-center">
                    <?php echo e($items->links()); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script>
        var special= '<?php echo e(url('dashboard/item/special')); ?>';
        function makeSpecial(item,val) {
            if (val==0) {
                var isspecial = 1;
            } else {
                var isspecial = 0;
            }

                $.post(special, {
                    id:item,
                    is_special:isspecial,
                    "_token": "<?php echo e(csrf_token()); ?>"

                }).done(function() {
                })
                    .fail(function() {
                        alert( "error" );
                    });

        };
        $(document).on('click','.btn--delete--item', function () {
            var form = $(this).parents('form:first');

            $.confirm({
                icon: 'fa fa-trash-o',
                title: 'Delete Item!',
                closeIcon: true,
                animation: 'rotate',
                closeAnimation: 'rotate',
                content: 'Are you want to delete this Item?',
                confirmButton: 'Yes',
                cancelButton: 'No',
                confirmButtonClass: 'btn-danger',
                cancelButtonClass: 'btn-info',
                confirm: function(){
                    form.submit();
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>