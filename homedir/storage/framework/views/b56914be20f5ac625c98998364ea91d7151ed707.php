<?php
    $SLIDER_IMAGES = $SITE_SETTING('site_slider_images');
?>
<?php $__env->startSection('header'); ?>
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-file-text" aria-hidden="true"></i>
        </div>
        <div class="header-title">
            <h1>Home Page</h1>
            <small>Manage site home page.</small>
            <?php echo $__env->make('dashboard.partials.quick-links', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/')); ?>"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="<?php echo e(url('/dashboard')); ?>"> Dashboard</a></li>
                <li class="active">Home Page</li>
            </ol>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Slider Settings</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <?php echo Form::open(['url'=>'dashboard/home-page/slider','method'=>'post','class'=>'form-horizontal','files'=>true,'enctype'=>'multipart/form-data']); ?>

                    <div class="row">
                        <div class="col-sm-5 col-sm-offset-3">

                           

                            <div class="slider-images">
                            <?php if(count($SLIDER_IMAGES) > 0): ?>
                                <?php $__currentLoopData = $SLIDER_IMAGES; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="form-group slider-image-item">
                                        <label for="image" class="col-sm-3 control-label">Slider Image <?php echo e(++$loop->index); ?></label>
                                        <div class="col-sm-7">
                                            <input type="file" name="image[<?php echo e($image->id); ?>]" class="input-image" accept="image/*">
                                            <div class="image-preview">
                                                <img style="width: 120px; height: 120px;" src="<?php echo e(asset('images/'.$image->value)); ?>" id="image_upload_preview" class="img-responsive img-thumbnail">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <button data-id="<?php echo e($image->id); ?>" type="button" class="btn btn-delete-slider-image btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="button" class="btn btn-default btn-add-more-slider-images"><i class="fa fa-plus-circle"></i> Add More</button>
                                </div>
                            </div>




                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <a href="<?php echo e(url('dashboard/home-page/slider')); ?>" class="btn btn-default">Cancel</a>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Update</button>
                                </span>
                        </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
    <?php echo Form::open(['url'=>'','method'=>'post','class'=>'form-horizontal frm-slider-image-deletion-form']); ?>

        <input name="id" id="slider-image-deletion-id" type="hidden">
        <?php echo e(method_field('DELETE')); ?>

    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $(document).ready(function(){
            $(document).on('click','.btn-add-more-slider-images',function(){
                var parent = $('.slider-images');
                var images_count = parent.find('.slider-image-item').length;
                var template = '<div class="form-group slider-image-item">' +
                                    '<label for="image" class="col-sm-3 control-label">Slider Image '+(++images_count)+'</label>' +
                                    '<div class="col-sm-7">' +
                                        '<input type="file" name="image[]" class="input-image" accept="image/*">' +
                                        '<div class="image-preview">' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="col-sm-2">'+
                                        '<button  type="button" class="btn btn-remove-slider-image btn-default btn-xs"><i class="fa fa-times-circle"></i></button>'+
                                    '</div>'+
                                '</div>';

                parent.append(template);
            });

            $(document).on('click','.btn-delete-slider-image', function () {
                var form = $('.frm-slider-image-deletion-form');
                var id = $(this).attr('data-id');
                form.find('#slider-image-deletion-id').attr('value',id);
                form.attr('action','<?php echo e(url('dashboard/home-page/slider')); ?>/'+id);
                $.confirm({
                    icon: 'fa fa-trash-o',
                    title: 'Delete Image!',
                    closeIcon: true,
                    animation: 'rotate',
                    closeAnimation: 'rotate',
                    content: 'Are you want to delete this image?',
                    confirmButton: 'Yes',
                    cancelButton: 'No',
                    confirmButtonClass: 'btn-danger',
                    cancelButtonClass: 'btn-info',
                    confirm: function(){
                        form.submit();
                    }
                });
            });
        });

        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        $(document).on('change','.input-image',function(){
            var preview_section = $(this).parents('.slider-image-item').find('.image-preview');
            preview_section.html('');
            readURL(this,function (e) {
                preview_section.append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
            });
        });


    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>