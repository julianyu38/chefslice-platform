<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $__env->yieldContent('meta'); ?>

    <title>
        <?php if (! empty(trim($__env->yieldContent('title')))): ?>
            <?php echo $__env->yieldContent('title'); ?>
        <?php else: ?>
            <?php echo e(env('APP_NAME',"Barbados Sotheby's")); ?> - Dashboard
        <?php endif; ?>
    </title>
    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="<?php echo e(asset('favicon.ico')); ?>" type="image/x-icon">
    
    <!-- Start Global Mandatory Style
    =====================================================================-->
    <!-- jquery-ui css -->
    <link href="<?php echo e(asset('assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap -->
    <link href="<?php echo e(asset('assets/backend/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap rtl -->
    <!--<link href="<?php echo e(asset('assets/backend/bootstrap-rtl/bootstrap-rtl.min.css')); ?>" rel="stylesheet" type="text/css"/>-->
    <!-- Lobipanel css -->
    <link href="<?php echo e(asset('assets/plugins/lobipanel/lobipanel.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- Pace css -->
    <link href="<?php echo e(asset('assets/plugins/pace/flash.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome -->
    <link href="<?php echo e(asset('assets/backend/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- Pe-icon -->
    <link href="<?php echo e(asset('assets/backend/pe-icon-7-stroke/css/pe-icon-7-stroke.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- Themify icons -->
    <link href="<?php echo e(asset('assets/backend/themify-icons/themify-icons.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- End Global Mandatory Style
    =====================================================================-->
    <!-- Start page Label Plugins
    =====================================================================-->

    <!-- Toastr css -->
    <link href="<?php echo e(asset('assets/plugins/toastr/toastr.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- Emojionearea -->
    <link href="<?php echo e(asset('assets/plugins/emojionearea/emojionearea.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- Monthly css -->
    <link href="<?php echo e(asset('assets/plugins/monthly/monthly.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- End page Label Plugins
    =====================================================================-->
    <!-- Start Theme Layout Style
    =====================================================================-->
    <!-- Theme style -->
    <link href="<?php echo e(asset('assets/backend/dist/css/styleBD.css')); ?>" rel="stylesheet" type="text/css"/>
    <!-- Theme style rtl -->
    <!--<link href="<?php echo e(asset('assets/backend/dist/css/styleBD-rtl.css')); ?>" rel="stylesheet" type="text/css"/>-->

    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/jquery-confirm/jquery-confirm.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/backend/style.css')); ?>">
    <!-- End Theme Layout Style
    =====================================================================-->
    <?php echo $__env->yieldContent('styles'); ?>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
    </script>
</head>