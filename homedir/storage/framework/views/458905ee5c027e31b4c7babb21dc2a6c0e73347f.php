<?php $__env->startSection('content'); ?>
    <?php $allCategories = $categories->count(); ?>
    <div id="container" >



        
            
                 
            
        

        <?php if($categories->count()>0): ?>
            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <div class="ex-item">
                    <div data-content="<?php echo e(url("$category->slug")); ?>" class="category-link" style="height: 100%; position: absolute; width: 100%;">
                        
                        <div style="    top: 42%;
    position: relative;
    text-align: center;
    width: 100%;
    font-size: 6rem;
    color: #000;
    border: 1px solid #ddd;
    background: rgba(255,255,255,.7);
    font-weight: bold;
    padding: 20px 0;"
                             class="title"><?php echo e($category->text); ?>


                        </div>
                    </div>

                    <div data-content="<?php echo e(url("$category->slug")); ?>" class="categoryimage category-link"
                         style=" width: 700px; background-repeat: no-repeat; background-position: center; background-size: cover; background-image: url(<?php echo e(asset('media/categories/images/'.$category->icon)); ?>)">
                    </div>

                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
        


    </div>

    <?php $mean = $allCategories / 2; ?>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

    <script>
        var categories = <?php echo json_encode($categories) ?>;
        var mean = <?php echo (int)$mean ?>;

    </script>

    <script src="<?php echo e(asset('theme/scripts/jquery-2.1.4.min.js')); ?>"></script>
    <script src="<?php echo e(asset('theme/scripts/mustache.min.js')); ?>"></script>
    <script src="<?php echo e(asset('theme/scripts/theta-carousel.min.js')); ?>"></script>
    <?php if(!app('mobile-detect')->isMobile()) : ?>

    <script src="<?php echo e(asset('theme/theta/bezier2/scripts/example.js')); ?>"></script>
    <?php endif; ?>
    <?php if (app('mobile-detect')->isTablet()) : ?>
    <script src="<?php echo e(asset('theme/theta/bezier2/scripts/tab/example.js')); ?>"></script>
    <?php endif; ?>
    <?php if (app('mobile-detect')->isMobile() && !app('mobile-detect')->isTablet()) : ?>
    <script src="<?php echo e(asset('theme/theta/bezier2/scripts/mobile/example.js')); ?>"></script>
    <?php endif; ?>

    <script src="<?php echo e(asset('theme/scripts/jquery.popupoverlay.js')); ?>"></script>
    <script>
        function configure() {

        }
    </script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        #container {
            top: 150px; bottom: 0px; position: absolute; right: 0px; left: 0px;
        }
        img {
            vertical-align: middle;
        }

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }



        .ex-item .round-button {
            font-size: 45px;
            height: 200px;
            width: 200px;
            background: rgba(71, 160, 71, 0.65);
            position: relative;
            top: -100px;
            border-radius: 100px;
            padding-left: 30px;
            padding-top: 27px;
            right: -600px;
            cursor: pointer;
        }

        .ex-item:hover .round-button {
            font-size: 64px;
            height: 300px;
            width: 300px;
            background: rgba(71, 160, 71, 0.85);
            position: relative;
            right: -550px;
            top: -150px;
            border-radius: 150px;
            padding-left: 45px;
            padding-top: 41px;
        }

        .button-hover-transition {
            -webkit-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
            -moz-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
            -ms-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
            -o-transition: all 0.5s cubic-bezier(0.42, 0, 0.15, 1.61);
        }
    </style>
    <?php if(!app('mobile-detect')->isMobile()) : ?>
    <style>
        #container {
            top: 150px;
        }
        .ex-item {
            height: 815px;
            width: 715px;
            display: none;
            /*overflow: hidden;*/
            border: 7px solid white;
            background-color: #5EBDC4;
        }
        .categoryimage {
            height: 800px;

        }
    </style>

    <?php endif; ?>

    <?php if (app('mobile-detect')->isTablet()) : ?>
    <style>
        #container {
            top: 85px;
        }
        .ex-item {
            height: 650px;
            width: 715px;
            display: none;
            /*overflow: hidden;*/
            border: 7px solid white;
            background-color: #5EBDC4;
        }
        .categoryimage {
            height: 640px;

        }
    </style>
    <?php endif; ?>
    <?php if (app('mobile-detect')->isMobile() && !app('mobile-detect')->isTablet()) : ?>
    <style>
        #container {
            top: 85px;
        }
        .ex-item {
            height: 515px;
            width: 715px;
            display: none;
            /*overflow: hidden;*/
            border: 7px solid white;
            background-color: #5EBDC4;
        }
        .categoryimage {
            height: 500px;

        }
    </style>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>