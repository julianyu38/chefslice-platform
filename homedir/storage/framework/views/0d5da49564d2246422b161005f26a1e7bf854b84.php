<?php $__env->startSection('header'); ?>
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-users"></i>
        </div>
        <div class="header-title">
            <h1>Users</h1>
            <small>Manage Admins.</small>
            <?php echo $__env->make('dashboard.partials.quick-links', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/')); ?>"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="<?php echo e(url('/dashboard')); ?>"> Dashboard</a></li>
                <li class="active">Users</li>
            </ol>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>New Admins User </h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="View Admins" data-placement="bottom" href="<?php echo e(url('dashboard/user')); ?>" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> View Admins</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">

                    <?php echo Form::open(['url'=>['dashboard/user'],'method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data']); ?>

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('name', 'Name *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::text('name',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'User Name ...']); ?>

                                    <?php if($errors->has('name')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('user_name') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('user_name', 'Subdomain *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::text('user_name',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'User Subdomain ...']); ?>

                                    <?php if($errors->has('user_name')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('user_name')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('email', 'Email *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::email('email',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'User Email ...']); ?>

                                    <?php if($errors->has('email')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('password', 'Password', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::text('password',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Password ...']); ?>

                                    <?php if($errors->has('password')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('mobile') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('mobile', 'Mobile', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::number('mobile',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Mobile no ...']); ?>

                                    <?php if($errors->has('mobile')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('mobile')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo e($errors->has('image') ? ' has-error' : ''); ?>">
                                <label for="image" class="col-sm-3 control-label"> Image</label>
                                <div class="col-sm-9">
                                    <input type="file" name="image" class="input-image" accept="image/*">
                                    <?php if($errors->has('image')): ?>
                                        <span class="help-block">
                                                    <strong><?php echo e($errors->first('image')); ?></strong>
                                                </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="image-preview col-sm-offset-3">
                                </div>
                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <button type="reset" class="btn btn-default">Cancel</button>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Create</button>
                                </span>
                        </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

    <script type="text/javascript">
        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }
        $(function() {
            $(".input-image").change(function () {
                $('.image-preview').html('');
                readURL(this,function (e) {
                    $('.image-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
                });
            });

        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>