
<?php $__env->startSection('header'); ?>
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-th"></i>
        </div>
        <div class="header-title">
            <h1>Pages</h1>
            <small>Manage Pages.</small>
            <?php echo $__env->make('dashboard.partials.quick-links', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/')); ?>"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="<?php echo e(url('/dashboard')); ?>"> Dashboard</a></li>
                <li class="active">Page</li>
            </ol>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>New Page </h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="View Pages" data-placement="bottom" href="<?php echo e(url('dashboard/page')); ?>" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> View Pages</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <?php echo Form::open(['url'=>'dashboard/page','method'=>'post','class'=>'form-horizontal']); ?>

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <?php if($users->count()>0): ?>
                            <div class="form-group <?php echo e($errors->has('user_id') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('user_id', 'User', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <select required class="form-control" name="user_id" id="user_id">
                                        <option value="">Page User</option>
                                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option <?php echo old('user_id')==trim($id)?'selected="selected"':""; ?> value="<?php echo e(trim($id)); ?>"><?php echo e($name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php if($errors->has('user_id')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('user_id')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php endif; ?>
                            <div class="form-group <?php echo e($errors->has('text') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('title', 'Title *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::text('title',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Page Title']); ?>

                                    <?php if($errors->has('title')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('slug') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('slug', 'Slug *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::text('slug',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category Slug']); ?>

                                    <?php if($errors->has('slug')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('slug')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>


                            
                                
                                
                                    
                                    
                                        
                                                        
                                                    
                                    
                                    
                                        
                                        
                                    
                                

                            


                        </div>


                        <div class="col-sm-9 col-sm-offset-2">
                            <div class="form-group <?php echo e($errors->has('body') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('body', 'Page Body *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9 ">
                                    <?php echo Form::textarea('body',$value= null, $attributes = ['id'=>'bodytext','class'=>'form-control summernote' ,'placeholder'=>'Page Body']); ?>

                                    <?php if($errors->has('body')): ?>
                                        <span class="help-block">
                                                    <strong><?php echo e($errors->first('body')); ?></strong>
                                             </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <button type="reset" class="btn btn-default">Cancel</button>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Create</button>
                                </span>
                        </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>
    <!-- summernote css -->
    <link href="<?php echo e(asset('assets/plugins/summernote/summernote.css')); ?>" rel="stylesheet" type="text/css"/>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <!-- summernote js -->
    <script src="<?php echo e(asset('assets/plugins/summernote/summernote.min.js')); ?>" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            "use strict"; // Start of use strict
            //summernote
            $('textarea.summernote').summernote({
                height: 300, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: true                  // set focus to editable area after initializing summernote
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>