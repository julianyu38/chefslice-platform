<?php
$GROUP = 'site_map';
    $INFO = $SITE_SETTING('site_map');
?>
<?php $__env->startSection('header'); ?>
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-file-text" aria-hidden="true"></i>
        </div>
        <div class="header-title">
            <h1>Home Page</h1>
            <small>Manage site home page.</small>
            <?php echo $__env->make('dashboard.partials.quick-links', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/')); ?>"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="<?php echo e(url('/dashboard')); ?>"> Dashboard</a></li>
                <li class="active">Home Page</li>
            </ol>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Home Page Map Settings</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <?php echo Form::open(['url'=>'dashboard/home-page/update_settings','method'=>'post','class'=>'form-horizontal','files'=>true,'enctype'=>'multipart/form-data']); ?>

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <input type="hidden" name="_group" value="site_map">
                            <div class="form-group <?php echo e($errors->has('site_map') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('site_map', "Enable Map", ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <select class="form-control select2" data-placeholder="site_map" name="site_map" id="site_map">

                                        <option <?php echo e($sections->site_map==1?'Selected':''); ?> value="1">Enabled</option>
                                        <option <?php echo e($sections->site_map==0?'Selected':''); ?> value="0">Disabled</option>
                                    </select>
                                    <?php if($errors->has('site_map')): ?>
                                        <span class="help-block">
                                                <strong><?php echo e($errors->first('site_map')); ?></strong>
                                            </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php if(count($INFO) >0): ?>
                                <?php $__currentLoopData = $INFO; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$site_info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($key == 'image' || $key=='map_marker'): ?>
                                        <div class="form-group slider-image-item">
                                            <label for="<?php echo e($site_info->title); ?>" class="col-sm-3 control-label"><?php echo e($site_info->title); ?></label>
                                            <div class="col-sm-9">
                                                <input type="file" id="<?php echo e($site_info->title); ?>" name="<?php echo e($key); ?>" class="input-image" accept="image/*">
                                                <div class="image-preview">
                                                    <img style="width: 120px; height: 120px;" src="<?php echo e(asset('assets/home/img/'.$site_info->value)); ?>" id="image_upload_preview" class="img-responsive img-thumbnail">
                                                </div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                    <div class="form-group <?php echo e($errors->has($key) ? ' has-error' : ''); ?>">
                                        <?php echo Form::label($key, $site_info->title, ['class' => 'col-sm-3 control-label']); ?>

                                        <div class="col-sm-9">
                                            <?php echo Form::text($key,$value= $site_info->value, $attributes = ['class'=>'form-control','placeholder'=>$site_info->title]); ?>

                                            <?php if($errors->has($key)): ?>
                                                <span class="help-block">
                                        <strong><?php echo e($errors->first($key)); ?></strong>
                                    </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                   <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>





                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <a href="<?php echo e(url('dashboard/home-page/site-info')); ?>" class="btn btn-default">Cancel</a>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Update</button>
                                </span>
                        </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>


        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        $(document).on('change','.input-image',function(){
            var preview_section = $(this).parents('.slider-image-item').find('.image-preview');
            preview_section.html('');
            readURL(this,function (e) {
                preview_section.append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
            });
        });


    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>