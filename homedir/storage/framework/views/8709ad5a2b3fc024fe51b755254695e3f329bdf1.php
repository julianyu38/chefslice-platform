<!-- Menu Start -->

<section class="koc-menu koc-menu--circle koc-menu--top-right; koc-menu--bottom-left; koc-menu--bottom-right;" data-spy="affix" data-offset-top="197">
    <input type="checkbox" id="koc-menu__active"/>
    <label for="koc-menu__active" class="koc-menu__active">

        <!-- Toggle Start -->

        <div class="koc-menu__toggle">
            <div class="icon">
                <div class="hamburger"></div>
            </div>
        </div>

        <!-- Toggle End -->

        <input type="radio" name="arrow--up" id="degree--up-0"/>
        <input type="radio" name="arrow--up" id="degree--up-1"/>
        <input type="radio" name="arrow--up" id="degree--up-2"/>

        <!-- Listings Start -->

        <div class="koc-menu__listings">
            <ul class="circle">

                <!-- Empty Start -->

                <li>
                    <div class="placeholder">
                        <div class="upside"><a href="#" class="button">&nbsp</a></div>
                    </div>
                </li>

                <!-- Empty End -->


                <!-- Ternary Start -->
            <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="<?php echo e(url('/page/'.$menu->slug)); ?>" class="button"><i class="<?php echo e($menu->icon); ?>"></i></a>
                        </div>
                    </div>
                </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



            </ul>
        </div>

        <!-- Listings End -->


        <!-- Arrow Container Start -->

        <div class="koc-menu__arrow koc-menu__arrow--top">
            <ul>
                <li>
                    <label for="degree--up-0">
                        <div class="arrow"></div>
                    </label>
                    <label for="degree--up-1">
                        <div class="arrow"></div>
                    </label>
                    <label for="degree--up-2">
                        <div class="arrow"></div>
                    </label>
                </li>
            </ul>
        </div>

        <!-- Arrow Container End -->

    </label>
</section>
<style>
    /* Note: Try to remove the following lines to see the effect of CSS positioning */
    .affix {
        top: 0;
        width: 100%;
    }

    .affix + .container-fluid {
        padding-top: 70px;
    }
</style>

<!-- Menu End -->