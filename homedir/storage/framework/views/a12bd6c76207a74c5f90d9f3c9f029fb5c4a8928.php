
<div class="row">
    <div class="col-md-9">
        <legend>Static Links</legend>
        <div class="list-group">
            <a target="_blank"
               href="<?php echo e(url(env('APP_URL_PROTOCOL').'://'.auth()->user()->user_name.env('SESSION_DOMAIN').'/?heatmap=1')); ?>"
               class="list-group-item">Home Page</a>
            <a target="_blank"
               href="<?php echo e(url(env('APP_URL_PROTOCOL').'://'.auth()->user()->user_name.env('SESSION_DOMAIN').'/special?heatmap=1')); ?>"
               class="list-group-item">Special Items</a>
        </div>
        <legend>Categories</legend>
        <div class="list-group">
            <?php if($categories->count()>0): ?>
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a target="_blank" href="<?php echo e(url("$category->slug?heatmap=1")); ?>"
                       class="list-group-item"><?php echo e($category->text); ?></a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        </div>
    </div>
</div>