<!DOCTYPE html>
<html lang="en">


<?php echo $__env->make('partials.restaurant.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body  id="heatmaptag"  style="background-repeat: no-repeat; background-size: cover;
        background-attachment: fixed; background-image: url(<?php echo e(asset('media/design/background/'.$settings->id.'/'.$settings->background)); ?>)">
<!-- main wrapper -->
<div id="wrapper">
    <!-- header -->
    <header>
        <div class="row">
            <div class="col-sm-3">
                <?php echo $__env->make('partials.restaurant.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="col-sm-6 text-center __app-logo">
                <a  href="<?php echo e(url('')); ?>">
                    <img style="width: 220px;" src="<?php echo e(asset('media/design/logo/'.$settings->id.'/'.$settings->logo)); ?>">
                </a>
            </div>
            <div class="col-sm-3">
                    <?php echo $__env->yieldContent('back-button'); ?>

            </div>
        </div>


        <!-- filters -->

       
       
    </header>
<?php echo $__env->yieldContent('content'); ?>
<!-- /main wrapper -->
    <!-- footer -->
    <?php echo $__env->make('partials.restaurant.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;



<!-- /footer -->
</div>
<div style="clear: both"></div>
<?php echo $__env->make('partials.restaurant.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>


