<?php $__env->startSection('content'); ?>
    <div id="container" >

        <?php $allCategories = $categories->count(); ?>
        <?php if($categories->count()>0): ?>
            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <div class="ex-item">
                    <img data-content="<?php echo e(url("$category->slug")); ?>" class="category-link"  src="<?php echo e(asset('media/categories/images/'.$category->icon)); ?>" />
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

   </div>

    <?php $mean = $allCategories / 2; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        #container {
            top: 150px; bottom: 0px; position: absolute; right: 0px; left: 0px;
        }
        img {
            vertical-align: middle;
        }

        .ex-item {
            height: 347px;
            width: 260px;
            display: none;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .ex-item img {
            width: 100%;
            height: 100%;

        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script>
        var categories = <?php echo json_encode($categories) ?>;
        var mean = <?php echo (int)$mean ?>;

    </script>
    <script src="<?php echo e(asset('theme/theta/cubical-parabola/scripts/jquery-2.1.4.min.js')); ?>"></script>
    <script src="<?php echo e(asset('theme/theta/cubical-parabola/scripts/theta-carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('theme/theta/cubical-parabola/scripts/example.js')); ?>"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>