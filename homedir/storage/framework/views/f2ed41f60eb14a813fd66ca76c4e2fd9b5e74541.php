<?php $__env->startSection('content'); ?>
	<section class="contact-block" id="contact-form">
		<div class="container">
			<?php echo $page->body; ?>

		</div>
	</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>