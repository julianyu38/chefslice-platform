
<?php $__env->startSection('header'); ?>
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-bar-chart-o"></i>
        </div>
        <div class="header-title">
            <h1>Analytics</h1>
            <small>Things related to site analytics.</small>
            <?php echo $__env->make('dashboard.partials.quick-links', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/')); ?>"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="<?php echo e(url('/dashboard')); ?>"> Dashboard</a></li>
                <li class="active">Analytics</li>
            </ol>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Visitor by Browsers</h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <select id="num_days">
                            <option  value="30">Last 30 days</option>
                            <option <?php echo isset($_GET['days']) && $_GET['days'] == 1?'selected=""':''; ?> value="1">Today</option>
                            <option <?php echo isset($_GET['days']) && $_GET['days'] == 7?'selected=""':''; ?> value="7">Last week</option>
                            <option <?php echo isset($_GET['days']) && $_GET['days'] == 90?'selected=""':''; ?> value="90">Last 3 months</option>
                            <option <?php echo isset($_GET['days']) && $_GET['days'] == 180?'selected=""':''; ?> value="180">Last 6 months</option>
                            <option <?php echo isset($_GET['days']) && $_GET['days'] == 365?'selected=""':''; ?> value="365">Last 1 Year</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">

                    <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Browser Name </th>
                                            <th>Number Of Sessions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(count($browsers)>0): ?>
                                            <?php $__currentLoopData = $browsers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$browser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($key+1); ?></td>
                                                    <td><?php echo e($browser['browser']); ?></td>
                                                    <td><?php echo e($browser['sessions']); ?></td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                            <tr>
                                                <td colspan="3">
                                                    <p class="alert text-center alert-warning">
                                                        No data found.
                                                    </p>
                                                </td>
                                            </tr>
                                        <?php endif; ?>    
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script>
    $(document).ready(function(){
            $('#num_days').change(function () {
                    window.location = '<?php echo e(url('dashboard/analytic/browsers')); ?>?days='+($(this).val());
            });
    });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>