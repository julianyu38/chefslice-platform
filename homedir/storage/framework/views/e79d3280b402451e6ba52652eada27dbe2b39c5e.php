<aside class="main-sidebar">
    <!-- sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel text-center">
            <div class="image">
                <?php if(!empty((auth()->user()->image)) && file_exists(public_path('images/users/'.(auth()->user()->image)))): ?>
                    <img class="img-circle" src="<?php echo e(asset('images/users/'.auth()->user()->image)); ?>"
                         alt="<?php echo e(auth()->user()->name); ?>">
                <?php else: ?>
                    <img class="img-circle" src="<?php echo e(asset('images/not-found.jpg')); ?>" alt="<?php echo e(auth()->user()->name); ?>">
                <?php endif; ?>
            </div>
            <div class="info">
                <p><?php echo e(auth()->user()->name); ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn"><i
                                            class="fa fa-search"></i></button>
                            </span>
            </div>
        </form> <!-- /.search form -->
        <!-- sidebar menu -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="<?php echo e(request()->segment(2)==''?'active':''); ?>">
                <a href="<?php echo e(url('/dashboard')); ?>"><i class="ti-home"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview <?php echo e((request()->segment(2)=='category') ?'active':''); ?>">
                <a href="#">
                    <i class="fa fa-th"></i> <span>Categories</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    <span class="clearfix"></span>
                </a>
                <ul class="treeview-menu ">
                    <li class="<?php echo e((request()->segment(2)=='category' && request()->segment(3)=='create') ?'active':''); ?>"><a href="<?php echo e(url('dashboard/category/create')); ?>">Add New</a></li>
                    <li class="<?php echo e((request()->segment(2)=='category' && request()->segment(3)=='') ?'active':''); ?>"><a href="<?php echo e(url('dashboard/category')); ?>">View All</a></li>
                </ul>
            </li>
            <li class="treeview <?php echo e((request()->segment(2)=='item') ?'active':''); ?>">
                <a href="#">
                    <i class="fa fa-list"></i> <span>Products</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    <span class="clearfix"></span>
                </a>
                <ul class="treeview-menu ">
                    <li class="<?php echo e((request()->segment(2)=='item' && request()->segment(3)=='create') ?'active':''); ?>"><a href="<?php echo e(url('dashboard/item/create')); ?>">Add New</a></li>
                    <li class="<?php echo e((request()->segment(2)=='item' && request()->segment(3)=='') ?'active':''); ?>"><a href="<?php echo e(url('dashboard/item')); ?>">View All</a></li>
                </ul>
            </li>



            <?php if(auth()->user()->user_name === 'admin'): ?>
            
             <li class="treeview <?php echo e((request()->segment(2)=='home-page') ?'active':''); ?>">
                    <a href="#">
                        <i class="fa fa-file-text" aria-hidden="true"></i> <span>Home Page</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        <span class="clearfix"></span>
                    </a>
                    <ul class="treeview-menu ">
                        <li class="<?php echo e((request()->segment(2)=='home-page' && request()->segment(3)=='slider') ?'active':''); ?>">
                            <a href="<?php echo e(url('dashboard/home-page/slider')); ?>">Slider Settings</a>
                        </li>
                        <li class="<?php echo e((request()->segment(2)=='home-page' && request()->segment(3)=='menu') ?'active':''); ?>">
                            <a href="<?php echo e(url('dashboard/home-page/menu')); ?>">Site Menu</a>
                        </li>
                        <li class="<?php echo e((request()->segment(2)=='home-page' && request()->segment(3)=='site-info') ?'active':''); ?>">
                            <a href="<?php echo e(url('dashboard/home-page/site-info')); ?>">Site Info</a>
                        </li>
                        <li class="<?php echo e((request()->segment(2)=='home-page' && request()->segment(3)=='site-about') ?'active':''); ?>">
                            <a href="<?php echo e(url('dashboard/home-page/site-about')); ?>">"About" Section</a>
                        </li>
                        <li class="<?php echo e((request()->segment(2)=='home-page' && request()->segment(3)=='services') ?'active':''); ?>">
                            <a href="<?php echo e(url('dashboard/home-page/services')); ?>">"Services" Section</a>
                        </li>
                        <li class="<?php echo e((request()->segment(2)=='home-page' && request()->segment(3)=='clients') ?'active':''); ?>">
                            <a href="<?php echo e(url('dashboard/home-page/clients')); ?>">"Clients" Section</a>
                        </li>
                        <li class="<?php echo e((request()->segment(2)=='home-page' && request()->segment(3)=='how-work') ?'active':''); ?>">
                            <a href="<?php echo e(url('dashboard/home-page/how-work')); ?>">"How it Works" Section</a>
                        </li>
                        <li class="<?php echo e((request()->segment(2)=='home-page' && request()->segment(3)=='pricing') ?'active':''); ?>">
                            <a href="<?php echo e(url('dashboard/home-page/pricing')); ?>">"Pricing" Section</a>
                        </li>
                        <li class="<?php echo e((request()->segment(2)=='home-page' && request()->segment(3)=='map') ?'active':''); ?>">
                            <a href="<?php echo e(url('dashboard/home-page/map')); ?>">Map Settings</a>
                        </li>


                    </ul>
                </li>

            <li class="treeview <?php echo e((request()->segment(2)=='user') ?'active':''); ?>">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Users</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    <span class="clearfix"></span>
                </a>
                <ul class="treeview-menu ">
                    <li class="<?php echo e((request()->segment(2)=='user' && request()->segment(3)=='create') ?'active':''); ?>"><a href="<?php echo e(url('dashboard/user/create')); ?>">Add New</a></li>
                    <li class="<?php echo e((request()->segment(2)=='user' && request()->segment(3)=='') ?'active':''); ?>"><a href="<?php echo e(url('dashboard/user')); ?>">View All</a></li>
                </ul>
            </li>

            


            
                <li class="treeview <?php echo e(request()->segment(2) =='analytic'?'active':''); ?>">
                    <a href="#">
                        <i class="fa fa-bar-chart-o"></i> <span>Google Analytic</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        <span class="clearfix"></span>

                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo e((request()->segment(2)=='analytic' && request()->segment(3)=="countries") ?'active':''); ?>"><a href="<?php echo e(url('dashboard/analytic/countries')); ?>">Sessions by Country</a></li>
                        <li class="<?php echo e((request()->segment(2)=='analytic' && request()->segment(3)=="browsers") ?'active':''); ?>"><a href="<?php echo e(url('dashboard/analytic/browsers')); ?>">Browser</a></li>
                        <li class="<?php echo e((request()->segment(2)=='analytic' && request()->segment(3)=="os") ?'active':''); ?>"><a href="<?php echo e(url('dashboard/analytic/os')); ?>">Operating System</a></li>
                        <li class="<?php echo e((request()->segment(2)=='analytic' && request()->segment(3)=="sources") ?'active':''); ?>"><a href="<?php echo e(url('dashboard/analytic/sources')); ?>">Traffic Sources</a></li>
                        <li class="<?php echo e((request()->segment(2)=='analytic' && request()->segment(3)=="mobile") ?'active':''); ?>"><a href="<?php echo e(url('dashboard/analytic/mobile')); ?>">Mobile Traffic</a></li>
                        <li class="<?php echo e((request()->segment(2)=='analytic' && request()->segment(3)=="sites") ?'active':''); ?>"><a href="<?php echo e(url('dashboard/analytic/sites')); ?>">Referring Sites</a></li>
                        <li class="<?php echo e((request()->segment(2)=='analytic' && request()->segment(3)=="keywords") ?'active':''); ?>"><a href="<?php echo e(url('dashboard/analytic/keywords')); ?>">Keywords</a></li>
                        <li class="<?php echo e((request()->segment(2)=='analytic' && request()->segment(3)=="landing_pages") ?'active':''); ?>"><a href="<?php echo e(url('dashboard/analytic/landing_pages')); ?>">Top Landing Pages</a></li>
                        <li class="<?php echo e((request()->segment(2)=='analytic' && request()->segment(3)=="exit_pages") ?'active':''); ?>"><a href="<?php echo e(url('dashboard/analytic/exit_pages')); ?>">Top Exit Pages</a></li>
                    </ul>
                </li>

           



            <li class="treeview <?php echo e(request()->segment(2) =='currency'?'active':''); ?>">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span>Settings</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo e(url('dashboard/settings/dashboard')); ?>">Dashboard</a></li>
                    <li><a href="<?php echo e(url('dashboard/settings/home_page')); ?>">Home Settings</a></li>
                    <li><a href="<?php echo e(url('dashboard/settings/info')); ?>">General Information</a></li>

                </ul>
            </li>
            <?php else: ?>


                <li class="treeview <?php echo e(request()->segment(2) =='page'?'active':''); ?>">
                    <a href="#">
                        <i class="fa fa-file-text" aria-hidden="true"></i> <span>Web Pages</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo e((request()->segment(2)=='page' && request()->segment(3)=="create") ?'active':''); ?>"><a href="<?php echo e(url('dashboard/page/create')); ?>">New Page</a></li>
                        <li class="<?php echo e((request()->segment(2)=='page' && request()->segment(3)=="") ?'active':''); ?>"><a href="<?php echo e(url('dashboard/page')); ?>">All Page</a></li>
                    </ul>
                </li>
                <li class="treeview <?php echo e(request()->segment(2) =='page'?'active':''); ?>">
                    <a href="#">
                        <i class="fa fa-file-text" aria-hidden="true"></i> <span>Themes</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo e((request()->segment(2)=='theme' ) ?'active':''); ?>">
                            <a href="<?php echo e(url('dashboard/theme')); ?>">Settings</a></li>

                    </ul>
                </li>
            <?php endif; ?>
        </ul>
    </div> <!-- /.sidebar -->
</aside>