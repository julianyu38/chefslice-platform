<!-- Contact -->
<?php $SITE_INFO = $SITE_SETTING('site_info'); ?>
<footer>
    <section id="contact">
        <div class="contact">
            <div class="container-fluid">
                 <?php echo $__env->yieldContent('footer'); ?>
                <div class="subfooter text-center">
                    <div class="row no-column-space equal-height-columns valign__middle">
                        <div class="col-sm-12 text-center page-scroll">
                            <a class="subfooter__logo" href="#body">
                                <img class="subfooter__logo__img g-mb-20" src="<?php echo e(asset('assets/home/img/').'/'.$SITE_INFO['logo']->value); ?>" alt="Logo"><br>
                            </a>
                            <p class="subfooter__copyright">© 2017 All right reserved.<span class="subfooter__copyright--pink"><a href="#">Chef Slice</a></span>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>
<!-- End Contact -->