<?php $__env->startSection('header'); ?>
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-th"></i>
        </div>
        <div class="header-title">
            <h1>Categories</h1>
            <small>Manage categories.</small>
            <?php echo $__env->make('dashboard.partials.quick-links', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <ol class="breadcrumb">
                <li><a href="<?php echo e(url('/')); ?>"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="<?php echo e(url('/dashboard')); ?>"> Dashboard</a></li>
                <li class="active">Category</li>
            </ol>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-8">
                        <h4>New Category </h4>
                    </div>

                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="View Categories" data-placement="bottom" href="<?php echo e(url('dashboard/category')); ?>" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> View Categories</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <?php echo Form::open(['url'=>'dashboard/category','method'=>'post','class'=>'form-horizontal','enctype'=>'multipart/form-data']); ?>

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <?php if($users->count()>0): ?>
                            <div class="form-group <?php echo e($errors->has('user_id') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('user_id', 'User', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <select required class="form-control" name="user_id" id="user_id">
                                        <option value="">Category User</option>
                                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option <?php echo old('user_id')==trim($id)?'selected="selected"':""; ?> value="<?php echo e(trim($id)); ?>"><?php echo e($name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php if($errors->has('user_id')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('user_id')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php endif; ?>
                            <div class="form-group <?php echo e($errors->has('text') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('text', 'Name *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::text('text',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category Name']); ?>

                                    <?php if($errors->has('text')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('text')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            
                                <div class="form-group <?php echo e($errors->has('locale') ? ' has-error' : ''); ?>">
                                    <?php echo Form::label('locale', 'Select Language', ['class' => 'col-sm-3 control-label']); ?>

                                    <div class="col-sm-9">
                                        <select required class="form-control" name="locale" id="locale">
                                            <option value="en">English</option>
                                            <option value="he">Hebrew</option>

                                        </select>
                                        <?php if($errors->has('locale')): ?>
                                            <span class="help-block">
                                        <strong><?php echo e($errors->first('locale')); ?></strong>
                                    </span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                            <div class="form-group <?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
                                <?php echo Form::label('description', 'Description *', ['class' => 'col-sm-3 control-label']); ?>

                                <div class="col-sm-9">
                                    <?php echo Form::textarea('description',$value= null, $attributes = ['class'=>'form-control','placeholder'=>'Category description']); ?>

                                    <?php if($errors->has('description')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('description')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo e($errors->has('icon') ? ' has-error' : ''); ?>">
                                <label for="image" class="col-sm-3 control-label"> Image</label>
                                <div class="col-sm-9">
                                    <input type="file" name="icon" class="input-image" accept="image/*">
                                    <?php if($errors->has('icon')): ?>
                                        <span class="help-block">
                                                        <strong><?php echo e($errors->first('icon')); ?></strong>
                                                    </span>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <div class="logo-preview">
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                                <span class="pull-right">
                                     <button type="reset" class="btn btn-default">Cancel</button>
                                     &nbsp;
                                     <button type="submit" class="btn btn-info ">Create</button>
                                </span>
                        </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        function readURL(input,callback) {
            if (input.files && input.files.length>0) {
                for (var i=0;i<input.files.length;i++){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        callback(e);
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        $(".input-image").change(function () {
            $('.logo-preview').html('');
            readURL(this,function (e) {
                $('.logo-preview').append('<img style="width: 120px; height: 120px;" src="'+ e.target.result +'" id="image_upload_preview" class="img-responsive img-thumbnail">')
            });
        });

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>