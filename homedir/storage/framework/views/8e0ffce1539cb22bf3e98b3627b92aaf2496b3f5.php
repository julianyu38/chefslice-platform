<head>
    <title>Restaurant | Unify - Responsive Website Template</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php if (! empty(trim($__env->yieldContent('title')))): ?>
        <?php echo $__env->yieldContent('title'); ?>
        <?php else: ?>
            <?php echo e(env('APP_NAME',"Barbados Sotheby's")); ?> - Danial
        <?php endif; ?>
    </title>
    <?php echo $__env->yieldContent('meta'); ?>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="../favicon.ico">

    <!-- Web Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Niconne' rel='stylesheet' type='text/css'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/bootstrap/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/home/css/app.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/home/css/blocks.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/home/css/one.style.css')); ?>">

    <!-- CSS Footer -->
    <!-- <link rel="stylesheet" href="assets/css/footers/footer-v7.css"> -->

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/line-icons-pro/styles.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/line-icons/line-icons.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/font-awesome/css/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/owl-carousel2/assets/owl.carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/hover-effects/css/custom-hover-effects.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/shhos/shhos.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/pace/pace-flash.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/slick/slick.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/master-slider/masterslider/style/masterslider.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/master-slider/u-styles/testimonials-1.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/plugins/master-slider/u-styles/promo-1.css')); ?>">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/home/css/restaurant.style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/home/css/global.css')); ?>">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/home/css/custom.css')); ?>">
    <?php echo $__env->yieldContent('styles'); ?>
            <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
    </script>

</head>
