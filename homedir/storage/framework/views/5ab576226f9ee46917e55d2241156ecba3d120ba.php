<!-- Scripts -->

<!-- jQuery Easing -->
<script src="<?php echo e(asset('theme/scripts/jquery-2.1.4.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/hydrogen/js/jquery.easing.1.3.js')); ?>"></script>
<script src="<?php echo e(asset('/assets/plugins/bootstrap/js/bootstrap.min.js')); ?>"></script>



<script src="<?php echo e(asset('assets/plugins/heatmap.min.js')); ?>"></script>
<?php if($settings->heatmap): ?>
    <?php echo $__env->make('partials.restaurant.heatmap', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php endif; ?>




<?php echo $__env->yieldContent('scripts'); ?>
<script>
    var heatmapurl= '<?php echo e(url('heatmap')); ?>';

    $(document).ready(function() {
        heatmapdot = <?php echo json_encode($heatmap)?>;
        $('body').click(function(e){
//            e.stopPropagation();
            var x = e.clientX;
            var y = e.clientY;
            $.post(heatmapurl, {
                x:x,
                y:y,
                "_token": "<?php echo e(csrf_token()); ?>",
                location:escape(document.location.pathname)
            });
        });
    });





</script>

<script>
    $(document).ready(function(){
        var el = $('.special-offer img');

        var animation = setInterval(function(){
            if(el.hasClass('animated') && el.hasClass('tada')){
                el.removeClass('animated');
                el.removeClass('tada');
                //el.removeClass('infinite');

            }else {
                el.addClass('animated');
                el.addClass('tada');
                // el.addClass('infinite');
            }
        },3000);

    });
</script>