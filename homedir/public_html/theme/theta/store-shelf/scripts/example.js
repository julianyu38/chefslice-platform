$(document).ready(function() {

    var container = $('#container');

    // fade in effect
    container.css({
        opacity: 0
    });
    container.delay(500).animate({
        opacity: 1
    }, 500);
    $('.category-link').click(function(e) {
        var $this = $(this);

        if ($(this).parent().hasClass('theta-current-item')) {
            window.location.href = $(this).data("content");

        }
    });
    container.theta_carousel({
        "filter": ".ex-item",
        "selectedIndex": mean,
        "distance": 197,
        "numberOfElementsToDisplayRight": 3,
        "numberOfElementsToDisplayLeft": 3,
        "designedForWidth": 1600,
        "designedForHeight": 705,
        "distanceInFallbackMode": 250,
        "path": {
            "settings": {
                "shiftY": 55,
                "rotationAngleZX": -91
            },
            "type": "line"
        },
        "perspective": 787,
        "popoutSelected": true,
        "popoutSelectedShiftY": -58,
        "popoutSelectedShiftZ": 170
    });
});