var lipsum = [{
    "caption": "Lorem ipsum dolor sit amet.",
    "content": "Vivamus bibendum placerat eleifend. Etiam porta, diam in venenatis euismod, ex leo tristique dui, ac varius tellus nisl rhoncus sem. Suspendisse bibendum sagittis tincidunt. Donec id volutpat arcu. Cras id gravida dolor. Ut ante mi, lobortis id mauris id, dapibus pellentesque justo. Mauris ut dolor quis tortor porttitor viverra at sit amet augue."
}, {
    "caption": "Sed aliquet tellus at nisl.",
    "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus non turpis ante. Etiam imperdiet pellentesque dolor vitae aliquet. Quisque consequat interdum purus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis facilisis nulla fringilla, dignissim lectus ac, vulputate lectus. Mauris vulputate rhoncus nisl, non interdum lorem gravida interdum. Nulla ac auctor sapien. Nam auctor felis in tortor hendrerit, eget cursus lorem elementum. Vestibulum luctus, enim non dictum efficitur, metus sapien egestas mi, a sagittis elit augue sit amet eros."
}, {
    "caption": "Ut et mauris id elit eleifend.",
    "content": "Nullam vitae justo mi. Integer sed nisl blandit, molestie est id, lobortis tellus. Praesent pretium nisl nisi, nec rhoncus magna vestibulum a. In vel quam sed augue placerat scelerisque euismod aliquam turpis. Donec eget euismod risus, in auctor lorem. Nullam elementum tincidunt mi et ultrices. Aliquam ullamcorper dapibus lacinia. Mauris sagittis nulla in ipsum lobortis, vestibulum pretium nunc pellentesque."
}, {
    "caption": "Ut gravida metus id sem.",
    "content": "Proin et aliquam orci. Fusce ut sapien vitae justo tincidunt lobortis nec in mi. Vestibulum felis dolor, euismod auctor ultrices nec, feugiat sit amet odio. Curabitur et velit euismod, convallis libero non, efficitur neque. Aliquam eu ligula vel diam vulputate congue. Duis et est dolor. Nulla blandit nec urna nec fringilla. Maecenas viverra venenatis velit nec vestibulum. Fusce non nisi velit. Aliquam tempus venenatis purus. Donec rutrum id ex sit amet porttitor. Ut porta vitae turpis et iaculis."
}, {
    "caption": "Integer viverra nunc.",
    "content": "Nulla varius consectetur tincidunt. Donec tellus dolor, pretium ac sem ac, vehicula facilisis elit. Vivamus in tellus ac orci efficitur fringilla. Quisque eu vestibulum nulla. Sed venenatis nibh eget magna ornare, nec accumsan augue volutpat. Vestibulum eget viverra sem. Vivamus iaculis quam ac orci pharetra rhoncus. Duis a leo tempor, mollis turpis eget, volutpat urna."
}, {
    "caption": "Cras et urna cursus.",
    "content": "Nam a vestibulum eros. Sed rutrum rutrum augue, aliquam porta quam bibendum at. Integer porttitor laoreet risus, sit amet ultricies ipsum fermentum sagittis. In pretium viverra euismod. Phasellus a augue sed dolor luctus vehicula a eu ante. Nunc vitae libero condimentum, convallis metus at, porttitor ipsum. Quisque massa nulla, mollis in massa vitae, laoreet vestibulum ante. In mollis euismod arcu, quis hendrerit diam fringilla vitae. In ultricies mi vitae arcu consequat tempus. Etiam non elementum velit."
}, {
    "caption": "Aliquam aliquam dui.",
    "content": "Mauris congue lectus ipsum, nec ornare augue sollicitudin quis. Pellentesque volutpat semper sapien vel tristique. Praesent eleifend eros et pellentesque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum sit amet sem vestibulum eros commodo ullamcorper ut quis ex. Aenean fermentum eleifend velit, quis bibendum ipsum elementum non. Duis suscipit ornare dolor non volutpat. Nulla ut tincidunt quam. Etiam aliquet fringilla nulla."
}, {
    "caption": "Duis mattis nulla id quam.",
    "content": "Praesent interdum quis diam sed dignissim. Ut non arcu id diam cursus placerat quis ac nisl. Integer quis nibh eget urna fringilla mattis eu sed metus. Cras bibendum viverra dui, vel convallis lorem tempus et. Nullam porttitor ac lacus ac lobortis. Phasellus nulla enim, fermentum at risus vel, lobortis sodales nibh. Maecenas laoreet viverra ex a consectetur. Suspendisse ac semper tellus. Aliquam elementum iaculis magna ac varius."
}]

function motionStart(e, data) {
    $(".title, .text", this).hide().removeClass('animated fadeInDown fadeInUp');
}

// function motionEnd(e, data) {

//     var text = lipsum[data.index % lipsum.length];
//     $(".title, .text", this).show();

//     $(".title", this).text(text.caption).addClass('animated fadeInDown');
//     $(".text", this).text(text.content).addClass('animated fadeInUp');
// }
function motionEnd(e, data) {

    var promo = categories[data.index % categories.length];
    $(".title, .description", this).show();

    $(".title", this).text(promo.text).addClass('animated fadeInRight');
    $(".description", this).text(promo.description).addClass('animated fadeInLeft');
}

function carouselCreated(e, data) {
    motionEnd.call(this, e, data);
    $('.category-link').click(function(e) {
        var $this = $(this);

        if ($(this).parent().hasClass('theta-current-item')) {
            window.location.href = $(this).data("content");

        }
    });
}

$(document).ready(function() {

    var container = $('#container');

    // fade in effect
    container.css({
        opacity: 0
    });
    container.delay(500).animate({
        opacity: 1
    }, 500);

    container.theta_carousel({
        "filter": ".ex-item",
        "distance": 160,
        "numberOfElementsToDisplayRight": 6,
        "numberOfElementsToDisplayLeft": 0,
        "designedForWidth": 1600,
        "designedForHeight": 660,
        "scaleX": 1.47,
        "scaleZ": 0.72,
        "distanceInFallbackMode": 350,
        "path": {
            "settings": {
                "shiftX": -175,
                "shiftZ": 226,
                "rotationAngleZX": -30
            },
            "type": "line"
        },
        "perspective": 351,
        "sensitivity": 0.7,
        "shadow": true,
        "shadowBlurRadius": 82,
        "shadowSpreadRadius": -16,
        "sizeAdjustment": true,
        "sizeAdjustmentNumberOfConfigurableElements": 7,
        "sizeAdjustmentBezierPoints": {
            "p1": {
                "x": 0,
                "y": 100
            },
            "p2": {
                "x": 58,
                "y": 94
            },
            "p3": {
                "x": 72,
                "y": 85
            },
            "p4": {
                "x": 100,
                "y": 55
            }
        },
        "popoutSelected": true,
        "popoutSelectedShiftX": 7
    });
    carouselCreated.call(container, null, {
        index: container.theta_carousel("option", "selectedIndex")
    });
    container.on('motionStart', motionStart);
    container.on('motionEnd', motionEnd);
});