$(document).ready(function() {

    var container = $('#container');

    // fade in effect
    container.css({
        opacity: 0
    });
    container.delay(500).animate({
        opacity: 1
    }, 500);
    $('.category-link').click(function(e) {
        var $this = $(this);

        if ($(this).parent().hasClass('theta-current-item')) {
            window.location.href = $(this).data("content");

        }
    });
    container.theta_carousel({
        "filter": ".ex-item",
        "selectedIndex": 20,
        "distance": 160,
        "numberOfElementsToDisplayRight": 6,
        "numberOfElementsToDisplayLeft": 3,
        "designedForWidth": 1600,
        "designedForHeight": 705,
        "scaleX": 1.47,
        "scaleZ": 0.72,
        "distanceInFallbackMode": 300,
        "path": {
            "settings": {
                "shiftX": 244,
                "shiftZ": -310,
                "rotationAngleZX": 9
            },
            "type": "line"
        },
        "perspective": 351,
        "sensitivity": -0.7,
        "shadow": true,
        "sizeAdjustment": true,
        "sizeAdjustmentNumberOfConfigurableElements": 7,
        "sizeAdjustmentBezierPoints": {
            "p1": {
                "x": 0,
                "y": 100
            },
            "p2": {
                "x": 58,
                "y": 94
            },
            "p3": {
                "x": 72,
                "y": 85
            },
            "p4": {
                "x": 100,
                "y": 55
            }
        },
        "popoutSelected": true,
        "popoutSelectedShiftX": -476,
        "popoutSelectedShiftZ": 220
    });
});