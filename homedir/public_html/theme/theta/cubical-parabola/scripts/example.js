$(document).ready(function() {

    var container = $('#container');

    // fade in effect
    container.css({
        opacity: 0
    });
    container.delay(500).animate({
        opacity: 1
    }, 500);
    $('.category-link').click(function(e) {
        var $this = $(this);

        if ($(this).parent().hasClass('theta-current-item')) {
            window.location.href = $(this).data("content");

        }
    });
    container.theta_carousel({
        "filter": ".ex-item",
        "selectedIndex": mean,
        "distance": 148,
        "numberOfElementsToDisplayRight": 12,
        "numberOfElementsToDisplayLeft": 12,
        "designedForWidth": 1600,
        "designedForHeight": 705,
        "distanceInFallbackMode": 350,
        "scaleX": 1.49,
        "scaleY": 0.42,
        "scaleZ": 0.93,
        "path": {
            "settings": {
                "shiftZ": -222
            },
            "type": "cubic"
        },
        "perspective": 1084,
        "shadow": true,
        "shadowBlurRadius": 80,
        "shadowSpreadRadius": -11,
        "sizeAdjustment": true,
        "sizeAdjustmentBezierPoints": {
            "p1": {
                "x": 0,
                "y": 67
            },
            "p2": {
                "x": 49,
                "y": 57
            },
            "p3": {
                "x": 74,
                "y": 60
            },
            "p4": {
                "x": 100,
                "y": 88
            }
        },
        "popoutSelected": true,
        "popoutSelectedShiftZ": 261
    });
});