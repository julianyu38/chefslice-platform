$(document).ready(function() {

    var container = $('#container');

    // fade in effect
    container.css({
        opacity: 0
    });
    container.delay(500).animate({
        opacity: 1
    }, 500);
   
    $('.category-link').click(function(e) {
        var $this = $(this);

        if ($(this).parent().hasClass('theta-current-item')) {
            window.location.href = $(this).data("content");

        }
    });
    


    container.theta_carousel({
        "filter": ".ex-item",
        "selectedIndex": mean,
        "numberOfElementsToDisplayRight": 9,
        "designedForWidth": 1600,
        "designedForHeight": 705,
        "scaleX": 1.79,
        "scaleY": 1.17,
        "distanceInFallbackMode": 370,
        "path": {
            "settings": {
                "shiftX": 3,
                "shiftY": 251,
                "shiftZ": -333,
                "pathLength": 1396,
                "width": 1078,
                "height": 500,
                "depth": 10,
                "xyPathBezierPoints": {
                    "p1": {
                        "x": -100,
                        "y": 0
                    },
                    "p2": {
                        "x": 0,
                        "y": 100
                    },
                    "p3": {
                        "x": 0,
                        "y": -100
                    },
                    "p4": {
                        "x": 100,
                        "y": 0
                    }
                },
                "xyArcLengthBezierPoints": {
                    "p1": {
                        "x": 0,
                        "y": 0
                    },
                    "p2": {
                        "x": 100,
                        "y": 0
                    },
                    "p3": {
                        "x": 0,
                        "y": 100
                    },
                    "p4": {
                        "x": 100,
                        "y": 100
                    }
                },
                "xzPathBezierPoints": {
                    "p1": {
                        "x": -100,
                        "y": 50
                    },
                    "p2": {
                        "x": 0,
                        "y": 0
                    },
                    "p3": {
                        "x": 0,
                        "y": 0
                    },
                    "p4": {
                        "x": 100,
                        "y": 50
                    }
                }
            },
            "type": "cubic_bezier"
        },
        "sensitivity": 0.4,
        "shadow": true,
        "fadeAway": true,
        "fadeAwayNumberOfConfigurableElements": 10,
        "fadeAwayBezierPoints": {
            "p1": {
                "x": 0,
                "y": 100
            },
            "p2": {
                "x": 100,
                "y": 100
            },
            "p3": {
                "x": 100,
                "y": 100
            },
            "p4": {
                "x": 100,
                "y": 0
            }
        },
        "sizeAdjustment": true,
        "sizeAdjustmentNumberOfConfigurableElements": 9,
        "sizeAdjustmentBezierPoints": {
            "p1": {
                "x": 0,
                "y": 100
            },
            "p2": {
                "x": 0,
                "y": 50
            },
            "p3": {
                "x": 10,
                "y": 40
            },
            "p4": {
                "x": 100,
                "y": 15
            }
        }
    });
});