function motionStart(e, data) {
    $(e.target).addClass('in-motion');
    $('.ex-item').removeClass('current');

    $('.ex-item').each(function() {
        var player = $(this).data('player');
        if (player)
            player.pauseVideo();
    });
}

function motionEnd(e, data) {
    $(e.target).removeClass('in-motion');
    $('.ex-item').removeClass('current');
    $($('.ex-item').get(data.index)).addClass('current');

    var item = $($('.ex-item').get(data.index));
    var player = item.data('player');
    if (item.data('status') === 'playing')
        player.playVideo();
}

function carouselCreated(e, data) {
    $('.category-link').click(function(e) {
        var $this = $(this);

        if ($(this).parent().hasClass('theta-current-item')) {
            window.location.href = $(this).data("content");

        }
    });
    var tag = $('<script></script>');
    tag.attr('src', 'https://www.youtube.com/iframe_api');
    tag.insertBefore($('script'));

    var onYouTubeIframeAPIReady = function() {
        $('.ex-item').each(function() {
                var video = $(this).data('video');
                var player = new YT.Player(video, {
                    height: '360',
                    width: '480',
                    videoId: video,
                    playerVars: {
                        autohide: 1,
                        theme: 'light'
                    },
                    events: {
                        'onReady': onPlayerReady,
                        'onStateChange': onPlayerStateChange
                    }
                });
            })
            .addClass('loading');
    };

    if (typeof(YT) == "undefined")
        window.onYouTubeIframeAPIReady = onYouTubeIframeAPIReady;
    else
        onYouTubeIframeAPIReady();

    $($('.ex-item').get(data.index)).addClass('current');
}

function onPlayerStateChange(event) {
    var item = $(event.target.getIframe()).closest('.ex-item');

    if (!($('.theta-carousel').theta_carousel('getIsInMotion'))) {

        if (event.data === YT.PlayerState.PLAYING ||
            event.data === YT.PlayerState.PAUSED ||
            event.data === YT.PlayerState.ENDED) {
            if (event.data === YT.PlayerState.PLAYING) {
                item.data('status', 'playing');
                item.removeClass('loading');
                item.data('player', event.target);
            }

            if (event.data === YT.PlayerState.PAUSED || event.data === YT.PlayerState.ENDED) {
                item.data('status', '');
            }
        }

    }
}

function onPlayerReady(event) {
    var item = $(event.target.getIframe()).closest('.ex-item');
    if (item.hasClass('current')) {
        event.target.playVideo();
    } else {
        $('.ex-item').removeClass('loading');
    }
}

$(document).ready(function() {

    var container = $('#container');

    // fade in effect
    container.css({
        opacity: 0
    });
    container.delay(500).animate({
        opacity: 1
    }, 500);

    container.theta_carousel({
        "filter": ".ex-item",
        "selectedIndex": mean,
        "designedForWidth": 1600,
        "designedForHeight": 729,
        "distance": 45,
        "distanceInFallbackMode": 550,
        "path": {
            "settings": {
                "shiftY": 136,
                "shiftZ": -1480,
                "rotateElements": true,
                "a": 1000,
                "b": 820,
                "endless": true
            },
            "type": "ellipse"
        },
        "perspective": 956,
        "sensitivity": 0.1,
        "allignElementsWithPath": true,
        "shadow": true,
        "popoutSelected": true,
        "popoutSelectedShiftY": -140,
        "popoutSelectedShiftZ": 741,
        "reflection": true,
        "reflectionBelow": 13,
        "reflectionHeight": 0.4
    });
    carouselCreated.call(container, null, {
        index: container.theta_carousel("option", "selectedIndex")
    });
    container.on('motionStart', motionStart);
    container.on('motionEnd', motionEnd);
});