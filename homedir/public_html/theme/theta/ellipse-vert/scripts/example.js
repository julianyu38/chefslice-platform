function carouselCreated(e, data) {
    var progress = {
        progress: 0
    };

    setTimeout(function() {

        $(progress).animate({
            progress: 100
        }, {
            easing: 'easeOutBack',
            duration: 700,
            step: function(progress) {
                if (isNaN(progress))
                    return; //for some easings we can get NaNs

                progress = progress / 100;
                var a = 320 + progress * 680;
                var b = 450 + progress * 230;
                var shiftY = -140 + progress * 140;
                var shiftZ = -2000 + progress * 580;

                $('.theta-carousel').theta_carousel({
                    'path.settings.shiftZ': shiftZ,
                    'path.settings.shiftY': shiftY,
                    'path.settings.a': a,
                    'path.settings.b': b
                });
            }
        });

    }, 300);
    
}

$(document).ready(function() {

    var container = $('#container');

    // fade in effect
    container.css({
        opacity: 0
    });
    container.delay(500).animate({
        opacity: 1
    }, 500);
$('.category-link').click(function(e) {
        var $this = $(this);

        if ($(this).parent().hasClass('theta-current-item')) {
            window.location.href = $(this).data("content");

        }
    });
    container.theta_carousel({
        "filter": ".ex-item",
        "distance": 35,
        "numberOfElementsToDisplayRight": 2,
        "numberOfElementsToDisplayLeft": 2,
        "designedForWidth": 1344,
        "designedForHeight": 1069,
        "distanceInFallbackMode": 400,
        "scaleX": 1.49,
        "scaleY": 0.42,
        "scaleZ": 0.93,
        "path": {
            "settings": {
                "shiftZ": -1420,
                "rotationAngleXY": 90,
                "rotationAngleZX": 39,
                "a": 1000,
                "b": 680,
                "endless": true
            },
            "type": "ellipse"
        },
        "perspective": 956,
        "sensitivity": 0.3,
        "verticalRotation": true,
        "sizeAdjustment": true,
        "sizeAdjustmentBezierPoints": {
            "p1": {
                "x": 0,
                "y": 83
            },
            "p2": {
                "x": 1,
                "y": 43
            },
            "p3": {
                "x": 31,
                "y": 0
            },
            "p4": {
                "x": 100,
                "y": 0
            }
        }
    });
    carouselCreated.call(container, null, {
        index: container.theta_carousel("option", "selectedIndex")
    });
});