<?php


Route::group(['domain' => '{account}'.env('SESSION_DOMAIN')], function () {
    Route::group(['prefix'=>'dashboard','namespace'=>'Dashboard','middleware'=>['auth','user_auth']],function (){
        Route::get('/','HomeController@index');
        Route::get('profile','HomeController@profile');
        Route::resource('category','CategoryController');

        Route::post('item/special','ItemController@special');
        Route::resource('item','ItemController');
        Route::resource('theme','ThemeController');
        Route::resource('settings','SettingsController');
        Route::post('settings/menu','SettingsController@menu');
        Route::resource('page','PagesController');

//        Route::put('/settings/{setting}','SettingsController@update')->name('settings.update');


        Route::group(['middleware'=>['admin_auth']],function () {
            Route::get('user/sendemail','UserController@sendemail');
            Route::resource('user','UserController');
            Route::resource('role','RoleController');
            Route::get('analytic/countries', 'AnalyticController@countries');
            Route::get('analytic/browsers', 'AnalyticController@browsers');
            Route::get('analytic/os', 'AnalyticController@os');
            Route::get('analytic/sources', 'AnalyticController@sources');
            Route::get('analytic/mobile', 'AnalyticController@mobile');
            Route::get('analytic/sites', 'AnalyticController@sites');
            Route::get('analytic/keywords', 'AnalyticController@keywords');
            Route::get('analytic/landing_pages', 'AnalyticController@landing_pages');
            Route::get('analytic/exit_pages', 'AnalyticController@exit_pages');
            Route::get('analytic/bounces', 'AnalyticController@bounces');

            Route::get('home-page/slider', 'HomeController@slider');
            Route::post('home-page/slider', 'HomeController@update_slider');
            Route::post('home-page/update_settings', 'HomeController@update_settings');
            Route::delete('home-page/slider/{id}', 'HomeController@delete_slider');
            Route::get('home-page/site-info', 'HomeController@site_info');
            Route::get('home-page/clients', 'HomeController@site_clients');
            Route::get('home-page/services', 'HomeController@services');
            Route::get('home-page/how-work', 'HomeController@how_work');
            Route::get('home-page/site-about', 'HomeController@site_about');
            Route::get('home-page/menu', 'HomeController@site_menu');
            Route::get('home-page/pricing', 'HomeController@pricing');
            Route::get('home-page/map', 'HomeController@map');
            Route::resource('home-page/service', 'ServiceController');
        });

    });


    Route::get('/', 'HomeController@index');
    Route::get('/items','HomeController@items');
    Route::get('/special','HomeController@special');
    Route::get('{category}', 'HomeController@products');
    Route::get('page/{slug}','HomeController@cms');
    Route::post('heatmap','HomeController@heatmap');
    Route::get('locale/{lang}', 'LanguageLocalizationController@index' );


});

Route::get('/', 'HomeController@home');
Route::get('logout', 'Auth\LoginController@logout');
Route::post('logout', 'Auth\LoginController@logout');
Route::post('login', 'Auth\LoginController@login');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');

Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::post('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
/*Route::get('/list', function () {
    return view('list');
});*/
//Route::get('/category/{slug}', 'CategoryController@index');

//Auth::routes();
/*
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix'=>'dashboard','namespace'=>'Dashboard','middleware'=>['auth']],function (){
    Route::get('/','HomeController@index');
    Route::resource('category','CategoryController');
    Route::resource('item','ItemController');
    Route::resource('user','UserController');
    Route::resource('role','RoleController');
});*/

//file_put_contents("test.mp3", fopen("http://www.koolmuzone.pk/main/wp-content/uploads/2009/08/Vital-Signs-Aisay-Hum-Jeeyan-Airforce-KoolMuzone.mp3", 'r'));
//dd();
