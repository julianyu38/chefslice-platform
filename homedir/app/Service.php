<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $table = 'services';
    public $fillable = ['title','icon','description','order'];
    public $timestamps = ['created_at','updated_at'];
}
