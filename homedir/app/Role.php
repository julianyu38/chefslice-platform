<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    public $table = "roles";
    public $fillable = ['name','display_name','description'];
    public $dates = ['created_at','updated_at'];
    
    
    public function permissions(){
        return $this->belongsToMany(Permission::class,'permission_role','role_id','permission_id');
    }
}

