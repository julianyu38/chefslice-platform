<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'settings';
    protected $fillable = [
        'user_id','logo', 'background', 'category_design','heatmap','count','specialoffer'
    ];
    protected  $dates =['created_at','updated_at'];
    public function user(){
        return $this->hasOne(User::class,'id');
    }
}
