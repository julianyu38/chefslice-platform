<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Heatmap extends Model
{
    protected $table = 'heatmaps';
    protected $fillable = [
        'user_id','x', 'y', 'location'
    ];
    protected  $dates =['created_at','updated_at'];
    public function user(){
        return $this->hasOne(User::class,'id');
    }
}
