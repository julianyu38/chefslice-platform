<?php

namespace App\Providers;

use App\Heatmap;
use App\Settings;
use App\User;
use Illuminate\Support\ServiceProvider;
use App\SiteSetting;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(User $account)
    {
//        $this->heatmap($account);

        $this->helpers();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    protected  function heatmap( $account){
//        print_r($account);
        if(auth()->user()->user_name === $account->user_name){
            $users = User::all()->pluck('name','id');
            view()->composer('*',function($view){
                $heatdots = function($account){
                    if(isset($_GET['heatmap']) && strlen(trim($_GET['heatmap'])) ==1){
                        return DB::table('heatmaps')->where('user_id', $account->id)
//                            ->where('location', '/')
                            ->get();
                    }

                    return 'USD';
                };
//                $settings = function($group=null){
//                    $_settings = Settings::orderBy('group')->orderBy('order');
//                    if($group!=null){
//                        $_settings = $_settings->whereGroup($group)->get();
//                    }else{
//                        $_settings = $_settings->get();
//                    }
//                    $data = $_settings->groupBy('group')->map(function($item){
//                        return $item->keyBy('type');
//                    });
//                    if($group!=null){
//                        return $data->only($group)->first();
//                    }
//                    return $data;
//                };

                $view->with('heatdots',$heatdots);
            });
        }

    }


   protected function helpers(){
        view()->composer('*',function($view){
            $base_url  =  url(env('APP_URL_PROTOCOL').'://'.env('APP_URL').'/');
            $app_url  =  function($domain){
                return url(env('APP_URL_PROTOCOL').'://'.$domain.'.'.env('APP_URL').'/');
            };
            $settings = function($group=null){
                    $_settings = SiteSetting::orderBy('group')->orderBy('order');
                    if($group!=null){
                        $_settings = $_settings->whereGroup($group)->get();
                    }else{
                        $_settings = $_settings->get();
                    }
                    $data = $_settings->groupBy('group')->map(function($item){
                        return $item->keyBy('type');
                    });
                    if($group!=null){
                        return $data->only($group)->first();
                    }
                    return $data;
                };
            $view->with('BASE_URL',$base_url)->with('APP_URL',$app_url)->with('SITE_SETTING',$settings);
        });
    }
}
