<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
   protected $table = 'images';
   protected $fillable = ['item_id','image'];
   protected $dates = ['create_at','updated_at'];
}
