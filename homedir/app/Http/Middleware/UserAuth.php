<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class UserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $account = $request->route()->parameter('account');
        $user_name = null;
        if($account instanceof User){
            $user_name = $account->user_name;
        }else{
            $user_name = $account;
        }

         $auth_user = auth()->user();
        //if($auth_user->user_name !== 'admin' && $request->route('account')->user_name !== $auth_user->user_name){
        if($user_name !== $auth_user->user_name){
           return redirect(env('APP_URL_PROTOCOL').'://'.env('APP_URL').'/login');
        }
        return $next($request);
    }
}
