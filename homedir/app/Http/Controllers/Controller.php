<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    
    private  function getClassName(){
        return trim(strrev(implode(strrev(""), explode(strrev("Controller"), strrev(class_basename($this)), 2))));
    }

    public function view($data=[],$prefix="dashboard"){
        $method_name =  debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function'];
        $view_name =  $prefix.'.'.strtolower($this->getClassName()).'.'.$method_name;
        return view($view_name,$data);
    }


    public function check_auth($user){
      $auth_user = auth()->user();
      if($auth_user->user_name !== 'admin' && $auth_user->user_name !== $user->user_name){
          abort(404);
      }

    }


}
