<?php

namespace App\Http\Controllers\Dashboard;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::paginate(50);
        return $this->view(compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();
        return $this->view(compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'display_name' => 'required|unique:roles,display_name'
        ]);
        $request->merge(['name'=>str_slug($request->input('display_name'))]);
       $role = Role::create($request->all());
        if($request->has('permissions')){
            $role->permissions()->attach($request->input('permissions'));
        }
        session()->flash("toastr", ["message" => "User role created successfully.", "title" => "Created!", "type" => "success"]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return $role;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $permissions = Permission::all();
        return $this->view(compact('role','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $this->validate($request,[
            'display_name' => 'required|unique:roles,display_name,'.$role->id
        ]);
        $request->merge(['name'=>str_slug($request->input('display_name'))]);
        $role->update($request->all());
        if($request->has('permissions')){
            $role->permissions()->attach($request->input('permissions'));
        }else{
            $role->permissions()->detach($role->permissions()->select('id')->pluck('id')->toArray());
        }
        session()->flash("toastr", ["message" => "User role updated successfully.", "title" => "Updated!", "type" => "success"]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        try{
            $role->delete();
            session()->flash("toastr", ["message" => "User role deleted successfully.", "title" => "Deleted!", "type" => "success"]);
            return redirect()->action('Dashboard\RoleController@index');

        }catch (\Exception $ex){
            session()->flash("toastr", ["message" => "User role can not be deleted.", "title" => "Oops!", "type" => "error"]);
            return back();
        }
    }
}
