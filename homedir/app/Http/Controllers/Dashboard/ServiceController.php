<?php

namespace App\Http\Controllers\Dashboard;

use App\Service;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'icon' => 'required'
        ]);
        $request->merge(['icon'=>'fa '.$request->input('icon')]);
        $service = Service::create($request->all());
        session()->flash("toastr", ["message" => "Service created successfully.", "title" => "Saved!", "type" => "success"]);
        return back();
    }





    /**
     * Update the specified resource in storage.
     *
     * @param  \App\User  $account
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(User $account,Request $request, Service $service)
    {
        $this->validate($request,[
            'title' => 'required',
            'icon' => 'required'
        ]);
        $service->update($request->all());
        session()->flash("toastr", ["message" => "Service updated successfully.", "title" => "Updated!", "type" => "success"]);
        return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $account,Service $service)
    {
       $service->delete();
        session()->flash("toastr", ["message" => "Service deleted successfully.", "title" => "Deleted!", "type" => "success"]);
        return back();
    }
}
