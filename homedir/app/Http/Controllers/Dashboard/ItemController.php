<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Item;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function index(User $account)
    {
        $items = $this->getProducts($account);
        return view('dashboard.item.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $account)
    {
        $categories = $categories = $this->getCategories($account);
        return view('dashboard.item.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\User  $account
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $account, Request $request)
    {
//        return $request->all();
//        $this->validate($request,[
//            'title' => 'required|max:255',
//            'category_id'=>'required'
//        ]);
        if ($request->input('is_special')=='on'){
            $special= 1;
        } elseif ($request->input('is_special')=='off') {
            $special=0;
        }
        $data=[
            'category_id'=>$request->input('category_id'),
            'media_type'=>$request->input('media_type'),
            'user_id'=>$account->id,
            'is_special'=>$special,
            $request->input('locale')=>[
                'title'=>$request->input('title'),
                'short_description' =>$request->input('short_description'),
                'description'=>$request->input('description'),
            ]

        ];

        //$request->merge(['slug'=>str_slug($request->input('text'))]);
        $item = Item::create($data);



        if($item->id){
            if ($request->hasFile('thumbnail')) {
                $logo = $request->file('thumbnail');
                $extension = $logo->getClientOriginalExtension();
                $filename = md5('thumbnail_'.$item->id).'.'. $extension;
                $path = public_path('media/items/images/');
                $logo->move($path, $filename);
                $item->thumbnail = $filename;
                $item->save();
            }
            if ($files = $request->file('media')) {
//                return $files;
                foreach($files as $media){
//                    $media = $request->file('media');
                    $originalFileName = $media->getClientOriginalName();
                    $extension = $media->getClientOriginalExtension();
                    $filename = $originalFileName.md5('media_'.time()).'.'. $extension;
                    $path = public_path('media/items/images/'.$item->id);
                    $media->move($path, $filename);
                    $images = $item->images()->create([
                        'item_id' => $item->id,
                        'image' => $filename
                    ]);

                }
            }
            session()->flash("toastr", ["message" => "Item created successfully.", "title" => "Created!", "type" => "success"]);
//            return redirect()->action('Dashboard\ItemController@index');
            return back();
        }
        session()->flash("toastr", ["message" => "Item can not be created.", "title" => "Oops!", "type" => "error"]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $id
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function show(User $account,$id)
    {
        $product = $this->getProduct($account,$id);
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $id
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(User $account,$id)
    {
        $categories =  $account->categories;
        $item = $this->getProduct($account,$id);
        return view('dashboard.item.edit',compact('item','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $id
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function update(User $account,Request $request, $id)
    {
        $product = $this->getProduct($account,$id);
        $this->validate($request,[
            'title' =>'required|max:255'
        ]);
        if($product->update($request->all())){
            if ($request->hasFile('thumbnail')) {
                if ( file_exists(public_path('media/items/images/' . $product->thumbnail)) && !empty($product->thumbnail)) {
                    unlink(public_path('media/items/images/' . $product->thumbnail));
                }
                $icon = $request->file('thumbnail');
                $extension = $icon->getClientOriginalExtension();
                $filename = md5('thumbnail_'.$product->id).'.' . $extension;
                $path = public_path('media/items/images/');
                $icon->move($path, $filename);
                $product->thumbnail = $filename;
                $product->save();
            }
//            if ($request->hasFile('media')) {
//                if ( file_exists(public_path('media/items/images/' . $product->media)) && !empty($product->media)) {
//                    unlink(public_path('media/items/images/' . $product->media));
//                }
//                $media = $request->file('media');
//                $extension = $media->getClientOriginalExtension();
//                $filename = md5('media_'.$product->id).'.' . $extension;
//                $path = public_path('media/items/images/');
//                $media->move($path, $filename);
//                $product->media = $filename;
//                $product->save();
//            }
            if ($files = $request->file('media')) {
//                return $files;
                $images = $product->images()->delete();
                foreach($files as $media){
//                    $media = $request->file('media');
                    $originalFileName = $media->getClientOriginalName();
                    $extension = $media->getClientOriginalExtension();
                    $filename = $originalFileName.md5('media_'.time()).'.'. $extension;
                    $path = public_path('media/items/images/'.$product->id);
                    $media->move($path, $filename);
                    $images = $product->images()->create([
                        'item_id' => $product->id,
                        'image' => $filename
                    ]);

                }
            }
            session()->flash("toastr", ["message" => "Item updated successfully.", "title" => "Updated!", "type" => "success"]);
        }else {
            session()->flash("toastr", ["message" => "Item can not be updated.", "title" => "Oops!", "type" => "error"]);
        }
        return back();
    }

    public  function special(User $account, Request $request){
        $product = $this->getProduct($account,$request->id);
        $product->is_special = $request->is_special;
        if($product->save()) {
            return 'success';
        } else {
            return 'failed';
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $id
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $account,$id)
    {
        $product = $this->getProduct($account,$id);
        if($product->delete()) {
            if ( file_exists(public_path('media/items/images/' . $product->thumbnail)) && !empty($product->thumbnail)) {
                unlink(public_path('media/items/images/' . $product->thumbnail));
            }
            if($product->media_type !=2 ) {
                if (file_exists(public_path('media/items/images/' . $product->media)) && !empty($product->media)) {
                    unlink(public_path('media/items/images/' . $product->media));
                }
            }else{
                if (file_exists(public_path('media/items/videos/' . $product->media)) && !empty($product->media)) {
                    unlink(public_path('media/items/videos/' . $product->media));
                }
            }
            session()->flash("toastr", ["message" => "Product deleted successfully.", "title" => "Product Deleted!", "type" => "success"]);
            return redirect()->action('Dashboard\ItemController@index');
        }

        session()->flash("toastr", ["message" => "Product can not be deleted.", "title" => "Oops!", "type" => "error"]);
        return redirect()->action('Dashboard\ItemController@index');
    }



    private function getProduct(User $account,$id){
        if($account->user_name === 'admin'){
            return Item::findOrFail($id);
        }
        return $account->products()->where('items.id','=',$id)->firstOrFail();
    }

    private function getProducts(User $account,$limit=50){
        if($account->user_name == 'admin'){
            return Item::paginate($limit);
        }else{
            return $account->products()->paginate($limit);
        }
    }
    private  function getCategories(User $account) {
        if($account->user_name == 'admin'){
            return Category::all();
        }else{
            return $account->categories;
        }
    }

}
