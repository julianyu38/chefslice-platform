<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\CategoryTranslation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $account)
    {

        $categories = $this->getCategories($account);
        return view('dashboard.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = collect([]);

        if(auth()->user()->user_name === 'admin'){
            $users = User::all()->pluck('name','id');
        }
        return view('dashboard.category.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $account,Request $request)
    {
//        return $request->all();
//        $this->validate($request,[
//            'text' => 'required|max:255|unique:categories,text'
//        ]);
        $slug = str_slug($request->input('text'));
        if($slug === 'dashboard'){
            $slug = 'dashboard1';
        }
        $request->merge(['slug'=>$slug]);
        if($account->user_name !== 'admin')
            $request->merge(['user_id'=>$account->id]);
        else
            $this->validate($request,[
                'user_id' => 'required'
            ]);
        $data = [
            'user_id'=>$account->id,
            $request->input('locale')=>[
                'slug'=>$request->input('slug'),
                'text' =>$request->input('text'),
                'description'=>$request->input('description'),
            ]
        ];
        $category = Category::create($data);
//        $translate = $request; //->all();
//        $id = $category->id;
//        $translate->merge(['category_id'=>$id]);
//
//        $translation = CategoryTranslation::create($translate->all());
        if($category->id){
            if ($request->hasFile('icon')) {
                $logo = $request->file('icon');
                $extension = $logo->getClientOriginalExtension();
                $filename = md5('_'.$category->id).'.'. $extension;
                $path = public_path('media/categories/images/');
                $logo->move($path, $filename);
                $category->icon = $filename;
                $category->save();
            }
            session()->flash("toastr", ["message" => "Category created successfully.", "title" => "Created!", "type" => "success"]);
            return redirect()->action('Dashboard\CategoryController@create',['account'=>$account]);
        }
        session()->flash("toastr", ["message" => "Category can not be created.", "title" => "Oops!", "type" => "error"]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $id
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function show(User $account,$id)
    {
        $category = $this->getCategory($account,$id);
        return $category;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $id
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(User $account,$id)
    {

        $category = $this->getCategory($account,$id);
        return view('dashboard.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(User $account ,Request $request, Category $category)
    {
//        $this->validate($request,[
//            'text' =>'required|max:255|unique:categories,text,'.$category->id
//        ]);
        $slug = str_slug($request->input('text'));
        $data = [
            
            'user_id'=>$account->id,
            $request->input('locale')=>[
                'slug'=>$request->input('slug'),
                'text' =>$request->input('text'),
                'description'=>$request->input('description'),
            ]


        ];

        if($category->update($data)){
            if ($request->hasFile('icon')) {
                if ( file_exists(public_path('media/categories/images/' . $category->icon)) && !empty($category->icon)) {
                   unlink(public_path('media/categories/images/' . $category->icon));
                }
                $icon = $request->file('icon');
                $extension = $icon->getClientOriginalExtension();
                $filename = md5('_'.$category->id).'.' . $extension;
                $path = public_path('media/categories/images/');
                $icon->move($path, $filename);
                $category->icon = $filename;
                $category->save();
            }
            session()->flash("toastr", ["message" => "Category updated successfully.", "title" => "Updated!", "type" => "success"]);
        }else {
            session()->flash("toastr", ["message" => "Category can not be updated.", "title" => "Oops!", "type" => "error"]);
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $id
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $account,$id)
    {
        $category = $this->getCategory($account,$id);
        if($category->delete()) {
            if ( file_exists(public_path('media/categories/images/' . $category->icon)) && !empty($category->icon)) {
                unlink(public_path('media/categories/images/' . $category->icon));
            }
            session()->flash("toastr", ["message" => "Category deleted successfully.", "title" => "Category Deleted!", "type" => "success"]);
            return back();
        }

        session()->flash("toastr", ["message" => "Category can not be deleted.", "title" => "Oops!", "type" => "error"]);
        return back();
    }


    private function getCategory(User $account,$id){
        if($account->user_name === 'admin'){
            return Category::findOrFail($id);
        }
        return $account->categories()->whereId($id)->firstOrFail();
    }

    private function getCategories(User $account,$limit=50){
        if($account->user_name == 'admin'){
            return Category::paginate($limit);
        }else{
            return $account->categories()->paginate($limit);
        }
    }
}
