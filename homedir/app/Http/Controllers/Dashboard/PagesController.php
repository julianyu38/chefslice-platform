<?php

namespace App\Http\Controllers\Dashboard;

use App\Page;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $account)
    {
        $pages = $this->getPages($account);
        return view('dashboard.page.index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = collect([]);
        if(auth()->user()->user_name === 'admin'){
            $users = User::all()->pluck('name','id');
        }
        return view('dashboard.page.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $account,Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:255',
            'slug' => 'required|max:255|unique:pages,slug'
        ]);
        $slug = str_slug($request->input('slug'));
        if($slug === 'dashboard'){
            $slug = 'dashboard1';
        }
        $request->merge(['slug'=>$slug]);
        if($account->user_name !== 'admin')
            $request->merge(['user_id'=>$account->id]);
        else
            $this->validate($request,[
                'user_id' => 'required'
            ]);
        $page = Page::create($request->all());
        if($page->id){

            session()->flash("toastr", ["message" => "Page created successfully.", "title" => "Created!", "type" => "success"]);
            return redirect()->action('Dashboard\PagesController@create',['account'=>$account]);
        }
        session()->flash("toastr", ["message" => "Page can not be created.", "title" => "Oops!", "type" => "error"]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $account,$id)
    {

        $page = $this->getPage($account,$id);
        return view('dashboard.page.edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $account , Request $request, Page $page)
    {
        $this->validate($request,[
            'title' => 'required|max:255'
        ]);
        $slug = str_slug($request->input('slug'));
        if($slug === 'dashboard'){
            $slug = 'dashboard1';
        }
        $request->merge(['slug'=>$slug]);
        if($account->user_name !== 'admin')
            $request->merge(['user_id'=>$account->id]);
        else
            $this->validate($request,[
                'user_id' => 'required'
            ]);
        $page->update($request->all());
        session()->flash("toastr", ["message" => "Page updated successfully.", "title" => "Updated!", "type" => "success"]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getPage(User $account,$id){
        if($account->user_name === 'admin'){
            return Page::findOrFail($id);
        }
        return $account->pages()->whereId($id)->firstOrFail();
    }
    private function getPages(User $account,$limit=50){
        if($account->user_name == 'admin'){
            return Page::paginate($limit);
        }else{
            return $account->pages()->paginate($limit);
        }
    }
}
