<?php

namespace App\Http\Controllers\Dashboard;

use App\CircularMenu;
use App\Settings;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ThemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $account)
    {
        $settings = $this->getSettings($account);
        $menus = $this->getMenus($account);
        $categories = $this->getCategories($account);
        return view('dashboard.settings.index', compact('settings','account', 'menus','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getSettings(User $account){
        if($account->user_name == 'admin'){
            return Settings::paginate(50);
        }else{
            return Settings::where('user_id', $account->id)->first();
        }
    }
    private function getMenus(User $account){
        if($account->user_name == 'admin'){
            return CircularMenu::paginate(50);
        }else{
            return $account->circularmenus;
        }
    }
    private  function  getCategories(User $account){
        return $account->categories;
    }

}
