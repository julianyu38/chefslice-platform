<?php

namespace App\Http\Controllers\Dashboard;

use App\Settings;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mockery\Exception;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
   public function menu(Request $request,  User $account) {
        $menus = $request->all();
        foreach ($menus as $key => $menu) {
            if (is_array($menu)) {
                foreach ($menu as $id => $item) {
                    $circularMenu = CircularMenu::findOrFail($id);
                    $circularMenu->$key =  $item;
                    $circularMenu->save();
                }
            }

        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $account)
    {
        $request->merge(['user_id'=>$account->id]);
        $matchThese = array('user_id'=>$account->id);
        $settings = Settings::updateOrCreate($matchThese,[
            'category_design'   =>$request->category_design,
            'heatmap'           =>$request->heatmap,
            'count'             =>  $request->count,
            'specialoffer'      => $request->specialoffer
        ]);

        if($settings->id){
            if ($request->hasFile('logo')) {

                $logo = $request->file('logo');
                $extension = $logo->getClientOriginalExtension();
                $filename = md5('_'.time()).'.'. $extension;
                $path = public_path('media/design/logo/'.$settings->id.'/');

                try {
                    $logo->move($path, $filename);
                    $settings->logo = $filename;
                    $settings->save();
                } catch (Exception $e) {
                    return $e->getMessage();
                }


            }
            if ($request->hasFile('background')) {
//                $background = $request->file('background');
                $file_name = $request->file('background')->getClientOriginalName();
                $destination = public_path('media/design/background/'.$settings->id.'/');

                try {
                    $request->file('background')->move($destination, $file_name);
                    $settings->background = $file_name;
                    $settings->save();
                } catch (Exception $e) {
                    return $e->getMessage();
                }

            }
            session()->flash("toastr", ["message" => "Settings saved successfully.", "title" => "Created!", "type" => "success"]);
            return redirect()->action('Dashboard\ThemeController@index',['account'=>$account]);
        }
        session()->flash("toastr", ["message" => "Settings can not be saved.", "title" => "Oops!", "type" => "error"]);
        return back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Settings $settings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
