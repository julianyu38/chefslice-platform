<?php

namespace App\Http\Controllers\Dashboard;

use App\Section;
use App\Service;
use App\SiteSetting;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Analytics;
use Spatie\Analytics\Period;

class HomeController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }
    public function index(User $account){
        if($account->user_name ==='admin') {
            $analytics['browsers'] = Analytics::fetchTopBrowsers(Period::days(30), 5);
            $analytics['countries'] = Analytics::performQuery(Period::days(30), 'ga:sessions', ['dimensions' => 'ga:country', 'sort' => '-ga:sessions', 'max-results' => 10])->rows;
            //$analytics['analytics'] = Analytics::performQuery(Period::days(30),'ga:sessions,ga:bounceRate,ga:newUsers,ga:avgSessionDuration,ga:pageviews,ga:hits,ga:exitRate')->totalsForAllResults;
            //$analytics['countries'] =  $this->shuffle_array($countries);
            $startDate = Carbon::now()->subDay(30);
            $endDate = Carbon::now();
            $analytics['analytics'] = Analytics::performQuery(Period::create($startDate, $endDate), 'ga:newUsers,ga:visits,ga:users,ga:bounceRate,ga:pageviews,ga:sessions,ga:entranceBounceRate,ga:hits,ga:exitRate', ['dimensions' => 'ga:date'])->rows;
            return view('dashboard.index', $analytics);
        }
        return view('dashboard.user-index');
    }


    /**
     * Show the form for editing the user profile.
     *
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function profile(User $account)
    {
        $user = $account;
        return view('dashboard.user.edit',compact('user'));
    }



    public function slider(){
        return view('dashboard.home-page.slider');
    }
    public function update_slider(Request $request){
        if($request->hasFile('image')){
            $images = $request->file('image');
            foreach ($images as $id => $image){
                $_image =  SiteSetting::whereId($id)->whereGroup('site_slider_images')->first();
               if($_image){
                   // Update Image => old image will be replaced
                    $file_name = md5('slider_image_'.$_image->id);
                   if ( file_exists(public_path('images/' .$_image->value)) && !empty($_image->value)) {
                       unlink(public_path('images/' . $_image->value));
                   }
                    if($file_name = $this->image_uploaded($image,'images/',$file_name)){
                        $_image->value = $file_name;
                        $_image->save();
                    }
               }else{
                   //Add New Image
                    $_image = SiteSetting::create([
                        'group' => 'site_slider_images',
                        'type' => 'image-'.$id,
                        'title' => 'Slider Image '.$id,
                        'value' => ''
                    ]);
                    if($_image->id){
                        $file_name = 'slider_image_'.$_image->id;
                        if($file_name = $this->image_uploaded($image,'images/',$file_name)){
                            $_image->value = $file_name;
                            $_image->save();
                        }
                    }
               }
            }
            session()->flash("toastr", ["message" => "Updated successfully.", "title" => "Updated!", "type" => "success"]);
            return back();
        }
        session()->flash("toastr", ["message" => "Can not updated.", "title" => "Error!", "type" => "danger"]);
        return back();
    }

    public function delete_slider(User $account,$id){
        $image =  SiteSetting::findOrFail($id);
        $message = "Can not deleted.";
        try {
            if (file_exists(public_path('images/' . $image->value)) && !empty($image->value)) {
                unlink(public_path('images/' . $image->value));
            }
            $image->delete();
            session()->flash("toastr", ["message" => "Deleted successfully.", "title" => "Deleted!", "type" => "success"]);
            return back();
        }catch (\Exception $ex){
            $message = $ex->getMessage();
        }
        session()->flash("toastr", ["message" => $message, "title" => "Error!", "type" => "danger"]);
        return back();

    }
    public function site_info(){
        return view('dashboard.home-page.info');
    }

    public function services(){
        $sections = Section::first();
        $services = Service::all();
        return view('dashboard.home-page.services',compact('services','sections'));
    }
    public function how_work(){
        $sections = Section::first();
        return view('dashboard.home-page.how', compact('sections'));
    }
    public function site_about(){
        return view('dashboard.home-page.about');
    }
    public function site_clients(){
        $sections = Section::first();
        return view('dashboard.home-page.clients', compact('sections'));
    }

    public function pricing(){
        $sections = Section::first();
        return view('dashboard.home-page.pricing', compact('sections'));
    }
    public function site_menu(){
        return view('dashboard.home-page.menu');
    }

    public function map(){
        $sections = Section::first();
        return view('dashboard.home-page.map',compact('sections'));
    }


    private function image_uploaded($file,$path,$name=null){
        if($name==null)
            $name = md5(uniqid().time().microtime());
            try{
                $extension = $file->getClientOriginalExtension();
                if(strtolower($extension) == 'png' || strtolower($extension) =='jpg' || strtolower($extension) =='gif'){
                       $filename = md5($name) . '.' . $extension;
                      $file->move($path, $filename);
                    return $filename;
                }
                return false;
            }catch (\Exception $e){
                return false;
            }
    }


    public function update_settings(Request $request){

        if($request->has('_group')){
            $section = Section::first();

            $group = $request->input('_group');
            $section->timestamps = false;
            $section->$group=$request->input($group);

            $section->save();
//            $request = $request->except($group);
            unset($request->$group);
            foreach ($request->all() as $name => $input){
               if($name != '_group' && $name!='_token' && $name !='_method' && $name!=$group ){
                   $setting = SiteSetting::firstOrNew(['group' => $group, 'type' => $name]);
                    if ($group) {

                    }
                   //Check for website logo
                   if($group == 'site_info' && $request->hasFile('logo') && $name =='logo'){
                       $upload_path = 'assets/home/img/';
                       $logo = $request->file('logo');

                       if (file_exists(public_path($upload_path . $setting->value)) && !empty($setting->value)) {
                           unlink(public_path($upload_path . $setting->value));
                       }
                      $input = $this->image_uploaded($logo,$upload_path,'logo');
                   }


                   $setting->value = $input;
                   $setting->save();
               }
            }
            session()->flash("toastr", ["message" => "Updated successfully.", "title" => "Updated!", "type" => "success"]);
            return back();
        }

        session()->flash("toastr", ["message" => "Can not updated.", "title" => "Error!", "type" => "danger"]);
        return back();
    }
}
