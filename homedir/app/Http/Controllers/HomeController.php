<?php

namespace App\Http\Controllers;

use App\Category;
use App\CircularMenu;
use App\Heatmap;
use App\Item;
use App\Page;
use App\Section;
use App\Settings;
use App\Service;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem as File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {

        $clients = User::where('user_name','!=','admin')->orderBy(\DB::raw('rand()'))->limit(10)->get();
        $sections= Section::first();
        $services = Service::all();
        return view('home',compact('clients','services','sections'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $account)
    {

        if($account->user_name === 'admin'){
            return redirect(env('APP_URL_PROTOCOL').'://'.env('APP_URL').'/login');
        }
//        app()->setLocale('he');
        $categories = $account->categories;
        $menus = CircularMenu::where('user_id',$account->id)->get();
        $heatmap = Heatmap::where('user_id',$account->id)->where('location','/')->get();
        $settings = Settings::where('user_id', $account->id)->first();

        return view('category-design.'.$settings->category_design, compact('categories','settings','heatmap', 'menus'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function products(User $account,$category)
    {
        if ($category=='login'){
            return redirect(env('APP_URL_PROTOCOL').'://'.env('APP_URL').'/login');
        }

//        $category = Category::whereSlug($category)->firstOrFail();
        $category = Category::whereTranslation('slug', $category)->firstOrFail();
        $category_id = $category->id;
        $menus = CircularMenu::where('user_id',$account->id)->get();
        $settings = Settings::where('user_id', $account->id)->first();
        $heatmap = Heatmap::where('user_id',$account->id)->where('location','/'.$category->slug)->limit($settings->count)->get();
        $query = Item::with('images');
        $query->where(function ($q) use ($category_id) {
            $q->where('category_id', '=', $category_id);
        });
        $items = $query->get();

//        $items = $account->category_products($category->id)->get();
        return view('list',compact('items','category','settings','heatmap','menus'));

    }

    public function special(User $account, Request $request){
        $query = Item::with('images')->where('is_special',1);
        $items = $query->get();
        $menus = CircularMenu::where('user_id',$account->id)->get();
        $heatmap = Heatmap::where('user_id',$account->id)->where('location','/special')->get();
        $settings = Settings::where('user_id', $account->id)->first();
        return view('list',compact('items','settings','heatmap','menus'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function cms(User $account,$slug)
    {
        $page = Page::whereSlug($slug)->firstOrFail();
        $menus = CircularMenu::where('user_id',$account->id)->get();
        $settings = Settings::where('user_id', $account->id)->first();
        $heatmap = Heatmap::where('user_id',$account->id)->where('location','/'.$slug)->limit($settings->count)->get();
        return view('cms',compact('page','settings','menus','heatmap'));
    }

    public function heatmap(User $account, Request $request){
        $request->merge(['user_id'=>$account->id]);
        Heatmap::create($request->all());
    }

    public function items(){
        $items = Item::all();

        foreach ($items as $item) {
            $path = public_path('media/items/images/');
            $copy = $path.'/165/';
            $path.=$item->id.'/';
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
                echo "created\n";
            }
            $this->recurse_copy($copy, $path);

        }


    }
    public function recurse_copy($src,$dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    recurse_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
}
