<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LanguageLocalizationController extends Controller
{
    public function index(Request $request) {
//        if ($request->lang <> '') {
//            app()->setLocale($request->lang);
//        }
        if (array_key_exists($request->lang, Config::get('languages'))) {
            Session::put('applocale',$request->lang);
        }
//        return Redirect::back();
        return back();
    }
}
