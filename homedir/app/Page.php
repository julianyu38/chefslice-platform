<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    protected $fillable = [
        'user_id','title', 'slug', 'body'
    ];
    protected  $dates =['created_at','updated_at'];
    public function user(){
        return $this->hasOne(User::class,'id');
    }
}
