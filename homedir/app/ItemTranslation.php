<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ItemTranslation extends Model
{

    protected $table ='product_translation';
    protected $fillable = ['title','short_description','description','item_id'];


}
