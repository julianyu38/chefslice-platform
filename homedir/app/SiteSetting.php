<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    public $table = 'site_settings';
    public $fillable = ['group','type','title','value','order'];
    public $timestamps = ['created_at','updated_at'];
}
