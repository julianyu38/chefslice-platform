<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    protected $table ='category_translation';
    protected $fillable = ['slug','text','description','category_id'];
    protected  $dates =['created_at','updated_at'];


}
