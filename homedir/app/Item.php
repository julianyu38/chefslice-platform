<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use \Dimsav\Translatable\Translatable;
    public $translatedAttributes = ['title','short_description','description'];
    protected $table = 'items';
    protected $fillable = [
        'category_id','image','is_special','user_id'
    ];
    protected  $dates =['created_at','updated_at'];
    public function category(){
        return $this->hasOne(Category::class,'id','category_id');
    }
    public function images() {
        return$this->hasMany(Images::class, 'item_id','id');
    }
}
