<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CircularMenu extends Model
{
    protected $table = 'circular_menus';
    protected $fillable = [
        'user_id','title', 'slug', 'icon'
    ];
    protected  $dates =['created_at','updated_at'];
    public function user(){
        return $this->hasOne(User::class,'id');
    }
}
