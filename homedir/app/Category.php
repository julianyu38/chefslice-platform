<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['slug','text','description'];
    protected $table = 'categories';
    protected $fillable = [
         'icon','user_id'
    ];
    protected  $dates =['created_at','updated_at'];
    public function items(){
     return $this->hasMany(Item::class,'category_id');
    }
    public function translations(){
        return $this->hasMany(CategoryTranslation::class,'category_id');
    }

}
