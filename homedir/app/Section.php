<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model

{
   public $table = 'sections';
   public $fillable = ['site_services', 'site_clients', 'site_how','site_pricing','site_map'];
    public $timestamps = ['created_at','updated_at'];


}
