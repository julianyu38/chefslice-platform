@extends('dashboard.layouts.app')
@section('header')
    <section class="content-header hidden-xs">
        <div class="header-icon">
            <i class="fa fa-university"></i>
        </div>
        <div class="header-title">
            <h1>Categories</h1>
            <small>Manage  categories of {{ env('APP_NAME',"Barbados Sotheby's") }}.</small>
            @include('dashboard.partials.quick-links')
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="pe-7s-home"></i> Home</a></li>
                <li><a href="{{ url('/dashboard') }}"> Dashboard</a></li>
               <li class="active">Categories</li>
            </ol>
        </div>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12 " >
            <div class="panel panel-bd ">
                <div class="panel-heading ">
                    <div  class="panel-title col-sm-10">
                        <h4>Category </h4>
                    </div>
                    <div  class="col-sm-2 text-right">
                        <a data-toggle="tooltip" data-title="New Category" data-placement="bottom" href="{{  url('dashboard/category/create') }}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add New</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name </th>
                                <th>Description </th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($categories->count()>0)
                                @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->id }}</td>
                                    <td>{{ $category->text }}</td>
                                    <td>{{ str_limit($category->description,50) }}</td>
                                    <td>{{ $category->created_at->diffForHumans() }}</td>
                                    <td>{{ $category->updated_at->diffForHumans() }}</td>
                                    <td>
                                       {{--  <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Show Properties"><i class="fa fa-eye" aria-hidden="true"></i></button> --}}
                                        <a href="{{  url('dashboard/category/'.$category->id.'/edit') }}" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="" data-original-title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        {!! Form::open(['url'=>['/dashboard/category/'.$category->id],'method'=>'delete','style'=>'display:inline;']) !!}
                                            <button type="button" class="btn btn-danger btn-sm btn--delete--item" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        {!! Form::close() !!}


                                    </td>
                                </tr>
                                @endforeach
                             @else
                                <tr>
                                    <td colspan="9">
                                        <p class="alert alert-warning text-center">
                                            No category found.
                                        </p>
                                    </td>
                                </tr>
                             @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix text-center">
                    {{ $categories->links() }}
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        $(document).on('click','.btn--delete--item', function () {
            var form = $(this).parents('form:first');

            $.confirm({
                icon: 'fa fa-trash-o',
                title: 'Delete Category!',
                closeIcon: true,
                animation: 'rotate',
                closeAnimation: 'rotate',
                content: 'Are you want to delete this category?',
                confirmButton: 'Yes',
                cancelButton: 'No',
                confirmButtonClass: 'btn-danger',
                cancelButtonClass: 'btn-info',
                confirm: function(){
                    form.submit();
                }
            });
        });
    </script>
@endsection