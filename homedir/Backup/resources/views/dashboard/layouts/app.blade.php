<!DOCTYPE html>
<html lang="en">
@include('dashboard.partials.head')
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    @include('dashboard.partials.header')
    <!-- =============================================== -->
    <!-- Left side column. contains the sidebar -->
    @include('dashboard.partials.sidebar')
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @yield('header')
        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section> <!-- /.content -->
    </div> <!-- /.content-wrapper -->
    @include('dashboard.partials.footer')
</div>
<!-- ./wrapper -->

<!-- Start Core Plugins
=====================================================================-->
@include('dashboard.partials.scripts')
</body>
</html>