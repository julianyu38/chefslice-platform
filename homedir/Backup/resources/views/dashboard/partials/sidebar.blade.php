<aside class="main-sidebar">
    <!-- sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel text-center">
            <div class="image">
                @if(!empty((auth()->user()->image)) && file_exists(public_path('images/users/'.(auth()->user()->image))))
                    <img class="img-circle" src="{{ asset('images/users/'.auth()->user()->image) }}"
                         alt="{{ auth()->user()->name }}">
                @else
                    <img class="img-circle" src="{{ asset('images/not-found.jpg') }}" alt="{{ auth()->user()->name }}">
                @endif
            </div>
            <div class="info">
                <p>{{ auth()->user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn"><i
                                            class="fa fa-search"></i></button>
                            </span>
            </div>
        </form> <!-- /.search form -->
        <!-- sidebar menu -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ request()->segment(2)==''?'active':'' }}">
                <a href="{{ url('/dashboard') }}"><i class="ti-home"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview {{ (request()->segment(2)=='category') ?'active':'' }}">
                <a href="#">
                    <i class="fa fa-th"></i> <span>Categories</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    <span class="clearfix"></span>
                </a>
                <ul class="treeview-menu ">
                    <li class="{{ (request()->segment(2)=='category' && request()->segment(3)=='create') ?'active':'' }}"><a href="{{ url('dashboard/category/create') }}">Add New</a></li>
                    <li class="{{ (request()->segment(2)=='category' && request()->segment(3)=='') ?'active':'' }}"><a href="{{ url('dashboard/category') }}">View All</a></li>
                </ul>
            </li>
            <li class="treeview {{ (request()->segment(2)=='item') ?'active':'' }}">
                <a href="#">
                    <i class="fa fa-list"></i> <span>Products</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    <span class="clearfix"></span>
                </a>
                <ul class="treeview-menu ">
                    <li class="{{ (request()->segment(2)=='item' && request()->segment(3)=='create') ?'active':'' }}"><a href="{{ url('dashboard/item/create') }}">Add New</a></li>
                    <li class="{{ (request()->segment(2)=='item' && request()->segment(3)=='') ?'active':'' }}"><a href="{{ url('dashboard/item') }}">View All</a></li>
                </ul>
            </li>



            @if(auth()->user()->user_name === 'admin')
            <li class="treeview {{ (request()->segment(2)=='user') ?'active':'' }}">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Users</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    <span class="clearfix"></span>
                </a>
                <ul class="treeview-menu ">
                    <li class="{{ (request()->segment(2)=='user' && request()->segment(3)=='create') ?'active':'' }}"><a href="{{  url('dashboard/user/create') }}">Add New</a></li>
                    <li class="{{ (request()->segment(2)=='user' && request()->segment(3)=='') ?'active':'' }}"><a href="{{  url('dashboard/user') }}">View All</a></li>
                </ul>
            </li>

            {{--<li class="treeview {{ ( request()->segment(2)=='role') ?'active':'' }}">
                <a href="#">
                    <i class="fa fa-lock"></i> <span>User Roles</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    <span class="clearfix"></span>
                </a>
                <ul class="treeview-menu ">
                    <li class="{{ (request()->segment(2)=='role' && request()->segment(3)=='create') ?'active':'' }}"><a href="{{ url('dashboard/role/create') }}">Add New</a></li>
                    <li class="{{ (request()->segment(2)=='role' && request()->segment(3)=='') ?'active':'' }}"><a href="{{  url('dashboard/role') }}">View All</a></li>
                </ul>
            </li>--}}


            {{--<li class="treeview {{ (request()->segment(2)=='page') ?'active':'' }}">
                <a href="#">
                    <i class="fa fa-file-o"></i> <span>Static Pages</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    <span class="clearfix"></span>

                </a>
                <ul class="treeview-menu">
                    <li class="{{ (request()->segment(2)=='page' && request()->segment(3)=="create") ?'active':'' }}"><a href="#">New Page</a></li>
                    <li class="{{ (request()->segment(2)=='page' && request()->segment(3)=="") ?'active':'' }}"><a href="#">All Page</a></li>
                </ul>
            </li>--}}
                <li class="treeview {{ request()->segment(2) =='analytic'?'active':'' }}">
                    <a href="#">
                        <i class="fa fa-bar-chart-o"></i> <span>Google Analytic</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        <span class="clearfix"></span>

                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ (request()->segment(2)=='analytic' && request()->segment(3)=="countries") ?'active':'' }}"><a href="{{ url('dashboard/analytic/countries') }}">Sessions by Country</a></li>
                        <li class="{{ (request()->segment(2)=='analytic' && request()->segment(3)=="browsers") ?'active':'' }}"><a href="{{ url('dashboard/analytic/browsers') }}">Browser</a></li>
                        <li class="{{ (request()->segment(2)=='analytic' && request()->segment(3)=="os") ?'active':'' }}"><a href="{{ url('dashboard/analytic/os') }}">Operating System</a></li>
                        <li class="{{ (request()->segment(2)=='analytic' && request()->segment(3)=="sources") ?'active':'' }}"><a href="{{ url('dashboard/analytic/sources') }}">Traffic Sources</a></li>
                        <li class="{{ (request()->segment(2)=='analytic' && request()->segment(3)=="mobile") ?'active':'' }}"><a href="{{ url('dashboard/analytic/mobile') }}">Mobile Traffic</a></li>
                        <li class="{{ (request()->segment(2)=='analytic' && request()->segment(3)=="sites") ?'active':'' }}"><a href="{{ url('dashboard/analytic/sites') }}">Referring Sites</a></li>
                        <li class="{{ (request()->segment(2)=='analytic' && request()->segment(3)=="keywords") ?'active':'' }}"><a href="{{ url('dashboard/analytic/keywords') }}">Keywords</a></li>
                        <li class="{{ (request()->segment(2)=='analytic' && request()->segment(3)=="landing_pages") ?'active':'' }}"><a href="{{ url('dashboard/analytic/landing_pages') }}">Top Landing Pages</a></li>
                        <li class="{{ (request()->segment(2)=='analytic' && request()->segment(3)=="exit_pages") ?'active':'' }}"><a href="{{ url('dashboard/analytic/exit_pages') }}">Top Exit Pages</a></li>
                    </ul>
                </li>

            <li class="treeview {{ request()->segment(2) =='heat_map'?'active':'' }}">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i> <span>HeatMap</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    <span class="clearfix"></span>

                </a>
                <ul class="treeview-menu">
                    <li class="{{ (request()->segment(2)=='heat_map' && request()->segment(3)=="countries") ?'active':'' }}"><a href="#">Sessions by Country</a></li>
                    <li class="{{ (request()->segment(2)=='heat_map' && request()->segment(3)=="browsers") ?'active':'' }}"><a href="#">Browser</a></li>
                    <li class="{{ (request()->segment(2)=='heat_map' && request()->segment(3)=="os") ?'active':'' }}"><a href="#">Operating System</a></li>
                    <li class="{{ (request()->segment(2)=='heat_map' && request()->segment(3)=="sources") ?'active':'' }}"><a href="#">Traffic Sources</a></li>
                    <li class="{{ (request()->segment(2)=='heat_map' && request()->segment(3)=="mobile") ?'active':'' }}"><a href="#">Mobile Traffic</a></li>
                    <li class="{{ (request()->segment(2)=='heat_map' && request()->segment(3)=="sites") ?'active':'' }}"><a href="#">Referring Sites</a></li>
                    <li class="{{ (request()->segment(2)=='heat_map' && request()->segment(3)=="keywords") ?'active':'' }}"><a href="#">Keywords</a></li>
                    <li class="{{ (request()->segment(2)=='heat_map' && request()->segment(3)=="landing_pages") ?'active':'' }}"><a href="#">Top Landing Pages</a></li>
                    <li class="{{ (request()->segment(2)=='heat_map' && request()->segment(3)=="exit_pages") ?'active':'' }}"><a href="#">Top Exit Pages</a></li>
                </ul>
            </li>



            <li class="treeview {{ request()->segment(2) =='currency'?'active':'' }}">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span>Settings</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('dashboard/settings/dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ url('dashboard/settings/home_page') }}">Home Settings</a></li>
                    <li><a href="{{ url('dashboard/settings/info') }}">General Information</a></li>
                    <li><a href="{{ url('dashboard/settings/social') }}">Social Accounts</a></li>
                </ul>
            </li>
            @else


                <li class="treeview {{ request()->segment(2) =='page'?'active':'' }}">
                    <a href="#">
                        <i class="fa fa-file-text" aria-hidden="true"></i> <span>Web Pages</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ (request()->segment(2)=='page' && request()->segment(3)=="create") ?'active':'' }}"><a href="#">New Page</a></li>
                        <li class="{{ (request()->segment(2)=='page' && request()->segment(3)=="") ?'active':'' }}"><a href="#">All Page</a></li>
                    </ul>
                </li>
                <li class="treeview {{ request()->segment(2) =='page'?'active':'' }}">
                    <a href="#">
                        <i class="fa fa-file-text" aria-hidden="true"></i> <span>Themes</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ (request()->segment(2)=='page' && request()->segment(3)=="create") ?'active':'' }}"><a href="#">New Page</a></li>
                        <li class="{{ (request()->segment(2)=='page' && request()->segment(3)=="") ?'active':'' }}"><a href="#">All Page</a></li>
                    </ul>
                </li>
            @endif
        </ul>
    </div> <!-- /.sidebar -->
</aside>