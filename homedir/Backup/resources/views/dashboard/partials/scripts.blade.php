<!-- Start Core Plugins
=====================================================================-->
<!-- jQuery -->
<script src="{{ asset('assets/plugins/jQuery/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
<!-- jquery-ui -->
<script src="{{ asset('assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js') }}" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ asset('assets/backend/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- lobipanel -->
<script src="{{ asset('assets/plugins/lobipanel/lobipanel.min.js') }}" type="text/javascript"></script>
<!-- Pace js -->
<script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="{{ asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset('assets/plugins/fastclick/fastclick.min.js') }}" type="text/javascript"></script>
<!-- AdminBD frame -->
<script src="{{ asset('assets/backend/dist/js/frame.js') }}" type="text/javascript"></script>

<!-- Toastr JS -->
<script src="{{ asset('assets/plugins/toastr/toastr.min.js') }}" type="text/javascript"></script>
<!-- Dashboard js -->
<script src="{{ asset('assets/backend/dist/js/dashboard.js') }}" type="text/javascript"></script>
<!-- End Theme label Script
=====================================================================-->
<!-- Notify -->
<script src="{{ asset('assets/plugins/jquery-confirm/jquery-confirm.min.js') }}"></script>

@if(Session::has('toastr') && isset(Session::get('toastr')['message']))
    <script>
        $(document).ready(function () {
            setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 5000
    //                        positionClass: "toast-top-left"
                };
                @if(Session::get('toastr')['type'] =='success')
                    toastr.success("{!! Session::get('toastr')['message'] !!}", "{!! Session::get('toastr')['title'] !!}");
                @elseif(Session::get('toastr')['type'] =='warning')
                    toastr.warning("{!! Session::get('toastr')['message'] !!}", "{!! Session::get('toastr')['title'] !!}");
                @elseif(Session::get('toastr')['type'] =='error')
                    toastr.error("{!! Session::get('toastr')['message'] !!}", "{!! Session::get('toastr')['title'] !!}");
                @endif
            }, 1300);
        });
    </script>
@endif

@yield('scripts')