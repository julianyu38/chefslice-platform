@extends('layouts.app')

@section('styles')
<!-- Animate.css -->
<link rel="stylesheet" href="{{ asset('theme/hydrogen/css/animate.css') }}">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="{{ asset('theme/hydrogen/css/icomoon.css') }}">

<!-- Salvattore -->
<link rel="stylesheet" href="{{ asset('theme/hydrogen/css/salvattore.css') }}">
<!-- Theme Style -->
<link rel="stylesheet" href="{{ asset('theme/hydrogen/css/style.css') }}">
@stop
@section('content')
    @include('partials.restaurant.circular')


<div id="fh5co-main">
    <div class="container">

        <div class="row">

            <div id="fh5co-board" data-columns>
                <?php
                $path = asset('media/items/images/');
                ?>
                @if($items->count()>0)
                    @foreach($items as $item)
                        <?php if($item->media_type == 2) { $path = asset('media/items/videos/'); } ?>
                        <div class="item">
                            <div class="animate-box">
                                <a href="#" onclick="buildModel(this)"  data-id="{{$item->id}}"
                                   class="image-popup fh5co-board-img"
                                   title="{{$item->title}}"><img
                                            src="{{$path.'/'.$item->media }}"
                                            alt="Free HTML5 Bootstrap template"></a>
                            </div>
                            <div class="fh5co-desc">{{$item->short_description}}
                            </div>
                        </div>

                    @endforeach
                @endif

            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal animated bounceInRight" id="myModal" role="dialog">
    {{--<div class="modal-dialog">--}}
    {{--<div class="container-fluid">--}}
    <div class="container">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Item Details</h4>
            </div>
            <div class="row">



                    <div class="col-md-8 pull-left">

                        <div id="rightImgHolder">
                            <img  src=""
                                        class="caroImg" id="caroImg0">
                        </div>
                    </div>
                    <div class="col-md-4  pull-right">
                        <div id="title" class="title"
                             style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); color: rgb(68, 68, 68); visibility: visible; opacity: 1;">
                            Portfolio Gallery
                        </div>
                        <div id="created-at" class="created-at"
                             style="transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); color: rgb(68, 68, 68); visibility: visible; opacity: 1;">
                            Created at Flash-gallery for FGN
                        </div>
                        <hr id="tophr" style="width: 100%;">
                        <div class="text trans3d" id="contentText"
                             style="height: 297px; top: 63px; overflow: hidden; opacity: 1; visibility: visible;">
                            <div class="contentScroll" id="contentDescription"
                                 style="width: 270px; transition-property: -webkit-transform; transform-origin: 0px 0px 0px; transform: translate(0px, 0px) translateZ(0px);">

                            </div>
                            <div style="position: absolute; z-index: 100; width: 7px; bottom: 2px; top: 2px; right: 1px; transition-property: opacity; overflow: hidden; opacity: 1;">
                                <div data-scrollbarindicator="true"
                                     style="position: absolute; z-index: 100; background: rgba(0, 0, 0, 0.5); border: 1px solid rgba(255, 255, 255, 0.9); -webkit-background-clip: padding-box; box-sizing: border-box; width: 100%; border-radius: 3px; transition-property: -webkit-transform; transition-timing-function: cubic-bezier(0.33, 0.66, 0.66, 1); transform: translate(0px, 0px) translateZ(0px); height: 238px;"></div>
                            </div>
                        </div>
                        <hr id="bottomhr" style="width: 100%; top: 380px;">
                        <div class="awards-list trans3d"
                             style="top: 400px; opacity: 1; visibility: visible; transform: matrix(1, 0, 0, 1, 0, 0);">
                            <a class="launch" style="color: rgb(68, 68, 68);">LAUNCH &gt;</a></div>
                    </div>
                    <div id="numImages" style="width: 18px; margin-left: -9px;">
                        <div class="caroImgItem selected" id="caroImgItem0"
                             style="opacity: 1; visibility: visible; transform: matrix(1, 0, 0, 1, 0, 0);"></div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@stop
@section('scripts')

<script>
    var obj = <?php echo json_encode($items) ?>;
    var assetUrl = '<?php echo $path ?>';
    //console.log(obj);
    values =obj;
    function buildModel(el){

     $('.animate-box').removeClass('bounceInLeft');
     $('.animate-box').addClass('animated bounceOutLeft');
//        values.forEach( function (arrayItem)
//        {
//            var x = arrayItem.text ;
//            alert(x);
//        });

    item = el.getAttribute('data-id');
        selectedItem=values[values.findIndex(x => x.id == item)];
        console.log(assetUrl);
        $('#caroImg0').attr( 'src',assetUrl+'/'+selectedItem.media);
        $('#caroImg0').css('width','100%');
        $('#title').html(selectedItem.title);
        $('#contentDescription').html(selectedItem.description);
        setTimeout(function() {
            $('#myModal').modal();
        }, 400);


    }
    $(document).ready(function () {
        $('.modal-content').addClass('animated bounceInLeft');
        var hideDelay = true;
        $('#myModal').on('hide.bs.modal',function(e){
            $('.animate-box').removeClass('bounceOutLeft');
           $('.animate-box').addClass('animated bounceInLeft');
            if(hideDelay){
                $('.modal-content').removeClass('animated bounceInLeft').addClass('animated bounceOutRight');
                hideDelay = false;
                setTimeout(function(){
                    $('#myModal').modal('hide');
                    $('.modal-content').removeClass('animated bounceOutRight').addClass('animated bounceInLeft');
                },700);
                return false;
            }
            hideDelay = true;
            $('.animate-box').removeClass('bounceOutLeft');
            $('.animate-box').addClass('animated bounceInLeft');
            return true;
        });
    });
</script>




<!-- Waypoints -->
<script src="{{ asset('theme/hydrogen/js/jquery.waypoints.min.js') }}"></script>

<!-- Salvattore -->
<script src="{{ asset('theme/hydrogen/js/salvattore.min.js') }}"></script>
<!-- Main JS -->
<script src="{{ asset('theme/hydrogen/js/main.js') }}"></script>
<!-- Modernizr JS -->
<script src="{{ asset('theme/hydrogen/js/modernizr-2.6.2.min.js') }}"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
<script src="{{ asset('theme/hydrogen/js/respond.min.js') }}"></script>
<![endif]-->


@stop