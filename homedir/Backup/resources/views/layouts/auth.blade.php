<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>
        @hasSection('title')
        @yield('title')
        @else
            {{ env('APP_NAME',"Barbados Sotheby's") }} - Dashboard
        @endif
    </title>

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    {{--<link rel="apple-touch-icon" type="image/x-icon" href="{{ asset('assets/backend/dist/img/ico/apple-touch-icon-57-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ asset('assets/backend/dist/img/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{ asset('assets/backend/dist/img/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{ asset('assets/backend/dist/img/ico/apple-touch-icon-144-precomposed.png') }}">
--}}
    <!-- Bootstrap -->
    <link href="{{ asset('assets/backend/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap rtl -->
    <!--<link href="{{ asset('assets/backend/bootstrap-rtl/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>') }}-->
    <!-- Pe-icon-7-stroke -->
    <link href="{{ asset('assets/backend/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" rel="stylesheet" type="text/css"/>
    <!-- style css -->
    <link href="{{ asset('assets/backend/dist/css/styleBD.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Theme style rtl -->
    <!--<link href="{{ asset('assets/backend/dist/css/styleBD-rtl.css" rel="stylesheet" type="text/css"/>') }}-->

    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck/square/blue.css') }}">
    <style>
        .login-logo{
            font-size: 35px;
            text-align: center;
            margin-bottom: 25px;
            font-weight: 300;
        }
        .default-bg{
            background-color: rgba(0,35,73,.8) !important;
            color: #eeeeee !important;
        }
        .default-bg.panel{
            border: none;
        }
        .view-header .header-icon{
            color: #eeeeee;
        }
        .panel-bd .panel-heading::before{
            border-top: 12px solid #eeeeee;
        }

    </style>
</head>
<body style="background:url('{{ asset('images/2.jpg') }}');background-repeat: no-repeat;background-size:cover">

@yield('content')

        <!-- jQuery -->
<script src="{{ asset('assets/plugins/jQuery/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
<!-- bootstrap js -->
<script src="{{ asset('assets/backend/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/icheck/icheck.min.js') }}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>