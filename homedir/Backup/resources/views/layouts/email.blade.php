<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
    <body style="margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;">
        {!! $content !!}
    </body>
</html>
