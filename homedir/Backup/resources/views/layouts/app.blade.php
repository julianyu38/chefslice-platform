
<!DOCTYPE html>
<html lang="en">

{{--<html xmlns="http://www.w3.org/1999/xhtml">--}}
@include('partials.restaurant.head')
<body style="background: #392338;">
<!-- main wrapper -->

    <!-- header -->
   {{--  @include('partials.header',['Info'=>$Info]) --}}
            <!-- /header -->
    {{-- @yield('breadcrumb') --}}
    @yield('content')
            <!-- /main wrapper -->
    <!-- footer -->
    {{-- @include('partials.footer',['Info'=>$Info]) --}}
  
  
<!-- /footer -->
@include('partials.restaurant.scripts')
</body>
</html>
