<!-- Menu Start -->

<section class="koc-menu koc-menu--circle koc-menu--top-right; koc-menu--bottom-left; koc-menu--bottom-right;">
    <input type="checkbox" id="koc-menu__active"/>
    <label for="koc-menu__active" class="koc-menu__active">

        <!-- Toggle Start -->

        <div class="koc-menu__toggle">
            <div class="icon">
                <div class="hamburger"></div>
            </div>
        </div>

        <!-- Toggle End -->

        <input type="radio" name="arrow--up" id="degree--up-0"/>
        <input type="radio" name="arrow--up" id="degree--up-1"/>
        <input type="radio" name="arrow--up" id="degree--up-2"/>

        <!-- Listings Start -->

        <div class="koc-menu__listings">
            <ul class="circle">

                <!-- Empty Start -->

                <li>
                    <div class="placeholder">
                        <div class="upside"><a href="#" class="button">&nbsp</a></div>
                    </div>
                </li>

                <!-- Empty End -->


                <!-- Ternary Start -->

                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="{{url('/')}}" class="button"><i class="fa fa-home"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-trash"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-battery-4"></i></a>
                        </div>
                    </div>
                </li>

                <!-- Ternary Stop -->


                <!-- Ternary Start -->

                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-calendar"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-cloud"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-wifi"></i></a>
                        </div>
                    </div>
                </li>

                <!-- Ternary Stop -->


                <!-- Ternary Start -->

                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-envelope-o"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-user"></i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="placeholder">
                        <div class="upside">
                            <a href="#" class="button"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                </li>

                <!-- Ternary Stop -->

            </ul>
        </div>

        <!-- Listings End -->


        <!-- Arrow Container Start -->

        <div class="koc-menu__arrow koc-menu__arrow--top">
            <ul>
                <li>
                    <label for="degree--up-0">
                        <div class="arrow"></div>
                    </label>
                    <label for="degree--up-1">
                        <div class="arrow"></div>
                    </label>
                    <label for="degree--up-2">
                        <div class="arrow"></div>
                    </label>
                </li>
            </ul>
        </div>

        <!-- Arrow Container End -->

    </label>
</section>

<!-- Menu End -->