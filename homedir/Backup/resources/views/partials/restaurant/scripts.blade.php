<!-- Scripts -->
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
<!-- jQuery Easing -->
<script src="{{ asset('theme/scripts/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('theme/hydrogen/js/jquery.easing.1.3.js') }}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{ asset('theme/scripts/mustache.min.js') }}"></script>
@yield('scripts')
