@extends('layouts.main')
@section('footer')
    <div class="row">
        <div class="col-sm-12 no-padding">
            <div class="map-wrapper">
                <div class="contact__floating-block">
                    <div class="row no-column-space equal-height-columns">
                        <div class="col-sm-6 no-padding equal-height-column">
                            <img class="img-responsive hidden-xs" src="{{ asset('assets/home/img-temp/contact/contact.jpg') }}" alt="">
                        </div>
                        <div class="col-sm-6 col-xs-12 contact__right--list text-center no-side-padding equal-height-column">
                            <div class="valign__middle">
                                <ul class="list-unstyled g-mb-20">
                                    <li class="first-item">Address</li>
                                    <li class="second-item">In sed lectus tincidunt</li>
                                </ul>

                                <ul class="list-unstyled g-mb-20">
                                    <li class="first-item">Phone number</li>
                                    <li class="second-item">+4586 585 4580</li>
                                </ul>

                                <ul class="list-unstyled g-mb-20">
                                    <li class="first-item">Email</li>
                                    <li class="second-item">info@unify.com</li>
                                </ul>

                                <ul class="list-unstyled">
                                    <li class="first-item">Toll Free</li>
                                    <li class="second-item">+4516 581 2340</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="map" class="contact__map"></div>
            </div>
        </div>
    </div>

@stop
@section('nav')
    <li class="page-scroll">
        <a href="#services">Services</a>
    </li>
    <li class="page-scroll hidden-md">
        <a href="#special">Special</a>
    </li>

    <li class="page-scroll">
        <a href="#good-taste">CUSTOMER CHOICE</a>
    </li>
    <li class="page-scroll">
        <a href="#team">Our team</a>
    </li>

    <li class="page-scroll">
        <a href="#booking">Booking Form</a>
    </li>
    <li class="page-scroll hidden-md">
        <a href="#contact">Contact</a>
    </li>
@stop
@section('content')
		<!-- About -->
		<section id="about">
			<div class="container-fluid no-padding">
				<div class="row no-margin equal-height-columns">
					<div class="col-md-6 bg-color-main equal-height-column about-us">
						<div class="valign__middle">
							<div class="heading-v20 heading-v20--diff text-center">
								<span class="heading-v20__pre-title g-dp-block g-mb-20">About us</span>
								<h2 class="heading-v20__title g-mb-30">We are the best</h2>
								<i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
								<p class="heading-v20__text">Sed feugiat porttitor nunc, non dignissim ipsum vestibulum in. Donec in blandit dolor. Vivamus a fringilla lorem, vel faucibus ante. Nunc ullamcorper, justo a iaculis elementum, enim orci viverra eros, fringilla porttitor lorem eros vel. </p><br>
								<p class="heading-v20__text">Fringilla lorem, vel faucibus ante. Nunc ullamcorper, justo a iaculis elementum, enim orci viverra eros, fringilla porttitor lorem eros vel.</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 no-padding">
						<div id="myCarousel" class="carousel slide carousel-v5">
							<div class="carousel-inner">
								<div class="item active equal-height-column">
									<div class="shadow"></div>
									<img class="equal-height-column" src="{{ asset('assets/home/img-temp/about/about1.jpg')}}" alt="">
								</div>
								<div class="item equal-height-column">
									<div class="shadow"></div>
									<img class="equal-height-column" src="{{ asset('assets/home/img-temp/about/about2.jpg') }}" alt="">
								</div>
								<div class="item equal-height-column">
									<div class="shadow"></div>
									<img class="equal-height-column" src="{{ asset('assets/home/img-temp/about/about3.jpg') }}" alt="">
								</div>
							</div>
							<div class="carousel-arrow">
								<a class="left carousel-control" href="#myCarousel" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								</a>
								<a class="right carousel-control" href="#myCarousel" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End About -->
<br />
		<br />
			<!-- Services -->
		<section id="services">
			<div class="container g-pb-80">
				<div class="heading-v20 text-center">
					<span class="heading-v20__pre-title g-dp-block g-mb-20">Services</span>
					<h2 class="heading-v20__title g-mb-30">What do we propose</h2>
					<i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
					<p class="heading-v20__text g-mb-20">Sed feugiat porttitor nunc, non dignissim ipsum vestibulum in. Donec in blandit dolor. Vivamus a fringilla lorem, vel faucibus ante.</p>
				</div>

				<div class="row">
					<div class="col-sm-3 service text-center">
							<span aria-hidden="true" class="icon-food-039 service__icon"></span>
							<h3 class="service__title g-mb-30"><a href="">Catering</a></h3>
							<p class="service__text">Nunc ligula nulla, efficitur et eros ut, vulputate gravida leo. Vestibulum ante ipsum primis in faucibus orci luctus et.</p>
					</div>
					<div class="col-sm-3 service text-center">
							<span aria-hidden="true" class="icon-food-008 service__icon"></span>
							<h3 class="service__title g-mb-30"><a href="">Wine collection</a></h3>
							<p class="service__text">Nunc ligula nulla, efficitur et eros ut, vulputate gravida leo. Vestibulum ante ipsum primis in faucibus orci luctus et.</p>
					</div>
					<div class="col-sm-3 service text-center">
							<span aria-hidden="true" class="icon-food-001 service__icon"></span>
							<h3 class="service__title g-mb-30"><a href="">Custom orders</a></h3>
							<p class="service__text">Nunc ligula nulla, efficitur et eros ut, vulputate gravida leo. Vestibulum ante ipsum primis in faucibus orci luctus et.</p>
					</div>
					<div class="col-sm-3 service text-center">
							<span aria-hidden="true" class="icon-food-276 service__icon"></span>
							<h3 class="service__title g-mb-30"><a href="">Wine collection</a></h3>
							<p class="service__text">Nunc ligula nulla, efficitur et eros ut, vulputate gravida leo. Vestibulum ante ipsum primis in faucibus orci luctus et.</p>
					</div>
				</div>
			</div>
		</section>
		<!-- End Services -->


		<!-- Special -->
		<section id="special">
			<div class="container-fluid no-padding bg-color-main">
				<div class="container content-lg">
					<!-- Owl Carousel v2-->
					<div class="special-dishes"><!--  owl2-carousel-v2 owl-theme -->
						<div class="item text-center">
							<div class="row">
								<div class="col-md-6 g-sm-mb-30">
									<div class="heading-v20 heading-v20--diff g-pt-70 g-md-pt-0 text-center">
										<span class="heading-v20__pre-title g-dp-block g-mb-20">From chef</span>
										<h2 class="heading-v20__title g-mb-30">Green soup with croutons</h2>
										<i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
										<p class="heading-v20__text g-mb-20">Sed feugiat porttitor nunc, non dignissim ipsum vestibulum in. Donec in blandit dolor. Vivamus a fringilla lorem, vel faucibus ante.</p>
										<span class="g-dp-block special__price g-mb-20">$14.00</span>
										<a href="#" class="btn-u global__btn-u">Book Now</a>
									</div>
								</div>
								<div class="col-md-6">
									<img class="img-responsive special__img" src="{{ asset('assets/home/img-temp/special/special1.png') }}" alt="">
								</div>
							</div>
						</div>

						<div class="item text-center">
							<div class="row">
								<div class="col-md-6 g-sm-mb-30">
									<div class="heading-v20 heading-v20--diff g-pt-70 g-md-pt-0 text-center">
										<span class="heading-v20__pre-title g-dp-block g-mb-20">From chef</span>
										<h2 class="heading-v20__title g-mb-30">Spaghetti</h2>
										<i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
										<p class="heading-v20__text g-mb-20">Sed feugiat porttitor nunc, non dignissim ipsum vestibulum in. Donec in blandit dolor. Vivamus a fringilla lorem, vel faucibus ante.</p>
										<span class="g-dp-block special__price g-mb-20">$14.00</span>
										<a href="#" class="btn-u global__btn-u">Book Now</a>
									</div>
								</div>
								<div class="col-md-6">
									<img class="img-responsive special__img" src="{{ asset('assets/home/img-temp/special/special2.png') }}" alt="">
								</div>
							</div>
						</div>

						<div class="item text-center">
							<div class="row">
								<div class="col-md-6 g-sm-mb-30">
									<div class="heading-v20 heading-v20--diff g-pt-70 g-md-pt-0 text-center">
										<span class="heading-v20__pre-title g-dp-block g-mb-20">From chef</span>
										<h2 class="heading-v20__title g-mb-30">Green soup with croutons</h2>
										<i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
										<p class="heading-v20__text g-mb-20">Sed feugiat porttitor nunc, non dignissim ipsum vestibulum in. Donec in blandit dolor. Vivamus a fringilla lorem, vel faucibus ante.</p>
										<span class="g-dp-block special__price g-mb-20">$14.00</span>
										<a href="#" class="btn-u global__btn-u">Book Now</a>
									</div>
								</div>
								<div class="col-md-6">
									<img class="img-responsive special__img" src="{{ asset('assets/home/img-temp/special/special3.png') }}" alt="">
								</div>
							</div>
						</div>

						<div class="item text-center">
							<div class="row">
								<div class="col-md-6 g-sm-mb-30">
									<div class="heading-v20 heading-v20--diff g-pt-70 g-md-pt-0 text-center">
										<span class="heading-v20__pre-title g-dp-block g-mb-20">From chef</span>
										<h2 class="heading-v20__title g-mb-30">Green soup with croutons</h2>
										<i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
										<p class="heading-v20__text g-mb-20">Sed feugiat porttitor nunc, non dignissim ipsum vestibulum in. Donec in blandit dolor. Vivamus a fringilla lorem, vel faucibus ante.</p>
										<span class="g-dp-block special__price g-mb-20">$14.00</span>
										<a href="#" class="btn-u global__btn-u">Book Now</a>
									</div>
								</div>
								<div class="col-md-6">
									<img class="img-responsive special__img" src="{{ asset('assets/home/img-temp/special/special4.png') }}" alt="">
								</div>
							</div>
						</div>

						<div class="item text-center">
							<div class="row">
								<div class="col-md-6 g-sm-mb-30">
									<div class="heading-v20 heading-v20--diff g-pt-70 g-md-pt-0 text-center">
										<span class="heading-v20__pre-title g-dp-block g-mb-20">From chef</span>
										<h2 class="heading-v20__title g-mb-30">Green soup with croutons</h2>
										<i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
										<p class="heading-v20__text g-mb-20">Sed feugiat porttitor nunc, non dignissim ipsum vestibulum in. Donec in blandit dolor. Vivamus a fringilla lorem, vel faucibus ante.</p>
										<span class="g-dp-block special__price g-mb-20">$14.00</span>
										<a href="#" class="btn-u global__btn-u">Book Now</a>
									</div>
								</div>
								<div class="col-md-6">
									<img class="img-responsive special__img" src="{{ asset('assets/home/img-temp/special/special5.png') }}" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Special -->

		

	
		
		<!-- Good Taste -->
		<section id="good-taste">
			<div class="container-fluid bg-color-secondary">
				<div class="news g-pt-85 g-pb-100 text-center">
					<div class="container">
						<div class="heading-v20 text-center g-mb-50">
							<span class="heading-v20__pre-title g-dp-block g-mb-20">Customer Choice</span>
							<h2 class="heading-v20__title g-mb-30">Good taste</h2>
							<i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
							<p class="heading-v20__text">Etiam ultrices lacus ut ligula vestibulum, sit amet mattis nunc elementum. Nam arcu enim, euismod nec purus non, aliquam congue ante. Nulla faucibus enim mauris, fringilla mollis ligula sollicitudin mollis.</p>
						</div>
						<!-- Owl Carousel v4-->
						<div class="owl2-carousel-v4 owl-theme">
							<div class="item">
								<div class="post">
									<img class="post__img img-responsive" src="{{ asset('assets/home/img-temp/good-taste/good_taste1.jpg') }}" alt="">
									<div class="post__child text-left">
										<span class="post__child--cat">Steak</span>
										<h3 class="post__child--title g-mb-20">Steak + Potatoes</h3>
										<p class="post__child--text g-mb-20">Curabitur eget tortor sed urna faucibus iaculis id et nulla. Aliquam erat volutpat.</p>
										<span class="post__child--price">$12.99</span>
									</div>
								</div>
							</div>

							<div class="item">
								<div class="post">
									<img class="post__img img-responsive" src="{{ asset('assets/home/img-temp/good-taste/good_taste2.jpg') }}" alt="">
									<div class="post__child text-left">
										<span class="post__child--cat">Steak</span>
										<h3 class="post__child--title g-mb-20">Steak + Potatoes</h3>
										<p class="post__child--text g-mb-20">Curabitur eget tortor sed urna faucibus iaculis id et nulla. Aliquam erat volutpat.</p>
										<span class="post__child--price">$12.99</span>
									</div>
								</div>
							</div>

							<div class="item">
								<div class="post">
									<img class="post__img img-responsive" src="{{ asset('assets/home/img-temp/good-taste/good_taste3.jpg') }}" alt="">
									<div class="post__child text-left">
										<span class="post__child--cat">Steak</span>
										<h3 class="post__child--title g-mb-20">Steak + Potatoes</h3>
										<p class="post__child--text g-mb-20">Curabitur eget tortor sed urna faucibus iaculis id et nulla. Aliquam erat volutpat.</p>
										<span class="post__child--price">$12.99</span>
									</div>
								</div>
							</div>

							<div class="item">
								<div class="post">
									<img class="post__img img-responsive" src="{{ asset('assets/home/img-temp/good-taste/good_taste4.jpg') }}" alt="">
									<div class="post__child text-left">
										<span class="post__child--cat">Steak</span>
										<h3 class="post__child--title g-mb-20">Steak + Potatoes</h3>
										<p class="post__child--text g-mb-20">Curabitur eget tortor sed urna faucibus iaculis id et nulla. Aliquam erat volutpat.</p>
										<span class="post__child--price">$12.99</span>
									</div>
								</div>
							</div>

							<div class="item">
								<div class="post">
									<img class="post__img img-responsive" src="{{ asset('assets/home/img-temp/good-taste/good_taste5.jpg') }}" alt="">
									<div class="post__child text-left">
										<span class="post__child--cat">Steak</span>
										<h3 class="post__child--title g-mb-20">Steak + Potatoes</h3>
										<p class="post__child--text g-mb-20">Curabitur eget tortor sed urna faucibus iaculis id et nulla. Aliquam erat volutpat.</p>
										<span class="post__child--price">$12.99</span>
									</div>
								</div>
							</div>

							<div class="item">
								<div class="post">
									<img class="post__img img-responsive" src="{{ asset('assets/home/img-temp/good-taste/good_taste6.jpg') }}" alt="">
									<div class="post__child text-left">
										<span class="post__child--cat">Steak</span>
										<h3 class="post__child--title g-mb-20">Steak + Potatoes</h3>
										<p class="post__child--text g-mb-20">Curabitur eget tortor sed urna faucibus iaculis id et nulla. Aliquam erat volutpat.</p>
										<span class="post__child--price">$12.99</span>
									</div>
								</div>
							</div>

							<div class="item">
								<div class="post">
									<img class="post__img img-responsive" src="{{ asset('assets/home/img-temp/good-taste/good_taste7.jpg') }}" alt="">
									<div class="post__child text-left">
										<span class="post__child--cat">Steak</span>
										<h3 class="post__child--title g-mb-20">Steak + Potatoes</h3>
										<p class="post__child--text g-mb-20">Curabitur eget tortor sed urna faucibus iaculis id et nulla. Aliquam erat volutpat.</p>
										<span class="post__child--price">$12.99</span>
									</div>
								</div>
							</div>

							<div class="item">
								<div class="post">
									<img class="post__img img-responsive" src="{{ asset('assets/home/img-temp/good-taste/good_taste8.jpg') }}" alt="">
									<div class="post__child text-left">
										<span class="post__child--cat">Steak</span>
										<h3 class="post__child--title g-mb-20">Steak + Potatoes</h3>
										<p class="post__child--text g-mb-20">Curabitur eget tortor sed urna faucibus iaculis id et nulla. Aliquam erat volutpat.</p>
										<span class="post__child--price">$12.99</span>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="post">
									<img class="post__img img-responsive" src="{{ asset('assets/home/img-temp/good-taste/good_taste9.jpg') }}" alt="">
									<div class="post__child text-left">
										<span class="post__child--cat">Steak</span>
										<h3 class="post__child--title g-mb-20">Steak + Potatoes</h3>
										<p class="post__child--text g-mb-20">Curabitur eget tortor sed urna faucibus iaculis id et nulla. Aliquam erat volutpat.</p>
										<span class="post__child--price">$12.99</span>
									</div>
								</div>
							</div>
						</div>
						<div class="g-mb-40"></div>
						<a href="#" class="btn-u global__btn-u global__btn-u--diff">View More</a>
					</div>
				</div>
			</div>
		</section>
		<!-- End Good Taste -->

		<!-- Team -->
		<section id="team">
			<div class="container content-lg">
				<div class="g-pb-90">
					<div class="heading-v20 text-center g-mb-30">
						<span class="heading-v20__pre-title g-dp-block g-mb-20">Our team</span>
						<h2 class="heading-v20__title g-mb-30">Meet the professionals</h2>
						<i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
					</div>

					<!-- Owl Carousel v3-->
					<div class="owl2-carousel-v3 owl-theme">
						<div class="item text-center">
							<div class="team-m">
								<img class="img-responsive" src="{{ asset('assets/home/img-temp/team/team1.jpg') }}" alt="">
								<div class="team-m__info">
									<span class="team-m__job">Chef</span>
									<h3 class="team-m__name">Mona Watson</h3>
									<p class="team-m__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
									<ul class="list-inline team-m__social">
										<li><a href="#" class="team-m__social-link"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#" class="team-m__social-link"><i class="fa fa-pinterest"></i></a></li>
										<li><a href="#" class="team-m__social-link"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#" class="team-m__social-link"><i class="fa fa-linkedin"></i></a></li>
									</ul>
							</div>
							</div>
						</div>
						<div class="item team-m text-center">
							<img class="img-responsive" src="{{ asset('assets/home/img-temp/team/team2.jpg') }}" alt="">
							<div class="team-m__info">
								<span class="team-m__job">Chef</span>
								<h3 class="team-m__name">Edward Luma</h3>
								<p class="team-m__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<ul class="list-inline team-m__social">
									<li><a href="#" class="team-m__social-link"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-pinterest"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="item team-m text-center">
							<img class="img-responsive" src="{{ asset('assets/home/img-temp/team/team3.jpg') }}" alt="">
							<div class="team-m__info">
								<span class="team-m__job">Chef</span>
								<h3 class="team-m__name">Shadiah Dollumn</h3>
								<p class="team-m__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<ul class="list-inline team-m__social">
									<li><a href="#" class="team-m__social-link"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-pinterest"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="item team-m text-center">
							<img class="img-responsive" src="{{ asset('assets/home/img-temp/team/team4.jpg') }}" alt="">
							<div class="team-m__info">
								<span class="team-m__job">Chef</span>
								<h3 class="team-m__name">George Freeman</h3>
								<p class="team-m__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<ul class="list-inline team-m__social">
									<li><a href="#" class="team-m__social-link"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-pinterest"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="item team-m text-center">
							<img class="img-responsive" src="{{ asset('assets/home/img-temp/team/team5.jpg') }}" alt="">
							<div class="team-m__info">
								<span class="team-m__job">Chef</span>
								<h3 class="team-m__name">Hilary Johnson</h3>
								<p class="team-m__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<ul class="list-inline team-m__social">
									<li><a href="#" class="team-m__social-link"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-pinterest"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="item team-m text-center">
							<img class="img-responsive" src="{{ asset('assets/home/img-temp/team/team6.jpg') }}" alt="">
							<div class="team-m__info">
								<span class="team-m__job">Chef</span>
								<h3 class="team-m__name">Alice Regen</h3>
								<p class="team-m__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<ul class="list-inline team-m__social">
									<li><a href="#" class="team-m__social-link"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-pinterest"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="item team-m text-center">
							<img class="img-responsive" src="{{ asset('assets/home/img-temp/team/team7.jpg') }}" alt="">
							<div class="team-m__info">
								<span class="team-m__job">Chef</span>
								<h3 class="team-m__name">Kate Dolli</h3>
								<p class="team-m__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								<ul class="list-inline team-m__social">
									<li><a href="#" class="team-m__social-link"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-pinterest"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#" class="team-m__social-link"><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Team -->



		<!-- Booking Form -->
		<section id="booking">
			<div class="container-fluid no-padding bg-color-main">
				<div class="booking content-lg">
					<div class="container">
						<div class="heading-v20 heading-v20--diff text-center g-mb-50">
							<span class="heading-v20__pre-title g-dp-block g-mb-20">Booking form</span>
							<h2 class="heading-v20__title g-mb-30">Reservation with open table</h2>
							<i class="heading-v20__icon fa fa-circle g-mb-30" aria-hidden="true"></i>
							<p class="heading-v20__text">Etiam ultrices lacus ut ligula vestibulum, sit amet mattis nunc elementum. Nam arcu enim, euismod nec purus non, aliquam congue ante. Nulla faucibus enim mauris, fringilla mollis ligula sollicitudin mollis.</p>
						</div>

						<form id="sky-form3" class="sky-form contact-style sky-form__booking">
							<fieldset>
								<div class="row">
									<div class="col-md-4 col-md-offset-0 g-sm-mb-30">
										<div>
											<input type="text" name="date" id="date" class="form-control booking__form-control" placeholder="Reservation date">
											<i class="fa booking__fa fa-calendar"></i>
										</div>
									</div>

									<div class="col-md-4 col-md-offset-0 g-sm-mb-30">
										<div>
											<input type="text" name="time" id="time" class="form-control booking__form-control" placeholder="Reservation time">
											<i class="fa booking__fa fa-clock-o"></i>
										</div>
									</div>

									<div class="col-md-4 col-md-offset-0 g-sm-mb-30">
										<div>
											<input type="text" name="number" id="number" class="form-control booking__form-control" placeholder="Persons">
											<i class="fa booking__fa fa-users"></i>
										</div>
									</div>
								</div>

								<div class="g-mb-40"></div>

								<div class="row">
									<div class="col-md-12 text-center">
										<button type="submit" class="btn-u global__btn-u">Book Now</button>
									</div>
								</div>
							</fieldset>

							<div class="message">
								<i class="rounded-x fa fa-check"></i>
								<p>Your message was successfully sent!</p>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
		<!-- End Booking Form -->

@stop
