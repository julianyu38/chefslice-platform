<?php

namespace App\Http\Controllers\Dashboard;

use App\Mail\AccountCreated;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin_auth', ['except' => ['update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $users = User::paginate(30);
       return $this->view(compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->view();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\User  $account
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $account,Request $request)
    {

        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|unique:users,email'
        ]);
        $request->merge(['password'=>substr(str_shuffle('23456789abcdefghjkmnpqrstuvwxyz'),0,6)]);
        // return $request->all();
        $user = User::create($request->all());
        if($request->hasFile('image')){
            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();
            $filename = md5($user->id).'.'. $extension;
            $path = public_path('images/users/');
            $image->move($path, $filename);
            $user->image = $filename;
            $user->save();
        }
        Mail::to($user->email)->send(new AccountCreated($request));
        $user->password = bcrypt($request->input('password'));
        $user->save();
        session()->flash("toastr", ["message" => "Admin user created successfully.", "title" => "Created!", "type" => "success"]);
        return back();


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $account
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $account, User $user)
    {
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(User $account,User $user)
    {
        return $this->view(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function update(User $account,Request $request, User $user)
    {
        $this->check_auth($user);
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id
        ]);
        $user->update($request->all());
        if($request->hasFile('image')){
            if(!empty($user->image) && file_exists(public_path('images/users/'.$user->image))){
                unlink(file_exists(public_path('images/users/'.$user->image)));
            }
            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();
            $filename = md5($user->id).'.'. $extension;
            $path = public_path('images/users/');
            $image->move($path, $filename);
            $user->image = $filename;
            $user->save();
        }

        session()->flash("toastr", ["message" => "Admin account updated successfully.", "title" => "Updated!", "type" => "success"]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $account,User $user)
    {
        $user->delete();
        if ( file_exists(public_path('images/users/' . $user->image)) && !empty($user->image)) {
            unlink(public_path('images/users/' . $user->image));
        }

        if(User::find($user->id)) {
            session()->flash("toastr", ["message" => "User can not be deleted.", "title" => "Oops!", "type" => "error"]);
            return back();
        }else{
            session()->flash("toastr", ["message" => "User deleted successfully.", "title" => "Deleted!", "type" => "success"]);
            return redirect()->action('Dashboard\UserController@index');
        }

    }





}
