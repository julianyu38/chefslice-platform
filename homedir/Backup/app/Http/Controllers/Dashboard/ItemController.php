<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Item;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function index(User $account)
    {
        $items = $this->getProducts($account);
        return view('dashboard.item.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $account)
    {
        $categories = $account->categories;//Category::all();
        return view('dashboard.item.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\User  $account
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $account,Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:255',
            'category_id'=>'required'
        ]);
        //$request->merge(['slug'=>str_slug($request->input('text'))]);
        $item = Item::create($request->all());
        if($item->id){
            if ($request->hasFile('thumbnail')) {
                $logo = $request->file('thumbnail');
                $extension = $logo->getClientOriginalExtension();
                $filename = md5('thumbnail_'.$item->id).'.'. $extension;
                $path = public_path('media/items/images/');
                $logo->move($path, $filename);
                $item->thumbnail = $filename;
                $item->save();
            }
            if ($request->hasFile('media')) {
                $logo = $request->file('media');
                $extension = $logo->getClientOriginalExtension();
                $filename = md5('media_'.$item->id).'.'. $extension;
                $path = public_path('media/items/images/');
                $logo->move($path, $filename);
                $item->media = $filename;
                $item->save();
            }
            session()->flash("toastr", ["message" => "Item created successfully.", "title" => "Created!", "type" => "success"]);
            return redirect()->action('Dashboard\ItemController@index');
        }
        session()->flash("toastr", ["message" => "Item can not be created.", "title" => "Oops!", "type" => "error"]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $id
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function show(User $account,$id)
    {
        $product = $this->getProduct($account,$id);
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $id
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(User $account,$id)
    {
        $categories =  $account->categories;
        $item = $this->getProduct($account,$id);
        return view('dashboard.item.edit',compact('item','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $id
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function update(User $account,Request $request, $id)
    {
        $product = $this->getProduct($account,$id);
        $this->validate($request,[
            'title' =>'required|max:255'
        ]);
        if($product->update($request->all())){
            if ($request->hasFile('thumbnail')) {
                if ( file_exists(public_path('media/items/images/' . $product->thumbnail)) && !empty($product->thumbnail)) {
                    unlink(public_path('media/items/images/' . $product->thumbnail));
                }
                $icon = $request->file('thumbnail');
                $extension = $icon->getClientOriginalExtension();
                $filename = md5('thumbnail_'.$product->id).'.' . $extension;
                $path = public_path('media/items/images/');
                $icon->move($path, $filename);
                $product->thumbnail = $filename;
                $product->save();
            }
            if ($request->hasFile('media')) {
                if ( file_exists(public_path('media/items/images/' . $product->media)) && !empty($product->media)) {
                    unlink(public_path('media/items/images/' . $product->media));
                }
                $media = $request->file('media');
                $extension = $media->getClientOriginalExtension();
                $filename = md5('media_'.$product->id).'.' . $extension;
                $path = public_path('media/items/images/');
                $media->move($path, $filename);
                $product->media = $filename;
                $product->save();
            }
            session()->flash("toastr", ["message" => "Item updated successfully.", "title" => "Updated!", "type" => "success"]);
        }else {
            session()->flash("toastr", ["message" => "Item can not be updated.", "title" => "Oops!", "type" => "error"]);
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $id
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $account,$id)
    {
        $product = $this->getProduct($account,$id);
        if($product->delete()) {
            if ( file_exists(public_path('media/items/images/' . $product->thumbnail)) && !empty($product->thumbnail)) {
                unlink(public_path('media/items/images/' . $product->thumbnail));
            }
            if($product->media_type !=2 ) {
                if (file_exists(public_path('media/items/images/' . $product->media)) && !empty($product->media)) {
                    unlink(public_path('media/items/images/' . $product->media));
                }
            }else{
                if (file_exists(public_path('media/items/videos/' . $product->media)) && !empty($product->media)) {
                    unlink(public_path('media/items/videos/' . $product->media));
                }
            }
            session()->flash("toastr", ["message" => "Product deleted successfully.", "title" => "Product Deleted!", "type" => "success"]);
            return redirect()->action('Dashboard\ItemController@index');
        }

        session()->flash("toastr", ["message" => "Product can not be deleted.", "title" => "Oops!", "type" => "error"]);
        return redirect()->action('Dashboard\ItemController@index');
    }



    private function getProduct(User $account,$id){
        if($account->user_name === 'admin'){
            return Item::findOrFail($id);
        }
        return $account->products()->where('items.id','=',$id)->firstOrFail();
    }

    private function getProducts(User $account,$limit=50){
        if($account->user_name == 'admin'){
            return Item::paginate($limit);
        }else{
            return $account->products()->paginate($limit);
        }
    }


}
