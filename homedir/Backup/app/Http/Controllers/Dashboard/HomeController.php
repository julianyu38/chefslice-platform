<?php

namespace App\Http\Controllers\Dashboard;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Analytics;
use Spatie\Analytics\Period;

class HomeController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }
    public function index(User $account){
        if($account->user_name ==='admin') {
            $analytics['browsers'] = Analytics::fetchTopBrowsers(Period::days(30), 5);
            $analytics['countries'] = Analytics::performQuery(Period::days(30), 'ga:sessions', ['dimensions' => 'ga:country', 'sort' => '-ga:sessions', 'max-results' => 10])->rows;
            //$analytics['analytics'] = Analytics::performQuery(Period::days(30),'ga:sessions,ga:bounceRate,ga:newUsers,ga:avgSessionDuration,ga:pageviews,ga:hits,ga:exitRate')->totalsForAllResults;
            //$analytics['countries'] =  $this->shuffle_array($countries);
            $startDate = Carbon::now()->subDay(30);
            $endDate = Carbon::now();
            $analytics['analytics'] = Analytics::performQuery(Period::create($startDate, $endDate), 'ga:newUsers,ga:visits,ga:users,ga:bounceRate,ga:pageviews,ga:sessions,ga:entranceBounceRate,ga:hits,ga:exitRate', ['dimensions' => 'ga:date'])->rows;
            return view('dashboard.index', $analytics);
        }
        return view('dashboard.user-index');
    }


    /**
     * Show the form for editing the user profile.
     *
     * @param  \App\User  $account
     * @return \Illuminate\Http\Response
     */
    public function profile(User $account)
    {
        $user = $account;
        return view('dashboard.user.edit',compact('user'));
    }

}
