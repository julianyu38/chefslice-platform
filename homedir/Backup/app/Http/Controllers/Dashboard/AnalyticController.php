<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Analytics;
use Spatie\Analytics\Period;

class AnalyticController extends Controller
{


    public function countries(){
        $days = 30;
        if(isset($_GET['days']) && $_GET['days']>0)
                $days = $_GET['days'];
        $analytics['countries'] = Analytics::performQuery(Period::days($days),'ga:sessions',['dimensions'=>'ga:country','sort'=>'-ga:sessions'])->rows;
        return view('dashboard.analytics.countries',$analytics);
    }
    public function browsers(){
        $days = 30;
        if(isset($_GET['days']) && $_GET['days']>0)
            $days = $_GET['days'];
        $analytics['browsers'] = Analytics::fetchTopBrowsers(Period::days($days));
        return view('dashboard.analytics.browsers',$analytics);
    }

    public function os(){
        $days = 30;
        if(isset($_GET['days']) && $_GET['days']>0)
            $days = $_GET['days'];
        $analytics['os'] = Analytics::performQuery(Period::days($days),'ga:sessions',['dimensions'=>'ga:operatingSystem,ga:operatingSystemVersion','sort'=>'-ga:sessions'])->rows;
        return view('dashboard.analytics.os',$analytics);

    }

    public function sources(){
        $days = 30;
        if(isset($_GET['days']) && $_GET['days']>0)
            $days = $_GET['days'];
        $analytics['sources'] = Analytics::performQuery(Period::days($days),'ga:sessions,ga:pageviews,ga:sessionDuration',['dimensions'=>'ga:source,ga:medium','sort'=>'-ga:sessions'])->rows;
        return view('dashboard.analytics.sources',$analytics);
    }

    public function mobile(){
        $days = 30;
        if(isset($_GET['days']) && $_GET['days']>0)
            $days = $_GET['days'];
        $analytics['mobile'] = Analytics::performQuery(Period::days($days),'ga:sessions,ga:pageviews,ga:sessionDuration',['dimensions'=>'ga:mobileDeviceInfo,ga:source','sort'=>'-ga:sessions','segment'=>'gaid::-14'])->rows;
        return view('dashboard.analytics.mobile',$analytics);

    }

    public function sites(){
        $days = 30;
        if(isset($_GET['days']) && $_GET['days']>0)
            $days = $_GET['days'];
        $analytics['sites'] = Analytics::performQuery(Period::days($days),'ga:sessions,ga:pageviews,ga:sessionDuration',['dimensions'=>'ga:source','sort'=>'-ga:sessions','filters'=>'ga:medium==referral'])->rows;
        return view('dashboard.analytics.sites',$analytics);

    }

    public function keywords(){
        $days = 30;
        if(isset($_GET['days']) && $_GET['days']>0)
            $days = $_GET['days'];
        $analytics['keywords'] = Analytics::performQuery(Period::days($days),'ga:sessions',['dimensions'=>'ga:keyword','sort'=>'-ga:sessions'])->rows;
        return view('dashboard.analytics.keywords',$analytics);
    }

    public function landing_pages(){
        $days = 30;
        if(isset($_GET['days']) && $_GET['days']>0)
            $days = $_GET['days'];
        $analytics['landing_pages'] = Analytics::performQuery(Period::days($days),'ga:entrances,ga:bounces',['dimensions'=>'ga:landingPagePath','sort'=>'-ga:entrances'])->rows;
        return view('dashboard.analytics.landing_pages',$analytics);
    }

    public function exit_pages(){
        $days = 30;
        if(isset($_GET['days']) && $_GET['days']>0)
            $days = $_GET['days'];
        $analytics['exit_pages'] = Analytics::performQuery(Period::days($days),'ga:exits,ga:pageviews',['dimensions'=>'ga:exitPagePath','sort'=>'-ga:exits'])->rows;
        return view('dashboard.analytics.exit_pages',$analytics);
    }
    public function bounces(){
        $days = 30;
        if(isset($_GET['days']) && $_GET['days']>0)
            $days = $_GET['days'];
        $analytics['analytics'] = Analytics::performQuery(Period::days(30),'ga:sessions,ga:bounces,ga:newUsers,ga:avgSessionDuration,ga:pageviews,ga:hits,ga:exitRate')->totalsForAllResults;
        return $analytics['analytics'];
        return view('dashboard.analytics.bounces',$analytics);
    }

}
