<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $dashboard_url = env('APP_URL_PROTOCOL','http').'://'.auth()->user()->user_name.'.'.env('APP_URL').'/dashboard';
            return redirect($dashboard_url);
           // return redirect('/home');
        }

        return $next($request);
    }
}
