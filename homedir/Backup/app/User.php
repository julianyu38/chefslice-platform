<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
class User extends Authenticatable
{
    use Notifiable,EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','image','mobile','user_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function categories(){
        return $this->hasMany(Category::class,'user_id','id');
    }

    public function products(){
        return $this->hasManyThrough(Item::class,Category::class,'user_id','category_id','id');
    }

    public function scopeCategory_products($query,$category_id){
        return $this->products()->whereCategory_id($category_id);
    }
    public function getRouteKeyName()
    {
        return 'user_name';
    }
}
