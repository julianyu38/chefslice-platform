<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [
        'text','slug', 'icon', 'description','user_id'
    ];
    protected  $dates =['created_at','updated_at'];
    public function items(){
     return $this->hasMany(Item::class,'category_id');
    }

}
