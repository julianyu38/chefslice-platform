<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    protected $fillable = [
        'category_id','title', 'short_description', 'description','image'
    ];
    protected  $dates =['created_at','updated_at'];
    public function category(){
        return $this->hasOne(Category::class,'id');
    }
}
